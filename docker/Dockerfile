FROM condaforge/mambaforge:latest

RUN apt-get update --fix-missing && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install --no-install-recommends -y \
    openssh-client \
    rsync \
    make \
    tzdata \
    locales && \
    apt-get --purge remove -y .\*-doc$ && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Right locale is necessary for pelican
RUN sed -i 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen fr_FR.UTF-8
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR
ENV LC_ALL fr_FR.UTF-8
RUN dpkg-reconfigure -f noninteractive locales && \
    echo "Europe/Paris" > /etc/timezone && \
    dpkg-reconfigure -f noninteractive tzdata


# Add the user that will run the app (no need to run as root)
RUN useradd -m -s /bin/bash calcul && \
    chown -R calcul:calcul /home/calcul && \
    chown -R calcul:calcul /opt/conda
ENV HOME /home/calcul
USER calcul

WORKDIR $HOME/website

# Install requirements
COPY environment.yml .
RUN mamba env create --file environment.yml && \
    rm -rf ~/.conda/pkgs/*
