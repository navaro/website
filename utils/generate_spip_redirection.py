import glob
import os
import re

def parse_spip_articles(root, extension):
    infos = dict()

    file_paths = glob.glob('{}/**/*.{}'.format(root, extension), recursive=True)

    for file_path in file_paths:
        content = open(file_path).read()
        spip_name_match = re.search(r"/spip_([^/]*)-([\d]+)\.{}$".format(extension), file_path)
        spip_name = "{}{}".format(spip_name_match.group(1), spip_name_match.group(2))
        creation_datetime = re.search(r"^:?date: ([\d:\- ]+) *$", content, re.M).group(1)
        title = re.search(r"^:?title: (.*?) *$", content, re.M).group(1)

        infos.setdefault(creation_datetime, []).append({
            'name': spip_name,
            'title': title
        })

    return infos
                
def parse_pelican_articles(root):
    infos = dict()

    file_paths = glob.glob('{}/**/*.rst'.format(root), recursive=True)

    for file_path in file_paths:
        content = open(file_path).read()
        pelican_name = re.search(r"/([^/]*)\.rst$", file_path).group(1)
        creation_datetime = re.search(r"^:?date: ([\d:\- ]+) *$", content, re.M).group(1)
        title = content.split('\n', 1)[0]

        infos.setdefault(creation_datetime, []).append({
            'name': pelican_name,
            'title': title
        })


    return infos

def find_articles_matches(spip_infos, pelican_infos):
    matches = []

    for creation_datetime, spip_articles in spip_infos.items():
        pelican_articles = pelican_infos.get(creation_datetime)
        
        # Multiple SPIP articles at same date
        if len(spip_articles) > 1:
            print("Multiple SPIP articles created the {}:".format(creation_datetime))
            for article in spip_articles:
                print("\t{}\t{}".format(article['name'], article['title']))

            if pelican_articles:
                print("Possible match in new website:")
                for article in pelican_articles:
                    print("\t{}\t{}".format(article['name'], article['title']))
            else:
                print("No match in new website!!!")
            print()
            continue

        spip_name = spip_articles[0]['name']
        spip_title = spip_articles[0]['title']

        # No corresponding Pelican article
        if not pelican_articles:
            print("No match for article {}\t{}\t{}".format(creation_datetime, spip_name, spip_title))
            print()
            continue

        # Multiple Pelican articles at same date
        if len(pelican_articles) > 1:
            print("Multiple Pelican articles that could match {}\t{}\t{}:".format(creation_datetime, spip_name, spip_title))
            for article in pelican_articles:
                print("\t{}\t{}".format(article['name'], article['title']))
            print()
            continue

        pelican_name = pelican_articles[0]['name']
        pelican_title = pelican_articles[0]['title']

        # Match found
        """
        print("Match found {} -> {} created the {}:".format(spip_name, pelican_name, creation_datetime))
        print("\t{}".format(spip_title))
        print("\t{}".format(pelican_title))
        print()
        """

        matches.append((spip_name, pelican_name))

    return matches


def generate_script(matches):
    from jinja2 import Environment, FileSystemLoader
    
    env = Environment(loader=FileSystemLoader("./"))
    template = env.get_template("spip.php.template.py")

    with open("spip.php.py", "w") as fh:
        fh.write(template.render(matches=matches))


if __name__ == "__main__":
    
    import argparse

    parser = argparse.ArgumentParser(description="Generate spip.php.py to redirect from old urls")
    parser.add_argument('spip', metavar='SPIP', help='Folder containing the original SPIP articles')
    parser.add_argument('pelican', metavar='PELICAN', help='Folder containing the new articles')
    parser.add_argument('--spip_ext', metavar='EXT', dest='extension', default='md', help='Extension of the spip articles')

    args = parser.parse_args()
    
    spip_infos = parse_spip_articles(args.spip, args.extension)
    pelican_infos = parse_pelican_articles(args.pelican)
    matches = find_articles_matches(spip_infos, pelican_infos)

    generate_script(matches)
