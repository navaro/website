Logo du Groupe Calcul
#####################

:date: 2019-05-10
:modified: 2019-05-10
:slug: logo

.. contents::

.. section:: Versions téléchargeables
    :class: description

    .. figure:: ../attachments/logo/Logo_small_fonc_txt.png
        :width: 100 px
        :align: center
        :target: ../attachments/logo/Logo_small_fonc_txt.png
        :alt: logo groupe Calcul PNG (petit, texte en gris foncé)

        Version PNG 400x32, fond transparent, texte en **gris foncé.**

    .. figure:: ../attachments/logo/Logo_fonc_txt.png
        :width: 250 px
        :align: center
        :target: ../attachments/logo/Logo_fonc_txt.png
        :alt: logo groupe Calcul PNG (grand, texte en gris foncé)

        Version PNG 1000×822, fond transparent, texte en **gris foncé.**

    .. figure:: ../attachments/logo/Logo_small_noir.png
        :width: 100 px
        :align: center
        :target: ../attachments/logo/Logo_small_noir.png
        :alt: logo groupe Calcul PNG (petit, texte en noir)

        Version PNG 400x32, fond transparent, texte en **noir.**

    .. figure:: ../attachments/logo/Logo_noir.png
        :width: 250 px
        :align: center
        :target: ../attachments/logo/Logo_noir.png
        :alt: logo groupe Calcul PNG (grand, texte en noir)

        Version PNG 1000×822, fond transparent, texte en **noir.**

    .. figure:: ../attachments/logo/Logo_small_blanc.png
        :width: 100 px
        :align: center
        :alt: logo groupe Calcul PNG (petit, texte en blanc)
        :target: ../attachments/logo/Logo_small_blanc.png
        :figclass: greybgfig

        Version PNG 400x329, fond transparent, texte en **blanc.**

    .. figure:: ../attachments/logo/Logo_blanc.png
        :width: 250 px
        :align: center
        :alt: logo groupe Calcul PNG (grand, texte en blanc)
        :target: ../attachments/logo/Logo_blanc.png
        :figclass: greybgfig

        Version PNG 1000×822, fond transparent, texte en **blanc.**


.. section:: Conditions de réutilisation
    :class: description

    **Crédit :** `Pierre Digonnet`_.

    Ces images sont mises à disposition selon les termes de la licence `CC-BY-NC-ND-2.0-FR`_.