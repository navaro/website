Présentation du |_| groupe calcul
#################################

:date: 2019-03-13
:modified: 2019-03-13
:slug: presentation_groupe
:summary: Depuis 2003, le Groupe Calcul œuvre pour la communauté du calcul en France.

.. contents::

.. section:: Présentation
    :class: description

    Le groupe Calcul est un groupe de communication et d’échanges de la communauté du calcul en France.
    Il a pour vocation d’être un réseau métier pour la communauté du calcul.

    Depuis 2009, ce groupe s’est structuré au sein du |CNRS| en un
    `Groupement de Recherche <{filename}presentation_groupe.rst#le-groupement-de-recherche>`_ (GdR) et un
    `réseau métier <{filename}presentation_groupe.rst#le-reseau>`_, et continue à oeuvrer pour la communauté par le
    biais d'actions telles que :

    - l'animation de listes de discussion
    - l'organisation de journées scientifiques et techniques
    - la participation à des manifestations scientifiques et techniques
    - le `soutien aux actions des mésocentres <mesocentres_en_france.html>`_

    Le Groupe Calcul est en interaction permanente avec ses
    `tutelles et partenaires institutionnels <{filename}presentation_groupe.rst#nos-partenaires>`_ .

    .. figure:: ../attachments/img/calcul.png
        :width: 100%
        :alt: Image situant le réseau et le GdR au croisement des disciplines et en interaction avec ses partenaires
        :align: center

        Le Groupe Calcul à la croisée des thématiques en interaction avec ses partenaires.


.. section:: Objectifs
    :class: description

    - Organiser et encourager la mise en place de rencontres scientifiques, d’actions de formation, aider aux
      développements des compétences, faciliter le transfert de connaissances à la fois scientifiques et techniques.
    - Développer, faciliter et encourager la communication entre les divers acteurs français du calcul.
    - Favoriser l’émergence d’une communauté dans le paysage scientifique du calcul en France. Faire le lien avec les
      communautés équivalentes en Europe et dans le reste du monde.

.. section:: Actions
    :class: description

    - Organisation de journées et formations techniques et scientifiques
    - Animation, diffusion d’informations via la liste de discussion pour impulser et encourager les échanges, fédérer
      les compétences de tous et favoriser la circulation des idées.
      Les discussions relèvent de trois catégories principales :

        - échanges techniques
        - annonces de séminaires et de formations
        - offres d’emploi

    - Gestion d'un site web publiant l'ensemble des journées et formations organisées par le groupe Calcul.
      Le site héberge les programmes des évènements et sert d'archive pour les supports de présentation et de formation.

.. section:: Structuration et pilotage
    :class: description

    Le groupe Calcul a été créé en 2003.
    En 2009, il s’est structuré afin d’améliorer sa visibilité et ses moyens d’action.
    Cette structuration reflète la diversité des thèmes scientifiques et techniques du calcul :

    - Création d’un `Groupement De Recherche (GdR 3275) <{filename}presentation_groupe.rst#le-groupement-de-recherche>`_
      du CNRS rattaché à l'INSMI_ pour les aspects scientifiques du calcul (mathématiques
      appliquées et recherche en informatique),
    - Création d’un `réseau métier <{filename}presentation_groupe.rst#le-reseau>`_ rattaché à la `Mission pour les Initiatives
      Transverses et Interdisciplinaires (MITI) <http://www.cnrs.fr/mi>`_ du CNRS pour les aspects technologiques
      (programmation, optimisation, architectures, etc.),
    - Partenariat avec `Grand Équipement National de Calcul Intensif (GENCI) <http://www.genci.fr/fr>`_ sur les
      questions liées aux mésocentres,
    - Pilotage de la structure par le `bureau du Groupe Calcul <{filename}presentation_groupe.rst#bureau>`_ regroupant
      les bureaux du GdR et du réseau.

.. section:: Le réseau
    :class: description

    Le réseau Calcul a pour objectif de favoriser les échanges au sein de la communauté du calcul, dans tous les aspects
    du métier, qu'ils soient scientifiques, techniques ou technologiques.
    Notre axe stratégique consiste à accompagner la communauté dans le renforcement de ses compétences quant à :

    - l'utilisation efficace des architectures de calcul par les applications scientifiques et les bibliothèques
      logicielles pour exploiter au mieux les ressources et répondre aux besoins de la recherche ;
    - la connaissance et l'usage expert des langages et bibliothèques logicielles pour développer ou sélectionner les
      outils adaptés à un problème donné ;
    - la culture approfondie des méthodes de calcul, de leur implémentation informatique et de leur validation pour
      susciter de nouveaux développements au profit de la simulation numérique et de l'analyse des données réalisés dans
      le cadre de travaux de recherche.

    Contribuer à l'articulation de ces compétences dans nos métiers est une des vocations du réseau Calcul.
    Cela nécessite un dialogue entre disciplines : les mathématiques appliquées, l'informatique et les sciences
    applicatives (physique, biologie, etc.) et une exploration critique qui passe par des ateliers pratiques.

    Les activités portées par le réseau favorisent la veille technologique, le retour d'expériences, la formation et
    l'échange de pratiques entre les différents acteurs du calcul. Par leur portée trans-disciplinaires, elles
    permettent aussi des regards croisés sur différentes applications du métier.

    .. button:: En savoir plus
        :target: ../attachments/reseau/evaluation_calcul_2018.pdf
        :blank: True

.. section:: Le groupement de recherche
    :class: description

    Le groupement de recherche GdR) Calcul s’intéresse aux nouvelles méthodes numériques et aux nouveaux algorithmes qui sont à la pointe de la
    recherche en calcul scientifique et en calcul intensif.
    Ces thématiques relèvent des mathématiques appliquées et de l'informatique mais sont également
    pluri-disciplinaires (physique, chimie, biologie, etc.).
    En règle générale, la validation des nouvelles méthodes passe par l’élaboration de logiciels de calcul qui sont
    développés au sein des laboratoires et sont distribués à la communauté du calcul en France et à l’international.

    Le GdR Calcul a donc également un rôle essentiel à jouer dans la promotion de ces développements et dans la
    transmission des savoirs qui leurs sont associés.
    En s'appuyant sur le `réseau métier <{filename}presentation_groupe.rst#le-reseau>`_, le GdR propose son expertise
    sur la parallélisation et l’optimisation des algorithmes.
    Il s’intéresse également à la visualisation des données et plus particulièrement aux traitements rapides de grandes
    masses de données et les thèmes de recherche associés.

    Ces dernières années, le GdR Calcul a souhaité étendre ses thématiques historiques en cherchant à couvrir tous les
    domaines des mathématiques appliquées où le calcul scientifique ou le calcul intensif jouent un rôle prépondérant.
    Il a identifié huit thématiques qui répondent à ce critère :

    - les équations aux dérivées partielles,
    - l'algèbre linéaire creux et dense,
    - le calcul et les équations stochastiques,
    - le traitement d’images,
    - l'optimisation et contrôle optimal,
    - le calcul exact,
    - l'arithmétique par intervalles,
    - l'apprentissage machine.

.. section:: Conseil scientifique du GdR
    :class: orga

    Le GdR Calcul est doté d'un conseil scientifique composé de membres externes :

    - Charles-Edouard Bréhier
    - Jean-Baptiste Caillau
    - Stéphane Cordier
    - Laura Grigori
    - Konrad Hinsen
    - Michel Kern
    - Bertrand Maury
    - Gabriel Peyré
    - Nathalie Revol
    - Clément Pernet
    - Stéphane Requena
    - Nicolas M. Thiéry

    et de membres du `bureau <{filename}./presentation_groupe.rst#bureau>`_ :

    - Matthieu Boileau
    - Anne Cadiou
    - Roland Denis
    - Thierry Dumont
    - Benoît Fabrèges
    - Loic Gouarin
    - Violaine Louvet
    - Marco Mancini
    - Fabrice Roy

    .. button:: En savoir plus
        :target: ../attachments/gdr/renouvellement_GdR_Calcul_2017.pdf
        :blank: True

.. section:: Nos partenaires
    :class: description

    - Le groupe Calcul est financé intégralement par des composantes du CNRS :

        - l'INSMI_ pour le GdR Calcul,
        - la MITI_ pour le réseau Calcul.

    - L'ensemble de l'infrastructure logicielle et matérielle des services numériques du groupe Calcul est fournie par
      le GdS Mathrice_ (site web, liste de diffusion, serveur Indico et serveur de questionnaires).
    - Dans le cadre de nos actions de formations et nos journées thématiques, le réseau Calcul collabore régulièrement
      avec d'autres réseaux de la MITI, en particulier DevLog_ et
      ResInfo_.
    - Que ce soit pour l'hébergement des sessions pratiques de formation ou la mise au point de leur contenu, le groupe
      Calcul travaille régulièrement avec les `Mésocentres <mesocentres_en_france.html>`_ et les
      `centres de calcul nationaux <paysage_du_calcul.html>`_, Gricad_ et la
      `Maison de la simulation <http://www.maisondelasimulation.fr>`_. Avec Genci_, il participe à la coordination des
      mésocentres et à la gestion du `label C3I <{filename}presentation_groupe.rst#c3i>`_.
    - Dans le cadre de sa veille technologique et de ses actions de mise en réseau telles que l'initiative FoCal_, le
      Groupe Calcul entretient des liens avec les différents acteurs du calcul : la MICADO_, le Cocin_, Genci_,
      l'AMIES_, la SMAI_, l'INRIA_, etc.
    - Dans le cadre de l'`ouverture thématique du GdR <{filename}presentation_groupe.rst#le-groupement-de-recherche>`_,
      le groupe échange régulièrement avec d'autres GdR tels que :

        - le `GdR MIA`_ en traitement d’images,
        - le `GdR IM`_ pour le calcul exact et l’arithmétique par intervalles,
        - le `GdR MOA`_ et le `groupe MODE`_ de la SMAI pour l’optimisation et le contrôle optimal,
        - le `GdR Madics`_ pour la science des données.

.. section:: C3I
    :class: description

    La CPU (Conférences des Président d’Universités) et GENCI se sont associés afin de mettre en place un label de compétences nommé **Certificat de Compétences en Calcul Intensif** (C3I)  visant à améliorer la visibilité des docteurs ayant développé et appliqué pendant leur thèse des compétences en calcul intensif.

    Le label C3I est par nature multidisciplinaire, couvre tous les domaines scientifiques, de la théorie à la recherche appliquée.

    Le candidat devra avoir développé et appliqué des compétences en calcul intensif comme notamment l’optimisation et la parallélisation de codes de calcul, l’algorithmique parallèle ou la gestion de gros volumes de données.

    Plus d'information sur le `site officiel du C3I <http://www.genci.fr/fr/content/c3i>`_


.. section:: Documents des tutelles
    :class: description

    Pour archive, ci-dessous informations, documents et rapports issus des tutelles du monde de la recherche académique (ministère, CNRS, ...) et des structures officielles (académie des sciences, ...), ainsi que les rapports auxquels est associé le Groupe Calcul.
    
    - Les `résultats de l'enquête interne aux unités des différents instituts du CNRS, visant à déterminer la place qu'occupe l'informatique dite "scientifique" dans les activités de recherche <http://www.cocin.cnrs.fr/documents/Livre-blanc-enquete-informatique-mars-2014.pdf>`__ de mars 2014.
    
    - Le `rapport de Gérard Roucairol sur la simulation haute performance au service de la compétitivité des entreprises d'avril 2013 <../attachments/spip/Documents/DocOfficiels/RapportRoucairol2013.pdf>`__
    
    - Le `rapport du CSCI (Conseil Comité Stratégique pour le Calcul Intensif) pour l'année 2013 <../attachments/spip/Documents/DocOfficiels/CSCI_Rapport2013.pdf>`__.
    
    - Le `rapport du CSCI (Conseil Comité Stratégique pour le Calcul Intensif) pour l'année 2011 <../attachments/spip/Documents/DocOfficiels/CSCI_Rapport2011.pdf>`__.
    
    - Le `rapport du CSCI (Conseil Comité Stratégique pour le Calcul Intensif) pour l'année 2009 <../attachments/spip/Documents/DocOfficiels/CSCI_Rapport2009.pdf>`__.
    
    - Suite à une demande de l'UPSACA (service des achats du CNRS) sur la problématique de l'externalisation du temps de calcul, et en collaboration avec `ResInfo <https://resinfo.org>`__, `"Le calcul dans les laboratoires de recherche : pratiques et moyens" <../attachments/spip/Documents/DocOfficiels/etude-moyen-calcul-mars-2009-V2.pdf>`__.
    
    - En novembre 2008, le CEA et le CNRS ont lancé la réflexion autour de la question de l’accompagnement de la première phase de la prospective nationale en matière de Calcul Intensif : le colloque "Penser Petaflops". A l'issue de ce colloque et des groupes de travail qui se sont constitués ont été rédigés différents rapports :
    
        - `Penser Pétaflops : synthèse des réflexions et premières conclusions <../attachments/spip/Documents/DocOfficiels/Penser.Petaflops-CR-V4Def-09.03.23.pdf>`__
        
        - `Rapport de l'atelier : Infrastructure du calcul intensif <../attachments/spip/Documents/DocOfficiels/Atelier1-Infrastructures-Rapport-08.11.13.pdf>`__
        
        - `Rapport de l'atelier : Mutualisation des codes et des grands outils logiciels <../attachments/spip/Documents/DocOfficiels/Atelier2-Mutualisation-Rapport.Fin-08.11.13.pdf>`__
        
        - `Rapport de l'atelier : Architecture, Algorithmique, Applications : l’intégration <../attachments/spip/Documents/DocOfficiels/Atelier3-Architecture-Rapport-08.11.13.pdf>`__
        
        - `Rapport de l'atelier : Les métiers du calcul numérique : formation, recherche et débouchés <../attachments/spip/Documents/DocOfficiels/Atelier4-Formation-Rapport-08.11.13.pdf>`__
    
    - Le `rapport du CSCI (Conseil Comité Stratégique pour le Calcul Intensif) pour l'année 2008 <../attachments/spip/Documents/DocOfficiels/CSCI_Rapport2008.pdf>`__.
    
    - `HCST, Haut Conseil de la science et de la technologie <http://www.hcst.fr/>`__, `rapport : Avis sur la situation de la France en matière de calcul scientifique intensif, décembre 2007 <../attachments/spip/Documents/DocOfficiels/hcst_csi_2007.pdf>`__
    
    - Groupe d'initiative "Calcul Scientifique" de l'Académie des Sciences, `rapport, décembre 2006 <../attachments/spip/Documents/DocOfficiels/groupe_ics_06_12_06.pdf>`__
    
    - `Rapport Héon-Sartorius, mars 2005 <../attachments/spip/Documents/DocOfficiels/rapport_heon-sartorius_2005.pdf>`__

.. section:: Bureau
    :class: orga

    - Matthieu Boileau
    - Anne Cadiou
    - Cécile Cavet
    - Roland Denis
    - Thierry Dumont
    - Bernard Dussoubs
    - Benoit Fabrèges
    - Loïc Gouarin
    - Matthieu Haefele
    - Violaine Louvet
    - Marco Mancini
    - Pierre Navaro
    - Fabrice Roy
    - Laurent Series

.. section:: Visuels
    :class: description

    .. figure:: ../attachments/logo/Logo_small_fonc_txt.png
        :width: 100 px
        :align: center
        :target: {filename}logo.rst
        :alt: logo Groupe Calcul

        `Différentes versions <{filename}logo.rst>`_ du logo sont téléchargeables.

.. |CNRS| image:: ../attachments/img/LOGO_CNRS_2019_CMJN_small.jpg
    :target: http://www.cnrs.fr
    :width: 35 px
    :alt: logo CNRS
