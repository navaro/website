École "Optimisation"
####################

:date: 2013-09-10 13:43:43
:category: formation
:tags: optimisation
:place: Saclay
:start_date: 2013-10-07
:end_date: 2013-10-11
:summary: Une école sur l'optimisation de code de calcul.

.. contents::

.. section:: Description
    :class: description

    Face au besoin croissant de puissance de calcul et à des applications numériques toujours plus complexes, s'exécutant sur des architectures parallèles en constante évolution (many-coeurs, GPU, machines hybrides, ...), il est nécessaire de pouvoir mieux contrôler l'efficacité et la performance de nos applications.

    Pour cela, le Groupe Calcul et la Maison de la Simulation organisent, dans le cadre d'equip@meso, une école sur l'optimisation qui se déroulera du 7 au 11 octobre 2013 à la `Maison de la Simulation <http://www.maisondelasimulation.fr/Phocea/Page/index.php?id=62>`_.

    L'enjeu principal de cette école est d'être capable :

    - d'identifier les points critiques d'une application dans son environnement d'exécution ;
    - à l'aide d'outils adaptés, améliorer son fonctionnement et son utilisation.

    En effet, la complexité des outils d'analyse suit celle des architectures et des applications, c'est pourquoi il est nécessaire de permettre à nos communautés d'acquérir rapidement les compétences nécessaires, en lien avec les équipes développant ces outils.

    Au programme, nous aurons

    - une introduction aux architectures actuelles et aux évolutions futures,
    - un cours sur les optimisations de base et la vectorisation,
    - un cours sur une bonne utilisation des caches,
    - un cours sur le benchmarking,
    - un cours sur les optimisations parallèles,
    - un cours sur l'optimisation des entrées-sorties,
    - un cours sur l'optimisation d'applications sur cartes graphiques.

    Il n'y a pas de prérequis particuliers pour suivre cette école si ce n'est la connaissance d'un langage bas niveau (C/C++ ou Fortran) et les connaissances de base des systèmes Unix.


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 07-10-2013

            .. event:: Introduction
                :begin: 10:30
                :end: 12:30
                :speaker: William Jalby (Université Versailles St Quentin)
                :support: attachments/spip/IMG/pdf/MdS_WJ_2013_V4.pdf

            .. break_event:: Repas
                :begin: 12:30
                :end: 14:00

            .. event:: Optimisation de base et vectorisation
                :begin: 14:00
                :end: 15:30
                :speaker: Romaric David
                :support: attachments/spip/IMG/pdf/2013_10_07_mds.pdf

            .. break_event:: Pause
                :begin: 15:30
                :end: 16:00

            .. event:: Optimisation de base et vectorisation
                :begin: 16:00
                :end: 17:30
                :speaker: Romaric David
                :support: attachments/spip/IMG/pdf/2013_10_07_mds.pdf

        .. day:: 08-10-2013

            .. event:: Caches (Partie 1)
                :begin: 09:00
                :end: 10:30
                :speaker: David Büttner (TUM)
                :support: attachments/spip/IMG/pdf/DavidBuettner-AnalysisandOptimizationoftheMemoryAccess.pdf

                - The Memory Hierarchy
                - Caches: Why & How do they work?
                - Bad Memory Access Patterns
                - How to not exploit Caches
                - Cache Optimization Strategies
                - How to exploit Caches even better

            .. break_event:: Pause
                :begin: 10:30
                :end: 11:00

            .. event:: Caches (Partie 2)
                :begin: 11:00
                :end: 12:30
                :speaker: David Büttner (TUM)
                :support: attachments/spip/IMG/pdf/DavidBuettner-AnalysisandOptimizationoftheMemoryAccess.pdf

                - Cache Analysis
                - Measuring on real Hardware vs. Simulation
                - Cache Analysis Tools
                - Case Studies
                - Hands-on

            .. break_event:: Repas
                :begin: 12:30
                :end: 14:00

            .. event:: Benchmark
                :begin: 14:00
                :end: 17:30
                :speaker: Laurent Gatineau (NEC)
                :support: attachments/spip/IMG/pdf/Maison_Simu_Benchmarking.pdf

                1. Introduction

                    - 1.1 Définitions
                    - 1.2 Objectifs des benchmarks
                    - 1.3 Les différentes phases d'un benchmark

                2. Benchmarks synthétiques et éléments d'architecture

                    - 2.1 Processeur
                    - 2.2 Mémoire et cache
                    - 2.3 Réseau rapide
                    - 2.4 Système disques
                    - 2.5 Accélérateur

                3. Benchmark applicatif

                    - 3.1 Portage et validation numérique
                    - 3.2 Classification de l'application
                    - 3.3 Profiling (CPU, MPI)
                    - 3.4 Projection des performances

                4. Travaux pratiques

                    - Contenu ouvert en fonction des utilisateurs:

                        - prise en main des benchmarks synthétiques
                        - classification / profiling d'un code utilisateur

        .. day:: 09-10-2013

            .. event: Message-Passing Interface - Selected Topics and Best Practices
                :begin: 09:00
                :end: 12:30
                :speaker: Florian Janetzko (Juelich Supercomputing Centre)
                :support: attachments/spip/IMG/zip/Slides_Janetzko.zip

                The Message-Passing Interface (MPI) is a widely-used standard library for programming parallel applications using the distributed-memory model. In the first part of this talk we will give a short overview over concepts and properties of modern HPC hardware architectures as well as basic programming concepts. A brief introduction into a design strategy for parallel algorithms is presented. The focus of the second part will be on the Message-Passing interface. After a short overview over general MPI properties and features we will present selected MPI topics including Derived Datatypes, MPI_Info objects and One-sided Communication and discuss best practices for their usage.

            .. break_event:: Repas
                :begin: 12:30
                :end: 14:00

            .. event:: Introduction to Performance Analysis
                :begin: 14:00
                :end: 17:30
                :speaker: Florian Janetzko (Juelich Supercomputing Centre)
                :support: attachments/spip/IMG/zip/Slides_Janetzko.zip

                The complexity of modern High-Performance-Computing systems impose great challenges on running parallel applications efficiently on them. Performance analysis is therefore a key issue to identify bottlenecks and efficiency problems in HPC applications in order to improve and optimize the performance of these applications. We start with an overview of different performance problems which frequently occur in parallel applications and give an introductions to the concepts of performance analysis. In the second part we will discuss selected performance analysis tools in more detail and show how to use them in order to identify bottlenecks.


        .. day:: 10-10-2013

            .. event:: Optimisation de noyaux CUDA avec les outils de "profiling" Nvidia.
                :begin: 09:00
                :end: 12:30
                :speaker: Julien Demouth (Nvidia)
                :support: attachments/spip/IMG/pdf/CUDA-Optimization-Julien-Demouth.pdf

                Lors de cette session nous présenterons une méthode pour optimiser des noyaux GPU écrits en CUDA et nous la mettrons en pratique sur un cas concret. Pour améliorer les performances de notre application, nous utiliserons les outils de « profiling » : Nsight et Nvprof.

            .. break_event:: Repas
                :begin: 12:30
                :end: 14:00

            .. event:: Introduction aux entrées-sorties parallèles
                :begin: 14:00
                :end: 17:30
                :speaker: Philippe Wautelet (IDRIS/CNRS)
                :support: attachments/spip/IMG/tgz/IOPara.tgz

                Les supercalculateurs sont de plus en plus massivement parallèles. Dans ce contexte, l'importance des entrées-sorties devient de plus en plus cruciale. Le but de cette introduction est de présenter les différentes approches existantes pour réaliser des entrées-sorties performantes, leurs avantages et inconvénients, ainsi que les principes de fonctionnement des systèmes de fichiers parallèles. Des travaux pratiques seront réalisés pour mettre en oeuvre les concepts introduits lors de cette présentation.

        .. day:: 11-10-2013

            .. event:: Expériences d'entrées-sorties sur architectures massivement parallèles
                :begin: 09:00
                :end: 12:30
                :speaker: Philippe Wautelet (IDRIS/CNRS)
                :support: attachments/spip/IMG/tgz/IOPara.tgz

                Les performances des systèmes d'entrées-sorties de plusieurs supercalculateurs ont été évaluées à l'aide d'une vraie application scientifique. Les résultats obtenus et les problèmes rencontrés seront exposés.


.. section:: Comité d'organisation
  :class: orga

  - Romaric David (Université de Strasbourg)
  - Loïc Gouarin (Laboratoire de Mathématiques d'Orsay)
  - Michel Kern (INRIA)
  - Anne-Sophie Mouronval (Ecole Centrale Paris)
  - Laurent Series (Ecole Centrale Paris)
