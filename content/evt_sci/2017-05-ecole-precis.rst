École thématique PRECIS
#######################

:date: 2017-01-26 14:35:23
:modified: 2017-01-26 14:35:23
:category: formation
:start_date: 2017-05-15
:end_date: 2017-05-19
:place: Fréjus
:summary: Une école sur la reproductibilité des calculs numériques.

.. contents::

.. section:: Description
    :class: description

    La page de l'école est https://precis.sciencesconf.org/
