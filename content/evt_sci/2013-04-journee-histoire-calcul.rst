10 ans du Groupe Calcul : "Histoire du Calcul"
##############################################

:date: 2012-12-17 08:33:42
:modified: 2012-12-17 08:33:42
:category: journee
:tags: histoire, calcul
:place: Paris
:start_date: 2013-04-09
:end_date: 2013-04-09
:summary: En 2013, le Groupe Calcul fête ses 10 ans d'existence. Une journée spéciale a été organisée pour l'occasion sur "L'histoire du Calcul".


.. contents::

.. section:: Description
    :class: description

    En 2013, le Groupe Calcul fête ses 10 ans d'existence. Une journée spéciale a été organisée pour l'occasion sur "L'histoire du Calcul".

    Elle a eu lieu le 9 avril 2013 à l'`IHP (Institut Henri Poincaré, Paris) <http://www.ihp.fr/>`__, amphi Hermite.
    L'histoire des méthodes numériques se mêle à des aspects plus institutionnels du développement du Calcul.

    Cette journée est parrainée par la `Société des Mathématiques Appliquées et Industrielles <http://smai.emath.fr>`__ et la `Société Informatique de France <http://www.societe-informatique-de-france.fr/>`__.

    .. container:: text-align-center

        .. image:: attachments/spip/Documents/Journees/avril2013/logo_smai.jpg
            :height: 100
            :alt: logo SMAI

        .. image:: attachments/spip/Documents/Journees/avril2013/logo_sif.png
            :height: 100
            :alt: logo SIF

    |

    Les vidéos de la journée sont disponibles en ligne, à l'url : http://www.youtube.com/playlist?list=PL9kd4mpdvWcC7qDicgr9yU0rb9Vl_wheK


.. section:: Programme
    :class: programme

    .. schedule::

       .. day:: 09-04-2013

            .. event:: Introduction
                :begin: 09:30
                :end: 09:50
                :speaker: Pierre-Louis Curien, CNRS & Université Paris 7

            .. event:: De l'analyse numérique aux sciences du numérique : comment l'informatique devint une discipline
                :begin: 09:50
                :end: 10:40
                :speaker: Pierre Mounier-Kuhn, CNRS & Université Paris-Sorbonne

            .. break_event:: Pause
                :begin: 10:40
                :end: 11:00

            .. event:: Gauss, Jacobi, Seidel, Richardson, Krylov : the invention of iterative methods
                :begin: 11:00
                :end: 11:50
                :speaker: Martin Gander, Université de Genève

            .. event:: L'école Lions et le calcul scientifique
                :begin: 11:50
                :end: 12:40
                :speaker: Oliver Pironneau, Laboratoire Jacques-Louis Lions, UPMC, Paris 6

            .. break_event:: Buffet
                :begin: 12:40
                :end: 14:00

            .. event:: Jean Kuntzmann et la naissance du numérique à l'université de Grenoble
                :begin: 14:00
                :end: 14:50
                :speaker: Laurent Desbat, TIMC-IMAG, Université Fourier, Grenoble

            .. break_event:: Pause
                :begin: 14:50
                :end: 15:20

            .. event:: L'évolution des méthodes de calcul en électromagnétisme : des différences finies aux méthodes 'mimétiques' modernes
                :begin: 15:20
                :end: 16:10
                :speaker: Alain Bossavit, Supélec
