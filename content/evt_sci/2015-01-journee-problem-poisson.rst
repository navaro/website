Journée "Problème de Poisson"
#############################

:date: 2014-12-16 14:41:41
:modified: 2014-12-16 14:41:41
:category: journee
:tags: poisson
:start_date: 2015-01-26
:end_date: 2015-01-26
:place: Paris
:summary: Il existe de nombreux problèmes où la résolution du problème de Poisson est un élément central et nécessite une résolution fine et rapide. Le Groupe Calcul propose une journée sur cette problématique à l'IHP en janvier 2015.


.. contents::

.. section:: Description
    :class: description

    Il existe de nombreux problèmes où la résolution du problème de Poisson est un élément central et nécessite une résolution fine mais rapide.

    Afin d'illustrer cette problématique, le groupe Calcul a organisé le 26 janvier 2015 à l'IHP (http://www.ihp.fr/) amphi Darboux une journée découpée de la façon suivante :

        - 3 ou 4 présentations courtes dans différents domaines où intervient la résolution du problème de Poisson en présentant pour chacune d'elles le problème étudié et les méthodes utilisées.
        - 4 ou 5 présentations sur des outils permettant de résoudre efficacement le problème de Poisson sous certaines contraintes.

    La fin de la journée fera place à l'échange entre les différents intervenants et le public.


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 26-01-2015

            .. break_event:: Accueil
                :begin: 09:30
                :end: 10:00

            .. event:: Pourquoi la résolution de l'equation de Poisson doit être précise et rapide pour les simulations plasmas froids ?
                :speaker: Francois Pechereau (Cerfacs) et Anne Bourdon (LPP, Ecole Polytechnique)
                :begin: 10:00
                :end: 10:30
                :support: attachments/spip/IMG/pdf/poisson_pechereau.pdf


                La résolution de l'équation de Poisson est particulièrement importante pour la
                simulation des plasmas froids à pression atmosphérique Ces plasmas sont étudiés
                pour des applications très variées comme la dépollution, la combustion assistée
                par plasma et les applications biomédicales.  La simulation de ces décharges
                électriques nécessite de calculer la génération et la propagation d'ondes
                d'ionisation, avec un front de décharge dans lequel règne un champ électrique
                élevé (100kV/cm) du à une charge d'espace et un canal plasma quasi-neutre où le
                champ électrique est beaucoup plus faible.  Dans les travaux que nous avons
                menés jusqu'à présent, nous réalisons des simulations 2D-axisymetriques avec un
                modèle fluide.  Dans ce modèle, les équations de continuité des espèces
                chargées sont couplées à l'equation de Poisson. Pour simuler la propagation des
                décharges, il faut donc résoudre l'équation de Poisson à chaque pas dans le
                temps.  Comme les termes sources des espèces et leurs paramètres de transport
                dépendent de façon très non-linéaire du champ électrique, la résolution de
                l'équation de Poisson doit également être très précise.  Dans les travaux que
                nous avons menés jusqu'à présent, le pourcentage de temps passé dans la
                résolution de l'équation de Poisson peut varier entre 30 et 60% du temps total.
                Dans cet exposé, nous discuterons des différentes méthodes que nous avons
                utilisées pour résoudre l'équation de Poisson avec leur avantages et leurs
                inconvénients.  Pour finir, nous présenterons les challenges actuels pour la
                simulation des décharges plasmas à pression atmosphérique et à basse pression.

            .. event:: Méthodes de projection pour la résolution des équations de Navier-Stokes pour les écoulements incompressibles
                :speaker: Bérengère Podvin (LIMSI)
                :begin: 10:30
                :end: 11:00
                :support: attachments/spip/IMG/pdf/poisson_podvin.pdf

                La dérivation des équations de Navier-Stokes pour les écoulements de fluide
                incompressible conduit à la fomulation d'un problème de Poisson pour la
                pression, pour laquelle des conditions aux limites adaptées doivent être
                définies. On s'intéressera ici plus particulièrement aux méthodes de projection
                et à leur résolution numérique dans un contexte parallèle (O(100) processeurs).
                Celle-ci sera illustrée dans le cas d'un code spectral ainsi que dans le cas
                du code SUNFLUIDH développé pour des volumes finis au LIMSI.

            .. event:: Scalable Poisson solver for gyrokinetic simulation
                :speaker: Virginie Grandgirard (CEA)
                :begin: 11:00
                :end: 11:30
                :support: attachments/spip/IMG/pdf/poisson_grandgirard.pdf

                Predicting the performance of fusion plasmas in terms of amplification factor,
                namely the ratio of the fusion power over the injected power, is among the key
                challenges in fusion plasma physics. In this perspective, turbulence and heat
                transport need being modeled within the most accurate theoretical framework,
                using first-principle non-linear simulation tools. The gyrokinetic equation for
                each species, coupled to Maxwell's equations is an appropriate self-consistent
                description of this problem. A new class of global full-f codes has recently
                emerged, solving the gyrokinetic equation for the entire distribution function
                on a large radial domain of the tokamak and using some prescribed external heat
                source. Such simulations are extremely challenging and require state-of-the-art
                high performance computing (HPC). The GYSELA code we develop at the IRFM
                institute is one of them. In the code the parallelization of Vlasov and Poisson
                equations are tightly coupled. Indeed, a large amount of distributed data are
                exchanged between these two parallel components. The Poisson solver is
                considered to get electric potential from ion density, assuming an adiabatic
                electron response within cylindrical surfaces. The numerical scheme of the
                Poisson solver relies on Fourier transform and a finite difference scheme. A
                brief overview of performances up to thousands cores will be given. Bottlenecks
                concerning parallel scalability and possible solutions will be discussed.

            .. event:: The gravitational field solving the Poisson equation for astrophysics purposes
                :speaker: Patrick Hennebelle (CEA)
                :begin: 11:30
                :end: 12:00

                In astrophysics, newtonian gravity is playing a fundamental role from the
                largest scales of our universe such as clusters of galaxies, up to solar
                systems. In many circumstances, self-gravitating fluids must then be considered
                and the Poisson equation must be solved at every time steps. Moreover, many
                systems present very different spatial scales and it is then necessary to use
                appropriate schemes with adaptive resolution. I will describe some of the
                methods used in astrophysics focussing more specifically on grid adaptive mesh
                refinement  and the so called smooth particle hydrodynamics, which is a
                Lagrangian techniques. For each of them specific approaches have been
                developed to solve the Poisson equation.

            .. break_event:: Repas
                :begin: 12:00
                :end: 14:00

            .. event:: On the design of parallel linear solvers for large scale problems
                :speaker: Mathieu Faverge (LABRI)
                :begin: 14:00
                :end: 14:45
                :support: attachments/spip/IMG/pdf/poisson_faverge.pdf

                In this talk we will discuss our research activities on the design of parallel
                linear solvers for large scale problems that range from dense linear algebra,
                to parallel sparse direct solver and hybrid iterative-direct approaches. In
                particular we will describe the implementations designed on top of runtime
                systems that should provide both code and performance portabilities.

            .. event:: Improving multifrontal solvers by means of Block Low-Rank approximations for PDEs
                :speaker: Alfredo Buttari (CNRS et INPT-IRIT)
                :begin: 14:45
                :end: 15:30
                :support: attachments/spip/IMG/pdf/poisson_buttari.pdf

                Matrices coming from elliptic Partial Differential Equations (PDEs) and in
                particular Poisson equations have been shown to have a low-rank property: well
                defined off-diagonal blocks of their Schur complements can be approximated by
                low-rank products. Given a suitable ordering of the matrix which gives to the
                blocks a geometrical meaning, such approximations can be computed using an SVD
                or a rank-revealing QR factorization. The resulting representation offers a
                substantial reduction of the memory requirement and gives efficient ways to
                perform many of the basic dense linear algebra operations. Several strategies,
                mostly based on hierarchical formats, have been proposed to exploit this
                property. We study a simple, non-hierarchical, low-rank format called Block
                Low-Rank (BLR), and explain how it can be used to reduce the memory footprint
                and the complexity of sparse direct solvers based on the multifrontal method.
                We present experimental results that show that even if BLR based factorizations
                are asymptotically less efficient than hierarchical approaches, they still
                deliver considerable gains. The BLR format is compatible with numerical
                pivoting, and its simplicity and flexibility make it easy to use in the context
                of a general purpose, algebraic solver. In this talk, we show how Block
                low-rank feature was incorportated in the general purpose solver MUMPS and
                provide experiments showing the gains obtained with respect to standard (full
                rank) MUMPS factorizations.

            .. break_event:: Pause
                :begin: 15:30
                :end: 16:00

            .. event:: Communication avoiding and hiding in preconditioned Krylov solvers for Poisson problems
                :speaker: Wim Vanroose (Université d'Anvers)
                :begin: 16:00
                :end: 16:45
                :support: attachments/spip/IMG/pdf/poisson_vanroose.pdf

                Traditionally Krylov solvers have been designed to minimize the number
                of flops to arrive at a solution. However, currently communication and
                synchronization overhead is now the main concern due to the dramatic
                increases in concurrency on all hardware. This introduces severe
                communication bottlenecks in Krylov solvers most importantly the
                latency of global reductions and the limited bandwidth to the memory.
                In this talk we discuss how Krylov algorithms can be redesigned to
                remove some of these communication bottlenecks and leading to a class
                of so called pipelined Krylov solvers. These methods have a better
                scaling behaviour on modern hardware.



            .. event:: Robust solution of Poisson-like problems with aggregation-based AMG
                :speaker: Yvan Notay (Université Libre de Bruxelles)
                :begin: 16:45
                :end: 17:30
                :support: attachments/spip/IMG/pdf/poisson_notay.pdf

                The fast iterative solution of discrete Poisson-like problems requires methods
                of multigrid or multilevel type, because classical iterative scheme cannot
                efficiently damp smooth error components.

                While there exists a plenty of such methods, their use often requires
                complicated coding, or, when a software code is available, a careful variant
                selection or parameter tuning. In this talk, we shall present a multilevel
                method based on the aggregation of the unknowns, which has the attractive
                feature that the related code offers stable performances while being used in a
                purely black box fashion. It also scales well in parallel and compare favorably
                with state-of-the-art competitors.
