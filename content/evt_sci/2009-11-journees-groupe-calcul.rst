Journées du Groupe Calcul
#########################

:date: 2009-06-26 14:50:44
:modified: 2009-06-26 14:50:44
:category: journee
:tags: gdr, réseau, calcul
:place: Paris
:start_date: 2009-11-09
:end_date: 2009-11-10
:summary: Le Groupe Calcul organise ses premières journées !

.. contents::

.. section:: Description
    :class: description

    Les premières journées du Groupe Calcul ont eu lieu les 9 et 10 novembre à l'`IHP <http://www.ihp.fr/>`_ à Paris.

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 09-11-2009

            .. event:: AMR, une plate-forme pour le calcul multi-niveaux; Algorithmes et implémentations explicites/implicites.
                :speaker: Juliette Ryan, ONERA.
                :begin: 10:30
                :end: 11:10
                :support: attachments/spip/Documents/Journees/nov2009/JRyan.pdf

                Dans cette présentation, par AMR on entend une technique dynamique de raffinements locaux par superposition de patches emboîtés avec des raffinements croissants. A priori tous les patches et la grille initiale sont des grilles cartésiennes. Ces patches évoluent avec le temps afin de suivre au mieux les phénomènes nécessitant une plus grande finesse de discrétisaton. Les principaux problèmes à résoudre sont comment générer ces patches, comment les raccorder, comment les faire évoluer, comment paralléliser, comment transformer un solveur monogrille pour s'adapter à cette structure de grilles. Les principaux algorithmes utilisés à l'Onera pour ces questions seront décrits pour des équations aux dérivées partielles instationnaires explicites ou implicites. Une extension à des grilles cartésiennes et non-structurés par décomposition de domaines sera aussi présenté ainsi que des applications à des écoulements réactifs.

            .. event:: Multirésolution adaptative pour la simulation d'écoulements visqueux compressibles
                :speaker: Christian Tenaud, LIMSI
                :begin: 11:10
                :end: 11:50
                :support: attachments/spip/Documents/Journees/nov2009/CTenaud.pdf

                Dans le régime des hautes vitesses, les écoulements aérodynamiques présentent, pour la plupart, des interactions entre ondes de choc et couches cisaillées qui restent des phénomènes fortement instationnaires difficiles à appréhender numériquement du fait de la multiplicité des échelles spatiales à représenter. En particulier, certains phénomènes (interaction onde de choc vorticité, production de vorticité par effet barocline, ...) ne peuvent être pris en compte par des modélisations (de type LES, par exemple) et l'utilisation de schémas de haute résolution couplée à un maillage localement très fin est indispensable à une prédiction de grande qualité de tels écoulements. Outre des schémas à capture de choc de haute précision, il est indispensable de s'orienter vers l'utilisation de maillage adaptatif (AMR: Adaptive Mesh Refinement) afin de pouvoir prendre en compte, par souci d'économie, les différents niveaux d'échelles et ce de façon dynamique. Depuis les travaux précurseurs de A. Harten [Harten (1994), (1995)] sur la multirésolution appliquée aux lois de conservation hyperboliques, de nombreuses études ont été menées sur les algorithmes de multirésolution « complètement » adaptatifs. Une des techniques qui semble prometteuse, est celle basée sur l'estimation d'erreur à partir de bases d'ondelettes [Cohen et al. (2003)].

                C'est dans ce cadre que sont poursuivis nos travaux sur la simulation des écoulements avec présence d'ondes de choc en interaction avec des couches cisaillées.

                Cette étude présente donc la capacité d'un schéma à capture de choc de haute résolution [Daru and Tenaud (2004)] couplé à un algorithme de multirésolution « complètement » adaptatif [Cohen et al. (2003)], à reproduire les interactions fondamentales en mécanique des fluides. Une étude de convergence en fonction du paramètre (ε) de raffinement adaptatif montre que la méthode est linéaire en ε et, ainsi, conforme à la théorie. Les résultats sont analysés en terme d'efficacité vis-à-vis de la précision de la solution mais également des gains en mémoire et temps CPU. Les résultats obtenus se comparent très favorablement à ceux répertoriés dans la littérature, pour tous les écoulements visqueux instationnaires étudiés.

                Références :

                    - Harten, A. (1994): Adaptive Multiresolution Schemes for Shock Computations. J. Comput. Phys. 115: 319—338.
                    - Harten, A. (1995): Multiresolution Alogrithms for the Numerical Solution of Hyperbolic Conservation Laws. Comm. Pure Appl. Math. 48: 1305—1342.
                    - Cohen, A., Kaber, S. M., Müller, S. and Postel, M. (2003): Adaptive Multiresolution for Finite Volume Solutions of Gas Dynamics. Computers & Fluids 32: 31—38.
                    - Daru, V., Tenaud, C. (2004): High Order One-step Monotonicity-Preserving Schemes for Unsteady Compressible flow Calculations. J. Comput. Phys., 193 : 563—594.
                    - Daru, V., Tenaud, C. (2008): Numerical simulation of the viscous shock tube problem by using a high resolution monotonicity-preserving scheme. Computers & Fluids, doi 10.1016/J.compfluid 2008.06.008.
                    - L\. Bentaleb, O. Roussel and C. Tenaud (2006): Adaptive multiresolution methods for the simulation of shocks/shear layer interaction in confined flows. Numerical mathematics and advanced applications, Springer, Berlin, 761—769.


            .. event:: Adaptive space-time multiresolution techniques for nonlinear PDEs
                :speaker: Kai Schneider, Laboratoire de Modélisation et Simulation Numérique en Mécanique et Centre de Mathématiques et d'Informatique, Marseille
                :begin: 11:50
                :end: 12:30
                :support: attachments/spip/Documents/Journees/nov2009/KSchneider.pdf

                We present efficient fully adaptive numerical schemes for evolutionary partial differential equations based on a finite volume (FV) discretization with explicit time discretization. A multiresolution strategy allows local grid refinement while controlling the approximation error in space. The costly fluxes are evaluated on the adaptive grid only. For time discretization we use an explicit Runge-Kutta scheme of second-order with a scale-dependent time step. On the finest scale the size of the time step is imposed by the stability condition of the explicit scheme. On larger scales, the time step can be increased without violating the stability requirement of the explicit scheme. Embedded Runge-Kutta methods of second and third order are then used to choose automatically the new time step while controlling the approximation error in time. Non-admissible choices of the time step are avoided by limiting its variation.

                The implementation of the multiresolution representation uses a dynamic tree data structure, which allows memory compression and CPU time reduction. This new numerical scheme is validated using different classical test problems in one, two and three space dimensions. The gain in memory and CPU time with respect to the finite volume scheme on a regular grid is reported, which demonstrates the efficiency of the new method.

                This work is joint work mit M. Domingues, S. Gomes and O. Roussel.

            .. event:: Présentation de l'école thématique prévue en 2010
                :begin: 12:30
                :end: 12:30
            
            .. break_event:: Repas
                :begin: 12:30
                :end: 14:30

            .. event:: Un solveur volumes finis interactif pour les fluides non-visqueux sur GPU
                :speaker: Vivien Clauzon, Université de Clermont-Ferrand
                :begin: 14:30
                :end: 15:10
                :support: attachments/spip/Documents/Journees/nov2009/VClauzon.pdf

                Comme premier pas vers le développement d'un solveur fluide compressible sur GPU, nous avons implémenté un solveur volumes finis pour les équations d'Euler sur maillage structuré. Une méthode MUSCL est mise en oeuvre pour monter en ordre et ainsi mieux capter les chocs. Le solveur de Riemann HLLC est utilisé. Une application interactive a été développée grâce à l'interopérabilité OpenGL/CUDA. Exécuté sur une carte Nvidia de dernière génération le code montre un speedup d'environ 60 par rapport au code mono-threadé sur core i7.

                L'avantage de ce programme est qu'il est entièrement codé sur GPU (à l'exception des I/Os qui sont cependant non-bloquantes car multi-theadées).

                Il devient facile de traiter des problèmes 2D sur une grille 2048x2048. Le code a été étendu à la 3D mais pas l'interaction avec l'utilisateur et la visualisation en temps-réel.

                Un exemple 2D sur petit maillage (256^2) est disponible sur :

                    - http://www.youtube.com/watch?v=ifmSQOr8qNg 
                    - http://www.youtube.com/watch?v=MjiSHqcL0Do 

                Ces simulations ont été effectuées sur un laptop muni d'une 8800GTX.


            .. event:: Analyse de sensibilité des précipitations aux paramètres physiques du modèle atmosphérique régional MAR en Afrique de l'Ouest
                :speaker: Laurence Viry, Université de Grenoble
                :begin: 15:10
                :end: 15:50
                :support: attachments/spip/Documents/Journees/nov2009/LViry.pdf

                L'Afrique de l'Ouest tire la plus grande partie de ses ressources en eau des précipitations associées à sa mousson. Il a été montré par des études précédentes que les conditions surfaciques océaniques et continentales sont des facteurs prédominants modulant les précipitations associées à ces moussons.

                L'outil le plus adapté pour étudier la sensibilité des précipitations à ces phénomènes de surfaces est le modèle numérique atmosphèrique mais en dépit d'hypothèses très simplificatrices sur les paramètres du modèle, l'approche déterministe utilisée jusqu'à présent pour le calcul des indices de sensibilité, engendre un nombre trop important de runs très couteux en temps CPU et en espace de stockage.

                L'idée est d'approcher le code par un méta-modèle (surface de réponse, émulateur) qui va simuler le comportement du code dans l'espace de variabilité des paramètres d'entrée influents, dans le cadre d'un déploiement sur grille tenant compte du nombre important de runs et de la gestion des données entre ces runs (utilisation du middleware DIET).

                Pour la mise en place du méta-modèle, nous considérons le cas où tant les entrées que les sorties du code numérique du modèle physique sont des fonctions de l'espace et du temps. Pour approximer le code de calcul et réaliser son analyse de sensibilité, il s'agira alors d'une part d'approcher les entrées et les sorties par des décompositions sur des bases (fonctionnelles) spatiotemporelles et de réaliser ensuite une régression non paramétrique à l'aide d'un méta-modèle approprié pour l'opérateur de liaison entre les entrées et les sorties. L'ajustement réalisé permettra alors le calcul des indices de sensibilité pour aboutir à une carte spatio-temporelle de ces indices.


            .. break_event:: Pause
                :begin: 15:50
                :end: 16:15

            .. event:: Some numerical strategies to be improved in the context of the controlled fusion
                :speaker: Boniface Nkonga, Université de Nice
                :begin: 16:15
                :end: 16:45
                :support: attachments/spip/Documents/Journees/nov2009/BNkonga.pdf

            .. event:: GdR Calcul
                :begin: 16:45
                :end: 17:45
                
                Présentation du bilan et des actions 2010.


        .. day:: 10-11-2009

            .. event:: Méthodes itératives pour le transport multi-espèces
                :speaker: Vincent Giovangigli, CMAP, Polytechnique
                :begin: 09:30
                :end: 10:15
                :support: attachments/spip/Documents/Journees/nov2009/VGiovangigli.pdf


            .. event:: System Identification in Tumor Growth Modeling
                :speaker: Thierry Colin, IMB, Université de Bordeaux 1
                :begin: 10:15
                :end: 11:00
                :support: attachments/spip/Documents/Journees/nov2009/TColin.pdf

                Mathematical modelling of tumor growth can be a useful tool to improve the understanding of cancer treatment in terms, for example, of prognosis, drug effect modelling, see [2], and clinical protocols definition, see [1].

                We consider continuous-type models based on mixture theory. They rely on a system of non-linear coupled parametric partial differential equations, in which a set of parameters accounts for the complexity of the different tissues attacked by the tumour as well as for the differences between the same tissues of different individuals. The tumour growth is investigated at a macroscopic scale and therefore all the microscopic and mesoscopic scale phenomena that we do not model directly are lumped in such parameters.
                In order to apply such models in practical situations these parameters need to be identified, i.e., a realistic value has to be estimated. One way to determine their values is by means of inverse problems, exploiting data coming from medical imagery The main difficulty is that the amount of data for system identification is scarce. Although medical scans allow a quite accurate localization of the tumor in space, little information can be inferred about its cellular nature or nutrient distribution. In addition, only two scans are usually available before treatment making estimation of time evolution a challenging problem.

                The aim of this presentation is to explain an efficient identification procedure that allows the parameter estimation of a two-dimensional tumor growth model, with the final objective of obtaining a prognostic model. In particular the results of several classes of inverse problems are discussed: we progressively increase the model complexity while decreasing the amount of available data, in order to approach realistic applications.

                References :

                    - [1] S. Schnell B. Ribba, T. Colin. A multiscale mathematical model of cancer, and its use in analizing irradiation therapies. Theoretical Biology and Medical Modelling, 3:1–19, 2006.
                    - [2] T. Colin D. Bresch E. Grenier J.P. Boissel B. Ribba, O. Saut. A multiscale mathematical model of avascular tumor growth to investigate the therapeutic benet of anti-invasive agents. Journal of Theoretical Biology, 2006.
                    - T\. Colin, A. Iollo, D. Lombardi, O. Saut, Institut de Mathématiques de Bordeaux, Université Bordeaux 1 and équipe-projet MC2, INRIA Bordeaux - Sud Ouest, e-mail: Thierry.Colin, Angelo.Iollo, Damiano.Lombardi, Olivier.Saut@math.u-bordeaux1.fr
                    - J\. Palussière, Service de Radiologie, Institut Bergonié, Bordeaux, e-mail: Jean.Palussiere@bergonie.fr


            .. break_event:: Pause
                :begin: 11:00
                :end: 11:30

            .. event:: Interfaces entre le GdR Math/Info et le GdR Calcul
                :begin: 11:30
                :end: 11:50
                :speaker: Brigitte Vallée, Informatique, Université de Caen
                :support: attachments/spip/Documents/Journees/nov2009/BVallee.pdf


            .. event:: Calcul intensif, algèbre linéaire exacte et applications
                :speaker: Clément Pernet, Laboratoire LIG, UJF Grenoble
                :begin: 11:50
                :end: 12:30
                :support: attachments/spip/Documents/Journees/nov2009/CPernet.pdf

                Nous présenterons un survol de différents aspects du calcul formel
                faisant appel au calcul intensif. En présentant différents domaines
                d'application, du calcul mathématique, à la cryptologie algébrique, nous
                aborderons certains aspects algorithmiques et logiciels mis en oeuvre
                au niveau de l'algèbre linéaire exacte, pilier fondamental de
                l'efficacité de la plupart de ces calculs. Nous mettrons en particulier
                en évidence les interactions, similitudes et différences avec les
                techniques issues du calcul numérique.


            .. break_event:: Repas
                :begin: 12:30
                :end: 14:30


            .. event:: Simulations numériques en microfluidique
                :speaker: Paul Vigneaux, UMPA, ENS Lyon
                :begin: 14:30
                :end: 15:00
                :support: attachments/spip/Documents/Journees/nov2009/PVigneaux.pdf

            .. event:: Simulation numérique pour le ferromagnétisme
                :speaker: Stéphane Labbé, LJK, Université de Grenoble
                :begin: 15:00
                :end: 15:30
                :support: attachments/spip/Documents/Journees/nov2009/SLabbe.pdf

                Les matériaux ferromagnétiques, aimants permanents, sont utilisés dans de nombreux domaines allant de la nano-électronique à l'enregistrement magnétique. Afin d'optimiser les performance des objets ferromagnétiques, il est de plus en plus fait appel à la simulation numérique. Le modèle régissant l'évolution de l'aimantation des ces matériaux est non seulement non-linéaire mais fait intervenir des opérateurs non locaux, ainsi, la simulation en est délicate. Venant s'ajouter aux difficultés dues au modèle, les difficultés liées à la comparaison des résultats de simulation et de l'expérience alourdissent le problème. Dans cet exposé, nous présenterons les principales techniques de simulation utilisées ainsi que les enjeux en calcul scientifique que soulève ce problème.


            .. event:: Transfert radiatif et application a la radiothérapie
                :speaker: Christophe Berthon, Laboratoire Jean Leray, Université de Nantes
                :begin: 15:30
                :end: 16:00
                :support: attachments/spip/Documents/Journees/nov2009/CBerthon.pdf

                On s'intéresse ici à l'approximation numérique du transfert radiatif pour différentes applications physiques. En particulier, on montrera les enjeux numériques prépondérants pour la réalisation de simulations pertinentes. Des extensions à la radiothérapie dans le cas du traitement du cancer de la prostate seront présentées ainsi que les challenges qui y sont associés. Plus spécifiquement, afin d'être exploitable par le corps médical, les simulations en radiothérapie devront être effectuées sur un ordinateur portable non dédié au calcul scientifique.
