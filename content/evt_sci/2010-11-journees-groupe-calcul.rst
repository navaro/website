Journées du Groupe Calcul
#########################

:date: 2010-07-23 06:49:48
:modified: 2010-07-23 06:49:48
:category: journee
:tags: gdr, réseau, calcul
:start_date: 2010-11-09
:end_date: 2010-11-11
:place: Lyon
:summary: Le Groupe Calcul organise ses deuxièmes journées !

.. contents::

.. section:: Introduction
    :class: description

    Le Groupe Calcul (réseau et GdR) a organisé ses journées 2010 les 9 et 10 novembre à Lyon, sur le thème **"Nouvelles tendances en calcul scientifique"**.

    `Affiche des journées <attachments/spip/Documents/Journees/nov2010/affiche2010.pdf>`__

.. section:: Exposés
    :class: description

    - **Christophe Prudhomme**, Université de Grenoble, *"Génération Automatique de Codes Efficaces pour des Architectures Hybrides pour des Méthodes de type Galerkin généralisé"*.

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/C-Prudhomme.pdf>`__

    We review and update the standard Galerkin context within Feel++(formerly known as Life): first we present the mathematical/computational framework as well as the variational formulation language. Then we propose a strategy to for our computational framework to exploit  hybrid architectures (CPU/GPU) and some first results.

    - **Nicolas Brisebarre**, CNRS, LIP/Arénaire, ENS Lyon, *"Synthèse de polynômes efficaces en machine"*

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/N-Brisebarre.pdf>`__

    Quand on implante des fonctions en machine, on utilise presque toujours des approximations polynomiales. Dans la plupart des cas, le polynôme qui approche le mieux (pour une norme et un intervalle donnés) une fonction a des coefficients qui ne sont pas exactement représentables sur un nombre fini de bits, ce qui est pourtant une des contraintes à respecter impérativement par ces bons approximants. Nous présenterons une méthode heuristique utilisant l'algorithmique des réseaux euclidiens, et notamment l'algorithme LLL, qui permet de produire une très bonne (voire la meilleure) approximation polynomiale sous ce type de contraintes quand la norme considérée est la norme sup. Des techniques similaires fonctionnent pour la norme L2. [Travaux de B., Chevillard, Hanrot, Muller, Tisserand et Torres].

    - **Victorita Dolean**, Université de Nice, *"Grilles grossières pour les méthodes de décomposition de domaine avec des fortes hétérogénéités"*

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/V-Dolean.pdf>`__

    Les grilles grossières sont un ingrédient important pour obtenir des méthodes de décomposition de domaine qui passent à l'échelle. On propose la construction d'un espace grossier en utilisant les modes basses fréquence des opérateurs DtN (Dirichlet-Neumann) et on applique le préconditionneur à deux niveaux ainsi obtenu au système linéaire issu d'une décomposition de domaine avec recouvrement. Notre méthode est adaptée à une implémentation parallèle et son efficacité est montrée à l'aide des exemples numériques sur des problèmes avec des grandes hétérogénéités.

    - **Jean Claude Yakoubsohn**, Université de Toulouse, *"LEDA : Logistique des Equations Différentielles Algébriques"*

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/JC-Yakoubsohn.pdf>`__

    Dans cet exposé j'expliquerai tout d'abord pourquoi les équations différentielles algébriques interviennent naturellement dans les applications (mécanique,chimie,biologie). Puis je présenterai le programme LEDA concernant leur résolution formelle et numérique.

    - **D. Komatitsch**, Université de Pau, *"Modélisation de la propagation des ondes sismiques en multi-GPUs"* 

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/D-Komatitsch.pdf>`__

    Dans cet exposé je discuterai de l'utilisation de gros clusters de cartes graphiques GPU combinées à du passage de messages non bloquants par MPI pour modéliser la propagation des ondes sismiques dans la Terre à la suite d'un tremblement de terre. Je montrerai que des facteurs d'accélération d'environ 20x ou 25x peuvent être obtenus par rapport au cas séquentiel sur un coeur de CPU.

    - **Francis Bach**, INRIA - Ecole Normale Supérieure, *"Calcul scientifique et apprentissage statistique"*

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/F-Bach.pdf>`__

    Les algorithmes d'apprentissage statistique utilisent de nombreuses techniques de calcul scientifique (systèmes linéaires, factorisation de matrices, matrice structurées, valeurs propres, etc.). Ces techniques sont souvent utilisées comme des boîtes noires. De plus en plus, la communauté apprentissage cherche à comprendre et étendre ces outils. Dans cet exposé, je présenterai les problèmes principaux liés a l'apprentissage statistique supervisé et non supervisé.

    - **Bruno Dubroca**, Université de Bordeaux, *"Modélisation et approximation numérique des collisions relativistes pour l'interaction laser-plasma et la production d'énergie par fusion"*

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/B-Dubroca.pdf>`__

    Le calcul du transport des électrons est un point important pour la fusion nucléaire contrôlée. Pour les nouvelles installations envisagées pour l'approche par fusion inertielles les effets cinétiques sont trop importants pour être négligés. Les équations d'Euler bi-températures (Ti-T-e) ne sont plus valides dans tout le domaine de calcul et il faut considérer la résolution des équations de Fokker-Planck-landau couplées aux équations de Maxwell. Nous aborderons les problèmes numériques et de calcul posées par la résolution de ces équations.

    La même théorie décrit la propagation des électrons dans le corps humain? Ce sont ces électrons qui détruisent les cellules cancéreuses pendant une radiothérapie.  Nous donnerons une application directe des considérations précédentes pour la simulation de la dose radiologique déposée dans un patient.

    - **Sébastien Barbier**, CEA, *Visualisation Scientifique Distante de Grands Volumes de Données*

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/S-Barbier.pdf>`__

    La croissance des supercalculateurs et de leur capacité de calculs repousse sans cesse les limitations en taille des maillages pour le code de calcul. Le dépouillement par le biais de la visualisation scientifique de ces données massives peut devenir problématique lorsque l'utilisateur réalise le post-traitement sur sa machine de travail et non sur le supercalculateur distant.

    La visualisation scientifique a mis en place des solutions dites distantes sur un modèle client/serveur pour de telles configurations. Nous proposons une solution reposant sur les approches multirésolution et la connaissance des zones d'intérêt au sein des données issues de la simulation. Notre approche consiste à envoyer un maillage grossier au sein duquel est insérée une zone à résolution fine, zone contrôlée par l'utilisateur. Un unique maillage dit birésolution est reçu par le client et est affiché avec des techniques de visualisation usuelles (sprites, isosurface, lignes de courant, ...).

    Le calcul du maillage birésolution est optimisé au sein des mémoires centrales et des cartes graphiques dans le cadre d'un usage local, puis au sein d'une version en mémoire externe pour un usage distant sur des supercalculateurs.

    - **Nicolas Louvet**, ENS Lyon, *"Arithmétique flottante IEEE 754 et amélioration de la précision des calculs"*

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/N-Louvet.pdf>`__

    Depuis 1985, le standard IEEE 754 pour l'arithmétique binaire à virgule flottante s'est largement imposé sur les machines généralistes et dans les environnements de programmation. Ce standard a fourni un cadre rigoureux pour l'implantation fiable, portable et efficace de programmes flottants. Néanmoins, 15 ans après sa publication, le besoin de révision du standard devenait clairement apparent : afin de standardiser les nouvelles instructions mises à disposition des programmeurs sur les processeurs, comme par exemple le Fused Multiply-Add (FMA) des architectures IBM-POWER ou Intel-IA64 ; pour formaliser les formats flottants décimaux ainsi que les formats de grandes précision ; pour prendre en compte les avancées, en matière d'arrondi correct des fonctions élémentaires par exemple ; mais également pour tenter de corriger certaines des ambiguïtés que laissait subsister le standard de 1985. La révision a débuté en 2000, et le nouveau standard, souvent appelé IEEE 754-2008, a été adopté en juin 2008.

    Dans cet exposé, nous passerons en revue certains des points les plus importants du standard IEEE 754-2008, en insistant sur les apports et les évolutions par rapport à sa précédente version. Nous verrons également comment les nouvelles opérations introduites dans le standard peuvent être utilisées afin d'améliorer la précision des algorithmes flottants : nous nous baserons pour cela sur l'exemple de l'évaluation de polynômes.

    - **Sylvain Collange**, ENS de Lyon, *"Parallélisme et Calcul flottant dans les GPU"*

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/S-Collange.pdf>`__

    Les GPU nous permettent d'atteindre en 2010 le teraflop au sein d'une même puce. Cette puissance de calcul a été jusqu'alors principalement étudiée et utilisée pour traiter plus rapidement des problèmes plus gros. Ces implémentations reposent sur une adéquation entre le parallélisme présent nativement ou pas dans l'application et l'organisation matérielle de ces processeurs.
    Après une présentation de l'arithmétique flottante disponible dans les GPU, nous montrerons à travers des exemples qu'il est possible d'utiliser ces accélérateurs pour implémenter efficacement d'autres arithmétiques.

    - **Philippe Helluy**, Université de Strasbourg, "Initiation à OpenCL, Applications à la mécanique des fluides compressibles" 

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/P-Helluy.pdf>`__

    OpenCL est un standard récent pour la programmation des périphériques de type GPU. Après une rapide formation à OpenCL et sa philosophie, je présenterai quelques applications à des calculs de type volumes finis pour des écoulements compressibles.

    - **Sylvie Putot**, CEA, *"Vers l'analyse statique de programmes numériques"*

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/S-Putot.pdf>`__


    Certains programmes, dont le bon fonctionnement est critique en particulier lorsqu'ils peuvent impacter la sécurité de personnes, requièrent une preuve formelle de leur bon comportement. Une propriété classique d'intérêt est l'absence d'erreurs à l'exécution dans le programme, qui a par exemple pu être prouvée automatiquement par analyse statique pour le programme complet de commande de vols des Airbus A340-A380. Cependant, la preuve automatiques de propriétés fines de comportement sur des programmes numériques même relativement simples reste un problème mal résolu.

    Nous présenterons ici une approche basée sur l'analyse statique par interprétation abstraite, dans laquelle nous bornons non seulement les valeurs prises par les variables du programme, mais également la différence de comportement entre l'implémentation en nombres flottants d'un programme et son comportement idéalisé en nombres réels. Nous montrerons ensuite des applications, depuis l'analyse de briques élémentaires de programmes de contrôle-commande, jusqu'à de premiers résultats sur des petits algorithmes classiques du calcul scientifique.

    - **Raymond Namyst**, Université de Bordeaux, *"Programmation des machines multicoeur hétérogènes: quel(s) support(s) d'exécution ?"*

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/R-Namyst.pdf>`__

    Les machines multicoeur équippées d'accélérateurs tels que les GPU ajoutent une dimension hétérogène à des architectures parallèles déjà difficiles à programmer, en raison du caractère hiérarchique de l'organisation de la mémoire et des coeurs. Dans cet exposé, je présenterai le support d'exécution StarPU conçu pour exploiter efficacement les configurations multi-accélérateur en cherchant à atteindre une répartition optimale des traitements sur l'ensemble des unités de calcul. Je discuterai plus généralement des défis posés par l'évolution actuelle des architectures parallèles, sous l'angle des modèles de programmation et des supports d'exécution.

    - **Emmanuel Jeannnot**, LABRI, *"Placement de processus MPI sur archi multicore NUMA"*

    `Support de l'exposé <attachments/spip/Documents/Journees/nov2010/E-Jeannot.pdf>`__

    Dans cet exposé nous discuterons de l'importance de l'affinité des processus parallèles entre eux et comment le placement de ces processus peu avoir de l'influence sur la durée des communications dans le cas des machines hiérarchiques multi-coeurs disponibles aujourd'hui.
