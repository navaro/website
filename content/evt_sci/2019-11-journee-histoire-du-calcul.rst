Journée histoire du |_| calcul
##############################

:date: 2019-11-28
:category: journee
:tags: histoire, calcul
:start_date: 2019-11-28
:end_date: 2019-11-28
:place: IMAG, Grenoble
:summary: Cette journée a pour objectif d'aborder l'histoire du calcul sous différents angles, depuis les mathématiques appliquées jusqu'au lien avec l'informatique et le calcul intensif.
:inscription_link: https://histcalcul2019.sciencesconf.org/registration


.. contents::

.. section:: Description
    :class: description

    Les 80 ans du CNRS incitent à prendre le temps d'appréhender et de comprendre les évolutions qui ont conduit au paysage actuel du calcul.
    La thématique est large, elle est aussi très transverse et trouve son origine bien antérieurement à l'émergence de l'ordinateur.

    L'arrivée de l'informatique et son développement fulgurant ont profondément changé notre perception du calcul.

    Cette journée a pour objectif d'aborder cette histoire sous différents angles, depuis les mathématiques appliquées jusqu'au lien avec l'informatique et le calcul intensif.
    Elle aura lieu le 28 novembre 2019, à Grenoble, sur le campus Saint Martin d'Hères, dans l'auditorium du `bâtiment IMAG <https://batiment.imag.fr/>`__.

    Les inscriptions sont gratuites mais obligatoires pour des questions de logistique (clôture le 22/11/2019).

    La journée sera retransmise en direct et enregistrée : `lien pour le direct <https://gricad.univ-grenoble-alpes.fr/multimedia/direct>`__.

    
.. section:: Intervenants
    :class: description

    - **Pierre Mounier-Kühn**, CNRS & Sorbonne Université
    - **Thierry Dumont**, Institut Camille Jordan, Université Lyon 1 
    - **Bernard Ycart**,  Laboratoire Jean Kuntzmann, Université Grenoble Alpes
    - **Patrick Chenin**, Laboratoire Jean Kuntzmann, Université Grenoble Alpes
    - **Gerhard Wanner**, **Martin J. Gander**, Université de Genève
    - **Olivier Pironneau**, Laboratoire Jacques Louis Lions, Université Pierre et Marie Curie, Académie des Sciences
    - **Alain Guyot**, Association ACONIT
    - **Jean-Guillaume Dumas**, **Françoise Jung**, **Clément Pernet**, Laboratoire Jean Kuntzmann, Université Grenoble Alpes

.. section:: Programme
    :class: description

    Le programme est disponible sur le `site de la conférence <https://histcalcul2019.sciencesconf.org/>`__.

.. section:: Organisation et soutien
    :class: orga

    Cette journée est organisée par `GRICAD <https://gricad.univ-grenoble-alpes.fr/>`__ et soutenue par :

    - Le `GDR Calcul <https://calcul.math.cnrs.fr/>`__,
    - L'`Université Grenoble Alpes <https://www.univ-grenoble-alpes.fr/>`__ et l'`Institut d'ingénierie Univ. Grenoble Alpes (Grenoble INP) <http://www.grenoble-inp.fr/>`__,
    - Le `Laboratoire Jean Kuntzmann <https://ljk.imag.fr/>`__,
    - L'`Institut National des Sciences Mathématiques et de leurs Interactions <https://www.cnrs.fr/insmi/>`__ (INSMI / CNRS),
    - L'association IMAG. 

    Elle s'intégre dans les manifestations liées aux `80 ans du CNRS <https://80ans.cnrs.fr/>`__.
