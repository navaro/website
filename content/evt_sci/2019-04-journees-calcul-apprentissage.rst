Journées "Calcul et |_| Apprentissage"
######################################

:date: 2019-03-25
:category: journee
:tags: machine learning, apprentissage
:start_date: 2019-04-24
:end_date: 2019-04-25
:place: Lyon
:summary: Le groupe Calcul propose une journée et demi de présentation des problématiques et des principaux outils de l'apprentissage automatique.
:inscription_link: https://indico.mathrice.fr/event/153/registration/register


.. contents::

.. section:: Description
    :class: description

    Les techniques d'apprentissage automatique (machine learning) sont devenues ces dernières années des outils très performants et très utilisés.
    En pratique, le calcul scientifique est mis en jeu pour rendre les méthodes efficaces et utilisables sur des problèmes réels.

    L'objectif de ces journées est de présenter les problématiques et les principaux outils de l'apprentissage automatique, en se concentrant sur les aspects algorithmiques et implémentation, à l'intention de la communauté du calcul scientifique.
    Elles ont un objectif de formation, et comporteront principalement des cours généraux et des TPs associés (dont un consacré à des librairies développées récemment).

    Ces journées sont organisées par le GdR Calcul et co-financées par le Labex Milyon.

.. section:: Programme
    :class: programme

    .. schedule::
        :indico_url: https://indico.mathrice.fr/
        :indico_event: 153

.. section:: Lieu
    :class: description

    Amphi Fontannes, bâtiment Darwin D, campus de La Doua, Villeurbanne

.. section:: Intervenants
    :class: orga

    - Freddy Bouchet (ENS Lyon)
    - Aymeric Dieuleveut (CMAP)
    - Jean Feydy (ENS)
    - Aurélien Garivier (ENS Lyon)
    - Patrick Pérez (Valeo, Paris)

.. section:: Organisateurs
    :class: orga

    - Charles-Édouard Bréhier
    - Roland Denis
    - Benoît Fabrèges
