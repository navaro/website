École "Optimisation"
####################

:date: 2014-04-14 15:47:48
:modified: 2014-04-14 15:47:48
:category: formation
:tags: optimisation
:place: Strasbourg
:start_date: 2014-07-07
:end_date: 2014-07-11
:summary: Une école sur l'optimisation de code de calcul.


.. contents::

.. section:: Description
    :class: description

    Face au besoin croissant de puissance de calcul et à des applications numériques toujours plus complexes, s'exécutant sur des architectures parallèles en constante évolution (many-coeurs, GPU, machines hybrides, ...), il est nécessaire de pouvoir mieux contrôler l'efficacité et la performance de nos applications.

    Pour cela, le Groupe Calcul et la Maison de la Simulation organisent, dans le cadre d'`Equip@meso <http://www.genci.fr/fr/content/equipmeso>`__, une école sur l'optimisation qui se déroulera du 7 au 11 juillet 2014 à l'Espe de `l'Université de Strasbourg <http://www.unistra.fr>`__, qui héberge le `mésocentre de l'Université <http://hpc.unistra.fr>`__.

    La formation a eu lieu à l'Espe Alsace, 141 Avenue de Colmar, 67100 Strasbourg. Accès : Tram A/E, Arrêt "Krimmeri/Stade de la Meinau".

    Il s'agissait de la 2ème édition de cette école, qui a rencontré un vif succès en 2013.

    L'enjeu principal de cette école est d'être capable :

    - d'identifier les points critiques d'une application dans son environnement d'exécution ;
    - à l'aide d'outils adaptés, améliorer son fonctionnement et son utilisation.

    En effet, la complexité des outils d'analyse suit celle des architectures et des applications, c'est pourquoi il est nécessaire de permettre à nos communautés d'acquérir rapidement les compétences nécessaires, en lien avec les équipes développant ces outils.

    Il n'y a pas de prérequis particuliers pour suivre cette école si ce n'est la connaissance d'un langage bas niveau (C/C++ ou Fortran) et les connaissances de base des systèmes Unix.



.. section:: Programme détaillé
    :class: programme

    .. schedule::

        .. day:: 07-07-2014

            .. break_event:: Accueil
                :begin: 10:00
                :end: 10:30

            .. event:: Introduction
                :begin: 10:30
                :end: 12:30
                :speaker: Andres S. Charif-Rubial (Université Versailles St Quentin)
                :support:
                    [Slides](attachments/spip/IMG/pdf/Optim_ASCR_2014_V1.pdf)
                    [Vidéo](https://pod.unistra.fr/video/12155-ecole-optimisation-introduction)

            .. break_event:: Repas
                :begin: 12:30
                :end: 14:00

            .. event:: Optimisation de base et vectorisation
                :begin: 14:00
                :end: 17:30
                :speaker: Romaric David
                :support:
                    [Slides](attachments/spip/IMG/pdf/2014_07_07_optim.pdf)
                    [Archive](http://hpc-web.u-strasbg.fr/optim.tgz)
                    [Vidéo](https://pod.unistra.fr/video/12159-bases-de-loptimisation)

        .. day:: 08-07-2014

            .. event:: Caches
                :begin: 09:00
                :end: 12:30
                :speaker: Josef Weidendorfer  (TUM)
                :support:
                    [Slides](attachments/spip/IMG/pdf/weidendorfer.pdf)
                    [Vidéo](https://pod.unistra.fr/video/12160-cours-caches-weidendorfer-partie-1)
                    [Vidéo](https://pod.unistra.fr/video/12163-caches-partie-2)
                    [Archive](http://hpc-web.u-strasbg.fr/cache.tgz)

                - The Memory Hierarchy
                - Caches: Why & How do they work?
                - Bad Memory Access Patterns
                - How to not exploit Caches
                - Cache Optimization Strategies
                - How to exploit Caches even better
                - Cache Analysis
                - Measuring on real Hardware vs. Simulation
                - Cache Analysis Tools
                - Case Studies
                - Hands-on

            .. break_event:: Repas
                :begin: 12:30
                :end: 14:00

            .. event:: Benchmark
                :begin: 14:00
                :end: 17:30
                :speaker: Laurent Gatineau (NEC)
                :support:
                    [Slides](attachments/spip/IMG/pdf/gatineau_Ecole_Opti_Benchmarking.pdf)
                    [Vidéo](https://pod.unistra.fr/video/12167-benchmarks)
                    [Vidéo](https://pod.unistra.fr/video/12170-benchmarks-partie-2)

                1. Introduction

                    - 1.1 Définitions
                    - 1.2 Objectifs des benchmarks
                    - 1.3 Les différentes phases d'un benchmark

                2. Benchmarks synthétiques et éléments d'architecture

                    - 2.1 Processeur
                    - 2.2 Mémoire et cache
                    - 2.3 Réseau rapide
                    - 2.4 Système disques
                    - 2.5 Accélérateur

                3. Benchmark applicatif

                    - 3.1 Portage et validation numérique
                    - 3.2 Classification de l'application
                    - 3.3 Profiling (CPU, MPI)
                    - 3.4 Projection des performances

                4. Travaux pratiques

                    - Contenu ouvert en fonction des utilisateurs:

                        - prise en main des benchmarks synthétiques
                        - classification / profiling d'un code utilisateur

        .. day:: 09-07-2014

            .. event:: Message-Passing Interface - Selected Topics and Best Practices
                :begin: 09:00
                :end: 12:30
                :speaker: Florian Janetzko (Juelich Supercomputing Centre)
                :support:
                    [Slides](attachments/spip/IMG/pdf/MPI-Best-Practices.pdf)
                    [Vidéo](https://pod.unistra.fr/video/12179-message-passing-interface-selected-topics-and-best-practices)
                    [Vidéo](https://pod.unistra.fr/video/12182-message-passing-interface-selected-topics-and-best-practices-part-2)

                 The Message-Passing Interface (MPI) is a widely-used standard library for programming parallel applications using the distributed-memory model. In the first part of this talk we will give a short overview over concepts and properties of modern HPC hardware architectures as well as basic programming concepts. A brief introduction into a design strategy for parallel algorithms is presented. The focus of the second part will be on the Message-Passing interface. After a short overview over general MPI properties and features we will present selected MPI topics including Derived Datatypes, MPI_Info objects and One-sided Communication and discuss best practices for their usage.

            .. break_event:: Repas
                :begin: 12:30
                :end: 14:00

            .. event:: Introduction to Performance Analysis
                :begin: 14:00
                :end: 17:30
                :speaker: Florian Janetzko (Juelich Supercomputing Centre)
                :support:
                    [Slides](attachments/spip/IMG/pdf/Introduction_to_Performance_Analysis.pdf)
                    [Vidéo](https://pod.unistra.fr/video/12184-introduction-to-performance-analysys-part-1)
                    [Vidéo](https://pod.unistra.fr/video/12185-mpi-best-practices)

                The complexity of modern High-Performance-Computing systems impose great challenges on running parallel applications efficiently on them. Performance analysis is therefore a key issue to identify bottlenecks and efficiency problems in HPC applications in order to improve and optimize the performance of these applications. We start with an overview of different performance problems which frequently occur in parallel applications and give an introductions to the concepts of performance analysis. In the second part we will discuss selected performance analysis tools in more detail and show how to use them in order to identify bottlenecks.

        .. day:: 10-07-2014

            .. event:: Optimisation de noyaux CUDA avec les outils de « profiling » Nvidia
                :begin: 09:00
                :end: 12:30
                :speaker: Julien Demouth (Nvidia)
                :support:
                    [Vidéo](https://pod.unistra.fr/video/12186-optimisation-de-noyau-cuda-avec-les-outils-de-profiling-nvidia)
                    [Vidéo](https://pod.unistra.fr/video/12187-optimisation-de-noyau-cuda-avec-les-outils-de-profiling-nvidia)

                Lors de cette session nous présenterons une méthode pour optimiser des noyaux GPU écrits en CUDA et nous la mettrons en pratique sur un cas concret. Pour améliorer les performances de notre application, nous utiliserons les outils de « profiling » : Nsight et Nvprof.

            .. break_event:: Repas
                :begin: 12:30
                :end: 14:00


            .. event:: Introduction aux entrées-sorties parallèles
                :begin: 14:00
                :end: 17:30
                :speaker: Philippe Wautelet (IDRIS/CNRS)
                :support:
                    [Partie 1](attachments/spip/IMG/pdf/intro_io_strasbourg2014_proj.pdf)
                    [Partie 2](attachments/spip/IMG/pdf/filesystems_strasbourg2014_proj.pdf)
                    [Vidéo](https://pod.unistra.fr/video/12188-entrees-sortie-partie-1)

                Les supercalculateurs sont de plus en plus massivement parallèles. Dans ce contexte, l'importance des entrées-sorties devient de plus en plus cruciale. Le but de cette introduction est de présenter les différentes approches existantes pour réaliser des entrées-sorties performantes, leurs avantages et inconvénients, ainsi que les principes de fonctionnement des systèmes de fichiers parallèles. Des travaux pratiques seront réalisés pour mettre en oeuvre les concepts introduits lors de cette présentation.

        .. day:: 11-07-2014

            .. event:: Expériences d'entrées-sorties sur architectures massivement parallèles
                :begin: 09:00
                :end: 12:30
                :speaker: Philippe Wautelet (IDRIS/CNRS)
                :support:
                    [Slides](attachments/spip/IMG/pdf/retoursexperiences_strasbourg2014_proj.pdf)
                    [Vidéo](https://pod.unistra.fr/video/12190-experience-dentrees-sorties-sur-architectures-massivement-paralleles)
                    [Vidéo](https://pod.unistra.fr/video/12191-retour-dexperiences-sur-systemes-de-fichiers-aralleles)

                Les performances des systèmes d'entrées-sorties de plusieurs supercalculateurs ont été évaluées à l'aide d'une vraie application scientifique. Les résultats obtenus et les problèmes rencontrés seront exposés.


.. section:: Comité d'organisation
    :class: orga

    - Romaric David (Université de Strasbourg)
    - Loïc Gouarin (Laboratoire de Mathématiques d'Orsay)
    - Michel Kern (INRIA)

