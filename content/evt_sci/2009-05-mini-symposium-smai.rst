Congrès SMAI 2009 : mini-symposium "Méthodes Multirésolutions"
##############################################################

:date: 2009-06-08 10:00:34
:modified: 2009-06-08 10:00:34
:category: journee
:tags: smai, multirésolution
:start_date: 2009-05-28
:end_date: 2009-05-28
:place: La Colle sur Loup
:summary: Mini-symposium sur les méthodes multirésolutions organisé au congrès SMAI 2009.

.. contents::

.. section:: Description
    :class: description

    Depuis quelques années, le besoin se fait sentir de réaliser la simulation numérique de problèmes multi-échelles de plus en plus complexes tant en terme de modélisation que de dynamique spatiale ou temporelle. La progression des des moyens informatiques au niveau local ou national permet d'aborder ces simulations avec une nouvelle génération d'ordinateurs alliant puissance de calcul et capacité mémoire. Simultanément l'innovation dans les méthodes numériques et les algorithmes doit permettre d'une part de simuler et comprendre la complexité des problèmes et d'autre part de tirer profit des avancées technologiques.

    Nous proposons, dans ce mini-symposium, de faire le point sur les méthodes de multirésolution adaptatives pour la simulation des équations aux dérivées partielles évolutives, qu'elles soient de type paraboliques ou hyperboliques. Ces méthodes sont particulièrement efficaces pour des solutions qui présentent soit des singularités, soit des fronts raides, fortement localisés en espace et se propageant dans le temps. Elles permettent alors de traiter le caractère multi-échelle spatial, et éventuellement  temporel, d'un ensemble de problèmes couvrant un large spectre d'applications (dynamique des gaz, biomédical, combustion, dynamique chimique non-linéaire, décharges plasmas).

    Ce mini-symposium s'appuie sur un récent colloque "Multiresolution and Adaptive Methods for Convection-Dominated Problems" organisé en janvier 2009 au Laboratoire Jacques-Louis Lions à Paris. Il est organisé en lien avec le GDR CALCUL  et avec le soutien de l'INSMI (Institut des Sciences Mathématiques et leur Interactions)  du CNRS.

    Ce mini-symposium a été organisé par Marc Massot, EM2C, ECP.

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 28-05-2009

            .. event:: Méthodes adaptatives pour l'équation de Vlasov
                :begin: 15:00
                :end: 15:45
                :speaker: Eric Sonnendrücker
                :support: attachments/spip/Documents/Manifestations/SMAI2009/Eric.Sonnendrucker.pdf

            .. event:: Présentation des méthodes de multirésolution pour les problèmes dominés par  la convection
                :begin: 15:45
                :end: 16:30
                :speaker: Marie Postel
                :support: attachments/spip/Documents/Manifestations/SMAI2009/smai2009Postel.pdf
