JCAD, Journées Calcul & Données
###############################

:date: 2021-01-07 10:00:00
:modified: 2021-01-07 10:00:00
:category: journee
:tags: jcad
:start_date: 2019-10-09
:end_date: 2019-10-11
:attendees: 100
:place: Toulouse
:summary: Les GIS FRANCE GRILLES et GRID'5000, le Groupe Calcul, le GDR RSD, GENCI et les partenaires de l'Equipex EQUIP@MESO organisent ensemble les JCAD, Journées Calcul Données.

.. contents::

.. section:: Description
    :class: description

    Les GIS FRANCE GRILLES et GRID'5000, le Groupe Calcul, le GDR RSD, GENCI et les partenaires de l'Equipex EQUIP@MESO organisent ensemble les JCAD 2019, Journées Calcul Données : Rencontres scientifiques et techniques du calcul et des données.

    Les JCAD sont dédiées à la fois aux utilisateurs et aux experts techniques des infrastructures et des services associés. Les objectifs de ces rencontres sont de présenter des travaux scientifiques, dans toutes les disciplines, réalisés grâce au soutien des infrastructures de grilles de calcul, de méso-centres ou de cloud, les travaux de la recherche en informatique associée et les évolutions techniques et travaux des administrateurs de ces infrastructures.

    Ces journées associeront exposés pléniers et tables rondes sur des sujets d'actualité.

    Cette année, elles auront lieu du 9 au 11 octobre 2019, à Toulouse, à la Maison de la Recherche et de la Valorisation de l'Université Fédérale de Toulouse (UFTMIP), en partenariat avec l'UFTMIP, le Mésocentre CALMIP, l'IRIT et le CNES.

    Pages des journées : https://jcad2019.sciencesconf.org/

