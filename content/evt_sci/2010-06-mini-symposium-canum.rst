CANUM 2010 : mini-symposium "Modélisation et calcul scientifique : les enjeux en génération d'images"
#####################################################################################################

:date: 2010-05-17 08:01:39
:modified: 2010-05-17 08:01:39
:category: journee
:tags: canum, image
:start_date: 2010-06-01
:end_date: 2010-06-01
:place: Carcans-Maubuisson
:summary: Mini-symposium au CANUM 2010 sur les enjeux en génération d'images


.. contents::

.. section:: Description
    :class: description

    Ce mini-symposium du `CANUM 2010 <http://smai.emath.fr/canum2010/>`__ a été organisé par Stéphane Labbé (LJK, Grenoble) pour le GDR Calcul.

    L'évolution des moyens de calcul de ces dernières années a permis non seulement d'augmenter considérablement le volume des simulations mais aussi la taille des images qui peuvent être traitées. Dans les deux cas, la mise en place de nouvelles techniques adaptées aux dernières architectures devient indispensable. Afin d'illustrer ces évolutions, trois exposés ont été proposés.

    Les thèmes abordés à travers ces trois exposés sont la segmentation d'images et le développement d'algorithmes adaptés à de grands volumes de données, la visualisation des données des simulations de grande taille et la comparaison d'architectures de machines dans le cadre de calculs pour le traitement d'images.


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 01-06-2010

            .. event:: Parallélisation d'opérateurs de TI: multi-coeurs, Cell ou GPU
                :begin: 14:00
                :end: 14:45
                :speaker: Lionel Lacassagne, Université Paris Sud
                :support: attachments/spip/Documents/Manifestations/CANUM2010/lacassagne.pdf

                Nous présentons une évaluation des performances de 3 classes architectures actuelles : processeurs généralistes multicoeurs (GPP), carte graphique (GPU) et processeur Cell. L'algorithme retenu est l'opérateur de Harris pour sa représentativité du traitement d'image bas niveau (calculs réguliers, opérateurs ponctuels et convolutions). Le but est de guider l'utilisateur dans le choix d'une architecture parallèle et d' évaluer l'impact des différentes transformations algorithmiques et optimisations logicielles afin d'avoir une implantation en adéquation avec l'architecture.


            .. event:: Visualisation Distante Temps-Réel de Données Massives
                :begin: 14:45
                :end: 15:15
                :speaker: Sébastien Barbier, CEA

                La croissance des supercalculateurs et de leur capacité de calculs repousse sans cesse les limitations en taille des maillages pour le code de calcul. Ceux-ci atteignent aujourd'hui les gigaoctets voire les petaoctets. Le dépouillement visuel par le biais de la visualisation scientifique de ces données massives peut devenir problématique lorsque l'utilisateur réalise le post-traitement sur sa machine de travail et non sur le supercalculateur distant. Ces problèmes sont de deux ordres : le stockage limité de l'information sur la machine locale et l'interactivité de la visualisation.

                Pour pallier à ces goulots d'étranglements, la visualisation scientifique a mis en place des solutions dites distantes sur un modèle client/serveur. Ces solutions proposent de diminuer la quantité d'informations transmise par le réseau. Elles peuvent reposer sur la simplification guidée des données mais provoquent une dégradation de l'image finale pouvant perturber l'analyse. Cette approche est couplée avec la génération d'images à haute qualité directement depuis le supercalculateur. Cela peut induire une latence lors de la manipulation spatiale du point de vue via les interfaces utilisateurs (souris, clavier) et impose une grappe d'odinateurs dédiée à la visualisation scientifique.

                Nous proposons une solution alternative pour la visualisation scientifique distante reposant sur les approches dites multirésolution et la connaissance des zones d'intérêt au sein des données issues de la simulation. Elle consiste à envoyer un maillage dégradé au sein duquel sera insérée une zone à résolution initiale (ou haute) contrôlée par l'utilisateur. Un unique maillage est affiché avec des techniques de visualisation usuelles (sprites, isosurface, lignes de courant). Ce maillage, dit birésolution, est constitué de trois zones : une simplifiée, une à haute résolution dirigée par l'utilisateur et une dite de recollement. Le maillage simplifié pourra être issu d'une précédente simulation à moindre échelle ou extrait par des algorithmes de simplification spécifiques en prétraitement. La zone de recollement est extraite à la volée en fonction de la zone à haute résolution grâce au précalcul d'une surjection unissant les sommets du maillage dégradée à ceux du maillage haute résolution. Le calcul du maillage birésolution est optimisé au sein des mémoires centrales et des cartes graphiques dans le cadre d'un usage local, puis au sein d'une version en mémoire externe pour un usage distant via un partitionnement préalable des données.

                Une telle solution assure ainsi un compromis sur l'envoi par le réseau de la géométrie. Plus précise qu'une dégradation complète, elle garantit une meilleure compréhension visuelle des données lors de manipulation du point de vue (rotation, translation) ou de la zone d'intérêt. Elle permet l'interactivité lors d'un usage en local et un dépouillement temps-réel relatif au débit du réseau disponible contrairement à l'utilisation d'un maillage à pleine résolution ne pouvant être transmis et stocké sur la machine locale.


            .. event:: Réduction de graphes pour la segmentation d'images par graph cuts
                :begin: 15:15
                :end: 16:00
                :speaker: Nicolas Lermé, Université Paris 13
                :support: attachments/spip/Documents/Manifestations/CANUM2010/lerme.pdf

                En quelques années, les graph cuts se sont imposés comme un outil majeur capable de résoudre de nombreux problèmes issus du traitement d'images et de la vision par ordinateur. Néanmoins, pour de gros volumes de données, cette technique implique la construction de graphes qui ne rentrent pas toujours en mémoire centrale. Actuellement, la plupart des algorithmes de flot maximum / coupe minimum sont incapables de résoudre de telles instances. Dans le cadre de la segmentation d'images, pour pallier ce problème, certains auteurs ont proposé des heuristiques grossières qui ne permettent d'obtenir qu'un optimum local de l'énergie. Nous proposons une nouvelle stratégie pour réduire les graphes durant  leur création : les noeuds du graphe réduit sont typiquement localisés dans une bande étroite autour des contours des objets à segmenter. Empiriquement, les solutions obtenues par le graphe réduit et le graphe non réduit sont identiques. De plus, grâce à un algorithme rapide, nous montrons que le temps de calcul nécessaire à la réduction est souvent compensé par le faible temps de calcul de la coupe minimum sur le graphe réduit. Le réglage d'un paramètre supplémentaire permet de diminuer davantage la taille du graphe réduit tout en contrôlant la segmentation obtenue. Enfin, nous présentons quelques résultats expérimentaux pour la segmentation de volumes de données 2D et 3D par un modèle semi-interactif de type Boykov/Jolly et un modèle basé sur la variation totale.


.. section:: Organisateurs
    :class: orga

    - Stéphane Labbé (LJK, Grenoble)
