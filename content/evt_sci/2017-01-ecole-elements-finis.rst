ANF "Boîtes à outil éléments finis open source"
###############################################

:date: 2016-10-19 23:11:36
:modified: 2016-10-19 23:11:36
:category: formation
:place: Paris
:start_date: 2017-01-24
:end_date: 2017-01-26
:summary: Présentation d'outils open source  pour la méthode des éléments finis.

.. contents::

.. section:: Description
    :class: description

    La méthode des éléments finis est utilisée dans de nombreux domaines de la physique et de l'ingénierie (mécanique des solides, des fluides, bio-physique, électronique, ...).

    Les chercheurs, les doctorants et les ingénieurs ont souvent besoin de résoudre rapidement un problème physique, et il est utile d'avoir des outils flexibles pour tester de nouvelles méthodes numériques ou comprendre le comportement des équations mises en jeu. Réécrire un code de calcul pour résoudre leur propre problème devient alors fastidieux et souvent éloigné de leur domaine de compétence.

    Parallèlement aux outils commerciaux spécialisés, des boîtes à outils généralistes en open source ont été récemment développées : allant au-delà de la simple bibliothèque, elles mettent à la disposition des utilisateurs des outils sophistiqués (géométrie complexe, modèles physiques, parallélisme) tout en offrant une prise en main simple des méthodes éléments finis.

    Ces outils sont de plus optimisés et parallélisés et permettent donc de tester des problèmes de grande taille.

    .. section:: Objectif
        :class: description

        L'objectif de cette formation est de présenter plusieurs outils récents, qui sont à la fois innovants dans les méthodes, et largement utilisés (`FreeFem++ <http://www.freefem.org/>`__, `Feel++ <http://www.feelpp.org/>`__, `Firedrake <http://www.firedrakeproject.org/>`__) en explicitant à chaque fois leurs avantages et leurs inconvénients. Ils peuvent effectivement être complémentaires l'un de l'autre suivant le problème étudié. Chacun de ces outils a son propre langage de programmation. Les participants apprendront à utiliser correctement chacun d'eux en s'appropriant leur langage.

        Ces outils proposent tous des solutions différentes pour s'affranchir des difficultés de programmation de la  méthode des éléments finis, et permettent aux chercheurs, aux doctorants et aux ingénieurs de résoudre des problèmes physiques de plus en plus complexes.

    .. section:: Dates et lieu
        :class: description

        Cette ANF aura eu lieu du mardi 24 janvier au jeudi 26 janvier 2017 à Jussieu (Paris) dans l'amphi Charpak du LPNHE. 


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 24-01-2017

            .. event:: Simulations numériques parallèles et multiphysiques en FreeFem++
                :speaker: F. Hecht et F. Nataf (Laboratoire J.L. Lions, UPMC, Paris)
                :begin: 9:00
                :end: 17:30
                :support:
                    [F. Hecht](https://www.ljll.math.upmc.fr/~hecht/ftp/ff++/ff-ANF-2017.zip)
                    [F. Nataf](attachments/spip/Documents/Ecoles/2017/FEM/presentationHpddmFF++.pdf)

                FreeFem++ est un logiciel pour résoudre numériquement des équation aux dérivées partielles (EDP)  dans des domaines bi et tri dimensionnels avec la méthode des éléments finis. Il concerne tous les secteurs intéressés par le calcul scientifique. Il permet le prototypage rapide de simulations mais peut aussi être l'outil de calcul d'une petite ou moyenne entreprise. Ce logiciel est basé sur un langage utilisateur de type DSL (Domain Specific Language) efficace qui permet de

                - définir naturellement des problèmes variationnels à valeurs réelles, complexes, scalaires ou vectoriels;
                - contrôler facilement l'algorithmique numérique, car une fonction éléments finis peut être vue comme une fonction ou comme un tableau de valeurs (degrés de liberté);
                - jouer avec l'algèbre linéaire: vecteurs, matrices, formes linéaires et bilinéaires;
                - construire, adapter, changer, bouger les maillages des domaines;
                - calculer des problèmes aux limites ou des problèmes au valeurs propres;
                - tirer profit des calculateurs parallèles;
                - l'étendre à travers  des bibliothèques dynamiques (plugins)

                Nous présenterons les différentes possibilités de FreeFem++ ainsi que ses capacités pour le calcul parallèle.


        .. day:: 25-01-2017

            .. event:: Solving PDEs with Feel++
                :speaker: Christophe Prud'homme (Institut de Recherche Mathématique Avancée, Strasbourg)
                :begin: 9:00
                :end: 17:30
                :support: attachments/spip/Documents/Ecoles/2017/FEM/lecture.feelpp.slides.pdf

                Feel++ (Finite Element Embedded Language in C++) is a C++ library for solving partial differential equations with the Galerkin type methods and among them the finite element method and the reduced basis method. It relies on a Domain Specific Embedded Language (DSEL) that allows to write very compact code focusing on the variational formulation of the problem in order to solve  seamlessly from a one to thousands of cores on a supercomputer.

                Feel++ supports a large range of finite elements as well as numerical methods for PDEs through the DSEL (Domain-Specific Embedded Languages). With some advanced mesh features, it enables advanced numerical methods for PDEs such as Hybridized Discontinuous Galerkin methods or Lagrange multipliers based methods. Solver-wise Feel++ takes advantage of PETSc and offers advanced and scalable solution strategies even in complex method settings. Feel++ easily allows to compare various algebraic strategies.

                Feel++ comes with a set of Toolboxes (CFD, CSM, FSI, Thermodynamics and more to come in electro-magnetism) that enables the user through few data files (geometry, parameters and model description) to run right away simulation or build more complex models by coupling different physics.

                This tutorial will explain how to solve finite element problems with Feel++ using the library covering various aspects of Feel++ features from simple problems to more advanced ones. We will also use the Toolboxes to quickly setup standard simulations problems and couple some of them. We will take some time to discuss solution strategies using Feel++ PETSc interface.

                Feel++ and Feel++ Toolboxes will be provided through docker containers and should be readily available on attendees computers. Attendees are welcome to bring their own problems to discuss how they could possibly be solved using Feel++.


        .. day:: 26-01-2017

            .. event:: Solving PDEs with Firedrake
                :speaker: Lawrence Mitchell (Departments of Computing and Mathematics, Imperial College London)
                :begin: 9:00
                :end: 17:30
                :support: attachments/spip/Documents/Ecoles/2017/FEM/26-01-Paris-firedrake.pdf

                Firedrake is a Python package for the automated solution of partial differential equations via the finite element method.  Users write their problems in Python using the FEniCS language to specify the finite element problem they wish to solve.  This results in compact code that is close to the mathematical formulation of the problem. The resulting code runs transparently in serial on a laptop and using thousands of cores on a supercomputer with MPI parallelism.

                Firedrake supports a wide range of finite element families on both simplex and tensor-product cells, and has special support for solving problems in high aspect ratio domains (as often appear in geophysical applications, such as ocean and atmosphere modelling).  For the solution of the discrete linear and nonlinear problems, it interfaces tightly with the PETSc library, giving access to a wide suite of composable algebraic solvers.

                In this tutorial, we will learn how to start using Firedrake for solving finite element problems.  We will start with an overview of the available features and then dive straight in to solving problems, ranging from simple stationary examples through to more complex coupled systems.

                Since writing down the model is normally reasonably straightforward, we will spend some time exploring how to configure the algebraic solvers from PETSc.  Particularly preconditioning of coupled systems using PETSc's fieldsplit preconditioning schemes. One of Firedrake's strengths is that it exposes the symbolic information in the model to manipulation by other computer programs, and a very successful example of this is the dolfin-adjoint project which can automatically derive the discrete adjoint of models written using the FEniCS language.  We will show some time-dependent examples. The tutorial will be quite hands-on, and attendees are of course welcome to bring their own problems to try and implement in Firedrake.


.. section:: Organisation
    :class: orga

    - Matthieu Boileau (CNRS & IRMA)
    - Loic Gouarin (CNRS & Laboratoire de Mathématiques d'Orsay)
    - Pierre Navaro (CNRS & IRMAR)
