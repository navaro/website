École Thématique GEOMDATA
#########################

:date: 2018-03-25 20:20:43
:modified: 2018-03-25 20:20:43
:category: formation
:tags: geomdata
:place: Fréjus
:start_date: 2018-09-10
:end_date: 2018-09-14
:summary: L'objectif de cette école est de présenter un panorama de l'analyse de données géométriques et d'images, en insistant sur la mise en œuvre pratique des algorithmes.

.. contents::

.. section:: Description
    :class: description

    Le groupe Calcul en partenariat avec les GdR IM et MIA, le groupe SMAI SIGMA, l'ERC Noria et Data@ENS organise du 10 au 14 septembre 2018 au CAES de Fréjus une école thématique intitulée GEOMDATA: Introduction à la science des données.

    Son objectif est de présenter un panorama de l'analyse de données géométriques et d'images, en insistant sur la mise en œuvre pratique des algorithmes, dans le langage Python. Elle sera structurée autour de quatre cours-TP (3h+3h):

    - Analyse topologique des données (Frédéric Chazal)
    - Anatomie computationnelle (Stéphanie Allassonière / Jean Feydy)
    - Méthodes d'évolution de front et fast-marching (Jean-Marie Mirebeau)
    - Méthodes variationnelles pour l'imagerie (Caroline Chaux /  Sandrine Anthoine)

    Pour plus d'informations et pour vous inscrire, merci de vous reporter à la page de l'événement

    `https://geomdata.sciencesconf.org/  <https://geomdata.sciencesconf.org/>`__

    **Contacts**

    geomdata@sciencesconf.org
