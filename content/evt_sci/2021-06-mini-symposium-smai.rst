Congrès SMAI 2021 : mini-symposium "Julia, un langage pour les mathématiciens"
#####################################################################################################

:date: 2021-06-23 11:00:00
:category: journee
:tags: smai
:place: La Grande-Motte
:start_date: 2021-06-23
:end_date: 2021-06-23
:summary: Le Groupe Calcul organise un mini-symposium au congrès SMAI 2021 sur le langage Julia


.. section:: Mini-symposium
    :class: description

    Le Groupe Calcul propose un mini-symposium pour découvrir le langage Julia avec une introduction, des exemples d'applications pour la résolution numérique d'équations différentielles et des retours d'expérience de personnes ayant fait le choix de Julia pour leur recherche. Le langage Julia a été créé par des scientifiques pour faire des sciences. Il offre une syntaxe avec des abstractions haut niveau qui permettent d'avoir un code informatique très proche des équations mathématiques. Avec Julia, vous vous concentrez sur l'algorithme peu importe l'architecture sur laquelle il sera utilisé. Il utilise un mécanisme de compilation simultané qui augmente la performance et s'adapte à la cible matérielle. 
    Le langage est stable depuis juillet 2018. Une nouvelle version LTS (Long-Term-Support) va sortir très prochainement avec Julia v1.6.0. Il est utilisable sous forme de notebooks et offre toutes les caractéristiques qui permettent une recherche "reproductible" avec un mécanisme de partage de code et de packages très performant. De 2019 à 2020, les téléchargements de Julia ont augmenté de 87% pour atteindre 24 millions et le nombre de packages a augmenté de 73% (4800). En 2019 le nombre de téléchargements avait déjà augmenté de 77% par rapport à 2018. Libre, facile à installer et à utiliser, Julia est aussi un candidat idéal pour l'enseignement des méthodes numériques.

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 23-06-2021

            .. event:: Julia pour les mathématiques : une introduction
                :begin: 11:00
                :end: 11:30
                :speaker: Olivier Garet
                :support: attachments/evt_sci/2021-06-mini-symposium-smai/smai_2021_garet.pdf

                Julia est un nouveau langage de programmation pour le calcul scientifique et les mathématiques. Son développement a commencé en 2009, dans le laboratoire Lincoln du MIT. On retrouve dans ce langage de haut niveau les facilités classiques des langages couramment utilisés en calcul scientifique, avec en plus une rapidité d’exécution comparable au C, tirant partie de la technologie de compilation Just In Time. Ainsi, le langage permet d'avoir un temps d'écriture rapide tout en préservant la vitesse d'exécution. Depuis son lancement public en 2012, le langage Julia a rassemblé une large communauté. La sortie de la version 1.0 en août 2018 marque la maturité du langage, qui bénéficie aujourd'hui d'un écosystème complet : large collection de bibliothèques en ligne, environnement intégré de qualité, débogueur et profileur. Le but de cet exposé est de présenter les fondements du langage ainsi que quelques exemples dans des domaines divers des mathématiques, avec une présentation succinte de quelques bibliothèques utiles. L'exposé sera délibérément très généraliste, car je suis convaincu que les qualités du langage (syntaxe naturelle, rapidité d'exécution, création simple d’objets mathématiques, sans être un pro de la POO), en font un excellent candidat pour être le couteau suisse du mathématicien.

            .. event:: Simulation numérique avec Julia
                :begin: 11:30
                :end: 12:00
                :speaker: Mickael Bestard
                :support: attachments/evt_sci/2021-06-mini-symposium-smai/smai_2021_bestard.pdf

                Dans cet exposé, nous allons présenter le langage de programmation Julia dans le contexte de la simulation numérique. On commencera par aborder la construction d'un code volumes finis 1D général en Julia que l'on comparera avec un code Python, avant de regarder les performances obtenues surun code 2D. On montrera dans un second temps comment Julia permet de s'attaquer à une large gamme de problèmes issus de l'analyse numérique, à travers un exemple de contrôle de trafic routier. Cette exemple mêlera volumes finis, graphes et optimisation avec notamment utilisation d'outils de différentiation automatique.

            .. event:: Julia, l'unique solution d'un problème d'optimisation
                :begin: 12:00
                :end: 12:30
                :speaker: Oskar Laverny
                :support: attachments/evt_sci/2021-06-mini-symposium-smai/smai_2021_laverny.pdf

                L'estimation de convolutions de lois gammas multivariés via leur projection dans une base de Laguerre, se traite plutôt facilement mathématiquement. Cependant, ce problème inverse de déconvolution fournit une fonction de perte à minimiser ayant plusieurs propriétés fâcheuses, nous forçant à utiliser une optimisation numérique :

                - Globale, car la perte n’est pas convexe et possède pléthore de minima locaux.
                - Compilée, car la perte est lourde, non-parallélisable et très combinatoire.
                - En précision arbitraire, sans quoi les problèmes numériques domine très rapidement le signal.

                À ce stade, l'on commence à comprendre que l'on va probablement devoir recoder une libraire entière : peu de librairies d'optimisation globale précompilées permettent l'utilisation de précision arbitraire dans la fonction de perte ainsi qu'à l'intérieur des algorithmes. Nous verrons comment et pourquoi Julia permet d'effectuer cette optimisation directement, bien que cet agglomérat de problème technique et l'entrelacement nécessaire des différentes librairies n’ai pas eu besoin d’être pensé lors de leurs écritures respectives.

            .. event:: Équations différentielles avec Julia
                :begin: 12:30
                :end: 13:00
                :speaker: Pierre Navaro
                :support: attachments/evt_sci/2021-06-mini-symposium-smai/smai_2021_navaro.pdf

                Le package DifferentialEquations.jl est certainement l'un des meilleurs packages, peut-être le meilleur, en langage Julia. Il est aussi l'une des meilleures bibliothèques disponibles, tous langages confondus, pour résoudre des équations différentielles. Dans cet exposé, nous présenterons des exemples de résolution de systèmes d'équations différentielles et nous verrons à quel point l'interface est à la fois simple car très proche de l’écriture mathématique, et efficace grâce aux performances du compilateur de Julia. Même si DifferentialEquations.jl offre un très grand nombre de méthodes numériques, certaines équations décrivant des phénomènes multi-échelles hautement oscillant restent difficiles à résoudre. Les méthodes classiques sont inefficaces et des méthodes numériques spécifiques sont nécessaires. Les membres de l'équipe projet INRIA MINGUS ont developpé plusieurs stratégies efficaces pour résoudre les EDO hautement oscillantes. Ces techniques sont fondées sur une reformulation mathématique adéquate du problème original. Cette reformulation peut alors être résolue numériquement avec des méthodes numériques usuelles. Nous montrerons comment nous avons procédé pour proposer ces nouveaux schémas aux utilisateurs Julia en utilisant la même interface.

.. section:: Comité d'organisation
    :class: orga

    - Benoit Fabrèges (ICJ, UCBL)
    - Pierre Navaro (IRMAR, Rennes)
