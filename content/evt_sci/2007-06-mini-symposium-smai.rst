Congrès SMAI 2007 : mini-symposium "Calcul Scientifique pour la médecine"
#########################################################################

:date: 2008-02-12 13:34:20
:modified: 2008-02-12 13:34:20
:category: journee
:tags: smai, medecine
:start_date: 2007-06-04
:end_date: 2007-06-04
:place: Praz sur Alry
:summary: Le Groupe Calcul propose un mini-symposium au congrès SMAI 2007 sur le calcul scientifique pour la médecine.

.. contents::

.. section:: Description
    :class: description

    Le `congrès SMAI 2007 <http://www-ljk.imag.fr/smai2007/>`__ a eu lieu à Praz sur Alry du 4 au 8 juin 2007. Dans ce cadre, le Groupe Calcul a participé avec Stéphane Descombes à l'organisation d'un mini-symposium sur le thème "Calcul scientifique pour la médecine".
   
    L'étude des mécanismes responsables ou en cause dans le cadre des maladies est très complexe et rend de moins en moins suffisantes les approches traditionnelles, comme les observations dans les organismes vivants ("in vivo") ou dans des conditions artificielles, en laboratoire, en dehors de l'organisme d'origine ("in vitro"). Après une longue période de tatonnements, la modélisation numérique associée aux techniques de simulation informatique ("in silico"), s'est fortement développée. C'est ainsi que l'on commence à envisager cette approche pour aider l'innovation thérapeutique et faire face à la baisse du taux de succès du développement des nouvelles thérapies. Ce mini-symposium a pour but de présenter quelques cas concrets et de montrer l'importance de l'interdisciplinarité dans ce domaine.

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 04-06-2007

            .. event:: Modélisation d'un accident vasculaire cérébral ischémique
                :begin: 15:00
                :end: 15:30
                :speaker: Marie-Aimée Dronne
                :support: attachments/spip/Documents/Manifestations/SMAI2007/avc.pdf

            .. event:: Modélisation et simulation numérique de l'écoulement de l'air dans les bronches
                :begin: 15:30
                :end: 16:00
                :speaker: Bertrand Maury
                
            .. event:: Étude d'un modèle de croissance tumorale
                :begin: 16:00
                :end: 16:30
                :speaker: Olivier Saut
                :support: attachments/spip/Documents/Manifestations/SMAI2007/tumeur.pdf
