Mini-symposium CANUM 2012 "Le GPU est-il l'avenir du calcul scientifique ?"
###########################################################################

:date: 2012-10-05 07:44:34
:category: journee
:place: Seignosse-Hossegor
:start_date: 21-05-2012
:end_date: 25-05-2012

.. contents::

.. section:: Contexte
  :class: description

  Dans le cadre du `CANUM 2012 <http://smai.emath.fr/canum2012>`__, le GDR Calcul a co-organisé un mini-symposium sur le thème : "Le GPU est-il l'avenir du calcul scientifique ?"

  Ces dernières années ont vu l'émergence d'un nouveau type d'architecture de calcul basé sur l'exploitation des processeurs graphiques (GPU).

  L'utilisation de ces technologies (GPU) permet de disposer de moyens de calcul hautes performances pour un coût et une consommation énergétique réduite. La recherche autour de l'exploitation des GPU est très active, et de nombreux outils et bibliothèques sont désormais disponibles pour développer des codes de calcul sur ces architectures.

  Ce mini-symposium a été l'occasion de faire le point d'une part sur l'évolution matérielle et logicielle de ces nouveaux moyens de calculs, ainsi que sur des retours d'expériences d'utilisation de ces moyens en calcul scientifique, et d'autre part sur l'arrivée sur le marché d'alternatives à ces architectures.

.. section:: Programme
  :class: programme

  .. schedule::

    .. day:: 22-05-2012

      .. event:: The Rise of Accelerators in HPC and Everyday Computing
        :begin: 09:00
        :end: 09:30
        :speaker: Guillermo Marcus, Université de Heidelberg

        The presentation will explore the upcoming role of hardware accelerators in the path to Exascale Computing as well as their transition into mainstream computing. The increasing significance of heterogeneous computing drives a closer hardware and software integration that will affect the way applications of all scales are designed and developed in the near future.

      .. event:: Quelques exemples de calculs instantanés de fluides sur GPU
        :begin: 09:30
        :end: 10:00
        :speaker: Christophe Labourdette, ENS Cachan

        Cette présentation fait état des travaux réalisés par notre équipe en calcul de Mécanique des fluides sur GPU. On présentera trois cas d'étude :

        - Méthodes Lattice Boltzmann pour les équations de Navier-Stokes instationnaires;
        - Méthodes volumes finis pour les équations d'Euler compressibles;
        - Méthodes particulaires pour les équations cinétiques.

        Dans ce retour d'expérience, nous donnerons des speedup-types pour chacun des cas d'étude. Nous discuterons des efforts d'implémentation et des questions de performance, ainsi que des démarches de développement.

        Pour la présentation, on réalisera des démos de simulations/visualisations interactives 2D sur grilles de taille-type 1024 x 1024.

      .. event:: Couplage de méthodes et architecture hybride
        :begin: 10:00
        :end: 10:30
        :speaker: Jean-Matthieu Etancelin, LJK, Grenoble

        Les méthodes particulaires sont particulièrement bien adaptées à la résolution numérique des équations aux dérivées partielles de conservation, en particulier, des équations de transport. Ce travail se décompose en deux parties. La première concerne le développement, sur des architectures distribuées, d'un solveur particulaire parallèle pouvant être couplé à un solveur fluide (spectral ou volume finis) pour la turbulence. La seconde est dédiée à la construction d'un code efficace sur des architectures hybrides multi-coeurs et multi-accélérateurs à l'aide du standard de programmation ouvert OpenCL.

         Le couplage des méthodes particulaires et des solveurs fluide permet de s'affranchir des conditions de CFL inhérentes aux méthodes classiques. Ainsi, la résolution est effectuée à deux niveaux : l'échelle du fluide et celle, plus petite, du transport de la vorticité. En effet, l'absence de condition de type CFL sur la résolution des équations de transport permet de raffiner le maillage sans conséquences sur le pas de temps. Les applications considérées dans cette partie sont des simulations d'écoulements à haut nombre de Schmidt.

         L'intérêt croissant que la communauté du HPC porte à l'utilisation des cartes GPU suggère que cette technologie est bien adaptée à nos simulations. Il a été montré que l'utilisation de ces
         accélérateurs donne accès à des améliorations conséquentes des algorithmes. Nous présentons ici la
         démarche ainsi que les outils employés pour l'implémentation d'un solveur particulaire pouvant s'adapter facilement aux différentes architectures hybrides des machines de calcul actuelles. Ce solveur est conçu de manière indépendante des ressources matérielles, permettant ainsi une
         grande souplesse d'utilisation.

         Les objectifs à plus long terme sont de coupler le solveur particulaire à un solveur Navier-Stokes sur une architecture hybride, multi-coeurs et multi-accélérateurs.

      .. event:: Parallel computing on multicore and Intel Many Integrated Core (MIC) architectures
        :begin: 10:30
        :end: 11:00
        :speaker: Thomas Guillet, Intel Corporation

        Conventional CPUs such as Intel Xeon processors provide efficient
        performance over a wide spectrum of scientific computing and HPC
        applications. The Intel Many Integrated Core (MIC) architecture
        introduces many more smaller cores, more hardware threads, and wider
        vectors units. This combination is well suited for achieving higher
        aggregate performance with highly parallel codes. By employing common
        compilers, libraries and parallel models to program for multicore and
        MIC (such as OpenMP, TBB, Cilk, etc.), developers can optimize their
        applications on CPU today, and enable scaling forward to manycore on
        Intel MIC co-processors.


        With the increase in number of cores, expressing parallelism at multiple
        levels (data, task, thread, etc.) becomes more and more important for
        application performance. In this presentation, we will discuss ways of
        introducing parallelism in applications for multi- and manycore
        architectures, using established and popular parallel models and
        libraries. We will also present some optimization techniques, relevant
        to both CPUs and Intel MIC, to make better use of caches and memory
        hierarchies and help optimize data locality in applications.
