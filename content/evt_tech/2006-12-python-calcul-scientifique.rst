Python en calcul scientifique
#############################

:date: 2010-11-02 14:30:18
:start_date: 2006-12-14
:end_date: 2006-12-14
:category: journee
:tags: python
:place: Institut Henri Poincaré

.. contents::

.. section:: Pré-requis
    :class: description

    Connaissance minimale du langage python (voir par exemple http://www.python.org).
    Expérience minimale en programmation.
    
.. section:: Date et lieu	    
    :class: description

    Le Jeudi 14 décembre 2006
    `Institut Henri Poincaré <http://www.ihp.fr/>`_, 11 rue Pierre et Marie Curie - 75231 Paris Cedex 05, **amphi Hermite**.
    
.. section:: Programme
    :class: programme

    .. schedule::

       .. day:: 14-12-2006

          .. break_event:: Accueil des participants
             :begin: 09:30
             :end: 10:00

          .. event:: Introduction à Python pour les applications scientifiques.
             :begin: 10:00
             :end: 11:00
             :speaker: Konrad Hinsen (Centre de Biophysique moléculaire-CNRS, Orléans)
             :support:
                [Support du cours](attachments/spip/Documents/Journees/dec2006/Formation_Python-Introduction.pdf)

	     Une présentation des spécificités du langage Python dans le contexte  du calcul scientifique et une discussion sur les rôles que Python peut jouer dans ce domaine :

	     - langage de script pour les petites tâches
	     - langage interactif pour le calcul exploratoire
	     - langage de script intégré dans une application
	     - langage principal pour le développement d'une application

          .. event:: Python dans le code Aster
             :begin: 11:00
             :end: 12:00
             :speaker: Christophe Durand (EDF R&D, Clamart)
             :support:
                [Support du cours](attachments/spip/Documents/Journees/dec2006/aster.pdf)

	     Code_Aster est un logiciel de simulation en thermo-mécanique développé depuis 1989 par EDF R&D (éléments finis, calcul de structures, analyses non-linéaires). Il est assez important en taille (>1Mlignes 90% Fortran 10% Python) et en richesse de fonctionnalités. Il a une vocation généraliste et capitalise la R&D d'EDF en mécanique. Il est diffusé en open-source depuis 2001.
	     Python a été choisi en 1999 pour remplacer le langage de commandes (mise en données par l'utilisateur) et la couche logicielle de supervision des calculs. Au delà de cette ambition, Python nous a aussi permis de repenser notre architecture logicielle, de renouveller ou créer l'environnement graphique du code, de l'interfacer plus facilement avec des codes tiers en particulier la plate-forme Salomé, de reprogrammer de façon plus élégante des fonctionnalités numériques.

          .. break_event:: Repas
             :begin: 12:00
             :end: 13:30

          .. event:: Interfaçage avec fortran
             :begin: 13:30
             :end: 14:15
             :speaker: Eric Sonnendrucker (IRMA, Université Louis Pasteur, Strasbourg)
             :support:
                [Support du cours](attachments/spip/Documents/Journees/dec2006/python-fortran.pdf)

	     Interfaçage de Python avec Fortran (avec f2py et forthon)

	  .. event:: Interfaçage avec C++
             :begin: 14:15
             :end: 15:15
             :speaker: Eric Boix (LIP, ENS Lyon) & Violaine Louvet (ICJ, Université Clause Bernard Lyon 1)
             :support:
                [Support Swig](attachments/spip/Documents/Journees/dec2006/SwigJourneeCalcul.pdf)
                [Support Boost-Python](attachments/spip/Documents/Journees/dec2006/SwigJourneeCalcul.pdf)

	     Interfaçage de python avec le C++ en utilisant les librairies suivantes :

		  - Swig
		  - Boost-Python

          .. break_event:: Pause
             :begin: 15:15
             :end: 15:30

          .. event:: Python Numérique
             :begin: 15:30
             :end: 16:30
             :speaker: Konrad Hinsen (Centre de Biophysique moléculaire-CNRS, Orléans)
             :support:
                [Support du cours](attachments/spip/Documents/Journees/dec2006/Formation_Python-Numeric.pdf)

	     Quelques bibliothèques Python pour le calcul  numérique, notamment le module "Numeric" qui propose un grand nombre d'opérations fondamentales qui permettent de coder beaucoup d'algorithmes en Python sans perte de rapidité par rapport aux langages compilés.

          .. event:: Python Graphique
             :begin: 16:30
             :end: 17:00
             :speaker: Thierry Dumont (ICJ, Université Claude Bernard Lyon 1)
	     :support:
		[Support du cours](attachments/spip/Documents/Journees/dec2006/PythonGraphique.pdf)

	     Autour des outils graphiques python, entre autres de mathplotlib, quasi-clone des bibliothèques matlab.

          .. event:: Discussion, débat, échanges
             :begin: 17:00
             :end: 18:00 


