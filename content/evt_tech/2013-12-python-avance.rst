ANF "Python avancé en calcul scientifique"
##########################################

:date: 2013-11-12 08:58:01
:category: formation
:tags: python
:place: Biarritz
:start_date: 2013-12-02
:end_date: 2013-12-06

.. contents::

.. section:: Hébergement
  :class: description

  - Domaine de Françon

  Les frais d'inscription sont gratuits gratuit pour les personnels CNRS. Pour les personnels non CNRS, les frais d'inscription s'élèvent à :

  - 200 euros TTC pour les personnels non-CNRS secteur académique (autres EPST, Universités…),
  - 500 euros TTC pour les personnels des EPIC et du secteur privé.

  L'inscription comprend les frais d’hébergement en chambre individuelle ainsi que les frais pédagogiques. Les frais de transport des agents CNRS sont pris en charge par la délégation d’origine de l’agent à sa demande. Ils doivent donc pour cela envoyer une copie de leur convocation à leur délégation d’origine. Pour les non CNRS, les frais de transport doivent être pris par l’organisme de tutelle ou le laboratoire.

  Le nombre de places est limité à 40 participants, les organisateurs se laissent la possibilité de sélectionner les participants en fonction de l’ordre d’arrivée des inscriptions et des renseignements portés sur la fiche d’inscription.

.. section:: Descriptif de la formation
  :class: description

  L'engouement pour le langage Python n'est plus à démontrer et nous pouvons voir dans le monde de l'Enseignement Supérieur et de la Recherche de plus en plus l'utilisation de ce langage pour la réalisation de codes scientifiques.
  Si Python est simple d'utilisation, il n'en reste pas moins un langage interprété qui souffre de certaines lenteurs. Or, il est primordial aujourd’hui de développer des applications performantes séquentielles et parallèles pour les architectures machines actuelles. La grande diversité des outils permet néanmoins d'améliorer grandement les performances d'un code de calcul utilisant Python afin d'aboutir à des temps de calcul proche d'un code écrit en langage bas niveau.

  Cette formation a pour but d'améliorer et d'enrichir les compétences des personnes utilisant ce langage et ses outils dédiés au Calcul Scientifique.

  Le format de cette ANF diffère un peu des précédentes puisque ce sont les participants qui feront une partie du programme en sélectionnant les modules qui les intéressent.
  Chaque module sera constitué d'un cours suivi de travaux pratiques.

.. section:: Descriptif des modules obligatoires
  :class: description

  - Cython

  Cython est un langage hybride entre le Python et le C fait spécifiquement pour le développement de modules d'extension Python. Cython permet d'optimiser du code Python ligne par ligne en remplaçant des variables Python non-typées par des variables C typées. Cython peut également être utilisé pour l'interfaçage avec des routines en C où Fortran. Nous proposons une initiation à son usage dans le cadre de l'optimisation.

  - Numpy

  NumPy propose une implémentation de tableaux multidimensionnels qui permettent d'exprimer un grand nombre d'algorithmes numériques de façon efficaces sans recours à un autre langage que Python. Pour pouvoir exploiter au mieux ce module, il faut adopter une approche de "programmation par tableaux" que nous allons présenter dans ce module

  - Matplotlib

  Lorsque l'on veut faire des représentations graphiques 1D ou 2D en Python, Matplotlib est le module de référence permettant de réaliser des courbes assez rapidement. Nous présenterons donc les briques de bases de Matplotlib permettant de faire des courbes 1D et 2D à partir de tableaux numpy, de mettre des annotations sur ces figures, d'exporter les figures pour les insérer dans des articles, ... Nous parlerons également des limitations de ce module et des alternatives possibles.

  - Packaging

    - Présentation et mise en oeuvre des outils de packaging autour de Python.
    - Le cycle de vie d'un module est parcouru, la production/installation (setup), la doc (sphinx), le test (unittest), la gestion de sources (mercurial)
    - L'objectif et de montrer que la gestion du packaging est simple et qu'il faut la mettre en place dès le départ du développement.

  - Performance et profiling

  Lors de la phase d'optimisation d'un module, il n'est pas facile de deviner les parties du module les plus gourmandes en temps. Les modules profile et pstats permettent, avec peu d'efforts, de détecter les parties du module prenant le plus de temps. L'utilisation de ces outils se fera sur des exemples plus ou moins basique et il sera présenter en même quelques modules livrés par défaut avec python et quelques astuces afin d'optimiser (ou non!) un code python.

.. section:: Descriptif des modules à la carte
  :class: description

  - ActivePapers : la recherche reproductible en Python

  La bibliothèque ActivePapers permet de créer des "papiers exécutables" en Python qui contiennent des donnés, du code, et les dépendances entre eux. Avec ActivePapers on peut archiver et publier l'ensemble d'un protocole de calcul.

  - f2py

  f2py permet d'utiliser du code Fortran depuis des programmes Python. Nous montrerons comment f2py génère automatiquement des interfaces et comment il est possible de les modifier si nécessaire.  Nous présenterons également les directives que l'on peut ajouter au code Fortran, par exemple pour préciser les arguments d'entrée et de sortie. De nombreux exemples accompagneront la présentation.

  - Gestion mémoire

  La gestion de la mémoire en simulation numérique est une partie critique du code, de part la taille des problèmes que l'on traite actuellement. Ce module parlera de la façon dont la mémoire est gérée en Python en abordant la notion de compteur de référence, de ses avantages et inconvénients avec la gestion du GIL ainsi que la notion de shallow copy (copie vide) et deep copy (copie profonde) et de quand et comment les utiliser.

  - HDF5

    - Présentation rapide de HDF5 et de quelques interfaces Python
    - Mise en oeuvre pour l'archivage et accès de gros volumes de données, en séquentiel, en parallèle, avec partage de liens, avec multi-threading

  - mpi4py

  mpi4py est un module Python permettant d'utiliser la librairie MPI (Message Passing Interface) pour faire du parallélisme sur machines distribuées. Développé par Lisandro Dalcin, ce module s'appuie sur cython qui permet de faire l'interfaçage entre la librairie C et Python. Les performances sont toutes aussi bonnes que celles rencontrées lors d'une utilisation de la librairie MPI via un programme C ou Fortran. Les échanges de messages se font au travers de tableaux numpy ou en utilisant pickle. Nous présenterons ici les bases du module en illustrant les communications de bases (point à point, collectives, ...).

  - Multithread

  Ce module propose une initiation à quelques modules et outils qui permettent d'exploiter le parallélisme des machines multiprocesseurs/multicoeurs à mémoire partagée.

  - Orienté Objets avancé

    - Rappels sur les classes en Python
    - Encapsulation (properties, introspection, surcharge des opérateurs)
    - Dérivation des classes Python

  - pyCuda/pyOpenCL

  pyCuda et pyOpenCL sont, comme leurs noms l'indiquent, deux modules Python permettant de faire du Cuda ou de l'OpenCL. Ils facilitent grandement le développement dans ces langages et permettent donc une bonne initiation à la programmation sur GPU. Nous comparerons les syntaxes pour ces deux modules en nous appuyant sur des exemples simples faisant intervenir des opérations sur des tableux numpy. Nous aborderons également quelques exemples avec pyOpenGL pour la visualisation des données se trouvant sur la carte.

  - Qt/PySide/PyQt

  La librairie Qt permet de faire des interfaces graphiques de qualités et est maintenant très répandue: elle est utilisée pour l'interface de KDE et Koffice, de Mathematica, de VLC media player, ... Elle est également facile d'utilisation notemment en utilisant l'un des 2 modules Python pySide ou PyQt. Nous donnerons sur un exemple simple les principes de développement en Qt pour des applications Python.

  - swig

  swig est un outil qui permet d'accéder à des programmes écrits en C/C++ depuis de multiples langages interprétés : Perl, Python, Ruby,... Seul l'utilisation de swig avec Python sera abordée. A partir d'un fichier d'entête où on lui précise les objets et méthodes C/C++ que l'on souhaite encapsuler, il génère le code d'un module Python. L'efficacité et la qualité du code généré sera étudié via des exemples concrets d'utilisation.

  - VTK

  Nous aborderons la conception d'outils de visualisation sur mesure. Cela a un intérêt lorsque des logiciels tels que ParaView, Visit, Ensight,... ne sont pas envisageables, lorsque l'on veut automatiser la fabrication des images ou lorsque l'on veut faire une visualisation en temps réel.

  Nous expliquerons les notions de pipeline de visualisation, de scène, de rendu et d'interaction via plusieurs exemples de scripts Python utilisant le module vtk.


.. section:: Programme détaillé
  :class: programme

  .. schedule::

    .. day:: 02-12-2013

      .. event:: Packaging/documentation/tests
        :begin: 14:00
        :end: 17:30
        :speaker: Marc Poinot (ONERA)
        :support:
          [cours](attachments/spip/Documents/Ecoles/2013/python/python2013_package.pdf)
          [TP step 0](attachments/spip/Documents/Ecoles/2013/python/python2013_package_step0.tgz)
          [TP step 1](attachments/spip/Documents/Ecoles/2013/python/python2013_package_step1.tgz)
          [TP step 2](attachments/spip/Documents/Ecoles/2013/python/python2013_package_step2.tgz)
          [TP step 3](attachments/spip/Documents/Ecoles/2013/python/python2013_package_step3.tgz)
          [TP step 4](attachments/spip/Documents/Ecoles/2013/python/python2013_package_step4.tgz)
          [TP step 5](attachments/spip/Documents/Ecoles/2013/python/python2013_package_step5.tgz)

        - Présentation et mise en oeuvre des outils de packaging autour de Python.
        - Le cycle de vie d'un module est parcouru, la production/installation (setup), la doc (sphinx), le test (unittest), la gestion de sources (mercurial)
        - L'objectif et de montrer que la gestion du packaging est simple et qu'il faut la mettre en place dès le départ du développement.

    .. day:: 03-12-2013

      .. event:: Numpy
        :begin: 09:00
        :end: 12:30
        :speaker: Konrad Hinsen (CNRS)
        :support:
          [cours](attachments/spip/Documents/Ecoles/2013/python/NumPy%20introduction.pdf)
          [cours avancé](attachments/spip/Documents/Ecoles/2013/python/NumPy%20avance.pdf)
          [TP step 0](attachments/spip/Documents/Ecoles/2013/python/python2013_numpy_step0.tgz)
          [TP step 1](attachments/spip/Documents/Ecoles/2013/python/python2013_numpy_step1.tgz)

        NumPy propose une implémentation de tableaux multidimensionnels qui permettent d'exprimer un grand nombre d'algorithmes numériques de façon efficaces sans recours à un autre langage que Python. Pour pouvoir exploiter au mieux ce module, il faut adopter une approche de "programmation par tableaux" que nous allons présenter dans ce module.

      .. event:: Cython
        :begin: 14:00
        :end: 17:30
        :speaker: Loïc Gouarin (LMO, CNRS), Xavier Juvigny (ONERA)
        :support:
          [introduction](attachments/spip/Documents/Ecoles/2013/python/cython_bases.pdf)
          [interface](attachments/spip/Documents/Ecoles/2013/python/CythonInterface.pdf)
          [tp step 0](attachments/spip/Documents/Ecoles/2013/python/python2013_cython_step0.tgz)
          [tp step 1](attachments/spip/Documents/Ecoles/2013/python/python2013_cython_step1.tgz)
          [tp step 2](attachments/spip/Documents/Ecoles/2013/python/python2013_cython_step2.tgz)

        Cython est un langage hybride entre le Python et le C fait spécifiquement pour le développement de modules d'extension Python. Cython permet d'optimiser du code Python ligne par ligne en remplaçant des variables Python non-typées par des variables C typées. Cython peut également être utilisé pour l'interfaçage avec des routines en C où Fortran. Nous proposons une initiation à son usage dans le cadre de l'optimisation.

    .. day:: 04-12-2013

      .. event:: Notions de performances et profiling
        :begin: 13:30
        :end: 15:00
        :speaker: Xavier Juvigny (ONERA)
        :support: attachments/spip/Documents/Ecoles/2013/python/OptimisationCPU.pdf

        Lors de la phase d'optimisation d'un module, il n'est pas facile de deviner les parties du module les plus gourmandes en temps. Les modules profile et pstats permettent, avec peu d'efforts, de détecter les parties du module prenant le plus de temps. L'utilisation de ces outils se fera sur des exemples plus ou moins basique et il sera présenter en même quelques modules livrés par défaut avec python et quelques astuces afin d'optimiser (ou non!) un code python.

      .. event:: Gestion mémoire
        :begin: 15:30
        :end: 17:30
        :speaker: Marc Poinot (ONERA)
        :support: attachments/spip/Documents/Ecoles/2013/python/NSCOPE-PRS-025-01-Memory.pdf

        La gestion de la mémoire en simulation numérique est une partie critique du code, de part la taille des problèmes que l'on traite actuellement. Ce module parlera de la façon dont la mémoire est gérée en Python en abordant la notion de compteur de référence, de ses avantages et inconvénients avec la gestion du GIL ainsi que la notion de shallow copy (copie vide) et deep copy (copie profonde) et de quand et comment les utiliser.

      .. event:: Orienté objet avancé
        :begin: 17:30
        :end: 19:00
        :speaker: Marc Poinot (ONERA)
        :support:
          [cours](attachments/spip/Documents/Ecoles/2013/python/NSCOPE-PRS-024-01-OO.pdf)
          [tp step 0](attachments/spip/Documents/Ecoles/2013/python/python2013_OO_step0.tgz)
          [tp step 1](attachments/spip/Documents/Ecoles/2013/python/python2013_OO_step1.tgz)
          [tp step 2](attachments/spip/Documents/Ecoles/2013/python/python2013_OO_step2.tgz)

        - Rappels sur les classes en Python
        - Encapsulation (properties, introspection, surcharge des opérateurs)
        - Dérivation des classes Python

    .. day:: 05-12-2013


      .. event:: VTK
        :begin: 09:00
        :end: 12:30
        :speaker: Sylvain Faure (LMO, CNRS)

        Nous aborderons la conception d'outils de visualisation sur mesure. Cela a un intérêt lorsque des logiciels tels que ParaView, Visit, Ensight,... ne sont pas envisageables, lorsque l'on veut automatiser la fabrication des images ou lorsque l'on veut faire une visualisation en temps réel.
        Nous expliquerons les notions de pipeline de visualisation, de scène, de rendu et d'interaction via plusieurs exemples de scripts Python utilisant le module vtk.

      .. event:: PySide
        :begin: 14:00
        :end: 17:30
        :speaker: Loïc Gouarin (LMO, CNRS)
        :support:
          [cours](attachments/spip/Documents/Ecoles/2013/python/pyside.pdf)
          [tp step 0](attachments/spip/Documents/Ecoles/2013/python/python2013_pyside_step0.tgz)
          [tp step 1](attachments/spip/Documents/Ecoles/2013/python/python2013_pyside_step1.tgz)

        La librairie Qt permet de faire des interfaces graphiques de qualités et est maintenant très répandue: elle est utilisée pour l'interface de KDE et Koffice, de Mathematica, de VLC media player, ... Elle est également facile d'utilisation notemment en utilisant l'un des 2 modules Python pySide ou PyQt. Nous donnerons sur un exemple simple les principes de développement en Qt pour des applications Python.

    .. day:: 06-12-2013

      .. event:: Multithread
        :begin: 09:00
        :end: 12:00
        :speaker: Konrad Hinsen (CNRS)
        :support:
          [cours](attachments/spip/Documents/Ecoles/2013/python/Multiprocessing.pdf)
          [tp step 0](attachments/spip/Documents/Ecoles/2013/python/python2013_multiprocessing_step0.tgz)
          [tp step 1](attachments/spip/Documents/Ecoles/2013/python/python2013_multiprocessing_step1.tgz)

        Ce module propose une initiation à quelques modules et outils qui permettent d'exploiter le parallélisme des machines multiprocesseurs/multicoeurs à mémoire partagée.

.. section:: Comité d’organisation
  :class: orga

    - Sylvain Faure (Laboratoire de mathématiques d'Orsay)
    - Konrad Hinsen (Centre de Biophysique Moléculaire, Orléans)
    - Loïc Gouarin (Laboratoire de mathématiques d'Orsay)
    - Xavier Juvigny (ONERA, Palaiseau)
    - Marc Poinot (ONERA, Châtillon)
