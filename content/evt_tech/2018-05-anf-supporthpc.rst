ANF User Support Tools for HPC
##############################

:date: 2018-03-25 19:13:19
:category: formation
:place: Fréjus
:start_date: 2018-05-14
:end_date: 2018-05-18

.. contents::

.. section:: Objectif
    :class: description

        L'objectif de cette formation, à destination des équipes en charge de l'administration et de l'exploitation de machines de calcul, est de faire le tour des technologies récentes en matière d'administration de machines de calcul intensif. Elle vise aussi à être un lieu d'échange d'expertises et de retour d'expérience entre des personnes qui doivent faire face à des problématiques communes.

    `https://ust4hpc.sciencesconf.org/  <https://ust4hpc.sciencesconf.org/>`__

.. section:: Organisation
    :class: orga

        - Violaine Louvet
        - Jean-Luc Hangouët
