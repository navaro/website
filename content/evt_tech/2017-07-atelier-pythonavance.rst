Atelier Python avancé pour le |_| calcul scientifique
#####################################################

:date: 2017-05-09 23:19:45
:category: formation
:place: Paris
:start_date: 2017-07-04
:end_date: 2017-07-06

.. contents::

.. section:: Description
    :class: description

        Le langage Python est maintenant largement utilisé par la communauté scientifique et ce, dans de nombreux domaines. Un certain nombre de packages sont disponibles (NumPy, SciPy, ...) et permettent de réaliser rapidement des prototypes validant des algorithmes de calcul. Mais qu'en est-il du passage à l'échelle ? C'est-à-dire lorsque l'on a validé notre algorithme et que l'on veut maintenant s'en servir sur des millions voire des milliards d'inconnues. Est-ce que le seul usage de NumPy et de Scipy est suffisant ? Bien souvent, la réponse est non. Il est alors nécessaire de s’appuyer sur d’autres outils permettant de passer à des langages proches de la machines en conservant une certaine abstraction. Ces outils sont aujourd'hui nombreux.

        Nous avons décidé lors de cet atelier de vous en présenter trois: Cython qui permet d’avoir un grain fin sur les optimisations; Pythran et Numba qui permettent, à moindre coût, d’avoir de bonnes performances.

        Cet atelier est un niveau avancé et s’adresse à des personnes ayant déjà une bonne compréhension de NumPy (stockage mémoire, manipulation des tableaux, …).


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 04-07-2017

            .. event:: Cython
                :speaker: Xavier Juvigny (ONERA)
                :begin: 9:00
                :end: 17:30
                :support:
                    [Présentation](attachments/spip/IMG/pdf/cythontalk.pdf)
                    [Exercices](attachments/spip/IMG/zip/mandelbrot-2.zip)

                    Cython est une extension du langage Python permettant soit d'optimiser des parties d'un module ou un script python soit de faciliter l'interfaçage d'une bibliothèque C ou C++ avec Python.

                    Utilisé comme optimiseur, Cython permet avec peu d'efforts de générer des modules Python écrits en C dont les performances sont proches d'un code écrit en pure C. Des modules sont proposés avec Cython permettant d'interfacer facilement un script Cython avec divers modules python tel que numpy. De même, Cython propose des scripts permettant de facilement faire appel à des fonctions ou des objets des bibliothèques standards C et C++ ( stdio, std::vector, etc. ). Il est également possible de relâcher le GIL à l'intérieur d'un script Python pour profiter du multithreading via les threads C ou OpenMP.

                    Utilisé comme langage d'interface avec le C ou le C++, Cython propose divers mécanisme permettant de gérer les fonctions ( passage par pointeur ou valeurs ) ou les structures C, ainsi que les classes C++, les fonctions et les classes templates mais aussi les passages par références, etc. Tout cela demande d'organiser son module d'une façon propre et rigoureuse, ce qui sera également vu dans cette formation.

                    Enfin, des exercices seront proposés sur un code de traitement de l'image écrit en pure Python utilisant numpy et scipy, montrant la puissance de Cython pour optimiser un code Python et la facilité d'interfacer une petite bibliothèque C avec Cython.


        .. day:: 05-07-2017

            .. event:: Pythran
                :speaker: Serge Guelton (Quarkslab)
                :begin: 9:00
                :end: 17:30
                :support:
                    [Profiling](http://serge-sans-paille.github.io/talks/perf_py/profiling.pdf)
                    [Pythran](http://serge-sans-paille.github.io/talks/calcul-2017-07-04.html)

                    Pythran est un compilateur pour un DSL embarqué dans Python, spécialisé pour le calcul scientifique, qui se veut le moins intrusif possible sur la base de code existante.

                    Inutile de préciser le type de chaque variable ou de réécrire les appels aux fonctions numpy à la main (même si cela reste possible) : idéalement, une fois un noyau de calcul identifié, celui-ci est isolé dans un module, un unique commentaire de type est ajouté et Pythran est capable de le traduire en un code natif, qui n'utilise plus la libpython pour faire un calcul équivalent. Et qui relâche le GIL si le code a besoin d'être appelé dans un contexte multi-threadé.

                    Le code python d'origine est toujours interprétable, ce qui permet de prototyper son application en pure Python, et d'accélerer les fonctions gourmandes en ressources CPU une fois le prototype validé, sans étape de conversion coûteuse en temps de développement.

                    Cette promesse sera vérifiée sur plusieurs cas pratiques après une introduction rapide aux différents concepts nécessaires à une bonne utilisation de pythran.


        .. day:: 06-07-2017

            .. event:: Numba
                :speaker: Loic Gouarin (LMO/CNRS)
                :begin: 9:00
                :end: 17:30
                :support:
                    [Présentation](https://github.com/gouarin/cours_numba_2017)
                    [TPs](https://github.com/gouarin/formation_python2017)

                    Numba est un projet open source permettant d'optimiser vos codes numériques en faisant du Just In Time sans écrire une ligne de code en langages bas niveau (C, C++ ou Fortran). Il s'appuie sur LLVM pour analyser la fonction Python à améliorer. Son utilisation est simple puisqu'il faut juste ajouter un décorateur Python pour optimiser les fonctions qui consomment du temps dans notre application.Il est également possible de faire, comme Pythran le propose, du Ahead Of Time. Numba offre tout un tas d'options permettant de voir chaque étape de l'optimisation réalisée par LLVM permettant de comprendre un peu mieux le code généré.

                    Tout comme Pythran, il est possible de relâcher le GIL. Les boucles sont vectorisées par défaut lorsque c'est possible. Un des points forts de Numba est qu'il est également possible de générer des fonctions qui peuvent être appelées sur votre GPU soit en utilisant un décorateur, soit à un niveau plus fin en écrivant un kernel CUDA en Python.

                    Lors de cette présentation, nous vous donnerons de bonnes bases pour utiliser Numba en illustrant ses fonctionnalités par des exemples.


.. section:: Organisation
    :class: orga

        - Loic Gouarin (LMO)
        - Serge Guelton (Quarkslab)
        - Konrad Hinsen (CBM)
        - Xavier Juvigny (ONERA)
        - Marc Poinot (SAFRAN)

.. section:: Partenariat
    :class: orga

        .. image:: attachments/spip/IMG/jpg/romeologo.jpg
           :alt: Logo Romeo

        Merci à  Arnaud Renard, Jean-Matthieu Etancelin et Fabien Berini !!
