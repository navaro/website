Questions-Réponses : Julia pour les sciences du vivant
######################################################

:date: 2021-11-19 10:00:00
:category: cafe
:tags: visio
:start_date: 2021-11-19 10:00:00
:end_date: 2021-11-19 11:00:00
:place: En ligne
:summary: Discussion sur l'utilisation de Julia pour les sciences du vivant.
:inscription_link: https://indico.mathrice.fr/event/305
:calendar_link: https://indico.mathrice.fr/export/event/305.ics

.. contents::

.. section:: Description
    :class: description

    Pour ce Café Questions-Réponses, nous accueillons `Remi Vezy  <https://www.remivezy.com>`_, chercheur en modélisation
    de l'architecture des plantes et des végétations au CIRAD. Il nous parlera de son 
    utilisation de Julia. Il développe actuellement deux packages `PlantBiophysics.jl <https://github.com/VEZY/PlantBiophysics.jl>`_ et `MTG.jl <https://github.com/VEZY/MTG.jl>`_.

    Il est aussi l'auteur de vidéos d'introduction au langage disponibles sur sa chaine `Youtube <https://youtu.be/ZZJJgQ2IzQQ>`_.

    Il fera une démonstration de l'utilisation d'un notebook interactif `Pluto <https://github.com/fonsp/Pluto.jl>`_. 
    Remi est également un développeur R et cette séance est l'occasion pour les 
    utilisateurs R, curieux de découvrir Julia, de lui poser des questions. 
    Les questions peuvent être posées sur https://app.meet.ps/attendee/groupecalcul.

    Cette session est accessible à tous et aura lieu sur la plateforme BBB de Mathrice.

    Merci de bien vouloir vous inscrire pour suivre cette session. Cette session sera enregistrée. 
    S'inscrire implique d'accepter ce principe.

.. section:: Invité
    :class: orateur

      - Rémi Vezy : CIRAD (L'organisme français de recherche agronomique et de coopération internationale pour le développement durable des régions tropicales et méditerranéennes).
      

