Bibliothèque C++/Kokkos
#######################

:date: 2021-10-22 10:00:00
:category: cafe
:tags: visio
:start_date: 2021-10-22 10:00:00
:end_date: 2021-10-22 11:00:00
:place: En ligne
:summary: Kokkos est un modèle de programmation en C++ pour l'écriture d'applications, portables en termes de performances, ciblant toutes les plateformes HPC. Il fournit des abstractions pour l'exécution parallèle du code et pour la gestion des données. 
:inscription_link: https://indico.mathrice.fr/event/303
:calendar_link: https://indico.mathrice.fr/export/event/303.ics

.. contents::

.. section:: Description
    :class: description

    Pierre Kestener du CEA présentera Kokkos dans ce Café.

    Kokkos est un modèle de programmation en C++ pour l'écriture d'applications, portables en termes de performances, ciblant toutes les plateformes HPC. Il fournit des abstractions pour l'exécution parallèle du code et pour la gestion des données. L'idée est de séparer au maximum l'écriture de la méthode numérique de son implémentation réelle pour telle ou telle architecture matérielle. Kokkos est conçu pour cibler des architectures complexes avec plusieurs types de ressources d'exécution: CPU multicoeurs ou GPU.
 
    Cette session est accessible à tous et aura lieu sur la plateforme BBB de Mathrice.

.. section:: Orateur
    :class: orateur

    - Pierre Kestener (CEA)
    
