Atelier C++: niveau intermédiaire
#################################

:date: 2016-03-21 10:37:54
:start_date: 2016-05-02
:end_date: 2016-05-04
:category: formation
:authors: Loïc Gouarin
:place: Villeurbanne

.. contents::

.. section:: Description
    :class: description

    Le groupe Calcul en partenariat avec le réseau `ARAMIS <http://aramis.resinfo.org/wiki/>`_ organise à Lyon du 2 au 4 mai 2016 une formation sur le langage C++ 11 et 14 réalisée par `Joël Falcou <https://www.lri.fr/~falcou/>`__ et `Serge Guelton <http://serge.liyun.free.fr/serge/>`__.
    Pour suivre pleinement cette formation, il est indispensable d'avoir de bonnes bases en C++ même si c'est dans les anciennes versions.
    Les matins seront consacrés au cours et les après-midi à de la pratique.


    **Lieu**

    `Bâtiment Nautibus <http://oscar.univ-lyon1.fr/appli-externe/plan/plans/fiches_ucbl/nautibus.html>`_, sur le campus de la Doua à Villeurbanne, dans la salle C5 au rez-de-chaussée dans la partie extension du bâtiment.


    **Supports**

    - `cours <attachments/spip/IMG/pdf/advanced_cpp.pdf>`__
    - `TPs <https://github.com/serge-sans-paille/land_of_cxx>`__

.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 2-05-2016

                .. event:: Aspect impératif
                    :begin: 9:30
                    :end: 11:00

                    - Définir une fonction
                    - Paramètres, arguments et valeurs de retour
                    - Inférence de type
                    - Gestion des erreurs

                .. event:: Gestion des ressources
                    :begin: 11:00
                    :end: 12:30

                    - Principe de la RAII
                    - Sémantique de valeur, sémantique d’entité
                    - Pointeurs à sémantique riche
                    - Règle des 3,5,7,0

                .. event:: Pratique
                    :begin: 14:00
                    :end: 17:30


                .. day:: 3-05-2016

                    .. event:: Programmation orientée objet
                        :begin: 9:00
                        :end: 11:00

                        - Notion d’interface
                        - Héritage
                        - Principes de substitution de Liskov

                    .. event:: Programmation Générique
                        :begin: 11:00
                        :end: 12:30

                        - Principes généraux
                        - Classes et fonctions template
                        - Paramètres template : types, valeurs et templates

                    .. event:: Pratique
                        :begin: 14:00
                        :end: 17:00


                    .. day:: 4-05-2016

                        .. event:: La bibliothèque standard
                            :begin: 9:00
                            :end: 11:00

                            - Conteneurs
                            - Algorithme
                            - Fonctions mathématiques
                            - Multithreading

                        .. event:: Les autres outils
                            :begin: 11:00
                            :end: 12:30

                            - Boost
                            - POCO

                        .. event:: Pratique
                            :begin: 14:00
                            :end: 17:00
