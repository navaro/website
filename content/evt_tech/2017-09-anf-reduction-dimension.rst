ANF Réduction de la |_| dimension dans la fouille de |_| données massives |_| : enjeux, méthodes et outils pour le |_| calcul
#############################################################################################################################

:date: 2017-03-03 11:59:40
:start_date: 2017-09-25
:end_date: 2017-09-29
:category: formation
:place: Oléron, France
:authors: Anne Cadiou

.. contents::

.. section:: Description
    :class: description

    Le calcul scientifique est de plus en plus confronté à l’analyse de volumes de données de grandes tailles, multidimensionnelles, qu’elles soient issues de simulations numériques relevant du calcul intensif ou produites par de grands équipements expérimentaux. La nature multidimensionnelle des données appelle cependant à renouveler les pratiques d'analyse, pour accompagner ce passage à l'échelle.
    La semaine de formation vise à familiariser les participants avec les méthodes de réduction de la dimension (classiques comme ACP, AFC, MDS, …) ou issues du « machine learning » (kernel PCA, …). Cela revient souvent à aborder une question de calcul matriciel (recherche de valeurs propres, décomposition en valeurs singulières).
    Les méthodes et outils pour ces méthodes matricielles seront présentées (cours et TP), afin de sensibiliser à la complexité cubique des algorithmes en fonction de la taille du jeu de données. Des algorithmes reposant sur une heuristique de projection dans un espace aléatoire de petite dimension seront présentés, qui permettent de réduire cette complexité, et réaliser ces calculs sur des matrices pleines de grande dimension.

    Cette ANF s'adresse aux **chercheurs.ses, ingénieur.e.s et doctorant.e.s**  amené.e.s à réaliser du calcul scientifique sur des données massives.


    **Lieu**

    `Village vacances CAES La Vieille Perrotine   <https://www.caes.cnrs.fr/sejours/la-vieille-perrotine/>`__ sur l'île d'Oléron.

    .. figure:: attachments/spip/IMG/jpg/oleron.jpg
        :width: 500 px
        :alt: Photo aérienne du village vacances

.. section:: Modalités
    :class: description

    La formation est gratuite pour les personnels du CNRS, universitaires des UMR CNRS, personnels de l'INRA et de l'INSERM. Pour les autres personnels du monde académique (universitaires hors UMR CNRS, autres EPST/EPIC), nous contacter (anne.cadiou AT ec-lyon.fr).

    Les stagiaires seront accueilis en pension complète `au village vacances CAES La Vieille Perrotine   <https://www.caes.cnrs.fr/sejours/la-vieille-perrotine/>`__.
    Un service de transport en car sera assuré le lundi 25 septembre entre la gare routière de La Rochelle et Saint Pierre d'Oléron.
    Départ à 13h de la gare.
    Retour à 13h30 à destination de la gare de la Rochelle le vendredi 29 septembre (départ de Saint Pierre d'Oléron à 12h).
    Merci de nous confirmer par email que vous souhaitez prendre cette navette.

    **NB :**  les frais de transport (hors car) sont à la charge des stagiaires.

    **Prérequis :**  connaissances de base en calcul matriciel, calcul de valeurs propres et vecteurs propres; connaissance des langages python et C++

.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 25-09-2017

                .. event:: Accueil et introduction
                    :begin: 14:00
                    :end: 16:00

                    - accueil
                    - présentation des participants (et de leurs questions) : 30 x 4 mn = 2h

                .. event:: Introduction à la réduction de dimension, et ses enjeux en données massives
                    :begin: 16:15
                    :end: 17:00
                    :speaker: Alain Franc
                    :support:
                        [introduction_afranc_anf2017](attachments/spip/IMG/pdf/introduction_afranc_anf2017.pdf)

                .. event:: Préparation de jeux de données
                    :begin: 17:00
                    :end: 17:30

                    pour mardi après midi, sur ordinateur

            .. day:: 26-09-2017

                .. event:: Réduction de la dimension dans les dynamiques spatio-temporelles (cours)
                    :begin: 9:00
                    :end: 12:00
                    :speaker: Laurent Cordier et Alain Franc
                    :support:
                        [réduire la dimension](attachments/spip/IMG/pdf/analysedonneesmultivariee_afranc_anf2017.pdf)
                        [réduction de dimension des dynamiques spatio-temporelles](attachments/spip/IMG/pdf/anf_reduction_modele_cordier_2017.pdf)
                        [POD](attachments/spip/IMG/pdf/vki_ln_pod_pod_pod_rom.pdf)
                        [DMD](attachments/spip/IMG/pdf/cordier20101129iffc2dmd_slides.pdf)

                    - Les différentes façons de réduire la dimension
                    - formulation du problème « calculatoire » en question de calcul matriciel sur recherche de vecteurs propres et valeurs propres de matrices construites à partir des données (diagonalisation, SVD)
                    - sensibilisation à la complexité cubique du problème (Alain Franc et Laurent Cordier)

                .. event:: Réduction de la dimension dans les dynamiques spatio-temporelles (TP)
                    :begin: 14:00
                    :end: 18:00
                    :speaker: Pierre Blanchard, Anne Cadiou, Laurent Cordier et Alain Franc
                    :support:
                        [réduire la dimension](attachments/spip/IMG/pdf/analysedonneesmultivariee_afranc_anf2017.pdf)
                        [réduction de dimension des dynamiques spatio-temporelles](attachments/spip/IMG/pdf/anf_reduction_modele_cordier_2017.pdf)
                        [POD](attachments/spip/IMG/pdf/vki_ln_pod_pod_pod_rom.pdf)
                        [DMD](attachments/spip/IMG/pdf/cordier20101129iffc2dmd_slides.pdf)

                    - Présentation de Numpy, travail sur PC des participants avec Numpy et leurs données, en local (Pierre Blanchard)
                    - Présentation de quelques commandes utiles pour la parallélisation
                    - Présentation de deux jeux de données massives :

                        - métabarcoding : Alain Franc et Pierre Blanchard
                        - mécanique des fluides - turbulence : Laurent Cordier et Anne Cadiou

            .. day:: 27-09-2017

                .. event:: Mise en oeuvre des méthodes numériques (cours)
                    :begin: 9:00
                    :end: 12:00
                    :speaker: Xavier Vasseur
                    :support:
                        [Cours](attachments/spip/IMG/pdf/cours_xavier_vasseur.pdf)

                    - Méthodes numériques pour le calcul des valeurs propres des matrices symétriques
                    - le ploint clé est le calcul matriciel dense : réduction, méthodes itératives
                    - méthodes numériques pour la décomposition en valeurs singulières

                .. event:: Mise en oeuvre des méthodes numériques (TP)
                    :begin: 12:00
                    :end: 18:00
                    :speaker: Pierre Blanchard, Anne Cadiou, Laurent Cordier et Alain Franc

                    application à :

                        - métabarcoding (avec Alain Franc et Pierre Blanchard)
                        - mécanique des fluides - turbulence (avec Laurent Cordier et Anne Cadiou)



            .. day:: 28-09-2017

                .. event:: Projection aléatoires ou sélection de colonnes (cours)
                    :begin: 9:00
                    :end: 12:00
                    :speaker: Pierre Blanchard et Alain Franc
                    :support:
                        [Cours](attachments/spip/IMG/pdf/high_dimension_afranc_anf2017.pdf)

                    - la malédiction de la dimension (Alain Franc)
                    - les méthodes de « projection aléatoire » pour les problèmes aux valeurs propres (Pierre Blanchard)

                .. event:: Projection aléatoires ou sélection de colonnes (TP)
                    :begin: 14:00
                    :end: 18:00
                    :speaker: Pierre Blanchard, Anne Cadiou, Laurent Cordier et Alain Franc

                    - présentation des librairies et travail sur jeux de données massives (Pierre Blanchard)
                    - métabarcoding (Alain Franc et Pierre Blanchard)
                    - mécanique des fluides - turbulence (Laurent Cordier et Anne Cadiou)


            .. day:: 29-09-2017

                .. event:: Finalisation et synthèse des TP
                    :begin: 9:00
                    :end: 11:00

                .. event:: Conclusion de la semaine
                    :begin: 11:00
                    :end: 12:00
                    :speaker: Anne Cadiou, Alain Franc et Vincent Miele

                    - discussion générale, debriefing, évaluation (Anne Cadiou, Vincent Miele et Alain Franc)



.. section:: Organisation
    :class: orga

    - `Alain Franc  <https://www6.bordeaux-aquitaine.inrae.fr/biogeco/UMR-Biogeco/Personnel/Annuaire-2019-A-Z/F/Franc-Alain>`__
    - `Anne Cadiou  <http://lmfa.ec-lyon.fr/spip.php?article267>`__
    - `Vincent Miele  <http://lbbe.univ-lyon1.fr/-Miele-Vincent-.html>`__
