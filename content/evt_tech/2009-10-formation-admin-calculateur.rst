Choix, installation et |_| exploitation d'un |_| calculateur
############################################################

:date: 2009-04-16 07:43:17
:start_date: 2009-10-05
:end_date: 2009-10-09
:category: formation
:tags: administration, installation
:place: Autrans

.. contents::

.. section:: Description
    :class: description

    Formation CNRS : Choix, installation, exploitation d’un calculateur.

    Autrans, du 5 au 9 octobre 2009.

.. section:: Programme
   :class: programme

      .. schedule::

	 .. day:: 05-10-2009

	    .. event:: Introduction
	       :begin: 13:30
               :end: 14:30

	       - Tour de table, attentes
	       - Introduction à la formation

            .. event:: État des lieux moyens de calcul, mésocentres et GENCI
               :begin: 14:30
               :end: 16:00
               :speaker: Violaine Louvet
	       :support: attachments/spip/Documents/Ecoles/2009/hpc.pdf

	       Le paysage français du calcul a beaucoup évolué ces dernières années. Ce cours a pour but de présenter les différentes structures liées au calcul (moyens matériels mais aussi structures de recherche et d’animations scientifiques) afin d’avoir une vue globale de la situation du calcul en France.
	       Les points qui seront en particulier abordés sont :

	       - PRACE, GENCI ;
	       - les mésocentres ;
	       - les grilles de recherche et de production ;
	       - les GDR et réseaux liés au calcul ;
	       - les structures d’animation scientifique ;
	       - l’offre de formation.

	    .. event:: Les grilles de production (+TP)
               :begin: 16:30
               :end: 18:30
               :speaker: Bruno Bzeznik
	       :support: attachments/spip/Documents/Ecoles/2009/computing_grids_2009-10-05.pdf

	       Il ne faut pas confondre "grille de calcul" et "grappe de calcul". Les grilles se situent à un autre niveau d’échelle, mais ce ne sont pas non plus toujours des grappes de grappes. Nous verrons dans cet exposé quelques définitions pour comprendre les grilles, nous aborderons quelques problématiques et des exemples. Enfin, pour comprendre certains concept à un niveau pratique, nous utiliserons ensemble une grille de production légère : CiGri.

	 .. day:: 06-10-2009

	    .. event:: Architectures aujourd’hui et demain
	       :begin: 8:30
               :end: 10:30
               :speaker: Françoise Roch
	       :support: attachments/spip/Documents/Ecoles/2009/architecture.pdf

	       Ce cours présente les caractéristiques des architectures des plateformes de calcul haute performance et détaille plus particulièrement les architectures de processeurs, les différents niveaux de hiérarchie mémoire, les solutions d’interconnexion. Nous nous intéresserons aux différents méthodes qui ont été mises en oeuvre au cours des 10 dernières années pour gagner en performance, et aux facteurs qui déterminent aujourd’hui l’évolution des processeurs et des calculateurs.

	    .. event:: Modèles de programmation
	       :begin: 11:00
               :end: 13:00
               :speaker: Guy Moebs
	       :support: attachments/spip/Documents/Ecoles/2009/parallele.pdf

	       Cet exposé est une introduction au calcul parallèle sur les architectures généralistes actuelles. Son usage permet des gains de performances significatifs mais cela nécessite un peu de méthode. Plusieurs paradigmes existent ; nous nous intéressons aux deux principaux : parallélisme de données (OpenMP) et parallélisme de tâches (MPI). Les performances et techniques sont fortement liées aux modèles de machines parallèles, architecture et interconnexion des processeurs.

	    .. event:: Contraintes d’hébergement, problématiques énergétiques, refroidissement
	       :begin: 15:00
               :end: 16:30
               :speaker: Dominique Boutigny
	       :support: attachments/spip/Documents/Ecoles/2009/infrastructure.pdf

	       Dans ce cours, nous présenterons la problématique de l’hébergement des ressources informatiques. En s’appuyant sur des exemples concrets nous montrerons quelles sont les principales contraintes liées à la consommation électrique, au refroidissement et à l’agencement des salles. Un point particulier sera consacré aux aspects environnementaux tant du point de vue légal que citoyen. Les stagiaires seront invités à échanger librement au cours de cette session et sont les bienvenus pour proposer des exemples ou des problèmes concrets.

	    .. event:: Etat des lieux des gestionnaires de batch, configuration
	       :begin: 17:00
               :end: 19:30
               :speaker: Olivier Richard
	       :support: attachments/spip/Documents/Ecoles/2009/gestionnaire-de-batch.pdf

	       Après une introduction sur l’utilité des gestionnaires de ressources (aussi appelés Batch Scheduler), nous présenterons les grands principes de leur structure. Par la suite nous détaillerons certaines fonctionnalités, et les principaux logiciels disponibles. Des études cas seront aussi abordés pour illustrer les choix de configurations auxquels doit faire face un adminstrateur de grappe de calcul.

	 .. day:: 07-10-2009

	    .. event:: Installation d’un cluster : différentes approches de déploiement de systèmes - Etat des lieux des installations de cluster / déploiements
	       :begin: 8:30
               :end: 13:00
               :speaker: Nicolas Capit
	       :support: attachments/spip/Documents/Ecoles/2009/deploiement.pdf

	       - Introduction sur les outils de déploiement
	       - Les distributions standards et leurs méthodes de déploiement intégrées
	       - Les outils de déploiement qui s’appuis sur des distributions standard
	       - Les distributions spécialisées
	       - Les mises à jours après déploiement
	       - Questions/réponses

	    .. event:: Déploiement d’un cluster
	       :begin: 15:00
               :end: 19:30
               :speaker: Sessions parallèles
	       :support:
		  [Clustervision](attachments/spip/Documents/Ecoles/2009/clustervision.pdf)
		  [Kerlabs](attachments/spip/Documents/Ecoles/2009/kerlabs.pdf)
		  [SGI](attachments/spip/Documents/Ecoles/2009/sgi.pdf)

	       Présentation des solutions actuelles par les différents prestataires du domaine (4 sessions en parallèle)

	       - Kerrighed
	       - SGI
	       - Bull
	       - Clustervision

	 .. day:: 08-10-2009

	    .. event:: Outils de développements et librairies
	       :begin: 08:30
               :end: 13:00
               :speaker: Thierry Dumont, Romaric David
	       :support:
		  [T. Dumont](attachments/spip/Documents/Ecoles/2009/outils.pdf)
	          [R. David](attachments/spip/Documents/Ecoles/2009/biblios.pdf)

	       Politique d’installation des librairies scientifiques : finalités des différentes librairies, installation, utilisation (avec TP), Thierry Dumont.
	       Outils de développement et d’optimisation, implémentation (MPI), installation et utilisation (avec TP), Romaric David.
	       Ce cours présentera un panorama des outils de base qu’on retrouve sur tout cluster de calcul et qui sont nécessaires à un chercheur lorsqu’il s’approprie une machine :

	       - Quels compilateurs puis-je utiliser ?
	       - Où sont les bibliothèques mathématiques ?
	       - Quel est le MPI géré et maintenu par les administrateurs système ?

	       Une fois ces outils connus, nous examinerons comment :

	       - analyser les performances d’un code parallèle
	       - utiliser un IDE pour le développement d’applications MPI

	    .. event:: Benchmarks : aspects théoriques, aspects pratiques, TP sur différentes architectures, interprétation des résultats
	       :begin: 15:00
               :end: 19:30
               :speaker: Ludovic Saugé
	       :support: attachments/spip/Documents/Ecoles/2009/benchmarks.tar.gz

	       La phase de benchmarking est bien évidemment primordiale pour orienter le choix d’une machine : elle permet entre autre de comparer les différentes solutions entre elles. Elle permet aussi aux différents vendeurs de contraindre l’architecture qui sera proposée en fonction des différents résultats obtenus par l’excution du jeu de test soumis. Au delà, ils permettent au cours de la vie de la machine de détecter d’éventuels problèmes ou régressions.

	       Les benchmarks sont des outils de mesure permettant de déterminer les performances d’un système, d’une sous-partie de ce système voire d’un élément particulier de celui-ci. Encore faut-il savoir ce que l’on souhaite mesurer et comment le mesurer.

	       Au cours de cette séance, je présenterai certains aspects théoriques mais aussi plus pratiques. Entre autre :

	       - les différents aspects touchant aux performances des divers élements d’un cluster (CPU, mémoire, interconnect rapide, système d’IO ...) et que sont les valeur pertinentes à mesurer et comment les mesurer,
	       - les bons usages pour préparer au mieux son jeu de tests et d’éviter certains pièges.

	       Suivra une séance de travaux pratiques où l’utilisateur sera invité à effectuer quelques benchmarks sur différents systèmes.

	 .. day:: 09-10-2009

	    .. event:: Procédures d’achat
	       :begin: 08:30
               :end: 10:30
               :speaker: Françoise Berthoud
	       :support: attachments/spip/Documents/Ecoles/2009/achats.pdf

	       Méthodologie, traduction des besoins en spécifications techniques dans un cahier des charges, choix des critères de sélection des offres/pondération : théorie et mise en pratique. Les stagiaires seront invités à apporter un CCTP et à travailler sur son analyse, amélioration par binome ou trinome.

	    .. event:: Monitoring de clusters, pourquoi, comment ?
	       :begin: 11:00
               :end: 12:00
               :speaker: Olivier Brand-Foissac
	       :support: attachments/spip/Documents/Ecoles/2009/monitoring.pdf

	       A quoi sert le monitoring, en quoi est-il nécessaire ? Comment fonctionne-t-il ? Quels sont les avantages et inconvénients des différents types ? Quelles spécificités dans un cluster de calcul ? Les outils disponibles les plus courant.
	       Exemples.
