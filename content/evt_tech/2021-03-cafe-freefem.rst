Calcul parallèle et éléments finis
##########################################################

:date: 2021-03-08 10:00:00
:category: cafe
:tags: visio
:start_date: 2021-03-08 10:00:00
:end_date: 2021-03-08
:place: En ligne
:summary: Le calcul parallèle pour tous pour la méthode des éléments finis
:inscription_link: https://indico.mathrice.fr/event/249
:calendar_link: https://indico.mathrice.fr/export/event/249.ics

.. contents::

.. section:: Description
    :class: description

      Ce Café sera réalisé par Frédéric Nataf.

      Le but de la présentation est de montrer que le calcul scientifique parallèle est de plus en plus accessible à tous les utilisateurs. Cela est dû à la conjonction de la disponibilité grandissante des moyens de calcul parallèle et à l’intégration dans les codes de calcul des algorithmes parallèles de type distribué via la librairie MPI. Le tout sera illustré sur le logiciel libre FreeFem (https://freefem.org/). On verra qu’il est possible de mener des simulations parallèles en ayant simplement ”conscience” de travailler dans un environnement parallèle mais sans avoir besoin d’écrire des instructions MPI. 

    La présentation durera 30-40 minutes et sera suivie d'une séance de questions.

    Elle est accessible à tous et aura lieu sur la plateforme BBB de Mathrice. Merci de bien vouloir vous inscrire pour suivre cette session.
    
.. button:: Supports de la présentation
    :target: https://indico.mathrice.fr/event/249/material/slides/0.pdf

.. button:: Vidéo sur canal-u.tv
    :target: https://www.canal-u.tv/video/groupe_calcul/le_calcul_parallele_pour_tous_pour_la_methode_des_elements_finis.59589

.. section:: Orateur
    :class: orateur

      - Frédéric Nataf (Laboratoire J.L. Lions)
      
