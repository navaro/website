Atelier "Profilage de codes de calcul OpenMP"
#############################################

:date: 2014-10-24 17:48:58
:category: formation
:start_date: 2014-12-10
:end_date: 2014-12-11
:place: CentraleSupélec, Châtenay Malabry
:tags: openmp
:summary: Un atelier sur le profilage de code de calcul OpenMP.

.. contents::

.. section:: Description
    :class: description

    Face au besoin croissant de puissance de calcul des applications scientifiques, l’optimisation des codes de calcul est devenue un point clef de la simulation numérique. L’étape préalable à toute action d’optimisation consiste à **analyser les performances du code (profilage) afin d’identifier les points critiques de celui-ci dans son environnement d’exécution**. Pour ce faire, il existe de nombreux outils d’analyse qu'il convient de savoir choisir et utiliser en fonction de ses besoins.

    Le Groupe Calcul en collaboration avec le `Mésocentre de Centrale Paris <http://www.mesocentre.ecp.fr/>`__ accueilleront Brian Wylie (Jülich Supercomputing Center) pour un  atelier sur le profilage des codes de calcul OpenMP. Cette formation aura lieu le mercredi 10 (de 9h00 à 17h00) et le jeudi 11 (de 9h00 à 13h00) décembre 2014 dans les locaux de l'Ecole Centrale Paris.

    L'enjeu de cet atelier était de donner aux participants de **bonnes bases pour débuter en profilage des applications OpenMP**.

    Cet atelier pratique comportera :

    - une introduction au profilage des codes OpenMP,
    - une présentation d'outils comme Scalasca,
    - des travaux pratiques sur machine utilisant ces outils.


    **Les prérequis pour assister à cet atelier sont une bonne connaissance des bases de OpenMP (pour les applications C/C++ ou Fortran) et des bases de Unix** . La formation aura lieu en Anglais.

    Les participants le souhaitant pourront tester les outils sur leur application OpenMP sur la machine du mésocentre de Centrale Paris.

    Les participants pourront également assister au séminaire du Mésocentre de Centrale Paris, portant sur la résolution des systèmes linéaires, jeudi 11 décembre à 14h00 avec deux exposés :

    - Luc Giraud (INRIA Bordeaux Sud-Ouest, HiePACS Project Team) : "Some progresses in parallel numerical linear algebra toward extreme scale".
    - Marc Baboulin (Université Paris-Sud et Inria) : "Fast linear algebra computations using randomized algorithms".
