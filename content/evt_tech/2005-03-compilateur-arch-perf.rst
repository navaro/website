Compilateurs, architectures et performances des codes
#####################################################

:date: 2008-02-07 09:24:27
:start_date: 2005-03-24
:end_date: 2005-03-24
:category: journee
:tags: compilateur, performance
:place: Lyon, France


.. contents::

.. section:: Description
    :class: description

    Le Groupe Calcul et l'Unité de Service CODICIEL s'associent pour proposer une journée de formation destinée aux acteurs du calcul scientifique et portant sur la compréhension des outils utilisés pour le développement des codes de calcul, en particulier des compilateurs.
    Cette journée est organisée avec le soutien de la Fédération Lyonnaise de Calcul Haute Performance.


    **Dates et lieu**

    Le jeudi 24 mars 2005, à l'Université Lyon 1, amphi du Bâtiment Dirac.


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 24-03-2005

            .. break_event:: Accueil / café
                :begin: 10:00
                :end: 10:30

            .. event:: Le compilateur : un outil mystérieux du calcul scientifique.
                :begin: 10:30
                :end: 12:30
		:speaker: Alain Lichnewsky (Laboratoire de Mathématique, Université Paris-Sud)
		:support: attachments/spip/Documents/Journees/mars2005/compilateur.pdf

		Les calculateurs utilisés dans les grands problèmes du calcul scientifique ont atteint des degrés élevés de parallélisme. D'autre part les applications deviennent très sophistiquées, que ce soit en raison de leur algorithmique interne ou en raison de la nécessité d'exposer un parallélisme de plus en plus massif.
		Les langages de programmation offrent une aide importante  à la mise en oeuvre de programmes. Nous essaierons dans cet exposé de donner une idée des concepts et des technologies sous-jacents, en particulier concernant la phase de compilation destinée à obtenir du code machine.
		Notre propos est surtout d'aider à la construction de grandes applications en facilitant la compréhension d'ensemble du processus de construction applicative.

		- Notions sur les langages de programmation
		- Notions de sémantique
		- Le processus de traduction
		- Les représentations intermédiaires
		- Traduction vers une représentation de haut niveau
		- Transformations Ã  haut niveau
		- Traduction vers une représentation de bas niveau
		- Génération de code
		- Optimisation des ressources
		- Ordonnancement
		   
            .. break_event:: Repas
                :begin: 12:30
                :end: 14:00

            .. event:: Code tuning : Structures des programmes, architectures et performances.
                :begin: 14:00
                :end: 16:00
                :speaker: François BODIN (IRISA, Université Rennes 1)
                :support: attachments/spip/Documents/Journees/mars2005/CodeTuning.pdf

		L'accroissement de performance des microprocesseurs réside dans l'augmentation de la fréquence d'horloge des processeurs et dans l'utilisation du parallélisme entre instructions (les architectures superscalaires telles que le Pentium d'Intel, ...). Cependant, à mesure que la performance crête augmente la structure des codes influe de plus en plus sur leurs temps d'exécution.
		Dans cette présentation nous abordons l'ensemble des notions nécessaires à la compréhension des performances des codes. D'un côté, nous rappelons les principaux mécanismes architecturaux influençant les performances des programmes (pipeline, parallélisme entre instructions, hiérarchie mémoire, etc.). De l'autre côté, nous montrons au travers de transformations de code l'impact de la structure des programmes sur les performances.

            .. event:: Discussion, débat, questions aux intervenants
                :begin: 16:00
                :end: 17:00

		Autour du développement de logiciels pour le calcul scientifique
