Questions-Réponses : les packages Julia
#######################################

:date: 2021-06-18 11:00:00
:category: cafe
:tags: visio
:start_date: 2021-06-18 11:00:00
:end_date: 2021-06-18 12:00:00
:place: En ligne
:summary: Discussion sur le développement de package en Julia
:inscription_link: https://indico.mathrice.fr/event/263
:calendar_link: https://indico.mathrice.fr/export/event/263.ics

.. contents::

.. section:: Description
    :class: description

    Pour ce Café Questions-Réponses, nous accueillons Juan Ignacio Polanco, chercheur au LMFA à Lyon, 
    auteur entre autres de `PencilFFTs.jl <https://github.com/jipolanco/PencilFFTs.jl>`_ 
    et `WriteVTK.jl <https://github.com/jipolanco/WriteVTK.jl>`_, et Romain Veltz, chercheur INRIA au centre de Sophia-Antipolis, auteur de `BifurcationKit.jl <https://github.com/rveltz/BifurcationKit.jl>`_. 

    Ils seront disponibles pour répondre à toutes vos questions sur le développement de packages en Julia pendant 45 minutes. Vous pourrez poser vos questions sur https://app.meet.ps/attendee/groupecalcul#questions. Cet évènement s'adresse aux personnes qui connaissent déjà un peu julia et qui souhaitent adopter de bonnes pratiques dans leur processus de développement. En effet, démarrer un package Julia est accessible et utiliser ce cadre est bénéfique même s'il s'agit d'un développement personnel. 

.. button:: Vidéo sur canal-u.tv
    :target: https://www.canal-u.tv/video/groupe_calcul/questions_reponses_sur_les_packages_julia.62275
    
.. section:: Invités
    :class: orateur

      - Juan Ignacio Polanco (`Laboratoire de Mécanique des Fluides et d’Acoustique de Lyon <https://jipolanco.gitlab.io>`_)
      - Romain Veltz (`INRIA Sophia-Antipolis <http://romainveltz.pythonanywhere.com>`_)
      

