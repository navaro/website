Atelier Git |_| et |_| Gitlab au |_| quotidien |_| : commandes principales et |_| workflows
###########################################################################################

:date: 2015-05-05 14:40:02
:category: journee
:start_date: 2015-06-04
:end_date: 2015-06-04
:place: CentraleSupélec, Châtenay-Malabry
:tags: git, gitlab
:summary: Atelier git et gitlab.

.. contents::

.. section:: Description
    :class: description

    Le groupe Calcul et le `Mésocentre de CentraleSupélec <http://www.mesocentre.ecp.fr/>`__ ont proposé un atelier sur Git et Gitlab le jeudi 4 juin 2015 à `CentraleSupélec <https://www.centralesupelec.fr/>`__ sur le campus de Châtenay-Malabry (anciennement Ecole Centrale Paris).

    Cette formation était composée de présentations et des sessions pratiques. Le prérequis pour suivre cet atelier était d’avoir déjà utilisé un outil de gestion de versions (CVS, SVN, Mercurial, Git, ...).


.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 04-06-2015

                .. event:: Connaître les principaux workflows de développement
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Benoît Bayol, MAS

                .. event:: Utiliser git avec un dépôt local
                    :begin: 10:00
                    :end: 12:00
                    :speaker: Benoît Bayol, MAS

                .. event:: Utiliser git avec un dépôt distant
                    :begin: 14:00
                    :end: 15:30
                    :speaker: Benoît Bayol, MAS

                .. event:: Utiliser gitlab pour mettre en place son workflow
                    :begin: 15:30
                    :end: 17:30
                    :speaker: Benoît Bayol, MAS
