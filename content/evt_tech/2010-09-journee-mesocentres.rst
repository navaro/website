3ème Journée Mésocentre
#######################

:date: 2010-05-18 14:43:58
:category: journee
:tags: mesocentres
:place: CNRS, Paris
:start_date: 2010-09-21
:end_date: 2010-09-21


.. contents::

.. section:: Description
  :class: description

  Calcul, GENCI et la CPU a organisé la 3ème journée mésocentres le 21 septembre 2010 à Paris à l'amphithéâtre du CNRS, rue Michel-Ange.
  Cette journée a eu pour thématique : "Animation scientifique et technique en lien avec les plateformes de calcul".

  Cette journée fait suite à deux premières journées :

    - `le 13 février 2008 <2008-02-journee-mesocentres.html>`_
    - `le 24 septembre 2009 <2009-05-journee-mesocentres.html>`_
    - et à une `demi journée sur les outils des mésocentres <2010-06-outils-mesocentres.html>`_.

  **Cette journée a été webcastée par l'équipe du CC-IN2P3, les vidéos sont en ligne à l'adresse http://webcast.in2p3.fr/2010/meso/.**


.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 06-12-2010

                .. event:: Le point de vue de la CPU sur la mésoinformatique
                    :begin: 09:40
                    :end: 10:15
                    :speaker: Daniel Egret, CPU

                .. event:: Evolution des moyens de calcul nationaux et européens
                    :begin: 10:15
                    :end: 10:30
                    :speaker: Catherine Rivière, GENCI
                    :support: attachments/spip/Documents/Journees/sept2010/CatherineRiviere.pdf
  
                .. event:: Point sur l’activité 2009 du CSCI : rapport et perspectives
                    :begin: 10:30
                    :end: 10:45
                    :speaker: Laurent Desbat, ministère
                    :support: attachments/spip/Documents/Journees/sept2010/LaurentDesbat.pdf
  
                .. event:: Le laboratoire commun INRIA/CERFACS
                    :begin: 10:45
                    :end: 11:15
                    :speaker: Jean Roman, INRIA
                    :support: attachments/spip/Documents/Journees/sept2010/JeanRoman.pdf
                 
                .. event:: Le mésocentre de l'ECP
                    :begin: 11:15
                    :end: 11:30
                    :speaker: Marc Massot, ECP
                    :support: attachments/spip/Documents/Journees/sept2010/MarcMassot.pdf
                 
                .. event:: Groupe de Travail Infrastructures
                    :begin: 11:30
                    :end: 12:00
                    :speaker: Françoise Berthoud, Olivier Brand Foissac, Romaric David, Calcul
                    :support: attachments/spip/Documents/Journees/sept2010/FrancoiseBerthoud.pdf

                .. event:: Les maisons de la simulation
                    :begin: 14:00
                    :end: 15:00
                    :speaker: Inconnu

                .. event:: Table ronde : Présentation du comité de coordination des mésocentres, interactions avec les structures d'animation
                    :begin: 15:00
                    :end: 16:00
                    :speaker: Inconnu

