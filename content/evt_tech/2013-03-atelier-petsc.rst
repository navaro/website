Atelier initiation à PETSc
##########################

:date: 2014-11-05 15:53:49
:category: formation
:start_date: 2013-03-18
:end_date: 2013-03-20
:place: Maison de la Simulation, Saclay
:tags: petsc
:summary: Atelier d'initiation à la librairie PETSc

.. contents::

.. section:: Description
    :class: description

    Le Groupe Calcul et la Maison de la Simulation ont proposé, dans le cadre du projet Equip@meso, une formation d’initiation à la librairie PETSc du lundi 18 mars au mercredi 20 mars 2013. Cette formation aura lieu à la Maison de la Simulation. Le programme complet est disponible `ici <http://www.maisondelasimulation.fr/Phocea/Page/index.php?id=83>`__.

    Les intervenants sont Jérémy Foulon, Loïc Gouarin et Serge Van Criekingen.

    L'organisation de cette formation, en collaboration avec le réseau LyonCalcul est à nouveau prévue (par les mêmes intervenants) sur le campus de la Doua, à Villeurbanne/Lyon.

.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 18-03-2013

                .. event:: Introduction
                    :begin: 09:30
                    :end: 12:00
                    :speaker: Loïc Gouarin
                    :support:
                        [Cours](attachments/spip/IMG/pdf/introduction.pdf)
                        [Exercices](attachments/spip/IMG/tgz/myProject.tgz)
                        [Corrections](attachments/spip/IMG/tgz/Correction_Introduction.tgz)

                .. event:: Les vecteurs
                    :begin: 14:00
                    :end: 17:30
                    :speaker: Serge Van Criekingen
                    :support:
                        [Cours](attachments/spip/IMG/pdf/vectors.pdf)
                        [Corrections](attachments/spip/IMG/tgz/Corrections_Vecteur.tgz)


            .. day:: 19-03-2013

                .. event:: Les matrices
                    :begin: 09:00
                    :end: 17:30
                    :speaker: Jérémy Foulon
                    :support:
                        [Cours 1](attachments/spip/IMG/pdf/matrix_lyon.pdf)
                        [Cours 2](attachments/spip/IMG/pdf/matrixFree.pdf)
                        [Exercices](attachments/spip/IMG/tgz/EF.tgz)


            .. day:: 20-03-2013

                .. event:: Les solveurs
                    :begin: 09:00
                    :end: 12:00
                    :speaker: Serge Van Criekingen
                    :support:
                        [Cours](attachments/spip/IMG/pdf/petscSolvers.pdf)


                .. event:: Les DMDA
                    :begin: 14:00
                    :end: 16:00
                    :speaker: Loïc Gouarin
                    :support:
                        [Cours](attachments/spip/IMG/pdf/dmda.pdf)


                .. event:: Les performances
                    :begin: 16:00
                    :end: 17:30
                    :speaker: Loïc Gouarin
                    :support:
                        [Cours](attachments/spip/IMG/pdf/Performance.pdf)
                        [Exercices](attachments/spip/IMG/tgz/exercices_Perf.tgz)


.. section:: Intervenants
    :class: orga

    - Jérémy Foulon (UPMC Paris 6, Institut du Calcul et de la Simulation)
    - Loïc Gouarin (CNRS et Laboratoire de Mathématiques d'Orsay)
    - Serge Van Criekingen (Maison de la Simulation)
