4ème Journée Mésocentres
##########################

:date: 2011-06-16 10:02:35
:category: journee
:authors: Romaric David
:place: Paris
:start_date: 2011-09-20
:end_date: 2011-09-21

.. contents::

.. section:: Introduction
  :class: description

  Calcul, GENCI et la CPU ont organisé les 4èmes journées mésocentres les 20 et 21 septembre 2011 à Paris à l'Institut de Physique du Globe.  Ces journée étaient décomposées en 2 parties :

  - le 20/9/2011, exemples de développements d'applications réalisées par les mésocentres pour leurs communautés d'utilisateurs.
  - le 21/9/2011, événements ayant marqués l'année 2011 pour ces centres de calcul de proximité et pour le calcul intensif de façon plus générale.

  Cette journée a été diffusée sur le web à l'adresse http://webcast.in2p3.fr/live/journee_mesocentres. Les vidéos des présentations sont en ligne.

  Ces journées font suite à trois premières journées :

    - `le 21 septembre 2010 <2010-09-journee-mesocentres.html>`_ et une `demi journée du 10/6/10 sur les outils des mésocentres <2010-06-outils-mesocentres.html>`_.
    - `le 24 septembre 2009 <2009-05-journee-mesocentres.html>`_
    - `le 13 février 2008 <2008-02-journee-mesocentres.html>`_

.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 20-09-2011

                .. event:: les différents modèles de participation d'un méso aux activités de recherche 
                    :begin: 09:00
                    :end: 09:30
                    :speaker: Nicolas Renon, Calmip 
                    :support: attachments/spip/Documents/Journees/sept2011/renon_2011.pdf
                    
                .. event:: Cas d'un mésocentre thématique, Exemples de développements en bioinformatique, le mésocentre comme lieu de rencontres entre chercheurs 
                    :begin: 09:30
                    :end: 10:00
                    :speaker: Olivier Collin, Genouest 
                    :support: attachments/spip/Documents/Journees/sept2011/collin_2011.pdf
                    
                .. event:: Présentation des développements d'applications réalisés par des partenaires du privé 
                    :begin: 10:00
                    :end: 10:30
                    :speaker: Anne Laurent, Montpellier 
                    :support: attachments/spip/Documents/Journees/sept2011/laurent_2011.pdf
                    
                .. event:: Parallélisation d'application pour tous 
                    :begin: 10:30
                    :end: 11:00
                    :speaker: Pierre Gay, Mésocentre de Calcul Intensif Aquitain 
                    :support: attachments/spip/Documents/Journees/sept2011/gay_2011.pdf
                    
                .. event:: optimisations et apports scientifiques d'un pool condor 
                    :begin: 11:00
                    :end: 11:30
                    :speaker: Yann Costes, Cergy 
                    :support: attachments/spip/Documents/Journees/sept2011/costes_2011.pdf
                    
                .. event:: Parallélisation et couplage de codes, une collaboration au long cours 
                    :begin: 11:30
                    :end: 12:00
                    :speaker: Tina Odaka, PCIM, Ifremer, Brest 
                    :support: attachments/spip/Documents/Journees/sept2011/odaka_2011.pdf
                    
                .. event:: Support avancé pour les entreprises 
                    :begin: 12:00
                    :end: 12:30
                    :speaker: Patrick Bousquet-Mélou, Crihan 
                    :support: attachments/spip/Documents/Journees/sept2011/bousquet_2011.pdf

            .. day:: 21-09-2011

                .. event:: Intervention de la CPU
                    :begin: 09:00
                    :end: 09:30
                    :speaker: Daniel Egret, CPU
                        
                .. event:: Relations de Genci avec les Mésocentres 
                    :begin: 09:30
                    :end: 10:00
                    :speaker: Catherine Rivière, GENCI  
                    :support: attachments/spip/Documents/Journees/sept2011/genci_2011.pdf
                    
                .. event:: Bilan de l'activité 2010-2011 
                    :begin: 10:00
                    :end: 10:30
                    :speaker: Mark Asch, Emmanuel Chaljub, Romaric David, Comité de Coordination des mésocentres  
                    :support: attachments/spip/Documents/Journees/sept2011/ccm_2011.pdf
                    
                .. event:: Le projet VERCE et l'évolution du mésocentre de l'IPGP 
                    :begin: 10:30
                    :end: 11:00
                    :speaker: Jean-Pierre Vilotte, IPGP, Paris 
                    :support: attachments/spip/Documents/Journees/sept2011/vilotte_2011.pdf
                    
                .. event:: Toxicité des nanostructures carbonées et Vectorisation des médicaments 
                    :begin: 11:00
                    :end: 11:30
                    :speaker: Fabien Picaud, Méso-Comté, Besançon 
                    :support: attachments/spip/Documents/Journees/sept2011/picaud_2011.pdf
                    
                .. event:: Rôle essentiel des mésocentres dans la pyramide HPC 
                    :begin: 11:30
                    :end: 12:00
                    :speaker: Laurent Desbat, MESR 
                    :support: attachments/spip/Documents/Journees/sept2011/desbat_2011.pdf
                    
                .. event:: visualisation de tores plats et de sphères de Nash-Kuiper 
                    :begin: 12:00
                    :end: 12:30
                    :speaker: Francis Lazarus, UJF 
                    :support: attachments/spip/Documents/Journees/sept2011/lazarus_2011.pdf
                    
                .. event:: Activités du COCIN 
                    :begin: 12:30
                    :end: 13:00
                    :speaker: Michel Daydé, CNRS 
                    :support: attachments/spip/Documents/Journees/sept2011/dayde_2011.pdf
                    
                .. event:: Recommandations émises en 2011 
                    :begin: 13:00
                    :end: 13:30
                    :speaker: Olivier Pironneau, CSCI 
                    :support: attachments/spip/Documents/Journees/sept2011/pironneau_2011.pdf
                    
                .. event:: Étude du devenir et de l'impact des composés halogénés émis par les volcans  à l'aide d'un modèle atmosphérique méso-échelle 
                    :begin: 13:30
                    :end: 14:00
                    :speaker: Line Jourdain, LPCEE, CCSC, Orléans 
                    :support: attachments/spip/Documents/Journees/sept2011/jourdain_2011.pdf
                    
                .. event:: Régionalisation dynamique des conditions de vague dans le Golfe de Gascogne pour différents scénarios futurs d’émission de gaz à effet de serre 
                    :begin: 14:00
                    :end: 14:30
                    :speaker: Élodie Charles,  BRGM, CCSC, Orléans 
                    :support: attachments/spip/Documents/Journees/sept2011/charles_2011.pdf
                    
                .. event:: Animation Scientifique du Groupe Calcul 
                    :begin: 14:30
                    :end: 15:00
                    :speaker: Romaric David, Groupe Calcul 
                    :support: attachments/spip/Documents/Journees/sept2011/calcul_2011.pdf
                    



