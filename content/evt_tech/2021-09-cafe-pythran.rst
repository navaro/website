Pythran
#######

:date: 2021-09-24 10:00:00
:category: cafe
:tags: visio
:start_date: 2021-09-24 10:00:00
:end_date: 2021-09-24 11:00:00
:place: En ligne
:summary: Pythran est un compilateur pour noyaux scientifiques de haut (ou bas) niveau écrits en Python. Basiquement, il prend en entrée du code Python dont les points d'entrées ont reçu quelques commentaires pour l'inférence de type, et il le transforme en code natif, enlevant l'étape d'interprétation après avoir appliqué plusieurs optimisations de haut niveau.
:inscription_link: https://indico.mathrice.fr/event/262
:calendar_link: https://indico.mathrice.fr/export/event/262.ics

.. contents::

.. section:: Description
    :class: description

    Serge Guelton présente Pythran dans ce Café.

    Pythran est un compilateur pour noyaux scientifiques de haut (ou bas) niveau écrits en Python. Basiquement, il prend en entrée du code Python dont les points d'entrées ont reçu quelques commentaires pour l'inférence de type, et il le transforme en code natif, enlevant l'étape d'interprétation après avoir appliqué plusieurs optimisations de haut niveau.  
 
    Pythran peut être utilisé dans un notebook, à travers distutils ou en ligne de commande. Il est utilisé dans le projet Scipy pour accélérer certains noyaux de calcul.  
 
    Cette présentation explicite les différents cas d'usage, le système d'annotation de Pythran, le support de la vectorisation et le support d'OpenMP.  
 
    
.. button:: Vidéo sur canal-u.tv
    :target: https://www.canal-u.tv/video/groupe_calcul/pythran.63669

.. section:: Orateur
    :class: orateur

    - Serge Guelton (RedHat)
    
