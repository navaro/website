Mini-symposium "Architectures émergentes pour |_| le |_| calcul", SMAI 2009
###########################################################################

:date: 2009-06-08 10:04:12
:start_date: 2009-05-25
:end_date: 2009-05-29
:category: journee
:place: La Colle sur Loup, Alpes Maritimes
:tags: architectures
:summary: Mini-symposium lors du congrès SMAI 2009 sur les nouvelles architectures de calcul.

.. contents::

.. section:: Description
    :class: description

    Depuis de nombreuses années, la plupart des supercalculateurs sont des clusters, regroupant des centaines de processeurs classiques, mis en commun. L'augmentation des performances de ces machines basée sur l'augmentation de la puissance des processeurs a atteint ses limites, la fréquence de fonctionnement ne pouvant grimper de façon continue. Cependant, les problèmes à traiter, eux, ne cessent de croître en complexité et en taille.

    Par ailleurs, la multiplication des processeurs standards dans les calculateurs rencontre d’autres limites physiques : celle de la consommation énergétique et de l'échauffement des composants. D'autres voies se dessinent pour surmonter ces limites, qui nous éloignent du modèle architectural classique, et donc des paradigmes de programmation actuels.

    Le but de ce mini-symposium est de faire un tour non exhaustif de ces nouvelles architectures et de leurs spécificités en terme de programmation :

    Ce mini-symposium a été organisé par Violaine Louvet (Institut Camille Jordan, Université Lyon 1 & CNRS).


.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 25-05-2009

                .. event:: Architectures massivement parallèles et en particulier la Blue Gene/P 
                    :begin: 09:30
                    :end: 18:30
                    :speaker: Philippe Wautelet, IDRIS
                    :support: attachments/spip/Documents/Manifestations/SMAI2009/Philippe.Wautelet.pdf
            .. day:: 26-05-2009

                .. event:: Architectures massivement parallèles et en particulier la Blue Gene/P 
                    :begin: 09:30
                    :end: 18:30
                    :speaker: Philippe Wautelet, IDRIS
                    :support: attachments/spip/Documents/Manifestations/SMAI2009/Philippe.Wautelet.pdf

            .. day:: 27-05-2009

                .. event:: Programmation sur processeur de type GPU
                    :begin: 09:30
                    :end: 18:30
                    :speaker: Dominique Houzet, INPG
               
            .. day:: 28-05-2009

                .. event:: Parallélisation sur grille de calcul
                    :begin: 09:30
                    :end: 18:30
                    :speaker: Vincent Danjean, LIG
                    :support: attachments/spip/Documents/Manifestations/SMAI2009/kaapi.pdf

            .. day:: 29-05-2009

                .. event:: Parallélisation sur grille de calcul
                    :begin: 09:30
                    :end: 18:30
                    :speaker: Vincent Danjean, LIG
                    :support: attachments/spip/Documents/Manifestations/SMAI2009/kaapi.pdf

               
               
