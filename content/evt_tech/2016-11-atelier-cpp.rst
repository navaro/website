Atelier C++ : les bases du 11 et du 14
######################################

:date: 2016-09-21 14:19:41
:start_date: 2016-11-29
:end_date: 2016-12-01
:category: formation
:place: Rennes, France
:summary: Atelier sur les bases de la programmation C++11 et 14 Ã  Rennes par Joel Falcou du 29/11/2016 au 01/12/2016.


.. contents::

.. section:: Description
    :class: description

    Après les deux ateliers de `Paris <http://calcul.math.cnrs.fr/spip.php?article254>`_ et `Besançon <http://calcul.math.cnrs.fr/spip.php?article261>`_, le groupe Calcul, en partenariat avec l'institut de recherche mathématique de Rennes et le service expérimentation et développement de l'INRIA Rennes-Bretagne Atlantique, propose un atelier pour débutant en programmation C++11 et 14 du 29 novembre au 1er décembre 2016 à Rennes.

    L’intervenant principal est `Joël Falcou <https://www.lri.fr/~falcou/>`__ secondé pour les TP par Anthony Baire et Pierre Navaro.

    Le prérequis pour ce cours est de maîtriser le langage C et d'avoir des notions de programmation objet.

    Les inscriptions sont closes.


    **Lieu**

    `IRMAR <https://irmar.univ-rennes1.fr/se-rendre-lirmar#section-2>`__ sur le campus de Beaulieu à Rennes.


    **Matériel - Logiciels**

    Les participants devront se munir d’un ordinateur portable.

    - Sous linux ou mac : l’installation de g++ 4.8+ ou clang 3.4+ est nécessaire (et suffisante).
    - sous windows il faudra installer MSVC 2015


    **Supports de cours et TP**

    Les supports de cours et de TP peuvent être téléchargés `ici <attachments/spip/IMG/zip/formation_base_cpp.zip>`_


.. section:: Références
    :class: description

    - Sur le web :

        - http://fr.cppreference.com/w/
        - http://en.cppreference.com/w/
        - http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines
        - https://isocpp.org/faq

    - Livres :

        - "Programming : Principles and Practice Using C++" - Bjarne Stroustrup (2014)
        - "Effective C++", Scott Meyers (2014)
        - "C++ Concurrency", Anthony Williams (2012)
        - "Accelerated c++", Andrew Koenig (2000)


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 29-11-2016

            .. event:: Introduction
                :begin: 09:00
                :end: 10:00

                - Historique du langage
                - Pourquoi C++ ?
                - Normes et évolutions

            .. event:: Eléments fondamentaux
                :begin: 10:00
                :end: 17:30

                - main()
                - Processus de compilation
                - Types et variables
                - Structures de contrôle
                - Fonctions, surcharge et générosité
                - Gestion des erreurs

        .. day:: 30-11-2016

            .. event:: Structure de Données Abstraites
                :begin: 09:00
                :end: 12:00

                - Structures et autres types composites
                - Types paramétriques
                - Principe de la RAII

            .. event:: La bibliothèque standard
                :begin: 14:00
                :end: 17:30

                - Conteneurs
                - Algorithme
                - Pointeurs à sémantique riche
                - Date et heure

        .. day:: 01-12-2016

            .. event:: Programmation orientée objets
                :begin: 09:00
                :end: 17:30

                - Notion d’interface
                - Polymorphisme statique
                - Polymorphisme dynamique
                - Principes de substitution de Liskov
