10èmes journées mésocentres
############################

:date: 2017-05-10 07:34:33
:category: journee
:authors: Bernard Dussoubs
:start_date: 2017-09-26
:end_date: 2017-09-27
:place: IHP, Paris

.. contents::

.. section:: Description
   :class: description

   Les 10èmes journées mésocentres auront lieu les 26 et 27/09/2017 à l'`Institut Henri Poincaré <http://www.ihp.fr>`__ à Paris.

   Au programme cette année, vous trouverez entre autres des présentations techniques, des ateliers, des débats, et des événements centrés sur les 10 ans de ces journées.

   Les inscriptions sont **désormais closes !**  Merci pour votre intérêt !
   `Liste des inscrits <#participants>`__ (104 inscrits - mise à jour le 18 septembre 2017)

   **Organisation**

    `Contact pour les journées : organisateurs 2017 <journees_mesos2017@groupes.renater.fr>`__

    Ces journées seront suivies, le 28 septembre 2017, même lieu, par la **5è journée MesoChallenge**  organisée par le projet Equip@Meso (contact : `Michel Kern <michel.kern@inria.fr>`_).

    `Annonce  <http://www.genci.fr/fr/node/856>`__ et `programme <http://www.genci.fr/sites/default/files/Programme%20journ%C3%A9e%20m%C3%A9sochallenges%202017_0.pdf>`__.

.. section:: Programme
   :class: programme

   .. schedule::

      .. day:: 26-09-2017

         .. event:: Accueil
            :begin: 9:30
            :end: 10:00

         .. event:: Estimation du coût de l’externalisation par Polytechnique de ses serveurs informatique dans les datacentres  de la Société du Grand paris.
            :support: [Présentation](attachments/spip/IMG/pptx/p.hennion-journeesmesocentre2017rmqgpinson.pptx)
            :speaker: Pascale Hennion, Polytechnique
            :begin: 10:00
            :end: 10:30

            La Société du Grand Paris se propose de profiter des travaux de construction du métro du Grand Paris Express ( GPE )  pour irriguer tous les territoires qu'il traverse de fibres optiques et d'une vingtaine de datacenters. 2 ou 3 de ces datacenters seront construits sur le territoire de l'UPsay.
            L'Upsay a souhaité savoir s'il est pertinent d'utiliser ces datacentres.
            Pour le GPE, il faut que le projet ait une  rentabilité financière ou du moins un gain socio-économique.
            Pour l'UPsay, il faut que le projet ne revienne pas plus cher que la construction de son propre datacentre. Pour étudier la rentabilité du projet, la salle informatique de l'école polytechnique a servi  de référence. Les résultats seront donnés dans cet exposé.

         .. event:: EXPLOR: Un Ensemble de Calcul Scientifique pour la Lorraine.
            :support: [Présentation](attachments/spip/IMG/pdf/jmeso20170925.pdf)
            :speaker: Fabien Pascale, Université de Lorraine
            :begin: 10:30
            :end: 11:00

            EXPLOR est le mésocentre de l'Université de Lorraine. Il vise à renforcer l'offre de simulation et de calcul en Lorraine par une mise en réseau des moyens de calcul existant, leur renforcement et l'accompagnement des chercheurs et entreprises à leur utilisation. EXPLOR fait partie des projets soutenus financièrement par l'Etat et la (ex-)Région Lorraine.
            Nous présentons ici l'infrastructure matérielle et logicielle qui a été mise en place au premier semestre 2017 ainsi que son évolution prévue: matériels de calcul, de stockage, infrastructure réseau, machines virtuelles, système d'administration, etc. A travers l'ensemble des technologies mises en œuvre pour assurer aux utilisateurs une performance optimale des machines HPC, nous illustrons la différence entre la vue utilisateur (connexion, bureau virtuel, stockage(s), ordonnanceur, calcul) et la vue administrateur.

         .. event:: Démystification du Deep Learning
            :support: [Présentation](attachments/spip/IMG/pdf/deeplearningdemystified26sept2017nvidia.pdf)
            :speaker: Guillaume Barat, Frédéric Pariente, Nvidia
            :begin: 11:00
            :end: 11:30

         .. event:: Deep-learning sur le calculateur Myria, retour d’expérience sur la mise en place des outils et sur l’accompagnement de cette nouvelle communauté d’utilisateurs
            :support: [Présentation](attachments/spip/IMG/pdf/benoistgaston-journeesmesocentres2017.pdf)
            :speaker: Benoist Gaston, CRIANN
            :begin: 11:30
            :end: 12:00

         .. event:: Buffet (offert par la société Nvidia)
            :begin: 12:00
            :end: 13:30

         .. event:: Container Singularity.
            :speaker: Laurent Philippe, Université de Besançon
            :support: [Présentation](attachments/spip/IMG/pdf/presentation.pdf)
            :begin: 13:30
            :end: 14:00

            La compilation et l'installation de logiciels dans les mésocentres est parfois une tâche difficile du fait des dépendances nécessaires au bon fonctionnement de ces logiciels. D'une part, pour privilégier la stabilité de nos plateformes, nous utilisons des distributions ayant fait leur preuves mais qui ne disposent pas des versions les bibliothèques. D'autre part certains logiciels utilisent tant de dépendances que leur installation à chaque mise à jour relève du casse-tête. Pour résoudre ce problème nous avons expérimenté, puis mis en exploitation, la technologie de containers. Les containers permettent de créer un environnement logiciel, comprenant toutes les dépendances, qui s'exécute sur le système natif. Nous présenterons l'utilisation des container Singularity, un système adapté au HPC, dans notre mésocentre. Ce système permet aux utilisateurs de créer eux-mêmes leur propres containers. Les principaux avantages obtenus sont la mobilité et la reproductibilité des calculs sans perte de sécurité.

         .. event:: NIX comme environnement de calcul à haute performance
            :speaker: Laure Tavard et Bruno Bzeznik, Université Grenoble Alpes
            :support:
                [Présentation](attachments/spip/IMG/pdf/nixintromesocentres2017.pdf)
                [Tutoriel](https://gricad.github.io/calcul/nix/tuto/2017/07/04/nix-tutorial.html)
            :begin: 14:00
            :end: 15:00

            NIX (http://nixos.org/nix) est un gestionnaire de paquets original qui
            peut s'utiliser au dessus de n'importe quelle distribution Linux ou
            MacOS. Il permet aux utilisateurs de configurer eux-même leur
            environnement et de développer des paquets sans dépendances avec le
            système de base, garantissant ainsi une reproductibilité et une
            portabilité exceptionnelles.
            Coté Administrateur système : Aujourd'hui nous évoluons avec des
            machines hétérogènes. Un des souhaits des administrateurs systèmes des
            centres de calculs est de s'assurer que les codes des utilisateurs
            peuvent tourner sur leurs machines et que,lors de la mise Ã  jour de
            librairies,codes,…, l'environnement reste stable pour que les codes
            continuent de tourner.
            Coté utilisateurs : NIX est l'espoir de pouvoir porter son code sur les
            différentes plateformes de la pyramide du calcul sans ce soucier des
            dépendances.
            Nous vous présenterons le gestionnaire de paquet Nix et vous proposerons
            une démonstration de mise en pratique.


         .. event:: Projet de formation 2018. Construction collective du programme.
            :speaker: Arnaud Renard, Université de Reims
            :support: [Présentation](attachments/spip/IMG/pdf/20170926-hpc_user_support_tools.pdf)
            :begin: 15:00
            :end: 15:30

         .. event:: Pause
            :begin: 15:30
            :end: 16:00

         .. event:: "Elapsed Time" reste-t-il un critère pertinent de refacturation ? Retour sur quelques années de métrologie.
            :speaker: Emmanuel Quemener, ENS Lyon
            :support: [Présentation](attachments/spip/IMG/pdf/mesocentres2017_eq.pdf)
            :begin: 16:00
            :end: 16:30

            Une accumulation de temps écoulés n'est pas plus une consommation de
            ressources qu'un tas de pierres n'est une maison !" Dans nos centres de
            ressources, l'essentiel de la métrologie (et de la facturation) repose
            souvent sur le "temps écoulé", parfois le "temps utilisateur". L'arrivée
            de traitements de données massives issus notamment de communautés de
            biologie désormais dessine un nouveau paradigme : le principal "stress"
            de nos infrastructures ne repose plus uniquement sur la sollicitation
            des unités de traitements mais dans le transport des données, tous les
            transports. Nous verrons, au travers d'une fouille menée sur des
            fichiers de métrologie du CBP et du PSMN, qu'il convient désormais de
            prendre en compte d'autres métriques pour évaluer au mieux ces nouveaux
            usages, et donc permettre une meilleure reventilation de leurs coûts.

         .. event:: Nouveaux modes d'accès au mésocentre
            :speaker: David Brusson, Université de Strasbourg
            :support: [Présentation](attachments/spip/IMG/pdf/hpc_journee_meso.pdf)
            :begin: 16:30
            :end: 17:00

         .. event:: Complexité des technologies et impact sur les applications et architectures
            :speaker: Marc Mendez-Bermond, DELL Technologies
            :support: [Présentation](attachments/spip/IMG/pdf/dellemc_-_marc_mendezbermond_-_technologies_and_application_performance_-_v3.pdf)
            :begin: 17:00
            :end: 17:30

            Les technologies évoluent à grande vitesse afin d’atteindre les cibles d’efficacité, de performance et de densité que nos grands défis scientifiques et techniques appellent. Cette présentation vise premièrement à évaluer combien les nouvelles technologies peuvent répondre à nos attentes. Dans un second temps, l’enjeu d’une adaptation continue de nos applications et de nos architectures est établi afin de bénéficier des dernières spécifications et de voir converger les résultats obtenus avec les attentes. La conclusion pose quelques directions pour des collaborations étroites avec Dell Technologies.

         .. event:: Cheminement vers l'IPGP.
            :support: [Comment s'y rendre](attachments/spip/IMG/png/ihp-ipgp.png)
            :begin: 17:30
            :end: 18:00

         .. event:: Apéritif des "10 ans", conclusion de la journée (délocalisé à l'IPGP) (offert par la société DELL)
            :begin: 18:00
            :end: 20:00


      .. day:: 27-09-2017

         .. event:: Intervention de Genci
            :speaker: Marie-Hélène Vouette
            :support: [Présentation](attachments/spip/IMG/pptx/presentation-genci-25-09-2017.pptx)
            :begin: 09:00
            :end: 09:30

         .. event:: Intervention du Ministère
            :speaker: Laurent Crouzet
            :support: [Présentation](attachments/spip/IMG/pdf/2017_journe_es_me_socentres_26_septembre_dgri_.pdf)
            :begin: 09:30
            :end: 10:00

         .. event:: Intervention du CNRS/COCIN
            :speaker: Denis Veynante
            :support: [Présentation](attachments/spip/IMG/pdf/journe_es_me_socentres_-_2017_dv.pdf)
            :begin: 10:00
            :end: 10:30

         .. event:: Pause
            :begin: 10:30
            :end: 11:00

         .. event:: Intervention de l'INSERM (annulé)
            :speaker: Isabelle Perseil
            :begin: 11:00
            :end: 11:30

         .. event:: Présentation de l'UMS Gricad
            :speaker: Violaine Louvet
            :support: [Présentation](attachments/spip/IMG/pdf/meso2017_gricad.pdf)
            :begin: 11:30
            :end: 12:00

         .. event:: Présentation du réseau MSO (Modélisation, Simulation, Optimisation)
            :speaker: Stéphane Cordier
            :support: [Présentation](attachments/spip/IMG/pdf/170926_slides_pre_sentation_amies_2017.pdf)
            :begin: 12:00
            :end: 12:20

         .. event:: Buffet
            :begin: 12:20
            :end: 14:00

         .. event:: L'aventure "mésocentres"
            :speaker: Françoise Berthoud, Violaine Louvet
            :support: [Présentation](attachments/spip/IMG/pdf/aventuremeso_v1.1.pdf)
            :begin: 14:00
            :end: 15:15

            L'aventure "mésocentres", ce sont des hommes et des femmes qui se sont impliqués, investis, passionnés dans la co-construction d'un modèle qui aujourd'hui fait ses preuves. A l'image de cette aventure partagée, cet exposé sera le résultat d'une élaboration commune, ponctué de faits marquants et d’anecdotes, retraçant les grandes étapes de ce qui a été tissé au fil de ces dix dernières années.

         .. event:: Pause
            :begin: 15:15
            :end: 15:45

         .. event:: Table Ronde : Infrastructure de Recherche - Tier 2
            :begin: 15:45
            :end: 17:15

         .. event:: Conclusion
            :begin: 17:15
            :end: 17:20


