Installer et utiliser des gitlab runners
##########################################################

:date: 2021-02-18 10:00:00
:category: cafe
:tags: visio
:start_date: 2021-02-18 10:00:00
:end_date: 2021-02-18
:place: En ligne
:summary: Déploiement et utilisation de gitlab runners pour effectuer de l'intégration continue avec gitlab.
:inscription_link: https://indico.mathrice.fr/event/247
:calendar_link: https://indico.mathrice.fr/export/event/247.ics

.. contents::

.. section:: Description
    :class: description

    Baptiste Mary exposera dans ce Café comment déployer et se servir de gitlab runners pour effectuer de l'intégration continue avec gitlab.

    L'intégration continue permet d'automatiser n'importe quel processus lors de la validation d'un commit. Ces processus sont classiquement des compilations et/ou des tests.

    Dans Gitlab, la chaîne d'intégration repose sur l'exécution de scripts par des runners, des serveur dédiés à l'intégration continue. Ces runners peuvent être partagés entre différents projets, c'est-à-dire pré-configurés pour l'utilisateur, ou spécifiques à chaque projet, donc déployés par l'utilisateur. Cette session montrera comment procéder dans l'un ou l'autre cas et sera l'occasion de discuter ensemble de différentes expériences. 

    Les démonstrations seront faites sur le serveur gitlab de CY Cergy Paris Université.

    La présentation durera 30-40 minutes et sera suivie d'une séance de questions.

    Elle est accessible à tous et aura lieu sur la plateforme BBB de Mathrice. Merci de bien vouloir vous inscrire pour suivre cette session. Cette session sera enregistrée. S'inscrire implique d'accepter ce principe.

.. button:: Supports
    :target: https://indico.mathrice.fr/event/247/material/slides/0.pdf

.. button:: Vidéo sur canal-u.tv
    :target: https://www.canal-u.tv/video/groupe_calcul/installer_et_utiliser_des_gitlab_runners.59823

.. section:: Orateur
    :class: orateur

      - Baptiste Mary (Centre de Calcul, CY Cergy Paris Université)
