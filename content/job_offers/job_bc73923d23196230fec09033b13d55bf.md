Title: Poste IR en calcul scientifique
Date: 2020-06-02 09:51
Slug: job_bc73923d23196230fec09033b13d55bf
Category: job
Authors: Franck Pérignon
Email: Franck.Perignon@univ-grenoble-alpes.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Grenoble
Job_Duration: 
Job_Website: 
Job_Employer: UGA
Expiration_Date: 2020-08-25
Attachment: job_bc73923d23196230fec09033b13d55bf_attachment.pdf

Un poste d&#39;ingénieur-e de recherche en calcul scientifique est ouvert au concours ITRF 2020, avec une affectation au Laboratoire Jean Kuntzmann à Grenoble, en interaction avec GRICAD et MaiMoSiNE. 

Profil en pièce jointe.

Dossier et inscription: https://www.enseignementsup-recherche.gouv.fr/cid23693/s-inscrire-aux-concours-ingenieurs-et-personnels-techniques-de-recherche-et-de-formation-externes-et-internes-de-categorie-a.html
