Title: Post doc in numerical modeling in coastal  hydrodynamics
Date: 2021-03-23 14:22
Slug: job_35764a4b810af79cdbbeb9563234a21d
Category: job
Authors: Julien Touboul
Email: julien.touboul@mio.osupytheas.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Toulon, France
Job_Duration: 36 months
Job_Website: https://www.mio.osupytheas.fr/fr
Job_Employer: Institut Méditerranéen d&#39;Océanologie (M.I.O)
Expiration_Date: 2021-05-01
Attachment: job_35764a4b810af79cdbbeb9563234a21d_attachment.pdf

**Context:**
The MORHOC’H 2 research project is a follow-up to the ANR ASTRID MORHOC’H project, which focused on studying processes that have a significant impact on wave propagation in the presence of a vertically sheared current. Indeed, it is not uncommon to encounter such currents in  coastal  areas,  since  the  combined  effects  of  wind  and  bathymetry  can  profoundly  modify  their vertical profile. However, the modelling, both physical and numerical, of such areas is a strategic challenge, both in the civilian field (coastal security, renewable marine energy, etc.) and in the military field (landing, rescue, naval applications).

Two  outcomes,  resulting  from  the  project,  are  the  focus  of  our  attention  here.  Thus,  the  MORHOC&#39;H  project  lead  us  developing  a  particularly  robust  experimental  device  aimed  at  experimentally  controlling  the  vertical  structure  of  a  hydrodynamic  channel  current.  In  addition,  a  new  model  has  been  developed,  named  CMS,  to  extend  the  scope  of  wave  propagation  models  with  resolved  phase  to  take  into  consideration  configurations  involving  high current vorticity.

The  MORHOC&#39;H  2  project  therefore  aims  to  increase  the  degree  of  maturity  of  these  two  results, bringing them closer to use in real conditions. First, the current profile control device will  be  extended  to  larger  configurations  and  will  become  applicable  in  three-dimensional basins. Many industrial actors, operators of hydrodynamic test tanks, will then have access to it.  In  addition,  the  CMS  propagation  model  will  be  coupled  with  the  Community  coastal  hydrodynamic  circulation  code  CROCO,  developed  by  SHOM,  IRD,  CNRS,  IFREMER,  and  INRIA, in order to make it usable under realistic conditions. Thus, the improvements resulting from the initial project will become accessible to realistic environments. The two approaches in coastal modelling, physical and numerical, will thus become more efficient and will be able to describe more realistic situations.

**Activities:**
The successful candidate will oversee the development of the numerical model, implementing a robust coupling between the CMS propagation model and the circulation model CROCO. Of course,  an  important  part  of  the  activities  will  consist  in  programming.  Also,  the  successful  candidate will participate in the project follow-up, and have strong interactions with the other teams involved in the project. 

**Requirements for application:**
Applicants  should  have  a  strong  background  in  marine  and  coastal  hydrodynamics,  with  a  special interest in theoretical and numerical modelling. Strong skills in high performance and parallel computing will also be necessary.The  position  is  opened  at  a  postdoctoral  level,  and  the  applicants  should  own  a  PhD  in  any  related  field.  Besides,  the  project  is  funded  by  the  French  “innovation  and  defense  agency”  (AID –  DGA).  Thus,  applications will be restricted to candidates owning  the citizenship of a European Union country only.
