Title: Génération automatique de code pour l’algèbre linéaire rapide sur GPU (ONERA et Google AI Paris)
Date: 2021-06-09 11:38
Slug: job_49a6db7cd96e23e634401e01ac4e0065
Category: job
Authors: Denis Gueyffier
Email: denis.gueyffier@onera.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: ONERA (Châtillon) et Google AI (Paris St Lazare)
Job_Duration: 12 à 18 mois 
Job_Website: http://www.onera.fr
Job_Employer: ONERA et Google AI Paris
Expiration_Date: 2021-08-01
Attachment: 

**Présentation du projet post-doctoral, contexte et objectif** 

L&#39;ONERA travaille à la génération automatique de code pour un futur logiciel de simulation en mécanique des fluides [1] nommé SoNICS. Le but de ces travaux est en particulier de fournir des paradigmes de programmation de haut niveau évitant au développeur de réaliser lui-même les optimisations nécessaires pour obtenir des performances HPC élevées. L&#39;ONERA cherche aussi à assurer automatiquement la portabilité vers différents langages et différentes architectures matérielles actuelles (CPU multi-coeurs, GPU, Vector Engines) et futures.
Par ailleurs, Google AI travaille à une approche générique pour la génération de code qui consiste à utiliser une approche hiérarchique à plusieurs niveaux de représentation (&#34;intermediate representation&#34;) [2]. L&#39;une des ambitions de cette approche est d&#39;obtenir un code optimal pour l&#39;exécution sur différents types d’architectures matérielles.
Ce travail en collaboration entre l&#39;ONERA et Google AI, visera à la résolution rapide de problèmes d&#39;algèbre linéaire issus des équations de la mécanique des fluides en écoulements compressibles discrétisées sur des maillages structurés. Nous chercherons à générer automatiquement un code optimisé pour l&#39;exécution sur GPU. Pour cela nous mettrons en place et étendrons potentiellement le formalisme du dialecte LinALG du framework MLIR [3, 4] (faisant partie de la LLVM) sur des problèmes d&#39;algèbre linéaire de difficulté croissante. Pour guider la génération automatique ultérieure, nous devrons réaliser une analyse des goulots d&#39;étranglement à chaque niveau de parallélisme (warps, blocs, threads, vectorisation). Nous étudierons les décisions d&#39;optimisation qui peuvent être prises pour générer du code, les choix des différents niveaux de parallélisme, les choix d’ordonnancement des boucles, les choix de tuilage (cache-blocking) ou de vectorisation de boucles. Après l&#39;étude de cas simples, nous étendrons l&#39;approche à la résolution par méthode de Gauss-Seidel (itération de l&#39;application à un vecteur d&#39;une décomposition LU de la matrice initiale), dans une approche de type wavefront [5] qui sera adaptée aux spécificités des GPU et aux autres architectures.
Des travaux seront aussi menés concernant la différentiation algorithmique à un niveau à determiner (haut, intermédiaire ou bas niveau) pour dériver en mode direct ou reverse le graphe des opérateurs constituant le futur logiciel SoNICS, ce qui servira ici à constituer le système à résoudre par les méthodes d&#39;algèbre linéaire. 

**Profil et compétences recherchées**

Docteur en sciences dans le domaine de la simulation numérique ou des langages de programmation/compilation,
Expérience de la programmation parallèle en mémoire partagée et distribuée sur CPU multi-cœurs ou GPU,
Compétences en algèbre linéaire numérique fortement appréciées. 

**La rémunération est constituée d&#39;un salaire et d&#39;une prime - des séjours alternés entre l&#39;ONERA de Châtillon et Google AI (Paris St Lazare) sont prévus - début du contrat septembre ou octobre 2021**

[1] B. Maugars, S. Bourasseau, C. Content , B. Michel, B. Berthoul, P. Raud, L. Hascoët, Algorithmic Differentiation for an efficient CFD solver, soumis à Journal of Computational Physics
[2] <https://pliss2019.github.io/albert_cohen_slides.pdf>
[3] <https://www.tensorflow.org/mlir>
[4] Ulysse Beaugnon, Basile Clément, Nicolas Tollenaere, Albert Cohen, On the Representation of GEN-F213-5 (GEN-SCI-034) Partially Specified Implementations and its Application to the Optimization of Linear Algebra Kernels on GPU, ArXiv <https://arxiv.org/pdf/1904.03383>
[5] R. Barrett, M.W. Berry, T.F. Chan, J. Demmel, J. Donato, J. Dongarra, V. Eijkhout, R. Pozo, C. Romine, H. Van der Vorst, Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods, SIAM, Jan 1, 1994 
