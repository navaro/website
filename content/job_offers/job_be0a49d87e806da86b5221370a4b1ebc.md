Title: Ingénieur en calcul haute performance H/F
Date: 2020-02-28 17:26
Slug: job_be0a49d87e806da86b5221370a4b1ebc
Category: job
Authors: Pierre FERY-FORGUES 
Email: pierre.fery-forgues@ifp.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Rueil Malmaison
Job_Duration: 
Job_Website: https://emploi.ifpen.fr/offre-de-emploi/emploi-ingenieur-en-calcul-haute-performance-h-f_260.aspx
Job_Employer: IFP Énergies Nouvelles
Expiration_Date: 2020-05-22
Attachment: 

Contexte
IFPEN est un centre de recherche traitant des problèmes scientifiques et techniques liés à la transition énergétique. Nos chercheurs ont de plus en plus recours à la simulations numérique pour étudier les phénomènes physiques rencontrés. Assurer la meilleure performance possible de ces codes de calcul sur les architectures matérielles actuelles et futures est un enjeu majeur pour nous.

Mission(s) principale(s) et activités
Le poste que nous proposons aujourd&#39;hui vise à renforcer l&#39;équipe HPC de la direction Sciences et Technologies du Numérique d&#39;IFPEN. Il comprend à la fois des aspects recherche et développement :

- d&#39;une part, vous participez à la recherche menée à IFPEN dans le domaine du HPC ;
- d&#39;autre part, en collaboration avec les chercheurs en charge des modèles physiques et numériques, vous intervenez sur les différents codes de calcul, pour en améliorer la performance sur les architectures cibles.

Critères candidat
Diplôme(s), niveau d&#39;études
Thèse en HPC.

Expérience(s) professionnelle(s) souhaitée(s) (nature, durée, précisez si débutant accepté)
Expérience de développement d&#39;importants codes de calcul parallèle.

Compétences techniques et aptitudes

- Programmation : C, C++, MPI, OpenMP, SIMD, CUDA, OpenACC.
- Analyse de performance : profileurs et outils de d&#39;analyse dans un environnement HPC.
- Architectures calcul haute performance : processeurs (x86, ARM,… ) et accélérateurs. 
- Connaissances en calcul scientifique et numérique.
- Aptitude à travailler en équipe avec des interlocuteurs d&#39;horizons différents (mécanique des fluides/solides, géosciences, chimie,...).
- Publication des travaux de recherche.
- Participation au montage de projets collaboratifs.
