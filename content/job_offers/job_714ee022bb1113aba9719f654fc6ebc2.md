Title: High performance simulation and optimization of an active device for preserving the quality of indoor air with respect to emissions from polluted soils
Date: 2020-10-26 10:07
Slug: job_714ee022bb1113aba9719f654fc6ebc2
Category: job
Authors: Cyrille Bonamy
Email: cyrille.bonamy@univ-grenoble-alpes.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Grenoble
Job_Duration: 12 mois
Job_Website: http://www.legi.grenoble-inp.fr
Job_Employer: LEGI - Grenoble INP
Expiration_Date: 2021-01-18
Attachment: job_714ee022bb1113aba9719f654fc6ebc2_attachment.pdf

Dans le cadre du projet BARIAIR, financé par l&#39;ADEME et en partenariat avec la société GINGER/BURGEAP, le LEGI recrute un post-doc de un an sur des problématiques liées à la mise en dépression de sous-bassement de batiments pour la protection de l&#39;air intérieur.