Title: Computational Scientist dans l&#39;ACH d&#39;EUROfusion
Date: 2021-09-14 07:59
Slug: job_5093a01204b3b264ebbbc88386bac6a3
Category: job
Authors: Roman HATZKY
Email: rmh@ipp.mpg.de
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Garching (Germany)
Job_Duration: until December 2025
Job_Website: https://www.ipp.mpg.de/job-368a5fd2121864152b59096a048943ab
Job_Employer: Max Planck Institut IPP
Expiration_Date: 2021-10-07
Attachment: 


Postdoctoral Physicist, Mathematician or Computer Scientist (m/f/d)

MAX-PLANCK-INSTITUT FÜR PLASMAPHYSIK, GARCHING

**TYPE OF JOB**

SCIENTIST
Particle, Plasma and Quantum Physics

Job Code: 21/060

Job offer from September 08, 2021
The Max Planck Institute for Plasma Physics as an institute of the Max Planck Society for the Advancement of Science (registered association) is the largest centre for fusion research in Europe with about 1100 employees in Garching near Munich and Greifswald. The scientists are investigating the physical principles for a fusion power plant which - like the sun - generates energy from the fusion of light atomic nuclei. The research is carried out in seven experimental and theoretical projects aimed at developing nuclear fusion into an inexhaustible and safe source of energy for the future.

The Max Planck Institute for Plasma Physics in Garching offers within the division ,Numerical Methods of Plasma Physics&#39; (NMPP) a position for a Postdoctoral Physicist, Mathematician or Computer Scientist (m/f/d). The work will be performed within the EUROfusion-project E-TASC (Theory and Advanced Simulation Coordination).

**Your tasks**

- Development and optimization of numerical and data-intensive computer applications on high-performance computers
- Development and implementation of numerical algorithms
- Porting of scientific computer applications to new multiprocessor architectures, e.g. GPUs
- User support for the efficient usage of high-performance computers
- Publication and communication of scientific results

**Your profile**

- PhD in physics, mathematics or computational sciences
- Several years of experience in the development and implementation of numerical algorithms on massive-parallel high-performance computers
- Profound experience in the design of large scientific codes and programming techniques
- Full proficiency in high level programming languages like e.g. Fortran 90/95 and C++
- A logical approach to problem solving, good time management, and task prioritization skills
- Good command of the English language, written and spoken

**We offer**

- An interesting job working on one of the largest scientific projects
- Flexible working hours
- Training and further education opportunities
- Company pension scheme
- Family service &amp; childcare facilities on the company premises
- Other social benefits (e.g. capital-forming benefits, discounted public transport tickets)

The position is limited until December 31, 2025. The salary is based on the assignment of tasks and qualifications and goes up to pay group 13 according to the collective agreement for the public sector (TVöD Bund).
The IPP seeks to increase the proportion of employed women in those areas where they are underrepresented and explicitly encourages women to apply for this position. The IPP is committed to employing more disabled individuals and especially encourages them to apply.

For information on data protection and the handling of your personal data please visit our website.

Are you currently interested?

Then we are looking forward to your complete online application until 07.10.2021.


**Team description :**


<https://www.ipp.mpg.de/3987588/ach>


**Advanced Computing Hub**

The Advanced Computing Hub (ACH) provides support to scientists from all Research Units of the EUROfusion consortium for the development and optimization of codes to be used on supercomputers.


**MARCONI supercomputer**

Currently there is the MARCONI supercomputer in the framework of the EUROfusion consortium being used to perform fusion relevant simulations. It is hosted by CINECA in Bologna, Italy with a total compute power of about 12.5 petaflop/s (Intel SKL partition: 9.3 petaflops + Nvidia GPU partition: 3.2 petaflops) dedicated to the usage of EUROfusion.
The Team

The ACH consists of a core team based at the Max-Planck-Institut für Plasmaphysik in Garching and a team member provided by another Research Unit of EUROfusion, who is located at the Dublin City University (DCU). The ACH members are all HPC experts with a background in developing large scientific applications and particular expertise in numerical algorithms.

**ACH support tasks**

- Parallelise codes using e.g. OpenMP and/or MPI standards for massively parallel computers;
- IImprove the performance of existing parallel codes both at the single node and inter node levels;
- Support the transfer of codes to new multiprocessors&#39; architectures, like e.g. GPUs;
- Choose and if necessary adapt algorithms and/or mathematical library routines to improve applications for the targeted computer architectures;
- Give feedback to the community, based on experience gained from specific project work;
- Provide guidance for young scientists on available training activities in HPC and towards upcoming new computer architectures.


**The supercomputers used :**

- Helios (previous one)
  <https://www.sciencedirect.com/science/article/abs/pii/S0920379617309018>

- Marconi and marconi100 (current ones)
  <https://www.hpc.cineca.it/hardware/marconi>
  <https://www.hpc.cineca.it/hardware/marconi100>

**What are ACH/E-tasc :**
<https://www.euro-fusion.org/news/2021/march/eurofusion-e-tasc/>
