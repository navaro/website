Title: Un(e) ingénieur(e) de recherche, Expert(e) en calcul scientifique
Date: 2021-01-05 10:05
Slug: job_20b105dccd6aacfcae94a24d8a003ebd
Category: job
Authors: Jean-Luc Beney
Email: Jean-Luc.Beney@subatech.in2p3.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Nantes
Job_Duration: 
Job_Website: http://www-subatech.in2p3.fr
Job_Employer: Laboratoire Subatech - Nantes
Expiration_Date: 2021-03-30
Attachment: 

Le laboratoire SUBATECH, localisé à Nantes,  recrute dans le cadre de la campagne de mobilité interne NOEMI : un(e) ingénieur(e) de recherche, Expert(e) en calcul scientifique (NOEMI S57030)

Dans le cadre du laboratoire, il/elle fera partie du groupe de calcul scientifique en support direct des équipes de recherche. Il/elle devra apporter son savoir-faire en calcul numérique à travers la vérification et l&#39;optimisation des algorithmes existants et la proposition d&#39;algorithmes alternatifs.

Dans un premier temps, au sein de l&#39;équipe théorie qui développe le logiciel EPOS (générateur d&#39;événements Monte-Carlo pour étudier les collisions entre particules à haute énergie et expliquer les résultats expérimentaux obtenus au RHIC et au LHC), l&#39;expert-e en calcul numérique devra améliorer la qualité et la diffusion du code en étroite collaboration avec le chercheur qui coordonne le développement.

Pour plus de détails sur le poste et/ou pour postuler (date limite 15 janvier 2021) :
<https://mobiliteinterne.cnrs.fr/afip/owa/consult.affiche_fonc?code_fonc=S57030&type_fonction=&code_dr=17&code_bap=E&code_corps=&nbjours=&page=1&colonne_triee=1&type_tri=ASC>

Site internet du laboratoire :
<http://www-subatech.in2p3.fr/>



