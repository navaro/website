Title: Ingénieur⸱e Chercheur.e HPC
Date: 2021-07-09 13:06
Slug: job_eb87a9de33b8dd76f1d42a5cbe70c234
Category: job
Authors: Bruno Collard
Email: bruno.collard@cea.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Saint-Paul-Lez-Durance
Job_Duration: 
Job_Website: https://cadarache.cea.fr/cad/Pages/Accueil.aspx
Job_Employer: CEA
Expiration_Date: 2022-01-01
Attachment: 

**Contexte**
Le Département d&#39;Etude des Combustibles du CEA a pour mission d&#39;acquérir et de capitaliser les connaissances relatives à la conception, à la fabrication à l&#39;exploitation en réacteur et au stockage à longs termes des éléments combustibles pour l&#39;énergie nucléaire. Il s&#39;appuie sur une plateforme expérimentale (fabrication, expérimentation et caractérisation des combustibles) et sur une plateforme numérique (modélisation et simulation du comportement des combustibles multiéchelles, multiphysiques et multifilières). Les outils de calcul scientifique pour la simulation du comportement des combustibles sont développés au sein du *Service d&#39;Etudes et de Simulation du comportement des Combustibles*. Ce service assure le développement, la vérification, la validation, la quantification des incertitudes et assure la maintenance opérationnelle des applications de simulation du combustible au sein d&#39;une plateforme numérique nommée PLEIADES. 


**Poste**
Dans le cadre du développement de nos activités, nous recherchons un ingénieur-chercheur-développeur HPC. Au sein d’une équipe existante de développeurs et d&#39;ingénieur-chercheurs, il s’agit de contribuer au développement d’un environnement HPC pour notre plateforme de simulation multiphysique et multiéchelle. D’un bon relationnel, autonome et force de proposition, votre rigueur et votre capacité d’analyse sont des atouts essentiels pour réussir dans ce poste. La mission proposée dans le cadre de ce poste est la suivante :

- Développer les outils de calcul de la plateforme numérique avec une mission première centrée sur le calcul haute performance
- Apporter son expertise pour l’optimisation de la parallélisation de PLEIADES et des outils de calcul scientifique
- Rédiger les documentations techniques et valoriser les travaux par des publications 
- Participer à la formation par la recherche par l&#39;encadrement de stagiaires et de doctorants

**Compétences recherchées**

- Solides connaissances en algorithmique parallèle haute performance, optimisation d&#39;applications de grandes tailles, mise en production de solutions de calcul HPC
- Solides connaissances en  développement d&#39;applications et de logiciels scientifiques
- Capacité à utiliser des serveurs de calcul numérique indispensable
- Programmation parallèle : OpenMP et MPI
- Architectures CPU, GPU
- Langages C++, python
- Chaîne de compilation Makefile, Cmake
- Intégration continue
- Méthode des éléments finis
- Des connaissances en mécanique des structures et des solides serait un plus

**Formation initiale :** Bac+5 ou thèse - spécialité informatique

**Expérience :** 3 à 5 ans 
