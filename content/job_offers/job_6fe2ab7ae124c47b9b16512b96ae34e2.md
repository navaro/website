Title: Ingénieur de Recherche et de Développement - Développeur informatique – calcul scientifique
Date: 2021-10-01 14:38
Slug: job_6fe2ab7ae124c47b9b16512b96ae34e2
Category: job
Authors: Julien BRUCHON
Email: bruchon@emse.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Saint-Étienne, campus de l&#39;École des Mines, société SCC
Job_Duration: 1 an
Job_Website: https://www.mines-stetienne.fr/
Job_Employer: École des Mines de Saint-Étienne
Expiration_Date: 2021-11-30
Attachment: job_6fe2ab7ae124c47b9b16512b96ae34e2_attachment.pdf

Contrat : CDD 1 an
Date de recrutement : Dès que possible – avant la fin de l’année
Structure d’accueil : Société Sciences Computers Consultants (Saint-Étienne, Loire)
Structure de recherche : Mines Saint-Étienne (site de Saint-Étienne), département SMS - MPE
Conditions pour postuler : être diplômé du grade master durant les années universitaires 2019-2020 et 2020-2021, conformément aux directives de l’action 3 de la mesure de Préservation de l’emploi R&amp;D du plan de relance gouvernemental

Salaire : selon profil et expérience 

L’École Nationale Supérieure des Mines de Saint-Étienne (Mines Saint-Étienne), École de l’Institut Mines Télécom (IMT), sous tutelle du Ministère de l’Économie, des Finances et de la Relance est chargée de missions de formation, de recherche et d’innovation, de transfert vers l’industrie et de culture scientifique, technique et industrielle.
Mines Saint-Étienne représente : 2 200 élèves-ingénieurs et chercheurs en formation, 400 personnels, un budget consolidé de 46 M€, 3 sites sur le campus de Saint-Étienne (Région Auvergne Rhône-Alpes) d&#39;environ 26 000 m², le campus Georges Charpak Provence à Gardanne (Région Sud) d&#39;environ 20 000 m², 6 Unités de de recherche, 5 centres de formation et de recherche, un centre de culture scientifique technique et industrielle (La Rotonde) de premier plan national (&gt; 40 000 visiteurs). Mines Saint-Étienne a des projets de développement sur Lyon, notamment sur le Campus Numérique de la région Auvergne-Rhône-Alpes et de nombreuses collaborations à l’international. Le classement du Times Higher Education (THE), nous place en 2021 au niveau mondial dans la gamme 301–400 dans le domaine de l’Engineering (6ème école d’ingénieurs en France et 1er établissement dans ses deux régions d’appartenance) ainsi que dans les domaines Computer Science (501-600) et Physical Sciences (601-800).

L’équipe MPE (Mécanique et Procédés d’Élaboration directe) est l’un des trois départements du centre SMS (Sciences des Matériaux et des Structures), entité d’enseignement et de recherche propre aux Écoles des Mines. En cohérence avec la dynamique mise en place via des thématiques de Leadership portées par l’IMT, notamment sur les « Transformations industrielles » (Matériaux à haute performance et éco-matériaux, Fabrication avancée) et les « Transformations numériques » (Intelligence artificielle), le département MPE structure des activités de recherche autour de procédés innovants et écoresponsables. 

La société Sciences Computers Consultants (SCC) s&#39;implique, depuis 1989, auprès de l&#39;industrie de transformation des matériaux, en étant spécialisée dans le transfert de travaux de recherche vers les industries. Historiquement tournée vers les métiers de consultants, SCC développe et commercialise des logiciels de simulation numérique, dédiés à la transformation et mise en forme des matériaux.

Missions 
Le centre Sciences des Matériaux et des Structures (SMS) de l’École des Mines de Saint-Étienne développe depuis plusieurs années une librairie de calcul (simulation numérique), capable de résoudre, par la méthode des éléments finis, les équations physiques couramment utilisées dans certains problèmes d’ingénierie et notamment dans la simulation de certains procédés d’élaboration des matériaux : équations de Stokes, Navier-Stokes, Darcy, équation de la chaleur, équation de transport, etc. Cette libraire, actuellement à l’état de prototype académique, est écrite en C++, compilée sous Linux, et s’exécute sur jusqu’à quelques centaines de cœurs de calcul du cluster de l’École des Mines.

L’objectif de ce CDD d’un an est de faire passer la libraire de cet état de prototype académique à celui de preuve de concept (ou version 1.0), pouvant être ensuite diffusée, pour des activités de recherche et/ou d’enseignement, mais aussi sous la forme d’applications dédiées, notamment, à un contexte industriel donné. Pour ce faire, le candidat retenu devra :

- Prendre en main l’environnement dans lequel est développée la librairie (serveur GIT, compilation avec cmake, gestion des dépendances, …), afin de l’optimiser (création de versions, de branches, optimisation de la compilation, …).
- Optimiser et étendre cet environnement afin de favoriser l’utilisation, la diffusion et le développement de la librairie : mise en place d’un suivi des bugs, mise en place d’une base de cas de validation avec solution de référence, via notamment des scripts Python, ...
- Élaborer une stratégie de portage sur des plateformes de calcul (Docker, Linux, …)
- Initier le développement d’une libraire IHM (Interface Homme Machine) sous forme de SDK réutilisable.

Exigences de l’offre d’emploi
Compétences et connaissances souhaitées :

- Connaissance de Linux et des outils OpenSource
- Maîtrise des outils de compilation
- Maîtrise des environnements de développement : GIT, bug reporting, ...
- Maîtrise du langage de programmation Python
- Maîtrise des langages de programmation C ou C++
- Maîtrise de l’usage des API (visualisation, calculs)
- Une expérience en dockerisation serait un plus

Niveau de formation requis :

- Diplômé du grade master en informatique ou calcul numérique, durant les années universitaires 2019-2020 et 2020-2021,

Modalités de candidature et conditions de recrutement 

CDD d’un an, embauche par l’École des Mines de Saint-Étienne, accueil à . Accueil à temps partiel SCC, Mines de Saint-Etienne.
Les candidatures seront examinées immédiatement jusqu&#39;à ce que le poste soit pourvu. Le salaire dépendra de l&#39;expérience, avec un minimum de 2260€ net/mois.
Ces missions s’exerceront sur le Campus de Saint-Étienne (42) de Mines Saint-Étienne et au siège de la société SCC.
Les candidats intéressés doivent soumettre un CV, une lettre de motivation (recommandations éventuelles) décrivant leur expérience et leurs intérêts en matière de recherche, ainsi que les noms et adresses électroniques de leurs références : 
Les dossiers de candidature sont à déposer sur la plateforme RECRUITEE le 30/11/21 au plus tard URL de dépôt de candidature : 
<https://institutminestelecom.recruitee.com/o/ingenieur-e-de-recherche-et-de-developpement-developpeur-informatique-calcul-scientifique>

Pour en savoir plus 

Philippe DAVID, société SCC <pdavid@scconsultants.com>
Julien BRUCHON, EMSE, <bruchon@emse.fr> 04 77 42 00 72
École Nationale Supérieure des Mines de Saint-Étienne <http://www.emse.fr>
Pour tout renseignement administratif, s’adresser à :
Amandine HIRONDEAU, Tel + 33 (0)4 77 42 01 03, Mel: <hirondeau@emse.fr>
Chantal DAVID, SCC, <chdavid@scconsultants.com>
