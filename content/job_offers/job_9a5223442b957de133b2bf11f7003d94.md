Title: Ingénieur·e Développement et traitement de données
Date: 2020-05-28 11:58
Slug: job_9a5223442b957de133b2bf11f7003d94
Category: job
Authors: Bruno Khelifi
Email: khelifi@in2p3.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Paris
Job_Duration: 1 an renouvellable 2 fois
Job_Website: https://www.ipgp.fr/fr/ingenieure-informatique
Job_Employer:  IPGP / Laboratoire AstroParticule et Cosmologie
Expiration_Date: 2020-08-20
Attachment: job_9a5223442b957de133b2bf11f7003d94_attachment.pdf

# Mission

Concevoir et participer aux développements informatiques pour le traitement des données des expériences d&#39;astronomie multi-messagers :

-  développement de la librairie python gammapy d’analyse et de modélisation des données de haut niveau
-  développements logiciels d’analyse des données multi-messagers (rayons X et gamma, neutrino et ondes gravitationnelles) dans le contexte de la plateforme web Multi-Messenger Observatory.

Toutes les informations sont dans le document joint. 