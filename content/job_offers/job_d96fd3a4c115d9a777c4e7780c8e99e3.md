Title: Ingénieur de recherche en incertitudes F/H
Date: 2021-02-18 09:56
Slug: job_d96fd3a4c115d9a777c4e7780c8e99e3
Category: job
Authors: Matthias De Lozzo
Email: matthias.delozzo@irt-saintexupery.com
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Toulouse
Job_Duration: 24 mois
Job_Website: https://taleez.com/apply/ingenieur-de-recherche-en-incertitudes-f-h-ref-21od-dsc-rev-01-toulouse-irt-saint-exupery-cdd?utm_source=career
Job_Employer: IRT Saint Exupéry
Expiration_Date: 2021-09-01
Attachment: 

# À propos de IRT Saint Exupéry
**L’Institut de Recherche Technologique (IRT) Saint Exupéry est un accélérateur de science, de recherche technologique et de transfert vers les industries de l’aéronautique et du spatial pour le développement de solutions innovantes sûres, robustes, certifiables et durables. **

Nous proposons sur nos sites de Toulouse, Bordeaux, Montpellier, Sophia Antipolis et Montréal un environnement collaboratif intégré composé d’ingénieurs, chercheurs, experts et doctorants issus des milieux industriels et académiques pour des projets de recherche et des prestations de R&amp;T adossés à des plateformes technologiques autour de 4 axes : les technologies de fabrication avancées, les technologies plus vertes, les méthodes &amp; outils pour le développement des systèmes complexes et les technologies intelligentes.

Nos technologies développées répondent aux besoins de l&#39;industrie, en intégrant les résultats de la recherche académique. 

# Description du poste
Dans le cadre du projet R-EVOL du pôle Multidisciplinary Design Optimisation (MDO) du domaine Développement de Systèmes Complexes, l’IRT Saint Exupéry recherche un **ingénieur de recherche en incertitudes** pour mener à bien les travaux **« Développement de méthodes de quantification d’incertitudes pour l’optimisation robuste »**. Le projet R-EVOL vise notamment à quantifier et gérer les incertitudes intervenant dans le processus de conception d’un avion. La personne retenue aura pour mission principale de développer des techniques multi-fidélité pour la propagation et la quantification d’incertitudes dans des systèmes multidisciplinaires. Elle étudiera aussi l’effet de la prise en compte de la dépendance stochastique entre les variables incertaines sur la solution du problème d’optimisation robuste.  Ces techniques devront être numériquement efficaces eu égard au coût calculatoire associé aux simulateurs disciplinaires limitant le nombre d’exécutions. Ces activités consisteront à :

* Mener un état de l’art en optimisation robuste, approches multi-fidélité (aussi appelées multi-niveaux), quantification d’incertitudes et analyse de sensibilité.
* Concevoir une méthodologie multi-fidélité pour l’optimisation robuste.
* Sélectionner et adapter des méthodes d’analyse de sensibilité pour les variables aléatoires dépendantes.
* Appliquer les techniques retenues à un cas académique puis à un cas industriel.
* Disséminer les résultats par le biais de conférences internationales et de publications dans des journaux à comité de lecture.

Les travaux seront implémentés dans le logiciel open source GEMS et appliqués à la conception avion, en lien direct avec les acteurs industriels. Ils seront réalisés conjointement à une thèse axée sur les formulations MDO pour l’optimisation robuste.

# Description du profil
Vous disposez d&#39;un doctorat en mathématiques appliquées et vous avez idéalement une première expérience dans un environnement de recherche mélant Mathématiques appliquées, méthodes numériques, statistiques, quantifications d&#39;incertitudes et programmation Python.

# Postuler
[https://taleez.com/apply/ingenieur-de-recherche-en-incertitudes-f-h-ref-21od-dsc-rev-01-toulouse-irt-saint-exupery-cdd?utm_source=career](https://taleez.com/apply/ingenieur-de-recherche-en-incertitudes-f-h-ref-21od-dsc-rev-01-toulouse-irt-saint-exupery-cdd?utm_source=career)