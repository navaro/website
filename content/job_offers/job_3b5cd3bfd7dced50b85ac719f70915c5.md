Title: Maître-Assistant &#34;Calcul intensif en mécanique&#34;
Date: 2021-02-25 18:25
Slug: job_3b5cd3bfd7dced50b85ac719f70915c5
Category: job
Authors: Bruchon Julien
Email: bruchon@emse.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Saint-Etienne
Job_Duration: 
Job_Website: https://institutminestelecom.recruitee.com/o/concours-maitre-assistant-en-calcul-intensif-en-mecanique
Job_Employer: Institut Mines-Telecom / Ecole des Mines de Saint-Etienne
Expiration_Date: 2021-04-23
Attachment: 

Poste pour un Maître-Assistant (statut similaire à Maître de Conférences au Ministère de l’Industrie - Institut Mines-Télécom) dans le domaine du calcul intensif et méthodes d’apprentissage pour la modélisation multi-échelle des procédés d’élaboration directe.

Le poste est à pourvoir dans notre notre équipe Mécanique et Procédés d’Elaboration directe / centre SMS et LGF UMR CNRS 5307 de l’Ecole des Mines de Saint-Etienne.

La date limite de dépôt des candidatures est fixée au 23/04/2021 pour une prise de fonctions souhaitée au 01/10/2021. Toutes les informations sont dans la fiche.