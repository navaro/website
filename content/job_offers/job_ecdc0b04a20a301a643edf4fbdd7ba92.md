Title: Ingénieur DevOps pour la plateforme Codalab
Date: 2021-06-04 15:00
Slug: job_ecdc0b04a20a301a643edf4fbdd7ba92
Category: job
Authors: Anne-Catherine Letournel
Email: anne-catherine.letournel@lisn.upsaclay.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Laboratoire LISN, Bât.650, rue Raimond Castaing, 91190 Gif-sur-Yvette
Job_Duration: 24 mois + 12 mois
Job_Website: 
Job_Employer: Université Paris-Saclay
Expiration_Date: 2021-08-27
Attachment: job_ecdc0b04a20a301a643edf4fbdd7ba92_attachment.pdf

**Contexte:**
Rejoindre l&#39;équipe dynamique qui gère et développe la plateforme de compétitions scientifiques (défis ou “challenges”) Codalab [instance publique hébergée à Paris-Saclay](https://competitions.codalab.org) [project open-source dont Paris-Saclay est community lead](https://github.com/codalab/codalab-competitions/issues). Les défis hébergés sur Codalab sont surtout des compétitions de Machine Learning et de Deep Learning, mais la plateforme est apte à héberger n&#39;importe quels types de défi de programmation. Les codes soumis par les participants sont exécutés et évalués automatiquement sur la plateforme. Codalab a plus de 50,000 utilisateurs, 1000 compétitions (dont 400 l’an dernier), et  environ 600 soumissions par jour.
Les serveurs sur lesquels s&#39;appuie Codalab sont hébergés au LISN et au sein du mésocentre de l&#39;université Paris-Saclay. Le LISN est un nouveau laboratoire de près de 400 personnes issue de la fusion du LRI et du LIMSI. Ses cinq départements couvrent un large spectre de thématiques scientifiques en sciences du numérique: sciences des données, interaction avec l’humain, algorithmes apprentissage et calcul, sciences et technologies des langues et enfin mécanique des fluides et énergétique.

**Missions:**
Pour faire face à l&#39;accélération de l’utilisation de Codalab, et particulièrement pour gérer des volumes de données de dizaines de téraoctets, nous recrutons un ingénieur DevOps pour accompagner la croissance de Codalab et la maintenir en condition opérationnelle. Vous aurez l’occasion de proposer des solutions innovantes pour le passage à l&#39;échelle du service, et de participer à l’organisation de compétitions académiques et industrielles dans le cadre de la chaire HUMANIA auquel le poste est rattaché.  Ce sera pour vous une occasion de collaborer avec des chercheurs en Intelligence Artificielle et de vous frotter au Deep Learning et aux techniques d’automatisation du Machine Learning, et participer à un projet open-source international. Les technologies mises en œuvre devront évoluer en permanence pour rester au meilleur niveau et évoluer en même temps que les outils de deep-learning du marché et  des jeux de données de plus en plus volumineux. 

**Compétences:**

- Architecture et l&#39;environnement technique du système d&#39;information: docker, serveurs web, micro-services, services de stockage objet... 
- Familiarité avec les GPUs.
- Familiarité avec les environnements de clouds computing.
- Méthodes de mise en production en environnement virtualisé (proxmox et openstack).
- Normes d&#39;exploitation des systèmes Linux.
- Performance et métrologie. 
- Diagnostic et résolution de problèmes. 
- Méthodes, outils, normes et procédures de la qualité: système de gestion de version git.
- Langage de programmation: python et scripts bash.
- Anglais technique: langue du projet, de la communauté Codalab, de la documentation et des réunions.

**Candidature:**
Pour postuler, merci de nous faire parvenir un CV ainsi qu’une lettre de motivation aux adresses suivantes: <anne-catherine.letournel@lisn.upsaclay.fr> et <isabelle.guyon@lisn.upsaclay.fr>.
