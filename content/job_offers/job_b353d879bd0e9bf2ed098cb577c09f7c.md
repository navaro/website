Title: Ingénieur Développeur de services pour le Centre de Calcul Ouvert du CEA
Date: 2021-03-19 14:42
Slug: job_b353d879bd0e9bf2ed098cb577c09f7c
Category: job
Authors: Mickaël Amiet
Email: mickael.amiet@cea.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location:  Bruyères-le-Châtel (91)
Job_Duration: 24 mois
Job_Website: https://www.emploi.cea.fr/offre-de-emploi/emploi-ingenieur-developpeur-de-services-pour-le-centre-de-calcul-ouvert-du-cea-f-h_16431.aspx
Job_Employer: CEA - DAM Ile de France
Expiration_Date: 2021-06-01
Attachment: 

*Rejoignez le CEA pour donner du sens à votre activité, mener et soutenir des projets de R&amp;D nationaux et internationaux, cultiver et faire vivre votre esprit d&#39;innovation.*

**CE QUE LE CEA PEUT VOUS PROPOSER :**

Le CEA recherche un.e ingénieur.e développeur pour son complexe de calcul scientifique. Ce poste de cadre, pour un CDD (24 mois), est basé sur le site du CEA de Bruyères-le-Châtel, Essonne (91).

Il s&#39;agit d&#39;un poste à pourvoir au 01/06/2021.

**VOTRE ENVIRONNEMENT DE TRAVAIL DIRECT :**

Le CEA est un acteur majeur et reconnu au niveau mondial dans des domaines d&#39;activités variés tels que le nucléaire, la santé, le climat, l&#39;aéronautique, la sécurité et la défense.

La Direction des Applications Militaires (DAM) du CEA possède de grands moyens de simulation numérique regroupés dans un complexe de calcul de dimension internationale. Celui-ci est constitué de deux centres de calcul :

- Le centre de calcul TERA dédié aux activités de la Défense.
- Le Très Grand Centre de Calcul (TGCC) qui héberge le calculateur PRACE, dédié à la recherche européenne, le Centre de Calcul Recherche et Technologie (CCRT) utilisé par le CEA et des industriels français.

L’usage des centres de calcul évolue, ce qui amène le CEA à mettre à disposition de ses utilisateurs de nouveaux services tels que du « cloud computing » ou du stockage objet. Ces évolutions permanentes nécessitent la réalisation de développements pour intégration de ces nouveaux services au centre de calcul

**VOTRE MISSION :**

Vous souhaitez travailler dans un environnement à la pointe de la technologie (HPC) s’appuyant sur les solutions « open source »  ?

Au sein des équipes opérationnelles, votre mission consiste à mettre à niveau les outils de gestion des utilisateurs du centre de calcul et des ressources qui leurs sont allouées, notamment dans le cadre de l’infrastructure européenne distribuée FENIX (<https://fenix-ri.eu/>).

En étroite collaboration avec les ingénieurs d’exploitation et en suivant la méthodologie Agile, vous assurerez le développement de ces outils, leur intégration ainsi que le suivi de leur bon fonctionnement.

Cela vous tente ? Alors postulez car ce poste est fait pour vous \!

**Profil du candidat**

Vous avez soif d&#39;aventures, de nouveaux challenges et d&#39;innovations technologiques ?

Vous êtes diplômé d&#39;école d&#39;ingénieur ou master 2 en génie informatique et logiciel.
Vous justifiez idéalement d&#39;une expérience de 3 à 15 ans dans le domaine du génie informatique et logiciel (ou semblable).

Vous pratiquez les langages de programmation Python, bash.

Vous êtes doté(e) d&#39;un bon relationnel et souhaitez mettre vos qualités techniques et humaines au profit de l&#39;accompagnement des communautés scientifiques françaises et européennes dans leur utilisation optimale de systèmes de calcul parmi les plus importants et complexes au monde.

Vous vous épanouissez dans le travail collaboratif \! Vous disposez d&#39;un esprit d&#39;initiative et d&#39;analyse \!

Nous vous proposons de venir partager votre talent et renforcer notre équipe d&#39;experts, en intégrant une entreprise responsable, qui vous aidera à développer vos compétences et construire votre parcours professionnel.
