Title: Ingénieur.e Développement Cloud
Date: 2020-11-25 15:26
Slug: job_1e15130492af2ab68df1d1951553d4d6
Category: job
Authors: Alain BUTEAU
Email: alain.buteau@synchrotron-soleil.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Saint Aubin
Job_Duration: 
Job_Website: https://www.synchrotron-soleil.fr/fr/emplois/ingenieur-developpement-cloud-0
Job_Employer: Synchrotron SOLEIL
Expiration_Date: 2021-02-17
Attachment: 

- Assurer la mise en œuvre technique des solutions de type cloud adoptées par le projet ExPaNDS pour la fourniture d’une plateforme d’accès distants à des services logiciels scientifiques existants, en adéquation avec les contraintes spécifiques à l’environnement de SOLEIL. 
- Définir et mettre en œuvre les architectures techniques de containerisation, d’automatisation et d’orchestration nécessaires au déploiement d’applications scientifiques et techniques de SOLEIL, en s&#39;appuyant dans la mesure du possible sur les acquis techniques d’ExPaNDS.