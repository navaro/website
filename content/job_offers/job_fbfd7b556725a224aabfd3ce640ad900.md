Title: Ingénieur support applicatif HPC, piles logicielles IA H/F 
Date: 2020-04-02 07:21
Slug: job_fbfd7b556725a224aabfd3ce640ad900
Category: job
Authors: Marc Joos
Email: marc.joos@cea.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Bruyères-le-Châtel
Job_Duration: 36 mois
Job_Website: https://www.emploi.cea.fr/offre-de-emploi/emploi-ingenieur-support-applicatif-hpc-piles-logicielles-ia-h-f_12941.aspx
Job_Employer: CEA
Expiration_Date: 2020-09-30
Attachment: 

## Description de l&#39;offre

Le complexe de calcul du CEA/DAM Ile-de-France est constitué des centres de calcul TERA (programme Simulation) et TGCC (recherches académiques française et européenne, recherches industrielles).  
Ces centres de calcul sont accessibles des communautés scientifiques utilisant en permanence les ressources de calcul et de stockage mises à leur disposition.  
Le CEA/DAM assure la qualité et la continuité des services proposés en déployant des technologies à l’état de l’art en matière de calcul scientifique et de stockage numérique. Il veille à la bonne utilisation de ces moyens et contribue à leur évolution, tant du point de vue matériel et logiciel que du point de vue des services proposés.  
Au sein d&#39;une équipe d&#39;ingénieurs-chercheurs en charge des supercalculateurs du CEA et en relation avec des partenaires industriels et académiques, en France et en Europe, vous contribuez à l&#39;émergence des solutions logicielles pour l&#39;intelligence artificielle et l&#39;apprentissage automatique (Machine Learning, Deep Learning) dans des environnements de calcul haute performance (HPC).
En particulier, vous participez :

- à la définition, au développement et à la mise en place sur les calculateurs du CEA de piles logicielles pour l&#39;intelligence artificielle et l’apprentissage automatique, piles à destination des communautés utilisatrices de ces moyens de calcul ;
- à l&#39;accompagnement des communautés utilisatrices dans leurs pratiques de l&#39;intelligence artificielle, en particulier pour le passage à l&#39;échelle de leurs applications et leur intégration dans un environnement HPC ;
- à la R&amp;D dans le domaine du HPC via l&#39;apport des techniques d&#39;intelligence artificielle.

## Profil du candidat

Vous êtes diplômé d&#39;école d&#39;ingénieur ou d&#39;un master 2 en informatique, spécialité calcul haute performance et/ou intelligence artificielle ou titulaire d&#39;un doctorat scientifique avec une pratique des techniques d&#39;apprentissage automatique.
Vous justifiez, de préférence, d&#39;une première expérience dans l&#39;utilisation et/ou le développement de méthodes d&#39;apprentissage statistique.
Vous connaissez le langage Python et les bibliothèques d&#39;apprentissage statistique associées (Scikit-Learn, TensorFlow et/ou PyTorch).
La connaissance du framework Horovod est un plus. Il en est de même si vous êtes familier des domaines du calcul haute performance, des technologies logicielles associées, des solutions de virtualisation et/ou conteneurisation. 