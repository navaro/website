Title: Couplage de codes pour la modélisation dynamique du prélèvement racinaire de l&#39;eau
Date: 2019-12-09 15:06
Slug: job_f0a37c27257b1cf39a0f8eb54f9c92d4
Category: job
Authors: Céline Blitz Frayret
Email: celine.blitz@cirad.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: UMR Eco&amp;Sols, campus SupAgro, Montpellier
Job_Duration: 6 mois
Job_Website: https://www.umr-ecosols.fr/
Job_Employer: CIRAD
Expiration_Date: 2020-03-02
Attachment: 

Afin de contribuer à la modélisation mécaniste du couplage des cycles des nutriments, du carbone et de l’eau, l’UMR Eco&amp;Sols propose une offre de stage de 6 mois rémunéré en développement logiciel impliquant les langages C++ et Fortran.
L’objectif du stage est d’initier le couplage entre un modèle de croissance racinaire de plante et un modèle de transport de l’eau et des nutriments dans le sol. Après avoir étudié un cas test de ces 2 codes, l’étudiant s’attachera à les préparer au couplage en isolant les fonctions de calcul de la croissance racinaire et du prélèvement en eau de la racine. La compatibilité des échelles de temps et d’espace sera alors vérifiée puis une interface d’interopérabilité pourra être développée afin d’aboutir à un premier essai de couplage dynamique de ces 2 programmes.