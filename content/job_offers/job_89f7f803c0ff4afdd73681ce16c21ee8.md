Title: Ingénieur Chercheur développement logiciel scientifique
Date: 2020-05-14 15:47
Slug: job_89f7f803c0ff4afdd73681ce16c21ee8
Category: job
Authors: Erwan ADAM
Email: erwan.adam@cea.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Saclay
Job_Duration: 
Job_Website: https://www.emploi.cea.fr/offre-de-emploi/emploi-dm2s-stmf-lgls-ingenieur-chercheur-developpement-logiciel-scientifique-saclay-91-h-f_13185.aspx
Job_Employer: CEA
Expiration_Date: 2020-12-31
Attachment: 

Le poste consistera à participer au développement de la plateforme SALOME et également d&#39;autres applications développées au LGLS, dans un souci d’amélioration continue de la qualité, de la maintenabilité et des performances.
Du fait du rôle transverse du LGLS au sein de la DES, il s’agira par ailleurs d’accompagner les équipes CEA dans :
- leur utilisation des modules et fonctionnalités de SALOME ;
- le développement d&#39;applications dédiées à leur métier, basées ou non sur SALOME ;
- la mise en place de méthodes et d&#39;outils de génie logiciel ;

Ce poste requiert des compétences en développement orienté objet en C++ et Python, programmation par couche et architecture logicielle.
Des compétences en CAO / maillage sont recommandées.
Des connaissances dans un domaine de la physique (mécanique des fluides, neutronique ou mécanique) seraient appréciées.

Au-delà des compétences techniques, ce poste nécessite une forte aptitude au travail en équipe et le sens du service.

Mots-clés :  IHM, FreeCAD, GMSH, SolidWorks, ICEM, Paraview

Profil du candidat

BAC+5 Diplôme École d&#39;ingénieurs avec spécialité en Informatique - Mathématiques appliquées
Compétences techniques et/ou spécifiques : C++, Python, QT ; OS : Linux et Windows
Outils utilisés : git, cmake, jenkins, paraview, salome, eclipse, tuleap