Title: Scientific Software Developer - Jupyter
Date: 2021-04-03 11:51
Slug: job_5de09ecaec8b8f39ce8aa0131097927c
Category: job
Authors: Sylvain Corlay
Email: sylvain.corlay@quantstack.net
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Remote
Job_Duration: 
Job_Website: https://quantstack.net
Job_Employer: QuantStack
Expiration_Date: 2021-06-26
Attachment: 

QuantStack is looking for a software developer to join our team of contributors to the Jupyter ecosystem to contribute to the development of the JupyterLab project. In this role, you will become a member of a vibrant open-source community, contribute to a high-impact project used by millions in the world, and grow to become a key maintainer of the ecosystem.

**About QuantStack**

QuantStack is an open-source development studio specializing in scientific computing. The team comprises core developers of major open-source scientific computing projects, such as Jupyter, Conda-forge, Voilà, Xtensor, Mamba, and many others. We strive to build high-impact software and grow communities.

**Your role at QuantStack**

You will support the community of users of the Jupyter and larger PyData ecosystem, and help shape the future of software adopted by millions of engineers and scientists in the world. Beyond your primary focus on the JupyterLab frontend, you will contribute to other components of the PyData ecosystem.

* We are looking for an experienced front-end web developer to contribute to the development of the JupyterLab front-end and the ecosystem of JupyterLab front-end extensions, as well as Jupyter Widgets. Experience with any of the following are strong assets to an application:
* JavaScript, TypeScript or Python programming languages.
* WCAG (Web Content Accessibility Guidelines).
* In-browser data visualization libraries and tools.

Prior experience with Jupyter development specifically is not required. A track record of contributing to open-source software is a huge plus.

**Where we can hire**

Our engineering team is fully remote. The company is headquartered in Paris, France, and the majority of our team is Europe-based. We will consider applications in the European Union.