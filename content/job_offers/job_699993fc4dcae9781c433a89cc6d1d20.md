Title: Postes MCF en informatique
Date: 2021-02-12 15:43
Slug: job_699993fc4dcae9781c433a89cc6d1d20
Category: job
Authors: Fabienne Jézéquel
Email: Fabienne.Jezequel@lip6.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Paris
Job_Duration: 
Job_Website: 
Job_Employer: Sorbonne Université
Expiration_Date: 2021-03-06
Attachment: 

L&#39;équipe PEQUAN (PErformance et QUalité des Algorithmes Numériques) du LIP6
à Sorbonne Université est susceptible de recruter sur deux postes de MCF avec
les profils suivants :

-Algorithmique, Calculs et Optimisations
-Informatique.

Les candidates et les candidats souhaitant intégrer l&#39;équipe PEQUAN
devront faire preuve d&#39;une expertise en fiabilité numérique et calcul
haute-performance. La personne recrutée viendra renforcer les
recherches menées dans l&#39;équipe sur la conception d&#39;algorithmes de
calcul performants et fiables exploitant au mieux les nouvelles
architectures parallèles et sur la validation numérique de codes de
calcul scientifique haute-performance.

En enseignement, les filières de formation concernées sont :

- Licence &amp; Master d’informatique
- Ecole Polytech Sorbonne.

La personne recrutée effectuera environ 1/3 de son service
d’enseignement dans les UE de l’Ecole Polytech Sorbonne. De plus, ses
interventions seront équilibrées en licence et en master d’informatique.

Des informations plus détaillées sont disponibles via la page &#34;emplois&#34; du LIP6 : 
<https://www.lip6.fr/recherche/emplois.php>

Pour les nouveaux MCF, l’UFR d’Ingénierie prévoit une décharge de 64h pendant
les deux premières années d’exercice. Pour les nouveaux arrivants, la Faculté
des Sciences et d’Ingénierie prévoit 10K€ de soutien aux activités de recherche.

ATTENTION, sur ce concours, la campagne se termine le 5 mars 2021 via le
portail Galaxie. Les informations sur la campagne de recrutement à Sorbonne 
Université sont disponibles ici : 
<http://recrutement.sorbonne-universite.fr/fr/personnels-enseignants-chercheurs-enseignants-chercheurs/enseignants-chercheurs.html>

Contact en recherche pour l&#39;équipe PEQUAN : <fabienne.jezequel@lip6.fr>
Contacts en enseignement :

- licence : <jean-lou.desbarbieux@sorbonne-universite.fr>
- master : <carlos.agon_amado@sorbonne-universite.fr>
- école Polytech Sorbonne : <myriam.comte@sorbonne-universite.fr>
