Title: Ingénieur support infrastructure de traitement de données
Date: 2020-04-10 08:47
Slug: job_afeff34079dc880433fe28e7a20746ee
Category: job
Authors: Jacques Thomazeau
Email: jacques.thomazeau@irit.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Toulouse
Job_Duration: 1 an
Job_Website: 
Job_Employer: CNRS - IRIT
Expiration_Date: 2020-07-03
Attachment: 

l&#39;IRIT recrute sur CDD de 12 mois un(e) IE ou IR (suivant profil) pour accompagner le développement de la plateforme OSIRIM (<http://osirim.irit.fr>).

L&#39;annonce du poste à pourvoir est disponible sur le portail emploi du CNRS :
<https://bit.ly/2y5GUTI>
