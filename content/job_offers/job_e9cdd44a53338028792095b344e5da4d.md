Title: Ingénieur Système &amp; Stockage HPC (H/F)
Date: 2020-03-11 11:07
Slug: job_e9cdd44a53338028792095b344e5da4d
Category: job
Authors: MORTIER
Email: sylvain.mortier@atos.net
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Bruyères-le-Châtel
Job_Duration: 
Job_Website: https://jobs.atos.net/job/Bruy%C3%A8res-Le-Ch%C3%A2tel-Administrateur-syst%C3%A8me-Stockage-HPC-HF-Ile/523040101/
Job_Employer: Atos
Expiration_Date: 2020-12-31
Attachment: 

Atos est un leader international de la transformation digitale avec plus de 110 000 collaborateurs dans 73 pays et un chiffre d’affaires annuel de plus de 11 milliards d’euros. Numéro un européen du Cloud, de la cybersécurité et des supercalculateurs, le groupe fournit des solutions intégrées de Cloud Hybride Orchestré, Big Data, Applications Métiers et Environnement de Travail Connecté. Partenaire informatique mondial des Jeux Olympiques et Paralympiques, le Groupe exerce ses activités sous les marques Atos, Atos Syntel, et Unify. Atos est une SE (Société Européenne) cotée sur Euronext Paris et fait partie de l’indice CAC 40.

La raison d’être d’Atos est de contribuer à façonner l’espace informationnel. Avec ses compétences et ses services, le groupe supporte le développement de la connaissance, de l’éducation et de la recherche dans une approche pluriculturelle et contribue au développement de l’excellence scientifique et technologique. Partout dans le monde, Atos permet à ses clients et à ses collaborateurs, et plus généralement au plus grand nombre, de vivre, travailler et progresser durablement et en toute confiance dans l’espace informationnel.

 
Atos est le leader européen du High Performance Computing et délivre des solutions de Calcul Haute Performance parmi les plus performantes au monde afin de résoudre les problèmes scientifiques les plus complexes d’aujourd’hui et de demain

 Au sein d’une équipe d’experts HPC, et en poste chez notre client CEA DAM (),* nous recherchons un ingénieur système pour assurer le support et l’administration d’un cluster de stockage HPC partagé par plusieurs supercalculateurs de classe mondiale.

 () Situé en Essonne, sur les communes de Bruyères-le-Châtel et d&#39;Ollainville, le centre CEA DAM-Île de France (DIF), est l&#39;un des centres de la Direction des applications militaires. Un dossier d’habilitation Secret Défense pour le candidat sera demandé aux autorités compétentes*.

 
Vos missions :

 - Déploiement, maintenance et support d&#39;un cluster de stockage HPC
- Installation Software, optimisation des configurations
- Déploiement et configuration de système de fichiers parallèle (Lustre)
- Mise en place de solution de haute disponibilité (Pacemaker, Corosync)
- Installation, configuration et administration de baies de stockage 
- Développement de procédures d’automatisation à l&#39;aide de scripts (bash, python)
- Rédaction de documentations techniques, des procédures d&#39;exploitation
- Analyses, diagnostics et résolutions d&#39;incidents de production
- Qualification et résolution de tickets client afin de répondre aux contraintes et à l&#39;amélioration des services de production
- Création et suivi des dossiers d’escalades techniques auprès des équipes de Support L3 internes ou de nos partenaires

- Support de niveau 1 et 2 de la stack software fournie par notre client basée sur CentOS : Diagnostics, mise en place des correctifs, escalade au support L3 (Client)

 
Votre profil :

De formation supérieure en informatique systèmes et réseaux type école d&#39;ingénieur ou cursus universitaire équivalent, vous justifiez d’une expérience significative en administration de système Linux dans un contexte de support avancé, idéalement dans un environnement HPC.

  Vous maitrisez tout ou partie des environnements/technologies suivants :

- Administration de systèmes Linux HPC : Redhat , CentOS

- Scripting : shell, python, perl

- Monitoring: Nagios, Shinken

 
Informations additionnelles :

Chez Atos la diversité est au cœur de notre politique RH. C&#39;est pourquoi Atos a mis en place un accord relatif à l’égalité professionnelle entre les hommes et les femmes. Par ailleurs, nos métiers sont tous accessibles aux personnes en situation de handicap, et ce quelle que soit la nature de leur handicap.