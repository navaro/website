Title: Dynamic anisotropic mesh adaptation for multiphase fluid and moving structure interaction
Date: 2019-11-22 14:24
Slug: job_23d5a618ccbe9cf4838e37c580a6d724
Category: job
Authors: Thierry Coupez
Email: thierry.coupez@mines-paristech.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Sophia Antipolis (06)
Job_Duration: 12 mois
Job_Website: http://www.cemef.mines-paristech.fr/sections/recherche/equipes-recherche/calcul-intensif
Job_Employer: Mines ParisTech - CEMEF 
Expiration_Date: 2020-02-14
Attachment: job_23d5a618ccbe9cf4838e37c580a6d724_attachment.pdf

Numerical multiphase simulations, in particular liquid gas, solid fluid or liquid gas structure couplings, depend directly on the construction of the mesh, which is therefore a crucial step in the success of calculations. Anisotropic mesh adaptation helps to simplify this construction and to make it particularly robust and precise. In the case of stationary simulation, many results have been obtained and the mesh, error estimation and metric construction technologies are well established. This remains a major challenge for dynamic simulations with a high temporal evolution. A number of difficulties persist we want to address here in the preliminary context of the interaction of one or more fluids with solids in free or forced movement. Fluid structure calculations may follow the mixed Lagragian Eulerian approach to preserve the interface definition between domains, or alternatively use the immersion methods in which the boundary of the structure is defined implicitly by a distance function. In the dynamic case, when the interface moves or is deformed, the two approaches require remeshing as soon as one wants to account for a significant time scale. ALE methods need to accommodate the deformation of the mesh, since Immersive approaches need to maintain a refined mesh in the vicinity of the interface. And therefore the question remains quite general of the optimal adaptation of the mesh in the highly dynamic context.
The subject is concerned with a space time error estimation and dynamic anisotropic adaptation from. The software framework is developed already including Stabilized Finite Element solvers for Navier Stokes, parallel dynamic anisotropic meshing and multiphase modelling components. The construction of the metric is derived from an error estimate rendering mainly the spatial discrepancy of the numerical solution. The novelty proposed in this work is about the time derivative of the metric from which we can deduce by duality a time derivative of the mesh, that could be translate in a mesh speed and a mesh deformation increment. The immediate applications of this work will concern the optimal mesh adaptation for fluid flow calculations around moving objects for low Mach aerodynamics and high Reynolds hydrodynamics with moving structures and free surfaces with numerous industrial application targets in the context of the Infinity chair and the ECO project.
