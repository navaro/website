Title: Development of a dynamic portal-workstation for supercomputing systems
Date: 2021-03-19 13:16
Slug: job_8bd4e96b4104dbde3955b1f126eddfc2
Category: job
Authors: Davide Rovelli
Email: davide.rovelli@ec-nantes.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Nantes
Job_Duration: 3 à 6 mois
Job_Website: https://ici.ec-nantes.fr/english-version/join-us
Job_Employer: Ecole Centrale Nantes - Institut de Calcul Intensif
Expiration_Date: 2021-07-01
Attachment: job_8bd4e96b4104dbde3955b1f126eddfc2_attachment.pdf

L&#39;institut de Calcul Intensif recherche un stagiaire en développement logiciel dans le but de poursuivre la réalisation d&#39;un portail de calcul intensif : une application web PHP associée à une API NodeJS pour la gestion des utilisateurs et la soumission de calculs à distance.

Informations complémentaires [ici](https://ici.ec-nantes.fr/medias/fichier/ligerion-internship_1615819058107-pdf?ID_FICHE=225128&amp;INLINE=FALSE).