Title: HPC and digital twins in metallurgy - 3D front-tracking modeling of evolving interface networks
Date: 2021-04-02 08:38
Slug: job_aa103d4712b616e83f0d6fbdcc3adf0b
Category: job
Authors: Bernacki
Email: marc.bernacki@mines-paristech.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Sophia Antipolis
Job_Duration: 3 ans
Job_Website: 
Job_Employer: MINES ParisTech
Expiration_Date: 2021-09-30
Attachment: job_aa103d4712b616e83f0d6fbdcc3adf0b_attachment.pdf

One of the European Union’s objectives in climate change consists of reaching net-zero greenhouse gas emissions by 2050. Such perspective puts the metallic materials industry, as a large contributor to carbon emissions, under tremendous pressure for change and requires the existence of robust computational materials strategies to enhance and design, with a very high confidence degree, new metallic materials technologies with a limited environmental impact. From a more general perspective, the in-use properties and durability of metallic materials are strongly related to their microstructures, which are themselves inherited from the thermomechanical treatments.

Hence, understanding and predicting microstructure evolutions are nowadays a key to the competitiveness of industrial companies, with direct economic and societal benefits in all major economic sectors (aerospace, nuclear, renewable energy, and automotive industry).

Multiscale materials modeling, and more precisely simulations at the mesoscopic scale, constitute the most promising numerical framework for the next decades of industrial simulations as it compromises between the versatility and robustness of physically-based models, computation times, and accuracy.  The digimu consortium is dedicated to this topic at the service of major industrial companies.

In this context, the efficient and robust modeling of evolving interfaces like grain boundary networks is an active research topic, and numerous numerical frameworks exist [1]. In the context of hot metal forming and when large deformation of the calculation domain and the subsequent migration of grain boundary interfaces are involved, a new promising, in terms of computational cost, 2D front tracking method called ToRealMotion algorithms [2,3] was recently developed.

This PhD will be firstly dedicated to developing a 3D ToRealMotion algorithm. If the extension of the data structure will be quite natural, the 3D meshing/remeshing procedures/operators enabling to preserve valid data structure, a good quality of the finite element mesh while remaining frugal in terms of numerical cost remain to be invented. 

Moreover, kinetics equations behind the interface networks migration will be enriched to increase the number of modeled physical mechanisms. Finally, a supervised neural network-based remeshing strategy will also be developed to improve repetitive and non-optimal operations in the existing remeshing procedures. The developments will be validated thanks to pre-existing experimental and numerical data concerning the evolution of grain boundary interfaces during recrystallization and related phenomena for different materials. They will also be integrated in the [DIGIMU](https://www.transvalor.com/fr/digimu) software.

[1]A. Rollett, G. S. Rohrer, and J. Humphreys, Recrystallization and Related Annealing Phenomena. 3rd Edition, 2017.

[2]S. Florez, K. Alvarado, and M. Bernacki. A new front-tracking lagrangian model for the modeling of dynamic and post-dynamic recrystallization. Modelling and Simulation in Materials Science and Engineering, In press, 2021.

[3]S. Florez, K. Alvarado, D. Pino Muñoz and M. Bernacki. A novel highly efficient lagrangian model for massively multidomain simulation applied to microstructural evolutions. Computer Methods in Applied Mechanics and Engineering, 367:113107, 2020.
