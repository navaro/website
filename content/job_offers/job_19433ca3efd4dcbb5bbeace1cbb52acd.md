Title:  Ingénieur F/M - Outils numériques parallèles pour le calcul scientifique : étude et implantation
Date: 2021-06-02 20:02
Slug: job_19433ca3efd4dcbb5bbeace1cbb52acd
Category: job
Authors: Luc Giraud
Email: luc.giraud@inria.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Talence
Job_Duration: 12 mois (renouvelable)
Job_Website: https://recrutement.inria.fr/public/classic/fr/offres/2021-03751
Job_Employer: Inria
Expiration_Date: 2021-08-25
Attachment: 


**A propos du centre ou de la direction fonctionnelle**

Les avancées significatives en terme de simulation numérique ont toujours été liées aux paliers importants franchis par les technologies du HPC. Après le seuil des machines teraflops des années 1990 et actuelles, la communauté scientifique se prépare  à utiliser de manière généraliste les architectures pétaflops et même exaflops dans les années 2020. Pour que des codes applicatifs puissent exploiter de telles puissances de calcul en utilisant des centaines de millions  de coeurs de calcul en passant effectivement à l&#39;échelle, il est nécessaire de repenser les modèles physiques, leur modélisation mathématique et les algorithmes associés, ainsi que faire une mise en oeuvre permettant d&#39;exploiter tous les niveaux de parallélisme de l&#39;architecture. Le traitement des données pour ces simulations sera aussi un problème critique vue les tailles qui sont en jeu.

Dans ce contexte, l&#39;objectif de HiePACS est de développer des compétences pluridisciplinaires de pointe en mathématiques appliquées et en informatique du HPC pour traiter des simulations frontières multi-échelles sur les machines petaflops et exaflops qui vont être disponibles bientôt.
Contexte et atouts du poste

Ces travaux seront réalisés au sein de l’équipe HiePACS   qui est spécialisée en particulier dans la conception et la mise en œuvre de briques logicielles hautes performances pour des applications de simulation numérique. La validation des méthodes étudiées et leurs passage à l&#39;échelle sera réalisée via l’intégration du solveur dans des codes applicatifs en collaboration avec des équipes partenaires en France et en Europe.
Mission confiée

Depuis de nombreuses années, l’équipe développe un savoir-faire algorithmique important pour le calcul hautes performances dans des bibliothèques. Ces solveurs ont notamment permis à nos utilisateurs de résoudre de manière efficace sur un grand nombre de coeurs de calcul des systèmes linéaires d’équations issus d’applications pour des cas réels 3D.

**Les principales tâches sont orientées vers les applications :**

- Contribution au développement méthodologique et à l’intégration de nouvelles méthodes numériques pour améliorer le comportement numérique de ces solveurs tout en réduisant leurs coûts mémoire et calculatoire
- Étude et algorithmes d&#39;algèbre linéaire numérique couvrant les aspects d&#39;analyse numérique (par exemple, calcul en précision multiple, schémas d&#39;ortogonalisation par blocs, ...).
- Mise en œuvre d&#39;un code avancé utilisant des paradigmes de programmation innovants tels que la programmation par  tâches sur moteurs d&#39;exécution pour contribuer aux bibliothèques open-source développées à l&#39;Inria Bordeaux (par exemple, Maphys, Fabulous, ...)
- Contribution à la publication et à la présentation les résultats de la recherche dans des revues à fort impact et des conférences de haut niveau dans les domaines du calcul parallèle, de l&#39;algèbre linéaire numérique et des domaines d&#39;application utilisateurs de nos logiciels.


**Principales activités**

- Etude et développement de solveurs linéaires parallèles
- Collaborations avec les équipes de développement en vue d&#39;identifier les possibles goulots détranglement  des performances

**Compétences**

Formation intiale en science computationnelle (mathématique appliquée, calcul scientifique ou informatique).

Calcul et algorithmique hautes performances ; programmation parallèle et distribuée ; algèbre linéaire  numérique ; Python; C++; MPI.

- Maîtrise du langage de programmation Python, C et/ou C++
- Excellentes aptitudes à la communication orale et écrite, particulièrement en anglais

**Avantages**

- Restauration subventionnée
- Transports publics remboursés partiellement
- Possibilité de télétravail et aménagement du temps de travail
- Équipements professionnels à disposition (visioconférence, prêts de matériels informatiques, etc.)
- Prestations sociales, culturelles et sportives (Association de gestion des œuvres sociales d&#39;Inria)
- Accès à la formation professionnelle
- Sécurité sociale

**Rémunération**

2834€ brut mensuel
