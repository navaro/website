Title: Architecte de solutions de traitement de données dans le cadre du projet européen PHIDIAS 
Date: 2019-11-28 16:38
Slug: job_a9fc6612240bbd096be3ec4a12078f21
Category: job
Authors: CINES / Equipe Projet PHIDIAS 
Email: recrutement@cines.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: CINES - Montpellier
Job_Duration: 3 ans
Job_Website: https://www.phidias-hpc.eu/ https://www.cines.fr
Job_Employer: CINES Centre Informatique National de l&#39;Enseignement Supérieur
Expiration_Date: 2020-02-20
Attachment: job_a9fc6612240bbd096be3ec4a12078f21_attachment.pdf

Rattaché(e) au Département Calcul Intensif (DCI), vous travaillerez avec une équipe technique dédiée au calcul intensif sur machine massivement parallèle. Le projet PHIDIAS est découpé en 7 Work Packages. Le CINES, qui pilote le projet, gère aussi les work packages 1 et 2, qui sont subdivisés en sous-tâches. Vous devrez ainsi piloter la sous-tâche 2.1 dédiée au HPC&amp;HPDA et qui est adossée aux « data sciences ». Vous serez aussi amené(e) à participer aux tâches 2.2 (Stockage et archivage pérenne) et 2.3 (On the fly computing).  En tant qu’architecte de solutions pour le traitement de données, vos principales missions seront les suivantes : 
• Etablir des liens avec les partenaires du projet afin de comprendre et formaliser les besoins des communautés ; 
• Déterminer la meilleure manière d’y répondre et travailler avec le work package WP3 autour des données nécessaires à leur mise en œuvre ; • Analyser et implémenter des stratégies et des outils d’échanges de données ;
• Définir et participer à la mise en place une infrastructure dédiée aux traitements des données ; • Proposer et adapter des modèles et outils d&#39;analyse de données : analyses sur-mesure, visualisation, etc… ;
• Partager les recherches et résultats avec le reste de l’équipe ; • Participer à « l&#39;évangélisation » des différentes communautés scientifiques partenaires de PHIDIAS pour l’adoption des solutions déployées ; 
• Réaliser une veille sur les méthodes d’échange et de gestion des flux de données. 