Title: Branchement d’une interface de solveurs linéaires dans un code éléments finis mixtes hybrides (EFMH)
Date: 2020-10-06 10:32
Slug: job_9ecfc0300761124b930883542070c3f4
Category: job
Authors: Bernard Vialay
Email: bernard.vialay@andra.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Andra (Chatenay-Malabry) & IFPEN (Rueil-Malmaison)
Job_Duration: 6 mois
Job_Website: https://www.andra.fr/
Job_Employer: Andra
Expiration_Date: 2020-12-29
Attachment: job_9ecfc0300761124b930883542070c3f4_attachment.pdf

**1. Contexte du projet**

TRACES (Transport RéActif de Contaminant dans les Eaux Souterraines) est un code de calcul utilisant une méthode d’éléments finis mixtes hybrides pour la résolution d’écoulement monophasique et de transport de soluté en milieu saturé ou insaturé par diffusion, convection, dispersion en prenant en compte les phénomènes de précipitation/dissolution, d’adsorption et de filiation des radionucléides. Initialement développé par l’IMFS, sa mise en oeuvre à l’Andra dans le cadre des études liées au stockage des déchets radioactifs conduit à la résolution de systèmes linéaires à plusieurs millions d’inconnues. Cette résolution est effectuée par différents solveurs parallèles issus de librairies déjà interfacées dans TRACES, dont la librairie mpi HYPRE (LLNL) et la librairie hybride mpi+openmp MAPHYS (INRIA). 

ALIEN est un outil de génie logiciel pour les codes de simulation numérique et les bibliothèques de solveurs linéaires. Il propose une nouvelle API pour interagir avec des bibliothèques de solveurs linéaires. En effet, bien que plusieurs approches soient apparues pour faciliter le développement de codes de calculs parallèles, par exemple en proposant une description abstraite du parallélisme, Il reste à la charge du développeur de mettre en place le branchement avec les bibliothèques d’algèbre linéaire. Cela nécessite de mettre en place des structures de données spécifiques pour chaque bibliothèque et de réaliser un branchement (parfois complexe) pour chacune. Afin de contourner cette difficulté, ALIEN propose une interface unique pour accéder aux principales bibliothèques d’algèbre linéaire de la communauté (PetSc, Trilinos, HYPRE, MTL…). ALIEN permet donc : 

* de pouvoir basculer entre les solveurs linéaires et les algorithmes au runtime ;
* de rendre le code d&#39;application indépendant d&#39;un solveur linéaire spécifique ;
* de traiter facilement les assemblages complexes qui se produisent dans les codes multi-physiquesétroitement couplés.

Alors que PETSc ou Trilinos donnent déjà accès à un large éventail de solveurs et de bibliothèques, leurs approches sont encore pour la plupart &#34; orientées solveurs &#34;, et profondément liées aux mises en oeuvre. La solution Alien est un wrapper C++ sur ces bibliothèques de solveurs linéaires : nous n&#39;implémentons aucun solveur, nous fournissons juste l&#39;accès aux solveurs externes. Son concept clé de conception est la notion de multi représentations.

**2. Objectifs du stages**

L’objectif de ce stage est le branchement d’ALIEN dans le code TRACES. Pour ce faire le programme de stage sera : 

* Familiarisation avec le code TRACES (EFMH) et l’interface ALIEN. Lors de cette étape, à partir d’une basede cas tests, le stagiaire définira des cas de référence qui seront in fine utilisés pour valider le branchementde l’interface ALIEN ;
* Branchement d’ALIEN dans TRACES et documentation de l’implémentation ;
* Validation sur les cas de référence : comparaison entre la résolution directe avec HYPRE et la résolutionavec HYPRE via ALIEN ;
* Comparaison avec les nouveaux solveurs disponibles via ALIEN (PETSC, …)
