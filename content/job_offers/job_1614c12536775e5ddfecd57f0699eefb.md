Title: Poste d&#39;ingénieur en calcul scientifique pour l&#39;océanographie (mobilité interne CNRS à Grenoble)
Date: 2020-12-08 08:21
Slug: job_1614c12536775e5ddfecd57f0699eefb
Category: job
Authors: Thierry Penduff
Email: Thierry.Penduff@cnrs.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Grenoble
Job_Duration: 
Job_Website: https://meom-group.github.io/
Job_Employer: CNRS
Expiration_Date: 2021-01-15
Attachment: 

L&#39;ingénieur·e en calcul scientifique pilotera, au sein de l&#39;équipe MEOM de l&#39;IGE, la réalisation de simulations océan-banquise haute résolution, utilisant la plateforme de modélisation communautaire NEMO parfois couplée à d&#39;autres modèles (d&#39;atmosphère, ou de calotte polaire). Certaines de ces simulations amèneront NEMO aux frontières de ses capacités en termes de fonctionnalités et de résolution. 

Une autre mission de l&#39;ingénieur·e sera d&#39;optimiser NEMO sur les centres de calcul nationaux et européens, de veiller à l&#39;intégration des développements réalisés sur NEMO à l&#39;IGE et à leur transfert vers les instances de développement du code, et à piloter les développements des outils de pré-/post-traitement de ces simulations. 

Au travers et au-delà des 2 premières, la troisième mission de l&#39;ingénieur·e sera de contribuer à la conception et l&#39;exploitation des missions spatiales d&#39;observation des océans et des systèmes de prévision opérationnelle.

Tous les détails sont disponibles sur [cette page](https://mobiliteinterne.cnrs.fr/afip/owa/consult.affiche_fonc?code_fonc=F58028&amp;type_fonction=&amp;code_dr=&amp;code_bap=E&amp;code_corps=IR&amp;nbjours=&amp;page=1&amp;colonne_triee=1&amp;type_tri=ASC)