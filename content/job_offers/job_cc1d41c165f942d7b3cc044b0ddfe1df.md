Title: Informaticien-développement applications système (H/F)
Date: 2020-08-26 11:56
Slug: job_cc1d41c165f942d7b3cc044b0ddfe1df
Category: job
Authors: Geneviève Morvan
Email: communication@idris.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Orsay (91)
Job_Duration: 12 mois
Job_Website: http://www.idris.fr
Job_Employer: IDRIS - CNRS
Expiration_Date: 2020-11-18
Attachment: 


**Missions**

L&#39;IDRIS renforce son équipe système composée de 7 ingénieurs, par un ingénieur (H/F) qui participera à des développements systèmes conduisant à la refonte complète de l&#39;application centrale de gestion des ressources et des utilisateurs.

**Activités**

- Étude de l&#39;application actuelle de gestion des ressources et des utilisateurs
- Détermination de l&#39;infrastructure et de la méthodologie de développement
- Recueil du besoin auprès des différentes équipes et établissement du cahier des charges
- Conception et implémentation de la nouvelle application et de ses interfaces web
- Transfert de compétences aux opérateurs/administrateurs/développeurs de l&#39;IDRIS en relation avec l&#39;application

**Plus d&#39;information sur le poste et sur l&#39;IDRIS :  <http://www.idris.fr/annonces/idris-recrute.html>**
