Title: Modélisation du comportement de matériaux composites soumis à des chargements thermomécaniques
Date: 2020-02-19 15:28
Slug: job_2f4462b790a4a4c07cfa10612d73bc83
Category: job
Authors: Nait-ali Azdine
Email: azdine.nait-ali@ensma.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: 
Job_Duration: 36 mois
Job_Website: 
Job_Employer: Institut Pprime 
Expiration_Date: 2020-05-13
Attachment: job_2f4462b790a4a4c07cfa10612d73bc83_attachment.pdf

Le but est de pouvoir fournir un outil prédictif à même de simuler le comportement thermomécanique de structures quelconques soumises simultanément à un chargement mécanique et à un flux de chaleur, où dégradation thermique, endommagement mécanique et transferts de chaleur sont couplés, tout en limitant les temps de calcul. Le développement d’un élément fini incluant le champ de phase comme degré de liberté supplémentaire sera la dernière partie du travail de thèse. Cette implémentation sera faite dans le code par éléments finis Foxtrot interne au laboratoire. Il pourra se baser sur les développements faits lors d’une récente thèse  pour le développement d’un élément fini adapté à cette étude et sur l’expérience de l’unité de recherche dans le calcul de structures soumises au feu.

Sur le volet numérique, le doctorant bénéficiera de compétences en mécanique de chercheurs de l’équipe Endommagement et Durabilité et en décomposition thermique et combustion, de chercheurs de l’équipe Combustion Hétérogène :
-	Appui de deux enseignants-chercheurs et un ingénieur de recherche de l’équipe, spécialistes en méthodes numériques et en simulation par éléments finis. 
-	Appui de trois enseignants-chercheurs en modélisation numérique de la décomposition thermique et de la combustion.
