Title: Mise en œuvre d'un logiciel coupleur pour des problèmes multi-physiques
Date: 2021-03-18 13:01
Slug: job_ce1ba6a5f203f8d161b61c6d69089e23
Category: job
Authors: Daniele Colombo, Ani Anciaux Sedrakian, Bernard Vialay, Stéphane de Chaisemartin
Email: daniele.colombo@ifpen.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Rueil-Malmaison
Job_Duration: 12 mois
Job_Website: https://www.ifpenergiesnouvelles.fr, https://www.andra.fr
Job_Employer: IFPEN
Expiration_Date: 2021-06-10
Attachment: job_ce1ba6a5f203f8d161b61c6d69089e23_attachment.pdf

**Sujet du post-doc : Mise en œuvre d’un logiciel coupleur pour des problèmes multi-physiques**

*(English version below)*

La mise à disposition croissante de codes de simulation open-source incite à étudier comment les utiliser pour construire des applications multi-physiques au travers d’un outil de couplage capable de gérer les transferts d’informations entre les codes, afin de mettre en place un schéma de couplage adapté aux phénomènes physiques concernés.
Dans la simulation d’applications multi-physiques, le temps de calcul est un enjeu majeur et nécessite l’utilisation du calcul haute performance. Dans ce contexte, l’algorithme de couplage est basé sur l&#39;échange et le traitement d&#39;informations entre deux ou plusieurs codes parallèles s’appuyant sur des grilles de calcul, des algorithmes et des discrétisations temporelles différentes selon le phénomène physique à simuler.
Les transferts d’informations doivent être pensés pour maintenir l&#39;évolutivité du code tout en assurant la précision numérique. Le transfert de données d&#39;une interface distribuée vers une autre sur un très grand nombre de cœurs de calcul est un défi majeur et les solutions existantes ne sont pas encore optimales.
Dans le contexte de la simulation géomécanique, une preuve de concept de couplage entre un code de simulation mécanique et un code de simulation d’écoulement en milieu poreux a été mise en œuvre à IFPEN. Après une première étape d’analyse de cet existant, l’objectif du post-doctorat sera d’y apporter une double rupture, afin de lever les verrous de performance numérique et informatique de ce schéma de couplage, ainsi que d’en améliorer sa robustesse et si possible son évolutivité.

- Tout d’abord, la solution en place aujourd’hui repose sur un couplage par fichiers, ce qui peut imposer d’importantes latences sur les transferts de données. Le premier défi sera donc de mettre en place une technologie moderne de couplage permettant de lever cette première limitation. Ce développement devra également permettre de mieux isoler l’interface de couplage entre les codes et de la rendre plus facilement évolutive.
- Le schéma de couplage maître-esclave séquentiel actuellement en place n’offre qu’une faible scalabilité parallèle. La seconde limitation que nous souhaitons lever concerne donc la performance du couplage lors d’un calcul parallèle sur les architectures modernes. La vraie difficulté de ce nouveau défi est que, si le coupleur mis en place pourra fournir les technologies parallèles adaptées, ses capacités parallèles ne pourront être exploitées sans modifier l’algorithme de couplage numérique existant, ce qui s’avère être très intrusif dans les codes couplés. Le post-doctorant devra donc étudier la possibilité de mettre en place de nouveaux algorithmes numériques de couplage. Ses travaux devront permettre de comprendre s’il est possible de trouver un nouvel algorithme plus propice à une décomposition parallèle et qui, appliqué à notre cas de couplage, donne les résultats attendus de façon robuste. Le post-doctorant étudiera pour cela la possibilité de modifier le schéma de communication entre les méthodes couplées. Il pourra également réfléchir à la mise en place de nouveaux algorithmes de couplage en remplacement de la méthode de point fixe utilisée actuellement.

Programme du post-doc (en fonction de la durée) :

- Etude bibliographique des coupleurs (ex : CWIPI, Padawan, Precice …) ;
- Familiarisation avec les simulateurs à coupler et le code couplé existant, ainsi que avec la physique des phénomènes couplés ;
- Identification d’un logiciel coupleur pertinent ;
- Réalisation d’un POC pour le cas d’usage défini :

    - couplage géomécanique entre un code d’écoulement en milieu poreux, et un simulateur mécanique, comme le Code_Aster.

- Etude de l’implantation d’un schéma de communication direct à la place de celui centralisé ;
- Etude de l’utilisation d’algorithmes itératifs de couplage plus performants que celui du point fixe pour les problèmes non-linéaires.

Niveau requis : connaissances en C/C++, Fortran 90/2003, Python, analyse numérique, couplage de codes, Linux
Durée : 12 mois
Moyens mis à disposition : cluster de calcul Linux de l’IFPEN
Encadrement : équipes pluridisciplinaires à compétences mathématiques, physiques et informatiques.
Localisation : IFPEN (Rueil-Malmaison) et Andra (Chatenay-Malabry)
Contact : Bernard Vialay : <Bernard.Vialay@andra.fr>, Daniele Colombo : <daniele.colombo@ifpen.fr> et Ani Anciaux-Sedrakian : <ani.anciaux-sedrakian@ifpen.fr>
 
**Postdoctoral position: implementation of a software coupling tool for multi-physics problems**

The increasing availability of open-source simulation codes facilitates the creation of multi-physics applications by means of a coupling tool capable of managing information transfers between codes in order to set up a coupling scheme adapted to the physical phenomena to simulate.
In the simulation of multi-physics applications, computing time is a major issue and requires the use of high-performance computing. The coupling algorithm is based on the exchange and processing of information between two or more parallel codes, each one using its own calculation grid, algorithms and temporal discretization adapted to the physical phenomenon to simulate.
Information transfers must be designed to maintain the code scalability and to preserve numerical precision. The transfer of data from one distributed interface to another one over a very large number of computing cores is a major challenge and existing solutions are not yet optimal.
In the context of geomechanical simulations, a proof of concept of coupling between a mechanical simulation code and a porous media flow simulation code has been implemented at IFPEN. After an analysis of the existing coupled code, the postdoctoral fellow will develop a new coupling tool in order to increase the performance of the existing coupled code, to improve its robustness and, possibly, its scalability:

- the data exchange strategy adopted by the existing coupled code is based on files, often originating significant latencies on data transfers. The postdoctoral fellow will overcome this limitation by implementing a new coupling tool based on a modern coupling technology. This development should also make it possible to better isolate the coupling interface between codes and make it more easily scalable.
- The sequential master-slave coupling scheme currently in place in the existing coupled code offers only a low degree of parallel scalability. The postdoctoral fellow will propose a solution to increase the parallel performance of the coupled code on modern architectures. The real difficulty of this challenge is the fact that it will not be possible to increase parallel performance by exploiting features available in the coupling interface without modifying the coupled codes. Indeed modifications of the coupled codes are expected to be very intrusive. The postdoctoral fellow will study a new coupling algorithm adapted to parallel decomposition. This new algorithm must be able to reproduce the results of the existing coupled code in a robust way. The postdoctoral fellow will probably need to modify the communication scheme between the coupled codes and to replace the fixed point method used in the existing coupled code.

Research program:

- study of existing coupling libraries (e.g.: CWIPI, Padawan, Precice) in order to be able to select the most suitable one for the new coupling tool;
- study of the existing coupling tool and of the coupled codes, analysis of the physics of the coupled phenomena;
- selection of the coupling library to use for the new coupling tool;
- realization of a POC for the defined use case:

    - geomechanical coupling between a flow code in a porous medium and a mechanical simulator, such as Code_Aster.

- study of the implementation of a direct communication scheme replacing the centralized one used in the existing coupling tool;
- study of the possibility to use more efficient iterative coupling algorithms for non-linear problems by replacing the fixed point algorithm currently used in the existing coupled code.

Required skills: knowledge of C/C++, Fortran 90/2003, Python, numerical analysis, code coupling, Linux
Duration: 12 months
Simulation environment: IFPEN Linux HPC cluster
Supervision: multidisciplinary teams with mathematical, physical and computer skills.

Location: IFPEN (Rueil-Malmaison) and Andra (Chatenay-Malabry)

Contact: Bernard Vialay: <Bernard.Vialay@andra.fr>, Daniele Colombo: <daniele.colombo@ifpen.fr> and Ani Anciaux-Sedrakian: <ani.anciaux-sedrakian@ifpen.fr>

