Title: Développment de méthodes de réduction de temps de calcul pour  la modélisation de procédés en élasto-plasticité non linéaire 
Date: 2021-09-10 14:57
Slug: job_f673da05aeee030b613b0a9e5bf74c05
Category: job
Authors: Katia Mocellin
Email: katia.mocellin@mines-paristech.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Sophia Antipolis
Job_Duration: 18 mois
Job_Website: 
Job_Employer: CEMEF - Mines parisTech
Expiration_Date: 2021-12-03
Attachment: 

**Incremental processes/CPU time**. 
Let’s consider large parts (up to a meter of diameter), let deform it by applying local and evolving contact with tools. Imagine pottery on metals at room temperature\! It will result in large deformation of the initial part but rotating speed, feeding rate, tool geometry have to be optimized.
From a computational point of view, you have an evolving mechanical configuration with local contact areas and plastic deformation resulting in numerous large non linear systems to solve\!
CPU time is thus excessively high even considering iterative solvers and parallel computing.
For this kind of models, CPU time can reach up to several weeks.

**A collaborative work.** 
This work will part of the FUI project, *FUOFAB* which aims to develop incremental processes for production of large thick parts for aeronautical or spatial industry.
It gathers, in a partenarial project:

- an industrial partner aiming at produce large parts for aerospace or energy applications with the use of a new flow forming device, 
- a software editor providing computational tools for metal forming simulation
- A SME involved in the real time analysis of production;
- the CEMEF working on improvement of numerical methods for the simulation of the process.
CEMEF is a well recognized laboratory in the domain of computational mechanics.

The main industrial challenge is to set up and control the process to lead efficiently to large and near net shape parts avoiding massive machining.
Simulation is an important lever of action to improve the development .

**PostDoc Goal**. 
The main objective is to develop numerical methods for reducing CPU time first with uncoupling strategies for local plastic deformation with regard to global elastic displacement. The idea is to limit the non linear resolution to the area where it is needed only. Improvement will be studied for the lagrangian approach of the software. ALE formulation could also be improved with respect to fiabilisation of the computed mechanical response and reduction of CPU time by mean of elastic and plastic domain decomposition.

**Candidate Profile**.

1.	PhD in computational mechanics
2.	Knowledge in parallel computation and finite elements
3.	Experience in publishing research papers.



