Title: Stage ingénieur : optimisation de code séquentiel en fortran
Date: 2020-11-13 15:35
Slug: job_c5bd5987e00b386f4a6198143e374148
Category: job
Authors: Céline Blitz Frayret
Email: celine.blitz@cirad.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Montpellier
Job_Duration: 6 mois
Job_Website: https://www.umr-ecosols.fr/
Job_Employer: CIRAD
Expiration_Date: 2021-04-05
Attachment: 

**Contexte**

Afin de contribuer à la modélisation des bilans de carbone, d&#39;eau et d&#39;énergie des systèmes couplés sol-plante-atmosphère étudiés par l&#39;UMR Eco&amp;Sols, le modèle MAESPA est utilisé pour prendre en compte l&#39;hétérogénéité inter-arbre et la variabilité spatiale intra-parcelle.
Ce modèle séquentiel écrit en Fortran a récemment été amélioré sur le plan scientifique par un calcul plus précis de la température de la canopée, à l&#39;origine d&#39;une forte augmentation des temps de calcul.

**Objectif**

Une première approche d&#39;optimisation en temps a été réalisée à partir d&#39;étude deprofilage du temps passé dans les subroutines, offrant des réductions de 30% du temps de calcul sur des simulations courtes, mais non transposable sur des simulations longues.
Après une période de prise en main et d&#39;analyse du code source du logiciel, l&#39;objectif du stage est de proposer et de tester des voies d&#39;optimisation des temps de calcul, en se basant sur l&#39;analyse du comportement du logiciel (temps et mémoire) et de sa structure. Des bancs d&#39;essais sur des simulations types seront réalisés afin d&#39;approuver les implémentations testées.

**Profil**

Formation Master ou équivalent en informatique, développement logiciel.

**Compétences souhaitées**

Programmation Fortran
Environnement Linux
Outils de profilage et debugger (par exemple Gprof, Maqao, Valgrind, GDB)

**Compétences optionnelles appréciées**

Git

**Environnement de travail**

Le stage sera réalisé au Cirad au sein de l&#39;UMR Eco&amp;Sols à Montpellier, sous la responsabilité de l’ingénieure responsable du code et en collaboration avec le chercheur référent scientifique du modèle MAESPA. Après une première prise de contact en présentiel dans le respect des règles sanitaires, une forte proportion de ce stage pourra être réalisée en télétravail, notamment par l&#39;intermédiaire de la forge logicielle de l&#39;unité et par l&#39;utilisation d&#39;un serveur distant lors des exécutions des simulations de validation.

