Title: un-e ingénieur-e d’étude en deep learning
Date: 2021-03-31 06:31
Slug: job_e0b5d908144b9b05e95a41b2968c48c0
Category: job
Authors: Ludovic Granjon
Email: ludovic.granjon@u-bourgogne.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Dijon
Job_Duration: 6 mois
Job_Website: 
Job_Employer: université de Bourgogne - MSH de Dijon
Expiration_Date: 2021-05-14
Attachment: job_e0b5d908144b9b05e95a41b2968c48c0_attachment.pdf

**ENVIRONNEMENT ET CONTEXTE DE TRAVAIL :**

L’Ingénieur-e d’étude travaillera au sein de la plateforme technologique géomatique Bourgogne-Franche-Comté (GEOBFC) à la Maison des Sciences de l’Homme de Dijon (USR uB-CNRS 3516). GEOBFC est une plateforme commune à la MSH de Dijon et à la Maison des Sciences et de l’Environnement de l’Université de Franche-Comté.

GEOBFC a pour vocation de soutenir les équipes de recherche, les doctorants en Sciences Humaines et Sociales (SHS) et les partenaires institutionnels des deux MSH dans le développement et la conduite des programmes de recherche pluridisciplinaires, de mutualiser des outils et des compétences spécialisées, de diffuser et de partager les compétences et les connaissances via des formations dans les domaines de l’information géographique. Les champs de compétences sont l’acquisition de données géographiques, l’élaboration et le traitement des données, la télédétection, la conception de systèmes d’information géographique et d’outils d’analyses et la restitution cartographique.

La plateforme GEOBFC a récemment fait l’acquisition d’un relevé LiDAR sur l&#39;ensemble du territoire du Parc Naturel Régional du Morvan et les communes du Vézelien d’environ 3 300 km². Sur cet espace, une équipe de recherche s’intéresse à la détection automatique de structures anthropiques sous forêt, en particulier les charbonnières et les minières / zones d’extraction.

**MISSIONS CONFIEES A L&#39;AGENT :**

A partir des Modèles Numériques de Terrain (MNT), l’Ingénieur-e d’étude aura pour mission de mettre en œuvre des solutions de deep learning ayant pour objectif de détecter deux types de structures : les charbonnières d’une part et les mines / zones d’extraction d’autre part sur la zone d’étude du Morvan (environ 3 300 km²). A cet effet, une base de données de 22 000 charbonnières dans des zones hors du massif du Morvan pourra être utilisée dans une perspective d’apprentissage du modèle. Pour les mines et zones d’extraction, la création de ces données d’apprentissage sera nécessaire.

**DESCRIPTION DES ACTIVITES :**

- Définir et développer les modèles
- Définir les indices issus du MNT à utiliser dans les modèles
- Réaliser l’apprentissage et comparer en modifiant les hyperparamètres
- Sauvegarde et comparaisons des modèles
- Création du jeu d’entraînement pour les minières / zone d’extractions
- Définir la stratégie pour lancer la détection sur l’ensemble de la zone d’étude

**COMPETENCES SOUHAITEES :**

- Connaissance du développement informatique (langage python en particulier)
- Connaissance en machine learning / deep learning
- Connaissance en traitement du signal/de l’image
- Connaissance des SIG appréciée
- Aptitude au travail en équipe
- Autonomie
- Rigueur
- Aptitude rédactionnelle

**DIPLOME SOUHAITE :**

- École d’Ingénieur, Master ou équivalents 

**Contrat :** CDD de 6 mois

**Rémunération :** 1816 € brut/mois

Les candidatures, qui comporteront une lettre de motivation et un Curriculum Vitae, sont à adresser par mail à : ludovic(dot) granjon  (at) u-bourgogne (dot) fr 

Les candidatures seront étudiées au fur et à mesure de leur arrivée

Poste à pourvoir le plus rapidement possible, au plus tard en mai 2021

Pour de plus amples renseignements, vous pouvez contacter Ludovic Granjon : ludovic(dot) granjon  (at) u-bourgogne (dot) fr ou au 03 80 39 35 96
