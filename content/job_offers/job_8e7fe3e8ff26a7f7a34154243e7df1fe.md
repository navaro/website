Title: Ingénieur en calcul haute performance H/F (x12)
Date: 2021-06-04 08:59
Slug: job_8e7fe3e8ff26a7f7a34154243e7df1fe
Category: job
Authors: Wilfried Kirschenmann
Email: wkirschenmann@aneo.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Boulogne-Billancourt
Job_Duration: 
Job_Website: https://www.linkedin.com/jobs/view/2476283467/?alternateChannel=search&amp;refId=TZNeaJKKJ5C6ixSWIrr3TQ%3D%3D&amp;trackingId=8a3HKrNzYR9x96tH4H95eQ%3D%3D
Job_Employer: ANEO
Expiration_Date: 2021-08-27
Attachment: 

Notre communauté Advanced Computing Technologies (ACT) composée en partie d&#39;experts en Intelligence Artificielle, Big Data, Cloud et Calcul Haute Performance souhaite s&#39;étoffer d&#39;autre(s) ingénieur(s) en calcul et haute performance H/F \!

### Missions &amp; activités

En tant que consultant(e) au sein d’ANEO, vous participerez, pour le compte de nos clients, à des projets qui seront pour la plupart basés à Paris ou en région parisienne. Cependant, nous sommes régulièrement sollicités sur d’autres parties du territoire français qui peuvent nécessiter des déplacements.

Vos activités consisteront par exemple à :

- Optimiser/paralléliser/porter des bibliothèques de calcul et en identifier les causes d’instabilité numérique ;
- Concevoir et mettre en œuvre une architecture cloud pour le calcul intensif ;
- Industrialiser une chaîne de calcul (CI/CD, conteneurisation…) ;

Des exemples de réalisations se trouvent sur le site d’Aldwin (<www.aldwin.eu>), notre marque dédiée aux technologies de calcul avancées.

### Profil

 Notre communauté (ACT) recherche donc une personnalité en adéquation avec son mode de vie : un profil curieux, ouvert, bon communicant, soucieux de faire grandir les plus jeunes et d’apprendre des plus expérimentés ; une personnalité qui a des compétences parmi les suivantes :

- Programmation parallèle
- C ou mieux, C++ : MPI, OpenMP, SIMD, TBB ;
- C# : TPL, SIMD, .Net Native ou CoreRT ;
- Analyse de performance : profileurs et outils de d&#39;analyse de codes parallèles et/ou distribués ;
- Architectures calcul haute performance : processeurs (x86, ARM,… ) et accélérateurs (CUDA, OpenACC, OneAPI) ;
- En calcul scientifique et numérique ;
- Aptitude à travailler en équipe avec des interlocuteurs d&#39;horizons et de niveaux différents ;
- Publication de travaux de recherche ;
- Anglais ;

Ces compétences sont diverses et nous ne recherchons personne qui dispose de l&#39;ensemble de ces compétences ; ce sont plutôt les compétences dont nous avons besoin dans l&#39;équipe et pour y parvenir, il nous faut donc des personnes disposant d&#39;une partie de ces compétences et ayant une appétance à discuter avec les autres.

La mise en œuvre de certaines de ces compétences dans le cadre d’une thèse sera un plus. 

### Infos pratiques

Démarrage souhaité : Dès que possible et jusqu’en septembre ou octobre ; pour les doctorants en fin de thèse, nous pouvons anticiper des recrutement jusque janvier 2022.

Remunération selon profil
