Title: Biomécanique de la peau humaine en données incertaines : Optimisation du maillage d’un domaine bi-matériau dans le but de délimiter la géométrie d’une tumeur nécessitant une exérèse chirurgicale
Date: 2020-01-20 08:38
Slug: job_8771612652d2b0d8274f60e7df3262de
Category: job
Authors: Arnaud LEJEUNE
Email: arnaud.lejeune@univ-fcomte.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Besancon avec quelques déplacements sur Dijon
Job_Duration: de 4 à 6 mois
Job_Website: 
Job_Employer: FEMTO-ST UMR CNRS 6174
Expiration_Date: 2020-04-13
Attachment: job_8771612652d2b0d8274f60e7df3262de_attachment.pdf

La modélisation éléments finis des tissus ou organes humains permet à la fois une compréhension accrue de leur physiologie et pathophysiologie, via l’identification de grandeurs mécaniques pertinentes, et aussi l’amélioration de dispositifs biomédicaux de prévention ou de traitement pour l’aide au chirurgien.
Dans le traitement des cicatrices chéloïdiennes, la quantification des champs de contraintes mécaniques dans un domaine de forme complexe plus ou moins définie et constitué de deux milieux matériels différents s’avère très utile. (...)
Les erreurs numériques associées à un calcul éléments finis peuvent être estimées a posteriori par des indicateurs locaux pour s’assurer de la qualité et de la densité du maillage généré à partir de la géométrie spécifiée. Ces erreurs peuvent être réduites en raffinant le maillage dans des régions trouvées par ces indicateurs. Dans le cas des contraintes planes, et en élasticité linéaire, quand une quantité d’intérêt est bien définie, des estimateurs d&#39;erreur de type DWR (« Dual Weighted Residuals ») [Becker &amp; Rannacher, 2001] appelés estimateurs « ciblés » ont montré leur efficacité sur des applications réelles en biomécanique comme la localisation de sources d’erreur induites dans le maillage d’un domaine comportant une hétérogénéité (paroi artérielle avec une nécrose [Duprez et al., 2020]).
Le présent projet vise à tester ces méthodes pour des tissus mous en hyperélasticité dans un cadre clinique en dermatologie [Elouneg et al. 2019]. Le travail consiste dans un premier temps à transposer l’approche développée par [Duprez et al., 2020] à un bi-matériau à comportement linéaire dans la plateforme FeniCs puis à l’étendre au cas hyperélastique. Il s’agit d’adapter le maillage de manière optimale minimisant l’erreur de discrétisation.
La seconde partie du travail visera à permettre l’identification de la zone chéloïdienne à partir de mesures expérimentales. On s’attachera à définir une nouvelle quantité d’intérêt permettant de localiser la transition entre la zone chéloïdienne et la peau saine.



**Profil recherché**
Ingénieur ou universitaire. Diplôme de master 2 ou master Ingénieur en mécanique numérique, ingénierie mathématique ou mathématiques appliquées.
**Compétences et spécialités**
Modélisation numérique, Simulation et programmation. Analyse numérique. Une motivation forte vers la mécanique et la biologie est fortement souhaitée.



**Références bibliographiques :**
[Becker &amp; Rannacher, 2001] R. Becker and R. Rannacher (2001). An optimal control approach to a posteriori error estimation in finite element methods, Acta Numerica, 10, 1-102.
[Bucki.et.al, 2010] M. Bucki, C. Lobos, and Y. Payan, A fast and robust patient specific finite element mesh registration technique: application to 60 clinical cases (2010). Medical Image Analysis, 14, 303-317. [Chambert et al., 2019] J. Chambert, T. Lihoreau, S. Joly, B. Chatelain, P. Sandoz, P. Humbert, E. Jacquet, G. Rolin (2019). Multimodal investigation of a keloid scar by combining mechanical tests in vivo with diverse imaging techniques, Journal of the Mechanical Behavior of Biomedical Materials, 99, 206-215.
[Duprez et al., 2020] M. Duprez, S.P.A. Bordas, M. Bucki, H.P. Bui, F. Chouly, V. Lleras, C. Lobos, A. Lozenski, P.H. Rohan and S. Tomar (2020). Quantifying discretization errors for soft tissue simulation in computer assisted surgery: A preliminary study. Applied Mathematical Modelling, 77, 709-723.
[Elouneg et al. 2019] A. Elouneg, D. Sutula, M. Sensale, F. Chouly, J. Chambert, A. Lejeune, D. Baroli, P. Hauseux, S.P.A. Bordas,and E. Jacquet. (2019). Mechanical parameters identification of keloid and surrounding healthy skin using Digital Image Correlation measurements in vivo, Congrès Français de Mécanique, Brest. [Jacquet et al., 2017] E. Jacquet, S. Joly, J. Chambert, K. Rekik, P. Sandoz (2017). Ultra-light extensometer for the assessment of the mechanical properties of the human skin in vivo. Skin Research and Technology, 23, 531-538