Title: HPC Software Engineer (C++)
Date: 2021-10-07 16:09
Slug: job_26ef5ea1f64b8d2101dd27512493c82f
Category: job
Authors: Sonia Portier
Email: emploi@nextflow-software.com
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Nantes, France
Job_Duration: 
Job_Website: https://www.nextflow-software.com/
Job_Employer: Siemens Industry Software
Expiration_Date: 2021-12-30
Attachment: 

Subsidiary of Siemens Digital Industry Software, Nextflow Software, is a Software Vendor headquartered in Nantes, France. 
Nextflow develops and sells advanced Computer-Aided Engineering (CAE) software in the field of Computational Fluid Dynamics (CFD)

Nextflow Software addresses engineering companies developing and manufacturing products/systems and equipment involving fluid flows, potentially 
with complex geometries and interactions with solids (e.g. moving parts, deformations), in the field of automotive, aerospace, marine, and many other industries.

Thanks to its talented team of researchers and engineers, and based on more than 10 years of close partnership with leading academic 
research laboratories from Ecole Centrale Nantes (ECN) and other universities, Nextflow Software is pushing the limits of hydrodynamics simulation. 

**Your Role &amp; Responsibilities**
Within Nextflow’s Solver team, you will guarantee and improve performance of massively parallel Computational Fluid Dynamics (CFD) solvers. You will also maintain and develop a new HPC solver framework for non-developer scientists. 

In this context, you will: 

- Manage new and existing benchmarks on workstations and supercomputers (with or without GPU) 
- Break down benchmark results, review code and write optimizations
- Collaborate to development kits and solver design to enable seamless and steady performance along time 
- Organize dialog with CFD R&amp;D engineer. Contribute to technical roadmaps and ensure technological watch in High Performance Computing (HPC) publications and in new libraries or compilers

**Your Profile**
You are a C++ developer and you are highly motivated in  modern C++ development and HPC. 
Innovative and pragmatic with good communication skills, you are a solution-oriented person. 
You have at least 3 years of experience in HPC. 

Autonomous, innovative and pragmatic with good  communication skills, you are a solution-oriented person. 

You are familiar with : 

- MPI communication protocol for parallel computing
- Cluster environment: Slurm, PBS, ssh scripting
- Performance profiling: Intel VTune/Advisor, Valgrind suite, Paraver, Vampir, Extrae, Score-P or others HPC analysis tools 
- Multithreading: OpenMP 
- SIMD vectorization and CPU architectures 
- C++ compilers : GCC, MSVC, nvcc 
- GPGPU: CUDA 
- Unit tests 
- Version control systems (Git) 

Good level of English.
Please apply online at <www.nextflow-software.com> or by email : <emploi@nextflow-software.com>
