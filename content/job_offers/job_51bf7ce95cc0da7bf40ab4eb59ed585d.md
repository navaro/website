Title: Post-doc/research engineer in medical data processing
Date: 2021-04-23 16:10
Slug: job_51bf7ce95cc0da7bf40ab4eb59ed585d
Category: job
Authors: Guillaume Dollé
Email: guillaume.dolle@univ-reims.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Reims
Job_Duration: 12 mois renouvelable
Job_Website: https://umr9008.pages.math.cnrs.fr/job/postdoc/pdf/2021_job_offer_eeg.pdf
Job_Employer: LMR/CReSTIC - Université de Reims
Expiration_Date: 2021-11-01
Attachment: job_51bf7ce95cc0da7bf40ab4eb59ed585d_attachment.pdf

### Post-doc/ingénieur de recherche traitement de données médicales

La personne recrutée sera intégrée dans le cadre d’un projet scientifique mené en partenariat entre leLaboratoire de Mathématiques de Reims (LMR) UMR CNRS 9008, le CReSTIC EA 3804, et le service de néonatologie du CHU de Reims. Ce projet est orienté sur la problématique de traitement de données issues de signaux EEG/aEEG, de données biologiques (et d’images IRM) pour le nouveau-né. Il est financé par l’Agence Nationale de la Recherche et l’American Memorial Hospital Foundation.

Dans ce contexte, le travail consistera plus précisémment à traiter et analyser des données issues d’une étude ancillaire d’une cohorte d’environ 800 nouveau-nés à terme dans le cadre de l’étude LyTONEPAL dont l’un des objectifs est d’étudier les facteurs prédictifs du devenir défavorable (neuropathologies, troublespsychomoteurs) à 3 ans.

Le travail consistera à analyser des biomarqueurs prédéfinis qui seront confrontés à des caractéristiques extraites de signaux EEG standard à l’aide d’outils de statistiques et d’apprentissage machine. En parallèle, il s’agira également de fournir des outils de traitement et de visualisation adaptés à la recherche cliniquepour déterminer les mesures de neuroprotection à mettre en place, en particulier pour la prise en charge pré-hospitalière des encéphalopathies anoxo-ischémiques.

<https://umr9008.pages.math.cnrs.fr/job/postdoc/pdf/2021_fiche_de_poste_eeg.pdf>

### Post-doc/research engineer in medical data processing

The recruited person will be integrated within the framework of a scientific project carried out in partnership between the Reims Mathematics Laboratory (LMR) UMR CNRS 9008, the CReSTIC EA 3804, and the service of neonatology service at the University Hospital of Reims. This project is focused on the issue of data processing from EEG / aEEG signals, biological data (and MRI images) for the newborn. It is funded bythe National Research Agency and the American Memorial Hospital Foundation.

In this context, the work will consist more precisely in processing and analyzing data from a ancillarystudy of a cohort of approximately 800 term newborns as part of the LyTONEPAL study one of theobjectives of which is to study the predictive factors of unfavorable outcome (neuropathologies, disorderspsychomotor) at 3 years.

The work will consist in analyzing predefined biomarkers which will be confronted with characteristics extracted from standard EEG signals using statistical and machine learning tools. In parallel, it will alsobe a question of providing treatment and visualization tools adapted to clinical research to determine theneuroprotection measures to put in place, in particular for the management pre-hospital anoxo-ischemicencephalopathies.

<https://umr9008.pages.math.cnrs.fr/job/postdoc/pdf/2021_job_offer_eeg.pdf>

