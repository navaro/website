Title: Mesh generation of cardiac meshes at cellular scale
Date: 2020-10-30 09:22
Slug: job_dfe720b51de0c97c44773ff187570b5a
Category: job
Authors: Algiane FROEHLY
Email: algiane.froehly@inria.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Talence
Job_Duration: 12 mois
Job_Website: 
Job_Employer: Université de Bordeaux
Expiration_Date: 2020-11-30
Attachment: job_dfe720b51de0c97c44773ff187570b5a_attachment.pdf

You will be in charge of the generation of 3D tetrahedral meshes for the  [MicroCard](http://www.microcard.eu/) european project test cases. To do so, you will develop and/or improve an open source toolchain that will allow mesh generation from segmented data (see linked file).