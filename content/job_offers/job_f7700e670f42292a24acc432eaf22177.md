Title: Etude de sensibilité aux données d&#39;entrée météorologiques sur la modélisation de la dispersion atmosphérique de polluants sur Paris et la grande couronne
Date: 2020-10-20 15:39
Slug: job_f7700e670f42292a24acc432eaf22177
Category: job
Authors: hergault
Email: virginie.hergault@interieur.gouv.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Paris
Job_Duration: 6 mois
Job_Website: http://laboratoirecentral.interieur.gouv.fr/
Job_Employer: Laboratoire Central de la Préfecture de Police
Expiration_Date: 2021-01-12
Attachment: 

Le Laboratoire Central de la Préfecture de Police (LCPP) met en place un projet qui porte sur l&#39;impact environnemental de dispersion de polluants dans l&#39;atmosphère lors d&#39;un incendie de grande ampleur. L&#39;objectif est de mettre en place des outils opérationnels permettant de prendre en compte le risque lié à la toxicité des fumées d&#39;incendie pour la population et l&#39;environnement. Une étude récente (Mémoire de formations spécialisées RCH4, ENSOSP 2016) souligne le manque de doctrine opérationnelle permettant d&#39;orienter les prélèvements dans l&#39;air environnant afin de caractériser les fumées d&#39;incendie et de réaliser des périmètres de sécurité. En effet, en cas d&#39;incendie de grande ampleur, les quantités importantes de composés gazeux et particulaires émises dans l&#39;atmosphère peuvent provoquer une pollution locale de l&#39;environnement. L&#39;estimation des zones impactées par le panache de fumées, l&#39;évaluation de l&#39;exposition des populations et l&#39;impact sur l&#39;environnement des retombées de substances polluantes sont complexes, la source étant par définition mal caractérisée.
La modélisation spatio-temporelle de la dispersion du panache de fumées sera réalisée via l&#39;utilisation du modèle lagrangien à particules ARIA City afin d&#39;estimer les niveaux de concentration et d&#39;orienter le déploiement d&#39;un nombre limité de dispositifs de mesure de polluants, traceurs des fumées de combustion (en particulier les particules PM1, PM2,5 et PM10). 
L&#39;objectif de ce stage est de quantifier l&#39;influence de différents paramètres atmosphériques liés aux données d&#39;entrée sur la modélisation de la dispersion des polluants.  Pour la modélisation, il est essentiel mais difficile d&#39;avoir accès à des données météorologiques appropriées, a fortiori en situation d&#39;urgence. Pourtant, les conditions initiales et aux limites, ainsi que leur incertitude associée, sont des points clé qui influencent fortement les sorties du modèle. Il est donc important d&#39;effectuer des tests de sensibilité aux données d&#39;entrée afin d&#39;évaluer l&#39;impact de chaque paramètre sur les résultats numériques. Les simulations permettront également de mettre en évidence des zones de dépôts privilégiés sur Paris et la grande couronne.

Dans le cadre du stage, la robustesse de l&#39;outil de modélisation pourra être testée par comparaison avec des données de terrain obtenues suite à des incendies réels en collaboration avec les services de secours.

L&#39;étudiant aura la possibilité de découvrir et de participer aux activités en lien avec les mesures de polluants sur le terrain.