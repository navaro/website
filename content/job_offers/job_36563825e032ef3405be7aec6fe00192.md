Title: Prise en compte et quantification de l&#39;impact de paramètres liés aux fumées d&#39;incendie dans les modèles de dispersion atmosphérique – Applications à des cas réels
Date: 2020-10-20 15:41
Slug: job_36563825e032ef3405be7aec6fe00192
Category: job
Authors: hergault
Email: virginie.hergault@interieur.gouv.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Paris
Job_Duration: 6 mois
Job_Website: http://laboratoirecentral.interieur.gouv.fr/
Job_Employer: Laboratoire Central de la Préfecture de Police
Expiration_Date: 2021-01-12
Attachment: 

Le Laboratoire Central de la Préfecture de Police (LCPP) met en place un projet qui porte sur l&#39;impact environnemental de dispersion de polluants dans l&#39;atmosphère lors d&#39;un incendie de grande ampleur. L&#39;objectif est de mettre en place des outils opérationnels permettant de prendre en compte le risque lié à la toxicité des fumées d&#39;incendie pour la population et l&#39;environnement. Une étude récente (Mémoire de formations spécialisées RCH4, ENSOSP 2016) souligne le manque de doctrine opérationnelle permettant d&#39;orienter les prélèvements dans l&#39;air environnant afin de caractériser les fumées d&#39;incendie et de réaliser des périmètres de sécurité. En effet, en cas d&#39;incendie de grande ampleur, les quantités importantes de composés gazeux et particulaires émises dans l&#39;atmosphère peuvent provoquer une pollution locale de l&#39;environnement. L&#39;estimation des zones impactées par le panache de fumées, l&#39;évaluation de l&#39;exposition des populations et l&#39;impact sur l&#39;environnement des retombées de substances polluantes sont complexes, la source étant par définition mal caractérisée.

La modélisation spatio-temporelle de la dispersion du panache de fumées peut être réalisée par des approches différentes en fonction des besoins et des objectifs (modèles gaussien, eulérien ou lagrangien). Les paramètres d&#39;entrée nécessaires à la prise en compte des fumées d&#39;incendie dépendent de l&#39;approche utilisée et peuvent varier en fonction des données disponibles. La prise en compte de ces données spécifiques aux fumées d&#39;incendie peut également être réalisée par des formulations différentes dans les modèles. 

L&#39;objectif de ce stage est de quantifier l&#39;influence des différents paramètres liés aux fumées d&#39;incendie (modèles de surhauteur, modèles de dépôts, distribution granulométrique, …) sur la modélisation de la dispersion des polluants pour les différentes approches et d&#39;identifier une paramétrisation optimale compatible à une situation opérationnelle. En effet, les différentes paramétrisations, ainsi que leur incertitude associée, sont des points clé qui peuvent influencer les sorties du modèle. Il est donc important d&#39;effectuer des tests de sensibilité afin d&#39;évaluer l&#39;impact de chaque paramétrisation sur les résultats numériques. 

Dans le cadre du stage, les outils de modélisation pourront être testés par comparaison avec des données de terrain obtenues suite à des incendies réels en collaboration avec les services de secours.
L&#39;étudiant aura la possibilité de découvrir et de participer aux activités en lien avec les mesures de polluants sur le terrain.