Title: Détection de crise d’épilepsie à partir de données EEG et extraction de patterns caractéristiques
Date: 2019-12-04 10:35
Slug: job_ad5d7e7e863c301f8c2e3c5effd5e3cc
Category: job
Authors: ALLIOD Charlotte
Email: charlotte.alliod@altran.com
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: LYON, Vaise
Job_Duration: 6 mois
Job_Website: 
Job_Employer: Altran
Expiration_Date: 2020-02-26
Attachment: job_ad5d7e7e863c301f8c2e3c5effd5e3cc_attachment.pdf

**Contexte :** 

Face à l’explosion du monde du numérique, et notamment l’intégration de l’Intelligence Artificielle et du Big Data, le processus de soins et de prise en charge du patient doit être repensé. L’émergence de ces technologies rend en effet possible l’amélioration de la conception des nouveaux projets de santé, l’optimisation du parcours de soin au sein de structures hospitalières ainsi que le développement de nouveaux dispositifs médicaux innovants, connectés et adaptés permettant ainsi un meilleur suivi des pathologies chroniques. 

Dans ce cadre, la Direction R&amp;I France, entité de recherche interne du groupe Altran, mène le projet PACIFIC (PAtient-Centered Innovations For the Improvement of Care). Ce projet vise à développer un vaste ensemble d’algorithmes d’analyse de données et de Machine Learning pour le domaine de la santé, allant du Process Mining, l’analyse de Séries Temporelles, au Text Mining.

**Missions :**

En collaboration avec le chef de projet PACIFIC, vous aurez pour missions :

* De mener les travaux de recherche sur la partie analyse de données en suivant les axes pré-établis&nbsp;:

    - Intégrer et prétraiter des données d’EEG de patients épileptiques.
    - Proposer, tester et mettre en place des méthodes de Data Mining / Machine Learning pour diagnostiquer et prévoir la survenue de crises d’épilepsies.
    - Proposer, tester et mettre en place des méthodes de Text Mining afin de déterminer les patterns caractéristiques d’une crise.
    - Rendre une vue synthétique de vos résultats.

* D’assurer les objectifs de production scientifique, tels que veille, publication d’articles scientifiques, conférences et communications internes. 

**Profil :**

- Formation ingénieur / master avec spécialisation en Data Sciences mathématiques appliquées, informatique, software engineering, traitement du signal, bio-informatique ou équivalent.
- Compétences en Machine Learning 
- Des compétences en traitement du signal, séries temporelles et classification supervisées.
- Maîtrise d’un langage de programmation d’analyse de données : Python (Pandas, Numpy, Scikit-learn, scipy…), R, matlab…
- Autonome, travail en équipe, ouvert d’esprit, force de proposition.
- Maîtrise du français et de l’anglais, à l’écrit comme à l’oral.
