Title: Méthodes numériques pour la modélisation des crues urbaines
Date: 2020-11-17 08:16
Slug: job_40b0d948e8b3a4f30b0a1e4783dfd160
Category: job
Authors: Konstantin Brenner
Email: Konstantin.Brenner@univ-cotedazur.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Laboratoire J.A. Dieudonne de l’Université Côte d’Azur 
Job_Duration: 6 mois
Job_Website: 
Job_Employer: Université Côte d’Azur 
Expiration_Date: 2021-02-09
Attachment: job_40b0d948e8b3a4f30b0a1e4783dfd160_attachment.pdf

Le stage aura lieu au  Laboratoire J.A. Dieudonne de l’Université Côte d’Azur et dans le cadre du projet ANR Top-up. Top-up est un projet interdisciplinaire qui vise à intégrer la topographie urbaine à haute résolution dans les simulations d&#39;écoulement de surface libre. Les inondations urbaines produites par des précipitations exceptionnelles peuvent causer des dommages importants tant en termes de pertes de vie que de destruction de biens. Cette question revêt une importance particulière pour la ville de Nice et ses environs. La modélisation numérique peut être utilisée pour prédire, anticiper et contrôler les inondations en aidant à dimensionner et positionner les systèmes de protection, y compris les barrages, les digues ou le réseau de drainage des eaux pluviales. Le défi majeur de la modélisation numérique des crues urbaines est que les petits éléments structurels imperméables (comme les bâtiments, les murs en béton, les voitures, etc.) affectent considérablement le débit.