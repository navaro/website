Title: Administrateurs systèmes et réseaux (H/F)
Date: 2020-06-08 14:23
Slug: job_a3c18bdb34d3c55178081365f64b290b
Category: job
Authors: Geneviève Morvan
Email: communication@idris.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Orsay (91) - France
Job_Duration: 
Job_Website: http://www.idris.fr
Job_Employer: CNRS- IDRIS
Expiration_Date: 2020-07-02
Attachment: 

Dans le cadre des concours de recrutement externes CNRS 2020, l&#39;IDRIS recrute 

**deux administrateurs systèmes et réseaux (H/F)** 

pour renforcer l&#39;équipe système, **ingénieurs d&#39;études, concours 93, 1er et 3e postes**

**Poste 1 :** la personne recrutée participera à la gestion et à l&#39;administration des plateformes hébergées à l&#39;IDRIS (MdlS, CentraleSupelec &amp; ENS Paris-Saclay, LabIA, …), en interaction directe avec les équipes déjà en place en charge de cette activité chez nos partenaires		
			
**Poste 3 :** la personne recrutée participera au déploiement, à la mise en œuvre et à l&#39;administration de services associés à l&#39;architecture matérielle et logicielle des plates-formes (supercalculateurs et autres serveurs) du centre de calcul, et en premier lieu au supercalculateur Jean Zay qui a été ouvert à la production pour l&#39;ensemble des utilisateurs en octobre 2019.**
			 
			 
Pour plus d&#39;informations sur les postes offerts au recrutement à l&#39;IDRIS, consultez notre site web 	: 	<http://www.idris.fr/annonces/idris-recrute.html>
   
   


    

