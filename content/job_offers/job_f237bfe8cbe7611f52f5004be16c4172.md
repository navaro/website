Title: Biomimetism and bioinspiration of a snake's anguilliform swimming: numerical twin and machine learning
Date: 2021-03-30 17:23
Slug: job_f237bfe8cbe7611f52f5004be16c4172
Category: job
Authors: Traoré Philippe
Email: philippe.traore@univ-poitiers.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: INRIA Bordeaux / Institut Pprime
Job_Duration: 3 ans
Job_Website: 
Job_Employer: Université de Poitiers
Expiration_Date: 2021-06-22
Attachment: job_f237bfe8cbe7611f52f5004be16c4172_attachment.pdf

**Context of this study**

The performance of aquatic locomotion is closely related to the circulation of fluid around the body of the animal and mainly to the intensity of the flow induced downstream from the tail and its characteristics. This developing wake consists of a Von Karman vortex street whose vortices are emitted periodically with each beat of the tail. The way the tail moves in the fluid (angle of attack, amplitude, frequency ...) determines the swimming performance. Animals can adjust their swimming kinematics to optimize performance. Different criteria can define performance, such as energy efficiency, maximum speed (prey-predator system), or furtivity.  The body shape and tribological properties of the skin, which influence the thrust and drag produced by swimming animals, must be taken into account.


**Objectives**

The central objective of this thesis is to contribute to the development of a numerical model based on real data such as geometry, kinematics, skin texture (data-driven numerical modeling) in order to simulate the complex phenomena of unsteady fluid-structure interaction (FSI), involved in the eel-like swimming within an incompressible viscous flow in the presence of a free surface. Therefore, from the kinematic and velocimetric data that characterize snake swimming, we will identify the hydrodynamic mechanisms that will allow us to estimate the efficiency of this swimming in terms of propulsive force and energy efficiency. Figure 1 shows preliminary simulations performed with the NaSCar code (which will be used as a basis in this thesis) using synthetic data (not measured in situ).


Biomimetism and bioinspiration are emerging fields of scientific research that tend to understand and then draw inspiration from the physical mechanisms implemented by nature in order to apply them in a technological context (drones for example). We can expect that the different parameters involved in the characterization of a physical process have been optimized by nature after millions of years of evolution. The anguilliform swimming is characterized by a certain number of parameters which define the undulatory kinematics of this swimming (amplitude and frequency of the oscillating movement, longitudinal and transverse, induced by the body of the snake). Adjusting, these different input parameters, we will optimize the propulsive force and the energy efficiency that result from this kinematics. Machine learning techniques will be used to determine the optimal parameters to maximize the propulsive force and energy efficiency. It will be of interest to asses wether nature has been able to propose an optimal solution from an energy point of view.

 

**Work program, methodologies and means**

Partially immersed snakes raise specific challenges; numerical simulation will require the use of methods that couple free surface flow analysis with fluid-structure interaction. The fluid flow around the deformable structure (snake body) is dictated by the motion of the structure. Conversely, the structure itself is influenced by the forces exerted by the moving fluid.
In the first part of this thesis, numerical developments will be conducted to couple an immersed boundary method (IBM) to take into account the motion of the snake and a method to treat the free surface (Level Set method), all in a context of fluid-structure interaction with backward coupling. The NasCar code, developed at Inria Bordeaux, can be used as a basis for future developments. This first part of the project is essentially based on the use of numerical modeling tools to provide initial answers to fundamental questions.

- In a first step and in a simplified approach, we will use an inclined and partially immersed ellipsoidal cylinder to mimic the flow around the head of a snake swimming at the surface. The cylinder will be animated by a transverse and longitudinal sinusoidal motion, typical of swimming snakes. The purpose is to elucidate the underlying physical mechanisms that generate the propulsive force, which is a key element to address the persistent problem of swimming efficiency.  

- In a second step, we will encompass the full complexity of this problem, considering a snake in its entirety (head, body and tail) having immersed part or all of its body. We will try to determine the propulsive force and the energy efficiency that will result from a geometry of the snake body and a kinematics of the swim that will be enforced. Preliminary results are obtained in figure 1. Improvements to these results will be required.
From videos made on real snakes, we shall extract the kinematics of the real swimming (to decouple the movement imposed by the snake from the hydrodynamic effects), we shall determine the geometry of the snake’s body when it is immersed (the effect of buoyancy modifies the geometry), and we shall model the influence of the texture of the skin of the snake. Thus from real data obtained from experiments carried out jointly by the experimentalists associated with this project we will be able to create a numerical twin.

Finally, in the last part of the thesis, different innovative control strategies based on machine learning methods (machine learning, genetic algorithms) will be applied to identify the optimal swimming conditions that maximize the snake&#39;s propulsive force.

This thesis work is part of a larger ANR project (Dragon II) in which biologists, fluid mechanicists, mathematicians and roboticians collaborate. A strong link will be maintained with the experimentalists of the project and experimental data on the swimming of real snakes and bio-inspired robots will be provided to validate the numerical model.

The thesis will be carried out in alternation between Inria Bordeaux - Sud-Ouest (Memphis team) and the PPRIME Institute in Poitiers (Curiosity team). 
