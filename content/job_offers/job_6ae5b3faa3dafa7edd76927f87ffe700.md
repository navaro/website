Title: Research engineer in (bio)process modelling
Date: 2020-06-04 09:01
Slug: job_6ae5b3faa3dafa7edd76927f87ffe700
Category: job
Authors: Patrick Perré
Email: patrick.perre@centralesupelec.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Pomacle (51) 
Job_Duration: 36 mois 
Job_Website: http://www.chaire-biotechnologie.centralesupelec.fr/
Job_Employer: CentraleSUpélec 
Expiration_Date: 2020-08-27
Attachment: job_6ae5b3faa3dafa7edd76927f87ffe700_attachment.pdf

## Research engineer in (bio)process modelling# 
3-year renewable fixed-term contract
Position open within the Chair of Biotechnology of CentraleSupélec, located in the European Centre for Biotechnology and Bioeconomy (CEBB), in Pomacle (20 km from Reims).
**ENVIRONMENT**:
The Chair of Biotechnology of CentraleSupélec, created in 2011 and located at the Bazancourt-Pomacle biorefinery (51), is structured around three main topics: i) lignocellulosics, ii) biotransformation and iii) separation techniques. These three topics are all based on a transversal modelling, instrumentation &amp; visualization base. It is one of the four groups hosted by the Centre Européen de Biotechnologie et de Bioéconomie (CEBB).
The Chair of Biotechnology of CentraleSupélec is backed by the Process and Materials Engineering Laboratory (LGPM) located in Gif-sur-Yvette (91).
The Chair of Biotechnology at CentraleSupélec is renewed in 20202. This renewal will allow a substantially increased power, with the ambition of extensive use of modelling coupled with micro-experimentation to move to industrial scale, with the objective of a virtual plant of biorefinery (digital twin concept). The CEBB&#39;s technical hall hosts a set of laboratory pilots allowing to validate this virtual approach thanks to a pilot plant. The ramp-up will notably result in several recruitments, the acquisition of remarkable scientific equipment and investment in shared computing resources.
**MISSIONS**:
In collaboration with the scientific teams of the LGPM, the research engineer will participate in the development of numerical tools (code development or use of open source software) to build our digital twin project in our fields of expertise: bioprocesses (physical/biological coupling in bioreactors, photobioreactors, fermentation in solid media), separation techniques (separation of very close molecules, fluid/solid coupling in fixed beds and out-of-equilibrium configurations in complex geometry). These tools will be deployed on computing architectures (ROMEO mesocentre in Reims or in connection with that of CentraleSupélec).
The person recruited will work in close interaction with the laboratory&#39;s physicists and biologists (2D and 3D imaging, bioreactors, PIV, monitoring of suspended organisms, immobilized cultures, etc.).
**SKILLS**:
Holder of an engineering degree, a doctorate or equivalent level attested by professional experience, candidate must:
- Have a good knowledge of applied mathematics,
- Have high proficiency in at least one scientific programming language (Fortran, C/C++, Matlab),
- Have good collaborative work skills,
- Have a good level in English,
- Have an interest or even knowledge in the Chair&#39;s disciplinary and application fields,
Desired skills:
- Process engineering: fluid mechanics, reactive coupled transfers
- Bioprocesses: coupled bio-active transfers, numerical methods for life sciences
**PRACTICAL DETAILS**:
The position is open within the European Centre for Biotechnology and Bioeconomy (CEBB), which hosts the Chair of Biotechnology: CEBB - 3, rue des Rouges Terres 51110 Pomacle
Travel to the CentraleSupélec site at Gif-sur-Yvette will be required.
Salary will be determined according to the candidate&#39;s experience.
**REQUIRED DOCUMENTS**:
Application letters of application, together with a curriculum vitae and, at the candidate&#39;s discretion, letters of recommendation, should be sent by e-mail only to the two contacts listed below.
**CONTACT**:
Prof. Patrick PERRÉ,
Directeur de la Chaire de Biotechnologie,
LGPM, CentraleSupélec
patrick.perre@centralesupelec.fr
Tél. : + 33 6 42 61 24 18
Julien COLIN,
Directeur-adjoint de la Chaire de Biotechnologie,
LGPM, CentraleSupélec
julien.colin@centralesupelec.fr
Tél. : + 33 6 88 16 58 44
____________________________________________________________________________________

##  **Ingénieur de Recherche en modélisation des (bio)procédés**
Poste ouvert au sein de la Chaire de Biotechnologie de CentraleSupélec, localisée dans le Centre Européen de Biotechnologie et de Bioéconomie (CEBB), à Pomacle (20 km de Reims).
CDD de 3 ans renouvelable
**ENVIRONNEMENT** :
La Chaire de Biotechnologie de CentraleSupélec, créée en 2011 et localisée au sein de la bioraffinerie de Bazancourt-Pomacle (51), est structurée autour de trois axes thématiques : i) lignocellulosiques, ii) bio-transformation et iii) techniques séparatives, le tout s’appuyant sur un socle transversal modélisation, instrumentation &amp; visualisation. Il s’agit de l’un des quatre groupes hébergés par le Centre Européen de Biotechnologie et de Bioéconomie (CEBB).
La Chaire de Biotechnologie de CentraleSupélec est adossée au Laboratoire de Génie des Procédés et Matériaux (LGPM) localisé à Gif-sur-Yvette (91).
La Chaire de Biotechnologie de CentraleSupélec est renouvelée en 20201. Ceci va permettre une montée en puissance substantielle, avec l&#39;ambition de l&#39;utilisation massive de la modélisation couplée à la micro-expérimentation pour passer à l&#39;échelle industrielle, dans un objectif d’usine virtuelle de bioraffinerie (concept de jumeau numérique). La halle technique du CEBB héberge un ensemble de pilotes de laboratoire propice à la validation de cette approche virtuelle grâce à une usine pilote. La montée en puissance se traduira notamment par plusieurs recrutements, par l&#39;acquisition d&#39;équipements scientifiques remarquables et l’investissement dans des moyens de calcul mutualisés.
1 La Chaire de Biotechnologie de CentraleSupélec est soutenue par le Conseil départemental de la Marne, la Communauté Urbaine du Grand Reims, la Région Grand Est et l’Union Européenne.
**MISSIONS** :
En collaboration avec les équipes scientifiques du LGPM, l’ingénieur de recherche participera au développement d&#39;outils numériques (développement de codes ou utilisation de logiciels open sources) pour construire notre projet de jumeau numérique dans nos domaines d&#39;expertise : bioprocédés (couplage physique/biologie en bioréacteurs, photobioréacteurs, fermentation en milieux solide), techniques séparatives (séparation de molécules très proches , couplage fluide/solide en lit fixe et configurations hors-équilibre en géométrie complexe) et au déploiement de ces outils sur les architectures de calcul (mésocentre ROMEO à Reims ou en relation avec celui de CentraleSupélec).
La personne recrutée travaillera en étroite interaction avec les physiciens et biologistes du laboratoire (imagerie 2D et 3D, bioréacteurs, PIV, suivi d&#39;organismes en suspension, cultures immobilisées, etc.).
**COMPÉTENCES** :
Titulaire d’un diplôme d’ingénieur, d’un doctorat ou d’un niveau équivalent attesté par une expérience professionnelle, le candidat devra :
- Avoir de bonnes connaissances en mathématiques appliquées,
- Avoir un bon niveau dans au moins un langage de programmation scientifique (Fortran, C/C++, Matlab),
- Avoir des bonnes aptitudes au travail collaboratif,
- Avoir une bonne maîtrise de l&#39;anglais,
- Avoir un intérêt voire des connaissances dans les champs disciplinaires et applicatifs de la Chaire.
Compétences souhaitées :
- Génie des procédés : mécanique de fluides, transferts couplés réactifs
- Bio-procédés : transferts couplés bio-actifs, méthodes numériques pour les sciences du vivant
**MODALITÉS PRATIQUES :**
Le poste est ouvert au sein du Centre Européen de Biotechnologie et de Bioéconomie (CEBB), qui héberge la Chaire de Biotechnologie : CEBB – 3, rue des Rouges Terres 51110 Pomacle
Des déplacements sur le site de CentraleSupélec à Gif-sur-Yvette seront à prévoir.
Le salaire sera déterminé en fonction de l’expérience du candidat.
**DOCUMENTS À FOURNIR** :
Les lettres de candidature, accompagnées d’un curriculum vitae et, à la discrétion des candidats, de lettres de recommandation, devront être adressées par courriel uniquement aux deux contacts mentionnés ci-après.
**CONTACT** :
Prof. Patrick PERRÉ,
Directeur de la Chaire de Biotechnologie,
LGPM, CentraleSupélec
patrick.perre@centralesupelec.fr 
Tél. : + 33 6 42 61 24 18
Julien COLIN,
Directeur-adjoint de la Chaire de Biotechnologie,
LGPM, CentraleSupélec
julien.colin@centralesupelec.fr
Tél. : + 33 6 88 16 58 44