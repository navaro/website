Title: Ingénieur HPC et stockage haute performance (H/F)
Date: 2021-04-15 12:32
Slug: job_03e5a36feda69cd344c329ea4a90f4da
Category: job
Authors: Mallory LEGRAIN
Email: recrutement@icm-institute.org
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: PARIS
Job_Duration: 
Job_Website: 
Job_Employer: Institut du cerveau
Expiration_Date: 2021-07-08
Attachment: job_03e5a36feda69cd344c329ea4a90f4da_attachment.pdf

Vous êtes passionné par les enjeux du calcul et du stockage scientifique de grands volumes de données (plusieurs Pétaoctets) et souhaitez mettre en œuvre des solutions les plus performantes en environnement multi-cloud public, privé, interne et institutionnel ? Rejoignez notre équipe et participez à la recherche médicale dans le domaine des neurosciences. Les technologies abordées concernent notamment du stockage distribué haute performance (Apache Lustre), du stockage objets, les réseaux faibles latences et le calcul haute performance.

**MISSIONS PRINCIPALES**

-	Gestion opérationnelle des infrastructures de stockage, en particulier le stockage pérenne de données scientifiques et le stockage haute performance lié à un cluster de calcul,
-	Contrôler l’accès aux données et l’usage des données en fonction des besoins des utilisateurs, des contraintes légales et conformités,
-	Développer l’architecture de stockage multi-cloud (cloud interne, public et privé),
-	Mise en œuvre du stockage et des flux sur AWS, 
-	Rédiger les documentations d’exploitation du stockage, maintenir à niveau des procédures,
-	Réaliser des tableaux de bord et veiller au bon usage des ressources de stockage,
-	Assurer le support des incidents, accompagner les utilisateurs (formation),
-	Apporter une expertise et un support aux utilisateurs des infrastructures,
-	Participer à la mise en œuvre du cluster de calcul de l’institut
-	Veille technologique

**SAVOIR**

-	Titulaire d’un diplôme Bac + 4 en informatique minimum
-	Vous possédez idéalement au moins 5 ans d’expérience dans un environnement similaire
-	Vous avez une connaissance opérationnelle des systèmes de stockage 
-	Vous avez déjà une expérience sur stockage Lustre, Open IO et/ou technologie objets

**SAVOIR-FAIRE**

-	Maîtrise de l’administration de système linux et des systèmes de stockage distribués hautes performances et/ou stockage objet
-	Vous avez une connaissance opérationnelle des environnements multi-cloud, et avez une expérience du cloud public AWS
-	Vous avez la maîtrise des environnements Linux, des systèmes de calcul haute performance et du traitement de grands volumes de données
-	Vous avez de bonnes notions des réseaux hautes performances, type Infiniband
-	Vous avez déjà fait du support niveau 2, et utilisé des outils de ticketing (GLPI, etc.)
-	Maîtrise de l&#39;anglais technique nécessaire


**SAVOIR-ETRE**

-	Rigoureux investi(e) 
-	Vous avez un sens du relationnel, du service aux utilisateurs et du travail en équipe
-	Vous êtes autonome et suivez les évolutions techniques
-	Vous êtes passionné(e) par l’environnement scientifique
-	Vous appréciez de transmettre votre savoir et de former les utilisateurs

