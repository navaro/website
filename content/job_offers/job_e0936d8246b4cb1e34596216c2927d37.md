Title: Ingénieur informatique  HPC &amp; génie logiciel
Date: 2020-12-15 08:15
Slug: job_e0936d8246b4cb1e34596216c2927d37
Category: job
Authors: collard
Email: bruno.collard@cea.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Saint Paul Lez Durance
Job_Duration: 3 ans
Job_Website: https://cadarache.cea.fr/cad/Pages/Accueil.aspx
Job_Employer: CEA
Expiration_Date: 2021-06-01
Attachment: 

Contexte :
Le Département d&#39;Etude des Combustibles a pour mission d&#39;acquérir et de capitaliser les connaissances relatives à la conception, à la fabrication à l&#39;exploitation en réacteur et au stockage des éléments combustibles pour l&#39;énergie nucléaire. Il s&#39;appuie sur une plateforme expérimentale (fabrication, expérimentation et caractérisation des combustibles) et sur une plateforme numérique (modélisation et simulation du comportement des combustibles).
Les outils de calcul scientifique nécessaires aux études sont développés au sein du Service d&#39;Etudes et de Simulation du comportement des Combustibles et plus particulièrement du Laboratoire de Simulation du comportement des Combustibles. Ce laboratoire développe, vérifie, valide, quantifie les incertitudes et assure la maintenance opérationnelle des applications de simulation du combustible pour les différentes filières de réacteurs au sein de la plateforme numérique PLEIADES. L’enjeu associé à cette plateforme est de disposer d’une capacité de simulation de référence du comportement des éléments combustibles, prédictive et validée, pour soutenir l’industrie nucléaire (principaux partenaires EDF, FRAMATOME, ORANO), préparer le futur (EPR2, SMR, Multi-Recyclage du Plutonium en REP, réacteurs de 4ème génération) et répondre aux besoins des applications militaires.

Descriptif :
Le poste, au sein de l&#39;équipe de développement de la plateforme numérique PLEIADES, a pour objet de contribuer au développement d’un nouvelle plateforme haute performance PLEIADES-HPC. Ce développement s’inscrit dans un objectif à moyen terme du programme de simulation du CEA visant à la réalisation d’une première simulation massivement parallèle du comportement en réacteur d&#39;un crayon combustible complet. Le framework PLEIADES-HPC permettra de coordonner et mettre en lien un certain nombre de composants ou sous-modèles physiques existants. Les travaux sur la plateforme seront coordonnés avec la construction du solveur prototype thermo-mécanique éléments finis HPC (nommé Helix) du CEA.
Les technologies et langages utilisés seront le C++, MPI, python. Les solutions mises en place seront spécifiées avec le concours de collaborateurs du CEA. 
Le titulaire du poste contribuera aux activités suivantes : 

- Définition de l’architecture globale (framework) et de l’interaction entre les modèles et les composants internes, en intégrant le savoir-faire de la plateforme PLEIADES existante.
- Conception objet de la plateforme, structuration des fonctionnalités en garantissant un très bon niveau de performances séquentiel et parallèle.
- Mise en place d’une API haut-niveau, et d’une procédure de mise en donnée.
- Mise en œuvre d’applications de démonstration utilisant la plateforme.
- Contribution au moteur thermo-mécanique éléments finis (Helix, en cours de développement).
- Eléments de documentation.

Ces travaux sont menés au sein de l&#39;équipe CEA de développement de la plateforme numérique PLEIADES, en collaboration avec les unités du CEA Saclay. La plateforme PLEIADES est également développée en collaboration avec les partenaires industriels EDF, FRAMATOME et ORANO.

Formation initiale : Bac+5, spécialité : informatique

Expérience : 3 à 5 ans				
Compétences techniques et/ou spécifiques : 

- Expérience dans la création ou la maintenance d’un framework C++.
- Expérience concernant les codes de calculs scientifiques.
- Environnement LINUX, environnement HPC. 
- Des compétences en mathématiques appliquées ou mécanique seraient un plus.

Outils utilisés : C++, MPI et environnement HPC, Python, outils d’intégration continue et de gestion de sources : git, gitlab, Jenkins.				

Langue : Anglais (niveau intermédiaire)