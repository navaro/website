Title: Chef(fe) de projet ou expert(e) en ingénierie des systèmes d&#39;information à l&#39;IDRIS (NOEMI, IR)
Date: 2020-12-09 10:06
Slug: job_c5ba4331b68b4f085667b4eda7125eac
Category: job
Authors: Geneviève Morvan
Email: communication@idris.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Orsay (91)
Job_Duration: 
Job_Website: http://www.idris.fr/annonces/idris-recrute.html
Job_Employer: IDRIS-CNRS
Expiration_Date: 2021-03-03
Attachment: 

 L&#39;IDRIS recrute, dans le cadre de la campagne de mobilité interne NOEMI-FSEP, ouverte jusqu&#39;au 15 janvier 2021 inclus :

 **un(e) ingénieur(e) de recherche, chef(fe) de projet ou expert(e) en ingénierie des systèmes d&#39;information**  (fonction S61014), 
 
Au sein de l&#39;équipe système, la personne recrutée participera au déploiement, à la mise en oeuvre et à l&#39;administration de services associés à l&#39;architecture matérielle et logicielle des plates-formes (supercalculateurs et autres serveurs) du centre de calcul, et en premier lieu au supercalculateur Jean Zay qui a été ouvert à la production pour l&#39;ensemble des utilisateurs en octobre 2019. 

Pour plus de détails sur le poste et/ou pour postuler :
<https://mobiliteinterne.cnrs.fr/afip/owa/consult.affiche_fonc?code_fonc=S61014&type_fonction=&code_dr=4&code_bap=E&code_corps=&nbjours=&page=1&colonne_triee=1&type_tri=ASC>