Title: Post-doctoral Position in High Performance Computing (HPC) for multi-physics applications
Date: 2021-09-20 07:37
Slug: job_c398b8394f28a4083fe5f961c591b587
Category: job
Authors: Xavier Besseron
Email: xavier.besseron@uni.lu
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Esch-sur-Alzette, Luxembourg
Job_Duration: 2 years
Job_Website: http://luxdem.uni.lu/
Job_Employer: University of Luxembourg
Expiration_Date: 2021-10-31
Attachment: job_c398b8394f28a4083fe5f961c591b587_attachment.pdf

**Job description for a Post-doctoral Position in High Performance Computing (HPC)**

The University of Luxembourg (UL) invites applications for a post-doctoral position (f/m/d) as part of an industry collaboration funded by the FNR Bridges funding instrument <https://www.fnr.lu/funding-instruments/>. The starting date is to be arranged as soon as possible to start the project.

<https://recruitment.uni.lu/fr/details.html?nPostingTargetId=96181>


**Research Framework:**

In order to deal with critical fuels that otherwise have to be disposed of, co-firing of these fuels in existing combustion plants is a an economic alternative. However, these critical fuels distinguish themselves significantly from the fuel the combustion technology was designed for and therefore, may have a negative impact on operation. Rather than investing into cost-intensive and time-consuming experimental campaigns, a multi-physics digital twin in a high performance computing environment allows exploring various co-firing scenarios in a cost-effective manner. Combining multi-physics i.e. motion and thermal conversion of solid fuels with a reacting gas phase requires a smart partitioning technology. It is the key technology to fast multi-physics digital twin simulation technology and paves the road to innovative approaches for co-firing of critical fuels.

**Your profile:**

- PhD degree in computer science, computational science or equivalent required to pursue a post-doctoral study
- Familiar with parallel computing programming models (OpenMP and MPI)
- Proficient in programming and working with C++, Python, Linux, Git
- Experience in using HPC platforms (e.g. SLURM)
- Background in load-balancing techniques (i.e. graph partitioning with METIS, SCOTCH, Zoltan, ...)
- Knowledge in numerical simulations, e.g. Computational Fluid Dynamics, or coupling of numerical simulations would be a significant advantage 
- Good English language skills (spoken and written)
- Willingness to familiarise with XDEM (<http://luxdem.uni.lu/>) and to work in an inter-cultural and international environment
- Ability to work independently and as part of a team
- Curiosity and self-motivation
- Good presentation skills will be an asset.

**We offer:**

* A dynamic and well-equipped research environment within XDEM research team (<http://luxdem.uni.lu/>)
* Intensive training in scientific and transferable skills, participation in schools, conferences and workshops.
* Fixed term employee contracts totalling 24 months
* Personal work space at University of Luxembourg

**Job responsibility:**

Your primary tasks as a post-doctoral candidate are to:

* Manage and drive forward your research project.
* Attend trainings and social events.
* Write scientific articles
* Disseminate your research at conferences and seminars.

**Gender policy:**

UL strives to increase the proportion of female post-doc students in its faculties.
Therefore, we explicitly encourage women to apply.

**Supervision:**

Your lead supervisor will be Prof Bernhard Peters.

**Application submission:**

Before proceeding with the submission of your application, please prepare the following
documents.

* Curriculum vitae
* Motivation letter (maximum two pages) detailing how you meet the selection criteria for the given research direction.
* Publication list.
* Full contact details of two persons willing to act as referees.
* Copies of diplomas, transcripts with grades, with English, French or German translation.
 
Please send a complete application to <bernhard.peters@uni.lu>. Please note that incomplete
applications will not be considered.

**Selection process:**

Candidates will be short-listed based on the criteria detailed above. Short-listed candidates
will be invited for an interview and/or interviewed through appropriate internet channels.
