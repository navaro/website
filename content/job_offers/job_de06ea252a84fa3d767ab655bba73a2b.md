Title: Modeling and Simulation of Supercavitating Flows
Date: 2019-11-13 12:46
Slug: job_de06ea252a84fa3d767ab655bba73a2b
Category: job
Authors: Eric GONCALVES
Email: eric.goncalves@ensma.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Poitiers
Job_Duration: 5 à 6 mois
Job_Website: 
Job_Employer: ISAE-ENSMA
Expiration_Date: 2020-02-28
Attachment: job_de06ea252a84fa3d767ab655bba73a2b_attachment.pdf

The main objective of this study is the numerical investigation of supercaviting flows by using micro geometries such as micro-metric size objects placed in a channel. Vehicles operating at high speeds in water are typically accompanied by high levels of skin friction drag and cavitation. Cavitation, however, instead of being an unwanted accompaniment to high speed underwater travel, can actually be employed to reduce skin friction drag and provide order-of-magnitude increases in speed compared to those achieved with conventional, fully-wetted undersea vehicles.
Simulations will be performed with the 1-fluid RANS compressible code CaviFlow.
Different geometries will be investigated such as, round, rectangle, beveled 2D
objects or hydrofoils. A special focus will be done on the performance and heat transfers.
