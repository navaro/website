Title: Parallel isovalue discretization and mesh adaptation on hybrid architectures
Date: 2020-10-30 09:30
Slug: job_a5ce0c59d92790dbae9ce6e57e1eb648
Category: job
Authors: Algiane FROEHLY
Email: algiane.froehly@inria.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Talence
Job_Duration: 12 mois
Job_Website: 
Job_Employer: Université de Bordeaux
Expiration_Date: 2020-11-30
Attachment: job_a5ce0c59d92790dbae9ce6e57e1eb648_attachment.pdf

At its current state of implementation, the open source software [ParMmg](https://github.com/MmgTools/ParMmg.git), based on iterative calls of the serial remesher [Mmg](https://www.mmgtools.org) over sub-meshes, allows to adapt a mesh using a distributed algorithm based on the MPI norm. You will be in charge of writing a new task-based algorithm and of implementing it using [StarPU task scheduler](https://starpu.gitlabpages.inria.fr/) in order to exploit both the shared memory available within a computational node and the distributed memory at the cluster level. See the linked file for more details.
