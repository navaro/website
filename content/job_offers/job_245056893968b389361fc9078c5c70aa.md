Title: Internship M2/Engineer: Reduction of computing precision, driven by stochastic arithmetic: application to low energy gamma radiation measurements.
Date: 2021-01-08 14:39
Slug: job_245056893968b389361fc9078c5c70aa
Category: job
Authors: David Chamont
Email: chamont@in2p3.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Orsay/Paris
Job_Duration: 4-6 mois
Job_Website: 
Job_Employer: CNRS
Expiration_Date: 2021-01-15
Attachment: job_245056893968b389361fc9078c5c70aa_attachment.pdf

If you like floating point computing, this internship will lead you to use CADNA, so to improve the online processing of the data issued from a detector of gamma rays. C++ required.
See attached file for further details.