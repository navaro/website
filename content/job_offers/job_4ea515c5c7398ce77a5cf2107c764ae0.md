Title: Optimisation des mouvements de données dans un code SYCL de physique des particules
Date: 2020-12-14 14:22
Slug: job_4ea515c5c7398ce77a5cf2107c764ae0
Category: job
Authors: Elisabeth BRUNET
Email: elisabeth.brunet@telecom-sudparis.eu
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: 
Job_Duration: 
Job_Website: 
Job_Employer: Télécom SudParis
Expiration_Date: 2021-03-08
Attachment: job_4ea515c5c7398ce77a5cf2107c764ae0_attachment.pdf

Ce stage s&#39;inscrit dans le cadre d&#39;une collaboration entre IJCLab (Orsay) et Télécom SudParis
(Palaiseau), autour de la programmation parallèle et hétérogène durable.
Le stagiaire devra porter vers SYCL des composants d&#39;une application de physique des
particules (ACTS), en vue de la faire exécuter sur des GPUs. Il devra plus particulièrement
profiler les échanges de données entre CPU et GPU, et optimiser ces derniers.

**Contexte scientifique et technique**
Dans le champ du calcul scientifique, le matériel a une durée de vie approximative de 5 ans, là
où une application va typiquement servir pendant 20 ou 30 ans. Nous sommes dans une
période ou chaque nouvelle génération de matériel est très différente de la précédente, et la
communauté scientifique n&#39;a pas les ressources humaines nécessaires pour réadapter en
permanence ses applications. Ceci nous amène à rechercher des solutions logicielles qui
favorisent une meilleure durabilité du code.
Parmi les nouveautés matérielles, l&#39;émergence des accélérateurs (GPUs) apporte des
questionnements sur les échanges de données entre ces derniers et les processeurs généraux
(CPUs), ainsi que sur la pertinence de regrouper ou non les accélérateurs dans des serveurs
spécialisés. On s&#39;attend à ce que les réponses dépendent fortement des applications.
Cette problématique vaut pour la bibliothèque logicielle ACTS, à laquelle contribuent des
chercheurs et ingénieurs d&#39;IJCLab. Cette bibliothèque est dédiée à l&#39;analyse des données issues
du Large Hadron Collider (LHC) du CERN, en particulier la reconstruction des trajectoires des
particules à partir des dépôts ponctuels mesurés.

**Travail à réaliser dans le cadre du stage**
Après avoir étudier les cas d&#39;utilisation d&#39;ACTS sélectionnés par l&#39;équipe d&#39;IJCLab, ainsi que la
technologie SYCL, le stagiaire procédera au portage vers SYCL du code qui le nécessite, et
s&#39;assurera que les résultats physiques restent valides.
Dans un deuxième temps, le stagiaire exécutera et comparera plusieurs cas d&#39;utilisation et/ou
plusieurs modèles de GPU. A chaque fois, il profilera les échanges de données entre processeur
et accélérateur, et comparera le comportement du code ACTS dans ces différentes conditions.
Il évaluera également la portabilité et les fonctionnalités des outils de profilage eux-mêmes.
Enfin, on attend du stagiaire qu&#39;il propose et implante des améliorations portable du code,
notamment à propos des échanges de données, avec l&#39;aide de l&#39;équipe PDS.

**Perspective de doctorat**
Un doctorat est envisageable, en co-tutelle avec les mêmes équipes, sur la problématique du
placement optimal des accélérateurs dans une grappe de calcul, et l&#39;arbitrage entre transferts
de données et remplissage de ces accélérateurs. Pour quel type d&#39;applications est-il préférable
de les distribuer dans tous les noeuds de calcul ? Pour quel type d&#39;applications est-il préférable
de regrouper les accélérateurs dans des noeuds spécialisés (GPU as a Service) ?

**Connaissances requises**
Langage de programmation impératif et/ou objet (C++, Fortran, Python...).
Notions de calcul parallèle et sur GPU.
Pratique de Linux.
**Mot-clefs**
SYCL, C++, GPU, ACTS, Reconstruction de traces, Physique des particules.
**Lieu**
Dans le respect des contraintes liées au COVID, le stage s&#39;effectuera au sein d&#39;IJCLab,
département informatique, à Orsay, ainsi qu&#39;à Télécom Sud-Paris, équipe PDS, à Palaiseau.
**Gratification &amp; ZRR**
Le CNRS rémunère les stages à hauteur de 591,51 euros bruts mensuels. Par ailleurs, IJCLab
étant ZRR, l&#39;accueil du stagiaire est soumis à l&#39;approbation du fonctionnaire de défense.
**Equipe encadrante**
David Chamont, david.chamont@ijclab.in2p3.fr
Elisabeth Brunet, elisabeth.brunet@telecom-sudparis.eu
Hadrien Grasland, hadrien.grasland@ijclab.in2p3.fr.
Gael Thomas, gael.thomas@telecom-sudparis.eu.

**Liens :**
SYCL (<https://www.sycl.tech/>)
ACTS (<https://acts.readthedocs.io/>)
IJCLab, Service Développement (<https://www.ijclab.in2p3.fr/pole-ingenierie/informatique-2/>)
Telecom SudParis, Equipe PDS (<https://www-inf.telecom-sudparis.eu/departement/pds/>)