Title: Génération de grands maillages pour le HPC
Date: 2021-09-10 07:15
Slug: job_5bf73b30853b95c271cbfd0e31fe39eb
Category: job
Authors: Latu Guillaume
Email: guillaume.latu@cea.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: CEA Cesta
Job_Duration: 1 an
Job_Website: 
Job_Employer: CEA
Expiration_Date: 2021-12-05
Attachment: job_5bf73b30853b95c271cbfd0e31fe39eb_attachment.pdf

De nombreuses applications de calcul scientifique s&#39;exécutent quotidiennement sur des configurations comprenant 10 000 cœurs ou plus sur les supercalculateurs du CEA. Dans la chaîne de calcul, la génération d’un maillage ou son adaptation en cours de calcul constituent des points critiques. Nous visons à établir une stratégie efficace de génération en calcul parallèle de maillages destinés à être employés par un solveur éléments finis. Typiquement, ces « grands » maillages compteront plus d’un milliard d’éléments, afin de tirer parti des machines actuelles et exaflopiques à venir.

Le travail proposé a pour objet les méthodes de génération de maillages de très grandes tailles nécessaires à la modélisation 3D des matériaux de furtivité (CEA/Cesta) et des combustibles nucléaires (CEA/DEC). Plusieurs mailleurs seront évalués (Gmsh, Mmg ...) sur des géométries différentes chez les deux partenaires (CEA/Cesta et CEA/DEC). On analysera plusieurs critères : l’adéquation aux besoins, les caractéristiques et la qualité du maillage produit, les temps de traitement du maillage et de restitution du code parallèle, et le nombre maximum de sommets/arêtes. Il s&#39;agira aussi de mettre en œuvre ces mailleurs au sein de codes éléments finis (EF) et évaluer les performances sur une machine parallèle.