Title:  Ingénieur Applicatif HPC - Intégration de solveurs (H/F)
Date: 2020-03-11 14:22
Slug: job_dfa1946d164ac2e4c764a3ec05d10b21
Category: job
Authors: MORTIER
Email: sylvain.mortier@atos.net
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Velizy
Job_Duration: 
Job_Website: https://jobs.atos.net/job/Bruy%C3%A8res-Le-Ch%C3%A2tel-91-Ing%C3%A9nieur-Applicatif-HPC-Int%C3%A9gration-de-solveurs-%28HF%29-Ile/594238901/
Job_Employer: Atos
Expiration_Date: 2020-06-03
Attachment: 

Atos est le leader européen du High Performance Computing et délivre des solutions de Calcul Haute Performance parmi les plus performantes au monde afin de résoudre les problèmes scientifiques les plus complexes d’aujourd’hui et de demain.

Vos missions :

- Définir les spécifications techniques en fonction des besoins fonctionnels métiers
- Développer les scripts Linux d’intégration des solveurs HPC
- Installer les binaires dans les différents environnements : DEV/TEST/PROD
- Créer ou mettre à jour l’interface de soumission (LSF Suite Spectrum PAC)
- Assurer l’évolution et le maintien en condition opérationnelle de l’intégration
- Créer ou mettre à jour la documentation associée
- Tracer l’activité dans un JIRA
- Veiller au respect des processus en vigueur au sein de la DSI
- Effectuer de la veille technologique
- Proposer des solutions pour améliorer les performances
- Participer à la mise en œuvre de Proof Of Concept dans le cadre d’innovations
 
Vous travaillerez principalement sur les solveurs CFD suivants : Nastran, Radioss, Thercat, Taitherm

Votre profil :

De formation supérieure en informatique vous justifiez d’une première expérience significative orientée HPC / calcul scientifique.

 Vous maitrisez tout ou partie des environnements/technologies suivants :

Scripting : bash, python
Système Linux : SUSE, RedHat
Programmation parallèle : MPI, OpenMP
Système d’ordonnancement : LSF, SLURM
 