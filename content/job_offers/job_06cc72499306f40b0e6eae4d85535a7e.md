Title: Researcher (research engineer) in Climate Modelling «  SWIO-Energy project »
Date: 2020-03-20 10:59
Slug: job_06cc72499306f40b0e6eae4d85535a7e
Category: job
Authors: Patrick Jeanty
Email: patrick.jeanty@univ-reunion.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Saint-Denis-de-La-Réunion
Job_Duration: 30 mois
Job_Website: https://www.le2p.fr 
Job_Employer: EnergyLAB - Université de La Réunion
Expiration_Date: 2020-06-12
Attachment: job_06cc72499306f40b0e6eae4d85535a7e_attachment.pdf

In the framework of a project of the regional cooperation programme, INTERREG V 2014-2020 (European funding), the University of La Réunion (UR), in partnership with the Universi-ty of Mauritius (UoM), is seeking a scientist (named as research engineer in France) in re-gional climate modelling for climate studies in the Southwest Indian Ocean.
The expertise of the Laboratory of Energy (EnergyLAB), the project leader, is located both in the field of solar metrology (the team manages a network of fifteen meteorological stations over La Réunion but also in neighbouring countries - South Africa, Mauritius, Comoros, Sey-chelles and Madagascar), and in the field of regional climate modelling (since 2011, the team has implemented a dynamic downscaling approach based on the use of regional climate models forced on their lateral boundaries by: 1) global climate models, 2) climate reanal-yses).
The main objective of the SWIO-Energy project is to study, for the first time in the region, the impact of climate change on renewable resources (solar, wind) at the local scale over La Réunion and Mauritius using ground measurements, satellite imagery and simulations from regional climate models. The researcher will 1) work in collaboration with and under the re-sponsibility of the project manager, himself overseen by the scientific manager of EnergyLab in charge of the project, and 2) interact with the study engineer in scientific calculation of the laboratory and the study engineer in data processing recruited as well on the project.

The applicant joins the laboratory for the entire project to ensure: 
**Simulations**
The applicant will be responsible for carrying out the WRF simulations required for the pro-ject according to a program defined by the team and its partners. His (Her) role is to prepare the simulated data sets used by all project partners, including: outputs from existing models (reanalyses, data generated by the models that will be used for the project - CMIP, CORDEX, Météo-France climate models, etc.) and WRF outputs produced as part of the project. This work will be done in conjunction with the project manager, the group of researchers involved in it (CRC, LACy, Météo France) and the study engineer in scientific calculation of the labora-tory. He (She) will also carry out statistical processing and analysis on the model outputs (validation…).
**Reporting**
His (Her) tasks include the regular writing and editing of technical reports for all the part-ners of the project.

The candidate should hold a doctoral degree and require the following skills/experience:

*	strong background in atmospheric/earth/climate sciences. Knowledge on renewable energy (solar, wind) and / or the climate of the Southwest Indian Ocean is an ad-vantage.
*	recent and relevant high-quality publications in international journals
*	experience in global/regional climate modelling (e.g., WRF)
*	experience in Linux/UNIX environment
*	competency in programming (e.g. Python, R, MATLAB), and the ability to cope with large amounts of data (observations / simulations)
*	advanced communication skills in oral and written English.

A first experience in the implementation and monitoring of projects of this type would be appreciated.

Rigorous and methodical, the applicant will:

*	provide the required technical solutions
*	work in autonomy and be highly adaptable to change
*	have good teamwork and communications skills
