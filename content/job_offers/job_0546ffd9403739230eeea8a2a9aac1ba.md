Title: Interdisciplinary PhD Positions - Mathematics, Physics and Computer Science
Date: 2021-04-06 15:37
Slug: job_0546ffd9403739230eeea8a2a9aac1ba
Category: job
Authors: Laurène Hume
Email: iti-irmiapp@unistra.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Strasbourg
Job_Duration: 3 ans
Job_Website: https://irmiapp.unistra.fr/
Job_Employer: Institut Thématique Interdisciplinaire IRMIA++
Expiration_Date: 2021-05-07
Attachment: 

7 interdisciplinary PhD projects are submitted for possible funding by IRMIA++, and positions are opened for PhD candidates. Covered topics include mathematics, physics and computer science.

Selection criteria will include the adequacy of the project to the IRMIA++ objectives, as well as the applicant’s academic results.


Information about the subjects : <https://irmiapp.unistra.fr/openpositions/>

Application : please contact us at <iti-irmiapp@unistra.fr> and we will connect you with the researchers.


Application deadline for PhD Candidates : May 7th 2021.
