Title: Machine learning technics and knowledge-based simulator combined for dynamic process state estimation 
Date: 2021-09-24 07:53
Slug: job_188fadcfeb82202c9ceac2adb40d5db2
Category: job
Authors: Cornez
Email: laurence.cornez@cea.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Saclay
Job_Duration: 12 mois
Job_Website: https://www.cea.fr
Job_Employer: CEA
Expiration_Date: 2022-01-30
Attachment: job_188fadcfeb82202c9ceac2adb40d5db2_attachment.pdf

The postdoctoral fellow will join an internal project shared between two CEA entities. The aim is to develop an algorithm with different machine learning technics (neural networks and active learning) able to estimate the operational parameters of a liquid-liquid extraction process. Indeed, the state of an industrial chemical process is accessible through operating parameters and available monitoring measures. Such information is essential to detect and evaluate operational changes in order to keep the process as close as possible to the target state. 

The project focuses on liquid-liquid extraction and benefits of its knowledge-based simulator as well as industrial data. However, the measures being inherently associated with uncertainty, it is necessary to make the data consistent with process knowledge - that is to say finding the best data set of operational parameters to provide the model in order to best describe the real state of process.  
Therefore, the main challenge of the project is to estimate the operational parameters (input of the knowledge-based simulator) thanks to their measurements as well as monitoring measures (output of the knowledge-based simulator). In other words, the goal is to solve this inverse problem dealing with times series.

The project proposes different steps to estimate the operational parameters in real conditions. Firstly, a collection of dynamic simulations (times series) calculated with the knowledge-based simulator has to be done with different operational conditions. A convolutional network (CNN) will be built on this database. Active learning should be used to achieve good performances for the CNN. Then, real data will be added to the learning database. Some improvements will be necessary to fit the CNN model on these real data and their uncertainty. At last, the CNN will have to deal with on-line constraints. As in real conditions, the model must take into account continuously updated data and be able to give a prediction as soon as the first data comes in. 

Thus, the neural network could provide precise results around the operating point. Once the results obtained are reliable, the tool would provide a consistent set of values for the on-line monitoring quantities of the process.

Through this project, we are at the heart of the thematic of digital simulation for the best control of complex systems. With the prospect of implementing &#34;deep learning&#34; tools to exploit the flow of operating data from the workshops, the tools enabling data analysis hand-in-hand with knowledge-based models will be valuable in this regard. This post-doc is an opportunity to build a tool to help the operation of a complex process by using the expertise of the process and modern techniques of artificial intelligence, which will be able to take into account the volume and “real time” requirements.

[1] B. Dinh, M. Montuir et P. Baron, «PAREX, a numeric code for process design and integration.,» chez Global 2013, Salt-Lake City, 2013
[2] B. Dinh, V. Vanel, C. Sorel, A. Duterme, M. Montuir, G. Ferlay, « Process Simulation Tools for Process Control and Safeguards Purpose », International Nuclear Fuel Cycle conference, Global 2019, Seattle, Washington, USA, proceedings vol.1, pp 313-317[3] P. Klotz, N. Bartoli, L. Cornez, M. Samuelides et P. Villedieu, «Database enrichment for optimization with artificial neural network.,» chez Surrogate Modelling and Space Mapping for Engineering Optimization (SMSMEO&#39;06), Copenhagen, Denmark, 2006
[4] K. Abid, M. Sayed-Mouchaweh, L. Cornez (2020). Deep Ensemble Approach for RUL Estimation of Aircraft Engines. In Mediterranean Forum of Data Science MEFDATA2020, Springer . 
