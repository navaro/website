Title: Etude numérique et expérimentale d’un nouveau type d’accélérateur de protons par voie laser
Date: 2019-12-06 08:55
Slug: job_ba259ae8b4ef5eea70d5be102cf2de1e
Category: job
Authors: BARDON
Email: matthieu.bardon@cea.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: CEA-CESTA, 15 avenue des Sablières, 33114 Le Barp, France
Job_Duration: 3 ans
Job_Website: 
Job_Employer: CEA
Expiration_Date: 2020-02-28
Attachment: job_ba259ae8b4ef5eea70d5be102cf2de1e_attachment.pdf

L’enjeu de cette thèse est de concevoir un nouveau type d’accélérateur de protons par voie laser, dont les performances seraient optimisées pour les applications : de chauffage protonique isochore, de production de radio-isotopes et de proton-thérapie pour le médical ou encore de radiographie protonique pour les expériences plasma laser. L’optimisation du dispositif sera réalisée par simulation numérique à l&#39;aide d&#39;un code 3D Particle In Cell, la fabrication de prototypes exploitera les technologies actuelles (fabrication additive par
exemple), et la validation expérimentale sera effectuée grâce à des campagnes d’expérience menées sur de grandes installations laser (LULI2000, LMJ-PETAL). C&#39;est un projet collaboratif associant plusieurs centres CEA et plusieurs laboratoires universitaires (LULI, CELIA ...).