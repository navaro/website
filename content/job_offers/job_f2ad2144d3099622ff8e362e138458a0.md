Title: Sensitivity analysis of the INRA 2018 feeding system for Ruminants
Date: 2021-01-14 07:48
Slug: job_f2ad2144d3099622ff8e362e138458a0
Category: job
Authors: Pierre Nozière ; Sophie Lemosquet
Email: pierre.noziere@inrae.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: INRAE, Clermont-Ferrand Theix
Job_Duration: 18 months
Job_Website: https://umrh-bioinfo.clermont.inrae.fr/Intranet/web/UMRH
Job_Employer: INRAE
Expiration_Date: 2021-01-31
Attachment: job_f2ad2144d3099622ff8e362e138458a0_attachment.pdf

The model of the INRA 2018 feeding system for ruminants (<https://doi.org/10.3920/978-90-8686-292-4>) improves the estimation of animal responses to diets and proposes new predictions. The system allows various rationing strategies to be considered by seeking compromises between different objectives, i.e. milk and meat production, management of body reserves, efficiency of protein use…(<https://www.inration-ruminal.fr/>). It also allows evaluating various other responses of the animal to the diet supplies (e.g. N and CH4 emissions into the environment, risk of acidosis, product composition). These advances were possible for a part because the estimation of the different nutrient supplies by the diet was improved. The reference methods for estimating nutrient supply require the use of animals with permanent digestive cannulas (rumen and/or duodenum) to insert nylon bags and monitor the degradation of incubated feed (&#34;in sacco&#34; methods). With a view to stopping the use of this practice in the short term, the Alterfi project was launched by the INRAE PHASE Division.

Alternative laboratory methods are currently being developed, with the aim that they may eventually replace in sacco methods. At the same time, it is essential to assess the necessary degree of accuracy (or acceptable inaccuracy) on these feed value parameters to ensure sufficient accuracy in the estimation of animal responses to diets, compared to measurements on experimental facilities and/or observation on farms. This work will make it possible to assess alternatives to in sacco measurements in the feeding system.


The post-doctoral will perform sensitivity analyses of the different animal responses to uncertainty in the estimation of feed values. He/she will have the opportunity to train (or develop his knowledge) on the INRA 2018 feeding system, including both estimation of feed values and animal’s responses to diets. He will use a high throughput simulation application allowing to test the sensitivity of the model to the uncertainties on feed values, on a wide range of nutritional scenarios. He/she will develop datasets required to the tests (enter of the model), and will thus interpret the output of the simulations, in the light of measurements on experimental facilities and/or observations on farms. This will be done in strong interaction with the different researchers involved in the development of the INRA 2018 system in Theix, Rennes, Paris and Montpellier, who will share relevant databases initially used to develop the system. The work will first be focused on dairy cows, then on beef cattle, and lastly on small ruminants. The post-doctoral will write a scientific publication based on the main results of this work.
