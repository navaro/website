"use strict";

function read(data){
  return data;
}

function MakeUpFocalElement(json_entry,base){
  let newevent = document.createElement('tr');
  newevent.className = 'focal';

  moment.locale('fr');

  let title = json_entry.tra_title,
      date = moment(json_entry.tra_dtstart.split('T')[0], ['YYYYMMDD']).format('DD-MM-YYYY'),
      urbanArea = json_entry.tra_urbanArea.split(',')[0],
      web = json_entry.tra_web,
      type = json_entry.tra_type
  ;


  newevent.innerHTML = '' +
    '<td class="focal_title"><a href="'+web+'">'+ title + '</a></td>' +
    '<td class="focal_date">'+ date + '</td>' +
    '<td class="focal_urbanArea">'+ urbanArea + '</td>' +
    '<td class="focal_type">'+ type + '</td>'
  ;

  base.appendChild(newevent);
};


function read_focal_data(url, nb_event)
{
  $.ajax( {
    type: "GET",
    url: "http://formation-calcul.fr/api/training/trainings/?format=jsonp",
    dataType: "jsonp",
    // jsonp: false,
    jsonpCallback: "read",
    success: function(jsonp) {
    },
    error: function(event) {
      alert("unable to read the event " + nb_event + " from " + url);
    },
    complete: function(data){
      var results = data.responseJSON.results;
      var base_element = document.getElementById('focal_data');

      for(var ii in results){
	MakeUpFocalElement(results[ii],base_element);
      }
      //FocalParse(data.responseJSON, nb_event,'focal_data');
    }

  });
}

read_focal_data();
