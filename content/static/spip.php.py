#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Permanently redirect urls from old web site to the new one.

If a requested url is not in the url matches dictionary,
then the client is redirected to the home page.
"""

# Old -> new urls links
url_links = {
    'article130': '2010-06-outils-mesocentres',
    'article7': '2004-06-session-speciale-canum',
    'article266': '2016-05-atelier-cpp',
    'article6': '2008-02-journee-mesocentres',
    'article251': '2014-12-atelier-profilage-openmp',
    'article3': '2005-03-compilateur-arch-perf',
    'article252': '2015-01-journee-problem-poisson',
    'article293': '2018-10-journee-repro',
    'article296': '2018-10-journee-evalperf',
    'article267': '2016-05-multiresolution-adaptative',
    'article135': '2010-05-journee-momas-calcul',
    'article9': '2007-06-mini-symposium-smai',
    'article240': '2014-07-ecole-optimisation',
    'article74': '2009-05-journee-mesocentres',
    'article131': '2010-06-ecole-thematique-multiresolution',
    'article219': '2013-04-journee-histoire-calcul',
    'rubrique107': '2017-05-ecole-precis',
    'article262': '2015-09-Green-HPC',
    'article243': '2014-06-atelier-profilage-codes-calcul',
    'article272': '2016-11-ANF-visu-data',
    'article76': '2009-05-mini-symposium-smai',
    'article289': '2018-06-ANF-cahiers-experience',
    'article157': '2010-11-journees-groupe-calcul',
    'article282': '2017-05-atelier-integration-continue',
    'article150': '2011-01-masse-de-donnees',
    'article181': '2011-09-journee-mesocentres',
    'article177': '2011-07-journees-groupe-calcul',
    'article82': '2009-11-journees-groupe-calcul',
    'article270': '2016-10-journees-mesocentres',
    'article284': '2017-07-atelier-pythonavance',
    'article274': '2017-01-ecole-elements-finis',
    'article273': '2016-11-atelier-cpp',
    'article259': '2015-06-atelier-julia',
    'article233': '2013-10-ecole-optimisation',
    'article246': '2014-10-atelier-c++',
    'article290': '2018-05-anf-supporthpc',
    'article287': '2017-12-journee-python-data',
    'article211': '2012-05-symposium-canum',
    'article214': '2012-10-journee-mesocentres',
    'article292': '2018-06-gpgpu',
    'article294': '2018-10-journees-jcad',
    'article228': '2013-05-mini-symposium-smai',
    'article261': '2015-09-atelier-cpp',
    'article263': '2015-10-journees-mesocentres',
    'article285': '2017-09-journees-mesocentres',
    'article165': '2010-12-journee-thierry-dumont',
    'article239': '2014-04-canum',
    'article8': '2005-05-session-speciale-smai',
    'article148': '2010-06-mini-symposium-canum',
    'article237': '2014-09-anf-informatique-pour-le-calcul-scientifique',
    'article258': '2015-06-journee-galerkin-discontinue',
    'article242': '2014-10-journee-mesocentres',
    'article75': '2009-05-mini-symposium-architectures-smai',
    'article149': '2010-09-journee-mesocentres',
    'article288': '2017-12-atelier-visualisation',
    'article260': '2015-06-atelier-git-gitlab',

    'rubrique94': '2011-09-lem2i',
    'rubrique93': '2011-11-ecole-thematique-decomposition-domaine',
    'rubrique98': '2013-03-ecole-precis',
    'rubrique101': '2013-06-atelier-petsc-avance',
    'rubrique99': '2013-03-atelier-petsc',
    'rubrique95': '2012-02-ecole-methodo-outils-optimisation',
    'rubrique90': '2012-07-cemracs',
    'article218': '2012-07-cemracs',
    'article176': '2012-07-cemracs',
    'article190': '2011-11-ecole-thematique-decomposition-domaine',
    'article183': '2011-11-ecole-thematique-decomposition-domaine',
    'article196': '2012-02-ecole-methodo-outils-optimisation',
    'rubrique106': '2014-11-ecole-thematique-multigrilles',
    'article247': '2014-11-ecole-thematique-multigrilles',
    'article257': '2015-04-r-avance-performances',
    'article192': '2011-09-lem2i',
    'article221': '2013-03-ecole-precis',
    'article201': '2011-09-lem2i',
    'article57': '2008-09-ecole-informatique-scientifique-module1',
    'article231': '2013-12-python-avance',
    'article254': '2016-11-atelier-cpp',
    'article141': '2010-06-ecole-thematique-multiresolution',
    'rubrique77': '2010-06-ecole-thematique-multiresolution',
    'article230': '2013-12-python-avance',
    'article169': '2011-10-angd-plasmas-froids',
    'article236': '2013-12-python-avance',
    'rubrique102': '2013-12-python-avance',
    'article229': '2013-09-journee-mesocentres',
    'article4': '2005-12-cpp-stl-lib',
    'article248': '2014-11-ecole-thematique-multigrilles',
    'article64': '2008-12-ecole-informatique-scientifique-module2',
    'article172': '2011-09-angd-masse-de-donnees',
    'article222': '2013-03-ecole-precis',
    'rubrique75': '2008-09-ecole-informatique-scientifique-module1',
    'article63': '2008-09-ecole-informatique-scientifique-module1',
    'article275': '2017-01-journee-runtime',
    'rubrique96': '2012-10-programmation-hybride',
    'article217': '2012-10-programmation-hybride',
    'article220': '2013-03-ecole-precis',
    'article250': '2014-11-ecole-thematique-multigrilles',
    'article215': '2012-07-cemracs',
    'article281': '2017-09-anf-reduction-dimension',
    'rubrique108': '2017-09-anf-reduction-dimension',
    'article5': '2006-12-python-calcul-scientifique',
    'article193': '2012-02-ecole-methodo-outils-optimisation',
    'rubrique89': '2011-09-angd-masse-de-donnees',
    'article173': '2011-09-angd-masse-de-donnees',
    'rubrique76': '2009-10-formation-admin-calculateur',
    'article71': '2009-10-formation-admin-calculateur',
    'article223': '2013-03-ecole-precis',
    'article210': '2012-10-programmation-hybride',
    'article152': '2010-12-angd-python',
    'article268': '2016-05-mini-symposium-canum',
    'rubrique85': '2010-12-angd-python',
    'article164': '2010-12-angd-python',
    'rubrique88': '2011-10-angd-plasmas-froids',
    'article171': '2011-10-angd-plasmas-froids',
    'article298': '2019-01-journee-julia',
    'article291': '2018-09-ecole-geomdata',
    'article301': '2019-09-anf-perf-eval-hpc',
    'article300': '2019-04-journees-calcul-apprentissage',
    'article299': '2019-01-journee-algorithmic-differentiation',
    'article297': '2018-11-data-fair',
    'article187': '2011-09-lem2i',
    'article303': '2019-05-atelier-optimisation',

    'rubrique38': 'category/job',
    'article202': 'tag/stage',
    'article198': 'tag/cdi',
    'article199': 'tag/postdoc',
    'article200': 'tag/these',
    'article197': 'tag/cdd',

    'article34': 'pages/presentation_groupe',
    'rubrique4': 'category/journee',
    'rubrique39': 'category/formation',
    'rubrique6': 'pages/mesocentres_en_france',
}


def get_new_url(page_id):
    """ Return new url given a spip page id. Home page if unknown. """
    try:
        return url_links[page_id] + '.html'
    except:
        return ''


###############################################################################
if __name__ == '__main__':

    import os

    # See https://stackoverflow.com/questions/11842547/distinguish-from-command-line-and-cgi-in-python
    if 'GATEWAY_INTERFACE' in os.environ: # CGI interface
        import cgi

        try:
            page_id = cgi.FieldStorage(keep_blank_values=True).keys()[0]
        except:
            page_id = ''

    else: # Command-line interface
        import sys

        try:
            page_id = sys.argv[1]
        except:
            page_id = ''


    # HTTP header
    print('Status: 301 Moved Permanently');
    print('Location: ./{}'.format(get_new_url(page_id)));
    print('')
