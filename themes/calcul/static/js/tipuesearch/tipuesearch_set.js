
/*
Tipue Search 7.0
Copyright (c) 2018 Tipue
Tipue Search is released under the MIT License
http://www.tipue.com/search
*/


/*
Stop words
Stop words list from https://pypi.org/project/stop-words/:
python -c "from stop_words import get_stop_words; print(get_stop_words('fr'))"
*/

var tipuesearch_stop_words = ['a', 'ai', 'aie', 'aient', 'aies', 'ait', 'alors', 'as', 'au', 'aucun', 'aura', 'aurai', 'auraient', 'aurais', 'aurait', 'auras', 'aurez', 'auriez', 'aurions', 'aurons', 'auront', 'aussi', 'autre', 'aux', 'avaient', 'avais', 'avait', 'avant', 'avec', 'avez', 'aviez', 'avions', 'avoir', 'avons', 'ayant', 'ayez', 'ayons', 'bon', 'car', 'ce', 'ceci', 'cela', 'ces', 'cet', 'cette', 'ceux', 'chaque', 'ci', 'comme', 'comment', 'd', 'dans', 'de', 'dedans', 'dehors', 'depuis', 'des', 'deux', 'devoir', 'devrait', 'devrez', 'devriez', 'devrions', 'devrons', 'devront', 'dois', 'doit', 'donc', 'dos', 'droite', 'du', 'dès', 'début', 'dù', 'elle', 'elles', 'en', 'encore', 'es', 'est', 'et', 'eu', 'eue', 'eues', 'eurent', 'eus', 'eusse', 'eussent', 'eusses', 'eussiez', 'eussions', 'eut', 'eux', 'eûmes', 'eût', 'eûtes', 'faire', 'fais', 'faisez', 'fait', 'faites', 'fois', 'font', 'force', 'furent', 'fus', 'fusse', 'fussent', 'fusses', 'fussiez', 'fussions', 'fut', 'fûmes', 'fût', 'fûtes', 'haut', 'hors', 'ici', 'il', 'ils', 'j', 'je', 'juste', 'l', 'la', 'le', 'les', 'leur', 'leurs', 'lui', 'là', 'm', 'ma', 'maintenant', 'mais', 'me', 'mes', 'moi', 'moins', 'mon', 'mot', 'même', 'n', 'ne', 'ni', 'nom', 'nommé', 'nommée', 'nommés', 'nos', 'notre', 'nous', 'nouveau', 'nouveaux', 'on', 'ont', 'ou', 'où', 'par', 'parce', 'parole', 'pas', 'personne', 'personnes', 'peu', 'peut', 'plupart', 'pour', 'pourquoi', 'qu', 'quand', 'que', 'quel', 'quelle', 'quelles', 'quels', 'qui', 'sa', 'sans', 'se', 'sera', 'serai', 'seraient', 'serais', 'serait', 'seras', 'serez', 'seriez', 'serions', 'serons', 'seront', 'ses', 'seulement', 'si', 'sien', 'soi', 'soient', 'sois', 'soit', 'sommes', 'son', 'sont', 'sous', 'soyez', 'soyons', 'suis', 'sujet', 'sur', 't', 'ta', 'tandis', 'te', 'tellement', 'tels', 'tes', 'toi', 'ton', 'tous', 'tout', 'trop', 'très', 'tu', 'un', 'une', 'valeur', 'voient', 'vois', 'voit', 'vont', 'vos', 'votre', 'vous', 'vu', 'y', 'à', 'ça', 'étaient', 'étais', 'était', 'étant', 'état', 'étiez', 'étions', 'été', 'étés', 'êtes', 'être']


// Word replace

var tipuesearch_replace = {'words': [
     {'word': 'tip', 'replace_with': 'tipue'},
     {'word': 'javscript', 'replace_with': 'javascript'},
     {'word': 'jqeury', 'replace_with': 'jquery'}
]};


// Weighting

var tipuesearch_weight = {'weight': [
     {'url': 'http://www.tipue.com', 'score': 60},
     {'url': 'http://www.tipue.com/search', 'score': 60},
     {'url': 'http://www.tipue.com/tipr', 'score': 30},
     {'url': 'http://www.tipue.com/support', 'score': 20}
]};


// Illogical stemming

var tipuesearch_stem = {'words': [
     {'word': 'e-mail', 'stem': 'email'},
     {'word': 'javascript', 'stem': 'jquery'},
     {'word': 'javascript', 'stem': 'js'}
]};


// Related

var tipuesearch_related = {'Related': [
     {'search': 'tipue', 'related': 'Search', 'include': 1},
     {'search': 'tipue', 'related': 'jQuery'},
     {'search': 'tipue', 'related': 'Features'},
     {'search': 'tipue', 'related': 'Support'},
     {'search': 'tipue search', 'related': 'Help', 'include': 1},
     {'search': 'tipue search', 'related': 'Support'}
]};


// Internal strings

var tipuesearch_string_1 = 'Pas de titre';
var tipuesearch_string_2 = 'Résultat pour';
var tipuesearch_string_3 = 'Au lieu de';
var tipuesearch_string_4 = '1 résultat';
var tipuesearch_string_5 = 'resultats';
var tipuesearch_string_6 = '<';
var tipuesearch_string_7 = '>';
var tipuesearch_string_8 = 'Aucun résultat.';
var tipuesearch_string_9 = 'Les mots communs sont ignorés.';
var tipuesearch_string_10 = 'Relié';
var tipuesearch_string_11 = 'Entrer au moins un caractère.';
var tipuesearch_string_12 = 'Entrer au moins';
var tipuesearch_string_13 = 'caractères.';
var tipuesearch_string_14 = 'secondes';
var tipuesearch_string_15 = 'Ouvrir l\'image';
var tipuesearch_string_16 = 'Aller sur la page';


// Internals


// Timer for showTime

var startTimer = new Date().getTime();

