{% if header %}
<div class="storage_list">
  <div class="container">
    <div class="panel-group">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h6 class="panel-title">
            <a data-toggle="collapse" href="#storages{{ num }}">Stockage</a>
          </h6>
        </div>
        <div id="storages{{ num }}" class="panel-collapse collapse">
{% endif %}
{% if footer %}
        </div>
      </div>
    </div>
  </div>
</div>
{% endif %}
