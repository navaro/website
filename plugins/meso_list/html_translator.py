from pelican import readers
from pelican.readers import PelicanHTMLTranslator
from jinja2 import Environment, FileSystemLoader, select_autoescape
import os

def jinja2_join_names(names, separator=', ', final_separator=' et ', end=''):
    """ Join a list of names using  and a final 'et' """

    if len(names) > 1:
        return separator.join(names[:-1]) + final_separator + names[-1] + end
    elif len(names) == 1:
        return names[-1] + end
    else:
        return ''

def jinja2_join_sentences(sentences, separator='. ', end='', trim_separator=True):
    """ Join multiple sentences by given separator """

    if trim_separator:
        for i in range(len(sentences)):
            while sentences[i][-1] == separator[0]:
                sentences[i] = sentences[i][:-1]

    return separator.join(sentences) + end

def jinja2_format_storage(value, power=0, units=['o', 'Kio', 'Mio', 'Gio', 'Tio', 'Pio', 'Eio'], base=1000):
    """ Format storage quantity with appropriate suffix """

    # The old way
    while value >= base and power < len(units) - 1:
        value /= base
        power += 1

    if value >= base:
        return f"{round(value)} {units[power]}"
    else:
        return f"{value:.3g} {units[power]}"


# Extend default translator
class myHTMLTranslator(PelicanHTMLTranslator):

    def __init__(self, *args, **kwargs):

        # Pre-loading Jinja2 templates and filters
        env = Environment(
            loader=FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')),
            autoescape=select_autoescape(default_for_string=True, default=True),
        )
        env.filters.update({
            'join_names': jinja2_join_names,
            'join_sentences': jinja2_join_sentences,
            'format_storage': jinja2_format_storage,
        })
        self.meso_template = env.get_template('meso.tpl')
        self.cluster_template = env.get_template('cluster.tpl')
        self.cluster_list_template = env.get_template('cluster_list.tpl')
        self.storage_template = env.get_template('storage.tpl')
        self.storage_list_template = env.get_template('storage_list.tpl')

        self.num_meso = 0

        super().__init__(*args, *kwargs)

    def visit_region(self, node):
        self.body.append(f'<div class="region"><h2>{node["name"]}</h2>')

    def depart_region(self, node):
        self.body.append('</div>')

    def visit_meso_list(self, node):
        pass
        # super().visit_section(node)
        # self.body.append('<div class="meso_list">')

    def depart_meso_list(self, node):
        pass
        #self.body.append('</div>')
        #super().depart_section(node)

    def visit_meso(self, node):
        self.num_meso += 1
        self.body.extend(self.meso_template.render(meso=node, num=self.num_meso, header=True).splitlines(True))

    def depart_meso(self, node):
        self.body.extend(self.meso_template.render(meso=node, footer=True).splitlines(True))

    def visit_cluster_list(self, node):
        self.body.extend(self.cluster_list_template.render(num=self.num_meso, header=True).splitlines(True))

    def depart_cluster_list(self, node):
        self.body.extend(self.cluster_list_template.render(num=self.num_meso, footer=True).splitlines(True))

    def visit_cluster(self, node):
        self.body.extend(self.cluster_template.render(cluster=node, num=self.num_meso, header=True).splitlines(True))

    def depart_cluster(self, node):
        self.body.extend(self.cluster_template.render(cluster=node, num=self.num_meso, footer=True).splitlines(True))

    def visit_cluster_storage_list(self, node):
        pass
        #self.body.extend(self.storage_list_template.render(num=self.num_meso, header=True).splitlines(True))

    def depart_cluster_storage_list(self, node):
        pass
        #self.body.extend(self.storage_list_template.render(num=self.num_meso, footer=True).splitlines(True))

    def visit_cluster_storage(self, node):
        pass
        #self.body.extend(self.storage_template.render(storage=node, num=self.num_meso, header=True).splitlines(True))

    def depart_cluster_storage(self, node):
        pass
        #self.body.extend(self.storage_template.render(storage=node, num=self.num_meso, footer=True).splitlines(True))

    def visit_storage_list(self, node):
        self.body.extend(self.storage_list_template.render(num=self.num_meso, header=True).splitlines(True))

    def depart_storage_list(self, node):
        self.body.extend(self.storage_list_template.render(num=self.num_meso, footer=True).splitlines(True))

    def visit_storage(self, node):
        self.body.extend(self.storage_template.render(storage=node, num=self.num_meso, header=True).splitlines(True))

    def depart_storage(self, node):
        self.body.extend(self.storage_template.render(storage=node, num=self.num_meso, footer=True).splitlines(True))

    def visit_node_type_list(self, node):
        pass

    def depart_node_type_list(self, node):
        pass

    def visit_node_type(self, node):
        pass

    def depart_node_type(self, node):
        pass

# Overwrite default translator so that extensions can be chained
readers.PelicanHTMLTranslator = myHTMLTranslator

